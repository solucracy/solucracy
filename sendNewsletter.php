<?php
require 'core/ini.php';
helper::loadHeader(
  'header.php',
  array(
  'TITLE' => 'Solucracy'
  , 'DESCRIPTION' => _("We all have the same problems, let's find a solution together."))
);
$form   = new form();
$fields = '';
$fields .= $form->createField('text', 'text', _("Description"), _("Description"));
$fields .= $form->createField('text', 'link', 'lien', 'lien');
$fields .= $form->createField('hidden', 'type', '', '', 'sendNewsletter');
$fields .= $form->createField('submit', 'submit', _("Send"), '', "processForm('newsletterForm','showAndReload')");
?>

<form id="newsletterForm">
  <?php
  echo $fields;
?>
</form>



<?php
require "inc/footer.php";
?>
