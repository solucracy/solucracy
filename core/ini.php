<?php
session_start();


// Create a global configuration

$config_file = "config.php";
if (isset($_ENV["CONFIG_FILE"])) {
  $config_file = "config-dev.php";
}
require $config_file;


// Autoload classes
function autoload($class)
{

  // Don't interfere with Swift autoloader
  if (0 === strpos($class, 'Swift_')) {
    return;
  }

  include_once $GLOBALS['config']['root_path'] . "classes/" . $class . '.php';
}
spl_autoload_register('autoload');
// load language
// helper::loadlanguage();
require $GLOBALS['config']['root_path']."/locale/i18n_setup.php";
//FIX ME ( this is to set a default session name, there's probably a better way to accomplish this)
if (!session::exists('userId')) {
  session::put('userId', 0);
}
