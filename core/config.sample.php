<?php

$GLOBALS['config'] = array(
  'base_url'       => '/',
  'blogAddress'    => '',
  'donationPage'   => '',
  'email'          => 'contact@example.com',
  'smtp'           => '127.0.0.1',
  'smtpPort'       => '25',
  'sendAddress'    => 'webmaster@example.com',
  'sendAddressPwd' => '**********',
  'noReply'        => 'no-reply@example.com',
  'GMap_API_Key'   => '************************',
  'stripe_API_Key' => '',
  'root_path'      => $_SERVER['DOCUMENT_ROOT'] . "/",
  'salt'           => 'solucracy',
  // 'base_geoloc' => array(
  //   'latitude' => 6.0833,
  //   'longitude' => 46.299999,
  //   'country' => 'France'
  // ),
  'mysql'          => array(
    'host'     => 'localhost',
    'username' => 'root',
    'password' => '',
    'db'       => 'solucracy',
  ),
  'remember'       => array(
    'cookie_name'    => 'hash',
    'cookie_expiry'  => 604800,
    'nb_votes'       => 4,
    'items_per_page' => 8,
    'firstVisit'     => true,
  ),
  'session'        => array(
    'session_name' => 'user',
  ),
);
