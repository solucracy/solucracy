<?php
require 'core/ini.php';
helper::loadHeader(
  'header.php',
  array(
  'TITLE' => _("Solution")
  , 'DESCRIPTION' => _("Solution"))
);

$solution        = new solution(input::get('id'));
$solutionDetails = $solution->data();
$problems        = $solution->getProblemList();
$displayProblems = "";
foreach ($problems as $problem) {
  $score = round($problem->pertinenceVotes / $problem->nbVotes * 100, 2);
  $displayProblems .= '<div class="blackBorder margin5 padding5"><div class="row"><div class="col-7"><h4><a href="problem-';
  $displayProblems .= $problem->problemId;
  $displayProblems .= '.html"><i class="fas fa-info-circle font_green"></i></a>';
  $displayProblems .= $problem->title;
  $displayProblems .= '</h4><div class="col-md-2" ><div class="font_red text-center">';
  $displayProblems .= $problem->nbVotes;
  $displayProblems .= ' ' . _("Votes") . '</div></div></div><div class="whiteBorder font_white gradient col-3 text-center" style="background-image: linear-gradient(to right, rgb(91, 200, 57) 0%, rgb(91, 200, 57) ';
  $displayProblems .= $score;
  $displayProblems .= '%, rgb(218, 50, 50) ';
  $displayProblems .= $score;
  $displayProblems .= '%, rgb(218, 50, 50) 100%);">' . _("adapted to") . ' ';
  $displayProblems .= $score;
  $displayProblems .= '%</div></div><span class="col-6 col-offset-1">"';
  $displayProblems .= $problem->description;
  $displayProblems .= '"</span></div>';
}

$user       = new user();
$reportSpam = "<div onclick='ajax(\"buildform.php\",{type:\"spam\",entityId:" . $solutionDetails->solutionId . ",entityType:\"solution\"},\"form\")' class='solucracy_btn'>" . _("Spam") . " <i class='fa fa-warning'></i></div> <!-- report -->";

$comments        = $solution->getComments();
$displayComments = "";
foreach ($comments as $comment) {
  if ($comment->value == 1) {
    $thumbs = '<i class="fa fa-thumbs-up font_green margin5"></i>';
  } else {
    $thumbs = '<i class="fa fa-thumbs-down font_red margin5"></i>';
  }
  $displayComments .= '<div class="row w-100 faded_gray_bkgd"><p>"';
  $displayComments .= $comment->comment;
  $displayComments .= '"</p></div>
              <div class="row w-100 mb-2 faded_gray_bkgd p-1"><a href="profile.php?userId=';
  $displayComments .= $comment->userId;
  $displayComments .= '">';
  $displayComments .= $thumbs . $comment->userName;
  $displayComments .= '</a> ' . _("on") . ' ';
  $displayComments .= $comment->createdOn;
  $displayComments .= '</div>';
}
$addToProblem = "";
if (session::exists('problemId') && session::get('problemId') > 0) {
  if (!$solution->propositionExists(session::get('problemId'))) {
    $addToProblem = '<a href="#" class="solucracy_btn" onclick="ajax(\'buildform.php\',{type:\'newProposition\',solutionId: ' . $solutionDetails->solutionId . '},\'form\')">' . _("Forgot password") . '</a>'; //add to problem
  }
}
?>
<div class="container-fluid">
  <div class='row'>
    <div itemscope class="col-md-8" itemtype="http://schema.org/ItemPage">
      <div class="row text-center m-1" id="infos" data-solutionid="<?php echo $solutionDetails->solutionId; ?>">
        <div class="col-md-2">
          <img itemprop="image" src="img/<?php echo $solutionDetails->categoryIcon ?>">
        </div>
        <div class="col-md-8">
          <div class="row">
              <h3  itemprop="name" id="title"><?php echo $solutionDetails->title; ?></h3>
          </div>
          <div id="topic_descr" itemprop="description" class="row">
            <span id="solutionDescription" class="col-12 text-left"><?php echo $solutionDetails->description ?></span>
          </div>
          <div class="row">
            <span class="col-12 text-left"><span itemprop="datePublished"><?php echo _("Created on") . " " . $solutionDetails->createdOn; ?> |
            <a href="profile.php?userId=<?php echo $solutionDetails->userId; ?>" itemprop="author"><?php echo $solutionDetails->userName; ?></a></span></span> <!-- added on -->
          </div>
        </div>

      </div>
      <div class='row mb-3'>
        <div id="voteBtns" class='col-md-4 offset-md-2'>
          <a  onclick="ajax('buildform.php',{type:'solutionVote',voteValue:1,solutionId:<?php echo $solutionDetails->solutionId ?>},'form')"><i class="fa fa-thumbs-up fa-3x font_green clickable margin5"></i></a> <!-- vote up -->
          <a  onclick="ajax('buildform.php',{type:'solutionVote',voteValue:-1,solutionId:<?php echo $solutionDetails->solutionId ?>},'form')"><i class="fa fa-thumbs-down fa-3x font_red clickable margin5"></i></a> <!-- vote down -->
        </div>
        <div class="col-4 d-flex justify-content-around">
          <a href="http://www.facebook.com/sharer.php?u=<?php echo "www.solucracy.com/solution-" . $solutionDetails->solutionId . ".html&title=" . $solutionDetails->title; ?>"><i class="fab fa-2x fa-facebook-square font_blue"></i></a>
          <a href="http://reddit.com/submit?url=<?php echo "www.solucracy.com/solution-" . $solutionDetails->solutionId . ".html&title=" . $solutionDetails->title; ?>"><i class="fab fa-2x fa-reddit-square font_blue"></i></a>
          <a href="https://twitter.com/intent/tweet?url=<?php echo "www.solucracy.com/solution-" . $solutionDetails->solutionId . ".html&TEXT=" . $solutionDetails->title; ?>"><i class="fab fa-2x fa-twitter-square font_blue"></i></a>
          <a href="https://plusone.google.com/_/+1/confirm?hl=fr&url=<?php echo "www.solucracy.com/solution-" . $solutionDetails->solutionId . ".html"; ?>"><i class="fab fa-2x fa-google-plus-square font_blue"></i></a>
        </div>
      </div>
      <div class='row d-flex justify-content-around'>
        <?php
        echo $addToProblem;
        if ($solutionDetails->statusId === '1') {
          echo $reportSpam;
        }
?>
      </div>
      <div class='row'>
      <h3 class="offset-md-1"><?php echo _("Proposed for the following problems"); ?>:</h3>
    </div>
  <?php
  echo $displayProblems;
?>
  </div>
  <div class=" col-md-4">
      <div class="greenBorder m-1 p-3"><h3><?php echo _("Upsides"); ?> :</h3> <?php echo $solutionDetails->avantages ?></div>
      <div class="redBorder m-1 p-3"><h3><?php echo _("Downsides"); ?> :</h3>  <?php echo $solutionDetails->inconvenients ?></div>
      <div class="grayBorder m-1 p-3">
        <h3><?php echo _("Votes"); ?> :</h3>
<?php
echo $displayComments;
?>
      </div>
    </div>
  </div>
</div>



<?php
require config::get('root_path') . "inc/footer.php";
?>
