
  <footer id="footer_content" class="d-flex align-self-end mt-4">
    &copy; <a href="#">Solucracy 2014-<?php echo date("Y"); ?> </a>| <a href="#" onclick="ajax('buildform.php',{type:'contactAdmins'},'form')">Contact</a>  | <a href="https://www.facebook.com/solucracy"><i class="fab fa-facebook-f"></i></a> | <a href="https://twitter.com/solucracy"><i class="fab fa-twitter"></i></a> | <a href="#" onclick="ajax('buildform.php',{type:'newsletterSubscription'},'form')"><?php echo _("Subscribe to newsletter"); ?><!--  subscribe to newsletter --></a>| <a href="<?php echo config::get('base_url') ?>allproblems.php"><i class="fa fa-list"></i></a>
  </footer>

  <!-- Insert JS scripts here -->
  <script src="//code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="growl/jquery.growl.js" type="text/javascript"></script>
  <script src='js/jquery.tagsinput.js'></script>
  <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo config::get('GMap_API_Key') ?>'></script>
  <script type="text/javascript">
  <?php
  //$location = "6.0833,46.299999";
  echo "var baseUrl = '" . config::get('base_url') . "'\n";
  echo "var itemsPerPage = " . config::get('remember/items_per_page') . "\n";
  if (isset($getProblemList)) {
      echo "var problemList = '" . $getProblemList . "'\n";
  }
  if (!isset($tag)) {
      echo 'var tag =" "' . "\n";
  } else {
      echo 'var tag ="' . $tag . '"' . "\n";
  }
  if (isset($problemDetails->problemId)) {
      echo 'var problemId = ' . $problemDetails->problemId . "\n";
  }
  if (input::get('problemId') > 0) {
      echo 'var problemId = ' . input::get('problemId') . "\n";
  }
  echo "var baseName = '" . basename($_SERVER['PHP_SELF']) . "'\n";
  if (isset($location)) {
      echo "var myLatlng = new google.maps.LatLng(" . $location . ");";
  } else {
      echo "var myLatlng = new google.maps.LatLng(" . helper::userIpLocation() . ");";
  }
?>

  //display message
  function showMessage(message, outcome){
    if(outcome===undefined){
          color = "red";
        } else if(outcome===true){
          color = "green";
        } else if(outcome===false){
          color = "red";
        }
    jqxAlert.alert(" "," ",color);
    $("#message").prepend(message);
  }
  function decodeHtml(html) {
      var txt = document.createElement("textarea");
      txt.innerHTML = html;
      return txt.value;
  }

  //login
  function login() {
    ajax('login.php',$( "#connect" ).serialize(),'reload');
    event.preventDefault();
  }
  //login as a community admin
  function logAsAdmin(id) {
    ajax('logasanadmin.php',{'id':id},'url');
  }

  // disconnect user
  function disconnect(){
    ajax('login.php',{'action':'logout'},'url');
  }

  function ajax(url,data,action){
    $.post('ajax/'+url,data, function(data,status) {
      //set action to debug to know what the server sends back
      if(action==='debug'){
        console.log(data);
      }
      var result = $.parseJSON(data);
      if (result['outcome'] == true) {
        switch(action){
          //figure out if we can display votes
          case 'searchVotes':
            setMarkers(result.message);
          break;
          //display earned badges
          case 'badges':
          var i = result.message.length-1;
          $.growl.notice({title: "", message: '<div class="row"><div class="col-4"><img class="badges" src="img/'+result.message[i].image+'"></div> <div class="col-8"><?php echo _("Congratulations ! You just earned the badge "); ?> "'+result.message[i][language]+'" !</div>', duration : 4000,location : 'tl'});
          i--;
          setInterval(function () {
            if(i>=0){
              $.growl.notice({title: "", message: '<div class="row"><div class="col-4"><img class="badges" src="img/'+result.message[i].image+'"></div> <div class="col-8"><?php echo _("Congratulations ! You just earned the badge "); ?> "'+result.message[i][language]+'" !</div>', duration : 4000,location : 'tl'});
              i--;
            }
          },4800);
          break;
          //Display message
          case 'notify':
          showMessage(result.message,result.outcome);
          break;
          //Display message
          case 'reloadCaptcha':
          $("#captchaImage").attr('src', $("#captchaImage").attr('src') + '?' + Math.random() );
          break;
          //set the markers on the map
          case 'setMarkers':
          d.topics = result['message'];
          setMarkers(result['message']);
          result.search === "" ? displayItems('',"problem"): displayItems('start',"problem");
          break;
          //display the solutions
          case 'getSolutions':
          d.solutions = result['message'];
          result.search === "" ? displayItems('',"solution"): displayItems('start',"solution");
          break;
          case 'getUserItems':
          d.solutions = result.message.solutions;
          d.topics = result.message.problems;
          d.propositions = result.message.propositions;
          console.log(d.votes);
          d.votes =  result.message.voted;
          break;
          //process votes on problems
          case 'newVote':
          if(result['props']>0){
            ajax("buildform.php",{type:"votePropositions",problemId: result["problemId"]},"form");
          }else{
            window.location.reload(true);
          }
          break;
          //send to popup to share the problem
          case 'newProblem':
          showMessage(result.message,result.outcome);
          $("#alert_button").click(function () {
            ajax("buildform.php",{type:"problemShare",id: result.problemId},"form");
          });

          break;
          //send to popup to share the solution
          case 'newSolution':
            if(result.url){
              ajax("buildform.php",{type:"newProposition",solutionId: result.id},"form");
            }else{
              ajax("buildform.php",{type:"solutionShare",id: result.id},"form");
            }
          break;
          case 'newCommunity':
          showMessage(result.message,result.outcome);
          //then send to payment
          break;
          case 'editUser' :
          if(result.message == 'delete'){
            window.location.replace('homepage.php');
          }else{
            showMessage(result.message,result.outcome);
            $("#alert_button").click(function () {
              window.location.reload();
            });
          }
          break;
          //show message and reload
          case 'showAndReload':
          showMessage(result.message,result.outcome);
          $("#alert_button").click(function () {
            window.location.reload();
          });
          break;
          //show message and redirect
          case 'showAndRedirect':
          showMessage(result.message,result.outcome);
          $("#alert_button").click(function () {
            window.location.replace(result.url);
          });
          break;
          //Go to URL
          case 'url':
          window.location.replace(result.url);
          break;
          case 'deleteAdmin':
          $('.toDelete').closest("tr").remove();
          showMessage(result.message,result.outcome);
          break;
          //Just reload the page
          case 'reload':
          window.location.reload();
          break;
          case 'userSettings':
            showMessage(result.message,result.outcome);
            $("#alert_button").click(function () {
              window.location.replace("profile.php");
            });
          break;
          case 'newsletterSubscription':
          showMessage(result.message,result.outcome);
          $("#alert_button").click(function () {
          window.location.replace("homepage.php");
          });
          break;
          case 'form':
          //empty the modal
          $('.modal-content').empty();
          //open the modal
          $("#formModal").modal();
          //add answer to modal
          $('.modal-content').append(result.message);
          //close modal when clicking on X
          $('#modalClose').click(function(){
            $('.modal-content').empty();
            $("#formModal").modal('hide');
          });
          //Launch the useful JS with the modal
          additionalFormJS();
          break;
          default :
          console.log('Il n\'y a rien dans la fonction ajax pour ce cas là');
          break;
        }
      } else if (result['outcome'] == false) {
        showMessage(result.message,result.outcome);
        $("#alert_button").click(function () {
          $(".submit").hide();
        });
      }
    });
}

  $('.lang').click(function(){
      var lang = $(this).data('name');
      changelanguage(lang);
    });
  //validate fields
var checkForm = "<?php echo _("Please check that the form has been filled properly"); ?>";

  function validate(){
    var inputs = $("form .validate");
    var alerts = [];
    var result = true;
    for (var i =inputs.length - 1; i >= 0; i--) {
      var inputVal = inputs[i].value;
      // console.log(inputs[i].id+' '+inputs[i].value);
      if(inputs[i].type == "text" || inputs[i].type == "textarea" || inputs[i].type == "phone" || inputs[i].id == "oldPassword" || inputs[i].type == "select-one"){
        if (inputVal == "") {
          if(alerts.length === 0){alerts.push("<?php echo _("Please check that the form has been filled properly"); ?><br>");} //Please check all fields have been filled
          inputs[i].style.backgroundColor = "rgba(255,113,112,0.5)";
          result = false;
        }
        else{
          inputs[i].style.backgroundColor = "rgba(91,200,57,0.5)";
        }

      } else if(inputs[i].id == "password"){
        if (scorePassword(inputVal)<60){
          if(scorePassword(inputVal)!==0){
            alerts.push("<?php echo _("Your password must contain at least 1 upper case letter, 1 lower case letter, 1 number, 1 special character and be at least 8 characters long....sorry...a sword isn't enough anymore to protect yourself from pirates"); ?><br>");
          }else{

          }
          inputs[i].style.backgroundColor = "rgba(255,113,112,0.5)";
          result = false;
        }else{
          inputs[i].style.backgroundColor = "rgba(91,200,57,0.5)";
        }
      } else if(inputs[i].id == "newPassword"){
        if (inputVal != "") {
          if (scorePassword(inputVal)<60){
            if(scorePassword(inputVal)!==0){
              alerts.push("<?php echo _("Your password must contain at least 1 upper case letter, 1 lower case letter, 1 number, 1 special character and be at least 8 characters long....sorry...a sword isn't enough anymore to protect yourself from pirates"); ?><br>");
            }else{

            }
            inputs[i].style.backgroundColor = "rgba(255,113,112,0.5)";
            result = false;
          }else{
            inputs[i].style.backgroundColor = "rgba(91,200,57,0.5)";
          }
        }
      } else if(inputs[i].type == "email"){
        if (!validateEmail(inputVal)){
          alerts.push("<?php echo _("Please enter a valid email"); ?><br>"); //Please enter a valid email
          result = false;
        }
        else{
          inputs[i].style.backgroundColor = "rgba(91,200,57,0.5)";
        }
      } else if(inputs[i].id == "phone"){
        if(!$("#phone").intlTelInput("isValidNumber")){
          var error = $("#phone").intlTelInput("getValidationError");
          switch(error){
          //Feedback to user
          case 2:
            showMessage('<?php echo _("The number is too short"); ?>',false);//The number is too short
            break;
            case 3:
            showMessage('<?php echo _("The number is too long"); ?>',false);//The number is too long
            break;
            case 4:
            showMessage('<?php echo _("The number is too long"); ?>',false);//This is not a valid phone number
            break;
            default :
            showMessage('<?php echo _("The number is too long"); ?>',false);//This is not a valid phone number
            break;
          }
          inputs[i].style.backgroundColor = "rgba(255,113,112,0.5)";
          result = false;
        }else{
          inputs[i].style.backgroundColor = "rgba(91,200,57,0.5)";
        }
      } else if(inputs[i].id == "passwordCheck"){
        var password = $('#password').val();
        if (password != inputVal){
          alerts.push("<?php echo _("The password field and the password check don't match"); ?><br>"); //Password field and password check don't match
          inputs[i].style.backgroundColor = "rgba(255,113,112,0.5)";
          result = false;
        } else{
          inputs[i].style.backgroundColor = "rgba(91,200,57,0.5)";
        }
      } else if(inputs[i].id == "settingsPasswordCheck"){
        var password = $('#newPassword').val();
        if (password != inputVal){
          alerts.push("<?php echo _("The password field and the password check don't match"); ?><br>"); //Password field and password check don't match
          inputs[i].style.backgroundColor = "rgba(255,113,112,0.5)";
          result = false;
        } else{
          inputs[i].style.backgroundColor = "rgba(91,200,57,0.5)";
        }
      }
      if(inputs[i].id == "emails"){
        var emails = inputVal.split(';');
        inputs[i].style.backgroundColor = "rgba(91,200,57,0.5)";
        for (var j = 0; j < emails.length; ++j) {
          if (!validateEmail(emails[j])){
            inputs[i].style.backgroundColor = "rgba(255,113,112,0.5)";
            result = false;
          }
        }
      }
    }
    $('#alerts').empty();
    if(result){
      clearInterval();
      $(".submit").show();
    }else{
      $(".submit").hide();
      if(alerts.length>0){
        $('#alerts').append('<p class="alert alert-warning" role="alert">'+alerts+'</p>');
      }
      window.setInterval(function(){
        validate();
        return;
      }, 1000);
    }
  }

//user is "finished typing," do something
function doneTyping () {
 validate();
}

  //post form to be processed
  function processNewUser() {
    ajax('processnew.php',$( "#newUserForm" ).serialize(),'showAndReload');
  }
 //hide the cookie notification
 function cookieNotif(){
  ajax('cookie.php',{'cookie': 'ok'},'reload');
 }

  //verify email
  function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
  }
//post form to be processed
function processNewUser() {
    ajax('processnew.php',$( "#newUserForm" ).serialize(),'showAndReload');
}
//function to handle most forms
function processForm(formName,action){
  ajax('processnew.php',$("#"+formName ).serialize(),action);
}

//post form to be processed
function processNewProblem() {
    ajax('processnew.php',$( "#newProblemForm" ).serialize(),'newProblem');
}
//post form to be processed
function processChanges() {
    ajax('edit.php',$( "#settingsForm" ).serialize(),'editUser');
}
 //hide the cookie notification
 function cookieNotif(){
  ajax('cookie.php',{'cookie': 'ok'},'reload');
 }
//change the language
function changelanguage(lang){
  window.location.search += '&lang='+lang;
}
function processNewCommunity() {
  ajax('processnew.php',$( "#newCommunityForm" ).serialize(),'newCommunity');
}
function cancelSubscription() {
  ajax('buildform.php',{type:'cancelSubscription'},'form')
  event.preventDefault();
}
function resendEmail(){
  ajax('processnew.php',{'type': 'resendValidation'},'showAndReload');
  event.preventDefault();
}
function processSpam(flaggedBy,spamId,outcome,entity,entityId){
  ajax('processspam.php',{'flaggedBy' : flaggedBy,'spamId' :spamId,'outcome' :outcome,'entity' : entity,'entityId' : entityId},'showAndReload');
  event.preventDefault();
}
function deleteAdmin(userId){
  ajax('deleteadmin.php',{userId: userId,communityId: $('#communityId').val()},'deleteAdmin');
  event.preventDefault();
}
function confirmCancelSubscription(){
  ajax('cancelSubscription.php',$( "#confirmationForm" ).serialize(),'showAndReload');
}
function subscribe(el){
  ajax('subscription.php',{'entityId': $(el).data('entityid'),'notificationTypeId': $(el).data('notificationtypeid'),action: el.checked},'notify');
}
function follow(value,communityId){
  ajax('communitysubscription.php',{'value': value,'communityId': communityId},'showAndReload');
}
function stopSupport(propositionId){
  ajax('stopsupport.php',{'propositionId': propositionId},'showAndReload');
}
function toggle(type,entityId){
  ajax('toggle.php',{'entityId': entityId,'type':type},'reload');
}
function newsletterSubscription(outcome) {
  var data = {email:$( "#email" ).val(),outcome:outcome};
  ajax('newslettersubscription.php',data,'newsletterSubscription');
}

function changeOption(item) {
  var divHtml = $(item).html();
  var classes = $(item).attr('class');
  var id = $(item).attr('id');
  var editableText = $("<textarea />");
  editableText.val(divHtml);
  editableText.addClass(classes);
  editableText.attr('id',id);
  $(item).replaceWith(editableText);
  editableText.focus();
  // setup the blur event for this new textarea
  editableText.blur(editableTextBlurred);
}

function editableTextBlurred() {
  ajax('editproblem.php',{'property': $(this).attr('id'),'value': $(this).val(),'problemId' : problemId},'notify');
}

function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}
//Create link and open page
  function openLink(){
    var type = $(this).attr("data-id").charAt(0);
    var id = $(this).attr("data-id").substring(1);
    switch(type){
      case 'p':
      window.location.replace("problem.php?id="+id);
      break;
      case 's':
      window.location.replace("solution.php?id="+id);
      break;
      case 'n':
      window.location.replace("proposition.php?propositionId="+id);
      break;
    }
  }

function additionalFormJS (){
  $('#topic_search').val(tag);
  $('form:first *:input[type!=hidden]:first').focus();
  ajax('notify.php',"",'badges');
  $("*:input[type!=hidden]").focusout(function(){
    validate();
  })
  //autocomplete tags
  if($('#problemTags').length > 0){
    //load the list of tags for the autocomplete
    $('#problemTags_tag').autocomplete({
      appendTo: '#problemTags_tagsinput',
      source: 'ajax/tags.php',
      minLength: 2,
      change: function(event, ui) {
        if (ui.item == null) {
            $("#problemTags_tag").val("");
            $("#problemTags_tag").focus();
        }
      }
    });
  }
  //autocomplete tags
  if($('#solutionTags').length > 0){
    //load the list of tags for the autocomplete
    $('#solutionTags_tag').autocomplete({
      appendTo: '#solutionTags_tagsinput',
      source: 'ajax/tags.php',
      minLength: 2,
      change: function(event, ui) {
        if (ui.item == null) {
            $("#solutionTags_tag").val("");
            $("#solutionTags_tag").focus();
        }
      }
    });
  }
  var options ={};
  //detect if it's a mobile browser since enter key doesn't work if autocomplete is activated on mobile
  if (typeof window.orientation !== 'undefined') {
    options = {width:'auto'}
  }else{
    options = {width:'auto', autocomplete_url: 'ajax/tags.php'};
  }
  $('#problemTags').tagsInput(options);
  $('#solutionTags').tagsInput(options);
  //mark notification as read
  var markAsRead = function (){
    ajax('markasread.php',{'id': $(this).attr('id'),'link': $(this).attr('data-link')},'url');
  }
  $("#notifications>a").hover(
    function(){
      $(this).bind("click",markAsRead);
    },
    function(){
      $(this).unbind("click",markAsRead);
    }
  );

  //for the problem.php page
  $('[id^="newText"]').focus(function(){
    var name = this.id.replace("newText","");
    $('#new'+name).prop('checked',true);
  })
  $('[id^="clauseSlide"]').click(function(){
    var name = this.id.replace("clauseSlide","");
    $('#props'+name).toggle("slow");
  })
  //for profile.php
  if($('#mapContainerEdit').length>0 || $('#mapContainerShow').length>0){
    d.geocoder = new google.maps.Geocoder() ;
    d.marker ;
    loadMap(10);
  }
  if($('#newCommunityForm').length > 0){
    //load the logic for the new community if it's a collectivity
    // $( "#communityTypeId" ).change(function() {
    //  if($(this).val() == 2){
      //load the list of tags for the autocomplete
      $('#name').autocomplete({
        appendTo: '.modal-content',
        source: 'ajax/communities.php',
        minLength: 3,
        change: function(event, ui) {
          if (ui.item == null) {
              $("#name").val("");
              $("#name").focus();
          }
        }
      });
    //add help on why can't I find my community ?
    $('#popUpTitle').show().popover();
  }
  //load the list if they want to subscribe to a community
  if($('#findCommunityForm').length > 0){
    //load the list of tags for the autocomplete
    $('#name').autocomplete({
      appendTo: '.modal-body',
      source: 'ajax/communitysubscription.php',
      minLength: 3,
      change: function(event, ui) {
        if (ui.item == null) {
            $("#name").val("");
            $("#name").focus();
        }
      }
    });
  }
  //if it's to change a community problem, load the list
  if($('#notMyProblemForm').length > 0 ){
    //load the list of tags for the autocomplete
    $('#name').autocomplete({
      appendTo: '.modal-content',
      source: 'ajax/delegateproblem.php',
      minLength: 3,
      change: function(event, ui) {
        if (ui.item == null) {
            $("#name").val("");
            $("#name").focus();
        }
      }
    });
  }
  if($('#adminManagementForm').length > 0){
    var x=0;
    $('#newAdmin').click(function(){
      $("#admins tr").eq(-1).before('<tr><td colspan="3" ><input id="email'+x+'" type="text" placeholder="<?php echo _("Email") ?>" class="greenBorder email" name="email'+x+'"/></td></tr>');
      x++
    });
    $('.fa-trash-alt').click(function(){
        $(this).addClass('toDelete');
    })
  }
  //if there's an addrees list, search it when clicking on enter
  if($('#location').length > 0){
    $('#location').keypress(function(event){
    if(event.which==13){
      event.preventDefault();
      SearchLocation();
    }
    });
    $('#locationSearch span').click(SearchLocation);
  }
}
function displayItems(clicked,type) {
  var list;
  switch(type){
    case 'problem':
      list = d.topics;
    break;
    case 'solution':
      list = d.solutions;
    break;
    case 'proposition':
    list = d.propositions;
    break;
    case 'votes':
    list = d.votes;
    break;
  }
  var page = $("#current").data('nb');
  var nbNonDisplayed = list.length-d.pageLength;
  // Displaying the right page
  switch(clicked){
    case 'previous':
      page-=d.pageLength;
      $("#current").data('nb',page);
      break;
    case 'next':
      if(nbNonDisplayed>0){
        page+=d.pageLength;
        $("#current").data('nb',page);
      }
      break;
    case 'start':
      page = 0;
      $("#current").data('nb',page);
      break;
    case 'end':
      page = nbNonDisplayed;
      $("#current").data('nb',page);
      break;
  }
  $("#itemList").empty();
  if (list.length == 0){
    $('#itemList').append('<p>Il n\'y a rien à afficher sur cette zone</p>');
    $('.pages').hide();
    return;
  }else{
    $('.pages').show();
  }
  if(page<0){
    page = 0;
    $("#current").data('nb',page);
  };
  if(page>(nbNonDisplayed) && nbNonDisplayed>0){
    page = nbNonDisplayed;
    $("#current").data('nb',page);
  };
  var last;
  if(list.length < d.pageLength){
    last = page+list.length;
  } else {
    last = page+d.pageLength;
  }
  for (var i = page ; i < last ; i++) {
    var item = renderItem(list[i],type);
    $( "#itemList" ).append( item );
    var pageNumber = Math.ceil(last/d.pageLength);
    $('#current').text(pageNumber);
    $("#previous").empty();
    $("#next").empty();
    $("#end").empty();
    $("#start").empty();
    if(pageNumber>1){
      $("#previous").append(pageNumber-1);
    }
    if(pageNumber!=1){
      $("#start").append(1);
    }
    if(Math.ceil(list.length/d.pageLength)!==pageNumber){
      $("#next").append(pageNumber+1);
    }
    if(Math.ceil(list.length/d.pageLength)!==pageNumber){
      $("#end").append(Math.ceil(list.length/d.pageLength));
    }

  }
}

function renderItem(obj,type){
  var item = '';
  switch(type){
    case 'votes':
    case 'problem':
      var helpIcon ='';
        if(obj.needHelp == 1){
          helpIcon = '<i class="fas fa-hand-paper"></i>';
        }
      item ='<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 p-1"><div class="card redBorder topic h-100" data-id="p';
      item += obj.itemId;
      item += '"><div class="card-header d-flex justify-content-between">';
      item += '<div class="topic_pix"><img src="img/';
      item += obj.icon;
      item += '"></div>';
      item += '<div class="topic_sharenvote p-2" id="';
      item += obj.itemId;
      item += '"><span>';
      item += obj.count;
      item += '</br><span class="word">votes</span></span></div>';
      item += helpIcon;
      item += '</div><div class="card-body"><h5 class="card-title text-center">';
      item += obj.title;
      item += '</h5><br></div><div class="card-footer d-flex justify-content-md-around p-0"><small class="text-muted p-0">';
      item += obj.age;
      item += '</small><small class="text-muted p-0">'
      item += obj.nbItems;
      item += '</small></div></div></div>';
    break;
    case 'solution':
      item ='<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 p-1"><div class="card greenBorder topic h-100" data-id="s';
      item += obj.itemId;
      item += '"><div class="card-header d-flex justify-content-between">';
      item += '<div class="topic_pix"><img src="img/';
      item += obj.icon;
      item += '"></div></div><div class="card-body"><h5 class="card-title text-center">';
      item += obj.title;
      item += '</h5><br></div><div class="card-footer d-flex justify-content-md-around p-0"><small class="text-muted p-0">Date :';
      item += obj.age;
      item += '</small><small class="text-muted p-0">Nb de problèmes :'
      item += obj.nbItems;
      item += '</small></div></div></div>';
    break;
    case 'proposition':
      item ='<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6"><div class="card greenBorder text-center p-1" data-id="p';
      item += obj.itemId;
      item += '"><div class="whiteBorder font_white gradient" style="background-image: linear-gradient(to right, rgb(91, 200, 57) 0%, rgb(91, 200, 57)';
      item += obj.nbItems;
      item += '%, rgb(218, 50, 50)';
      item += obj.nbItems;
      item += '%, rgb(218, 50, 50) 100%)"><?php echo _("Pertinence") ?> : ';
      item += obj.nbItems;
      item += '%</div><div class="row"><div class="offset-md-1 col-md-5 red no-padding font_white propThumbnail"><div class="triangleTopRight"></div>'
      item += obj.title;
      item += '</div><div class="col-md-5 green no-padding font_white propThumbnail"><div class="triangleBottomLeft"></div>'
      item += obj.description;
      item += '</div></div><div class="card-footer"><small class="text-muted"><?php echo _("Created on") ?>';
      item += obj.age;
      item += '</small></div></div></div>';
    break;
  }
    return item;
}
    $(document).ready(function() {
      additionalFormJS();
      $('.list').on('click', '.card', openLink);
      $('[data-toggle="tooltip"]').tooltip();
      $('#connect').keypress(function(event) {
        if (event.which == 13) {
          login();
        }
      });
      $('#communityAdmins').change(function(){
        window.location.replace("communityprofile.php?communityId="+$(this).val());
      })
      $('#communitySubscriptions').change(function(){
        window.location.replace("communityprofile.php?communityId="+$(this).val());
      })
<?php if (input::defined('email')) {?>
      ajax('buildform.php',{type:'newsletterSubscription',email:'<?php echo input::get('email') ?>'},'form');
<?php }
if (input::defined('validationEmail')) {?>
      ajax('buildform.php',{type:'userComplete',code:'<?php echo input::get('validationEmail') ?>'},'form');
<?php }
if (input::defined('contact')) {?>
      ajax('buildform.php',{type:'contactAdmins'},'form');
<?php }
if (input::defined('passwordId')) {?>
      ajax('buildform.php',{type:'setNewPassword',code:'<?php echo input::get('passwordId') ?>'},'form');
<?php }?>
      $('form:first *:input[type!=hidden]:first').focus();
      ajax('notify.php',"",'badges');
      $("*:input[type!=hidden]").focusout(function(){
        validate();
      });
      $('[data-toggle="popover"]').popover();
              //définir un timer pour lancer la fonction de validation après que l'utilisateur ait fini de saisir les infos
      $('li[role="presentation"]').click(function(){
        if($(this).children('a').attr('href')==='#badges'){
          $(".pages").hide();
        }else{
          $(".pages").show();
        }
        $('#current').text(1);
      });

    });
  <?php
  require_once config::get('root_path') . 'js/map.js';
  require_once config::get('root_path') . 'js/markerclusterer.min.js';
  require_once config::get('root_path') . 'js/newmarker.js';
  require_once config::get('root_path') . 'js/displaylist.js';
?>

//external stuff

  jqxAlert = {
         // top offset.
         top: 0,
         // left offset.
         left: 0,
         // opacity of the overlay element.
         overlayOpacity: 0.8,
         // background of the overlay element.
         overlayColor: '#ddd',
         // display alert.
         alert: function (message, title, color) {
              if (title == null) title = 'Alert';
              if(color===undefined){
                color = 'green';
              }
              jqxAlert._show(title, message, color, false);
         },
         // initializes a new alert and displays it.
         _show: function (title, msg,color, isOkNeeded) {
        if(isOkNeeded===undefined)
          isOkNeeded=true;
              jqxAlert._hide();
              jqxAlert._handleOverlay('show');
        var btn = isOkNeeded==true?'<button id="alert_button" type="button" value="Ok" class="solucracy_btn"/>':'<div class="popUpCloser font_'+color+'" id="alert_button"><i class=" fa fa-times font_white'+color+'"></i></div>';
              $("BODY").append(
                        '<div class="jqx-alert '+ color +
                        'Border rounded white" style="max-width: 50%;" id="alert_container">' +
                        '<div id="alert_title"></div>' +
                        '<div id="alert_content" class="p-1">' +
                             '<div id="message"></div>' + btn +
                        '</div>' +
                        '</div>');
              $("#alert_title").text(title);
              $("#alert_title").addClass('jqx-alert-header');
              $("#alert_content").addClass('jqx-alert-content');
              $("#message").text(msg);
              $("#alert_button").width(64);
              $("#alert_button").click(function () {
                   jqxAlert._hide();
              });
              jqxAlert._setPosition();
         },
         // hide alert.
         _hide: function () {
              $("#alert_container").remove();
              jqxAlert._handleOverlay('hide');
         },
         // initialize the overlay element.
         _handleOverlay: function (status) {
              switch (status) {
                   case 'show':
                        jqxAlert._handleOverlay('hide');
                        $("BODY").append('<div id="alert_overlay"></div>');
                        $("#alert_overlay").css({
                             position: 'absolute',
                             zIndex: 99998,
                             top: '0px',
                             left: '0px',
                             width: '100%',
                             height: $(document).height(),
                             background: jqxAlert.overlayColor,
                             opacity: jqxAlert.overlayOpacity
                        });
                        break;
                   case 'hide':
                        $("#alert_overlay").remove();
                        break;
              }
         },
         // sets the alert's position.
         _setPosition: function () {
              // center screen with offset.
              var top = (($(window).height() / 2) - ($("#alert_container").outerHeight() / 2)) + jqxAlert.top;
              var left = (($(window).width() / 2) - ($("#alert_container").outerWidth() / 2)) + jqxAlert.left;
              if (top < 0) {
                   top = 0;
              }
              if (left < 0) {
                   left = 0;
              }
              // set position.
              $("#alert_container").css({
                   top: top + 'px',
                   left: left + 'px'
              });
              // update overlay.
              $("#alert_overlay").height($(document).height());
         }
    }
    tarteaucitron.user.gajsUa = 'UA-XXXXXXXX-X';
  tarteaucitron.user.gajsMore = function () { /* add here your optionnal _ga.push() */ };
  (tarteaucitron.job = tarteaucitron.job || []).push('gajs');
  (tarteaucitron.job = tarteaucitron.job || []).push('googlemapssearch');
  </script>
  </body>
  </html>
