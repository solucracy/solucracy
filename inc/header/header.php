<?php
$deny = array("111.111.111", "222.222.222", "333.333.333");
if (in_array($_SERVER['REMOTE_ADDR'], $deny)) {
  header("location: https://example.com/");
  exit();
}
$user        = new user();
$notifs      = '';
$moderator   = "";
$adminButton = "";
helper::logHistory($_SERVER['PHP_SELF']);
if (!$user->isLoggedIn()) {
  $username = '';
  $login    = "ajax('buildform.php',{type:'login'},'form')";
} else {
  if (session::exists('communityName')) {
    $username = '<li class="m-1"><a href="' . config::get('base_url') . 'communityprofile.php?communityId=' . session::get('communityAdmin') . '" id="login_username" class="solucracy_btn"><i class="fas fa-user"></i> ' . session::get('communityName') . '</a></li>';
  } else {
    $username = '<li class="m-1"><a href="' . config::get('base_url') . 'profile.php" id="login_username" class="solucracy_btn"><i class="fas fa-user"></i> ' . $user->data()->userName . '</a></li>';
    if (notification::countUnread($user->data()->userId) > 0) {
      $notifs = '<li class="m-1"><span class="badge clickable red font_white" onclick="ajax(\'buildform.php\',{type:\'notifications\'},\'form\')">' . notification::countUnread($user->data()->userId) . '</span></li>';
    }
    if ($user->checkRole('moderator')) {
      $spam       = new spam();
      $countSpams = $spam->getList();
      if (count($countSpams) > 0) {
        $moderator = '<li class="m-1"><a onclick="ajax(\'buildform.php\',{type:\'moderation\'},\'form\')" class="solucracy_btn" data-toggle="tooltip" data-placement="bottom" title="Moderation"><i class="fa fa-eye">' . count($countSpams) . '</i></a></li><!-- Moderation -->';
      }
    }
  }
  $login = "disconnect()";

  //Check if admin of one or more communities
  $communityAdmin = $user->getUserCommunityAdmins();
  if (count($communityAdmin) > 1) {
    $adminButton = '<li class="m-1"><a onclick="ajax(\'buildform.php\',{type:\'communityChoice\'},\'form\')" class="solucracy_btn" data-toggle="tooltip" data-placement="bottom" title="' . _("Manage your communities") . '">' . _("Administration") . '</a></li><!-- Administration -->';
  } elseif (count($communityAdmin) === 1) {
    if (!session::exists('communityName')) {
      $adminButton = '<li class="m-1"><a onclick="logAsAdmin(' . $communityAdmin[0]->id . ')" class="solucracy_btn" data-toggle="tooltip" data-placement="bottom" title="Mode administration de communauté">' . _("Administration") . '</a></li><!-- Administration -->';
    } else {
      $adminButton = '<li class="m-1"><a onclick="logAsAdmin(-1)" class="solucracy_btn" data-toggle="tooltip" data-placement="bottom" title="Mode administration de communauté">' . _("User Profile") . '</a></li><!-- User Profile -->';
    }
  }
}
?>
<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
  <head>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="growl/jquery.growl.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/pageguide.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <script type="text/javascript" src="tarteaucitron/tarteaucitron.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="UTF-8"/>
    <title>%TITLE%</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <meta name="twitter:card" content="summary"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="https://solucracy.com/img/socialLogo.png">
    <meta property="og:site_name" content="Solucracy">
    <meta name="language" content="<?php echo _("anglais"); ?>">
    <meta content="%DESCRIPTION%" property="og:description">
    <meta content="%TITLE%" property="og:title"/>
    <meta content="%URL%" property="og:url"/>
      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Logo design by Ynkaba (https://www.facebook.com/pages/Ynkaba/286716328056727) -->
  </head>

<body>
<header class="faded_green_bkgd2">
<nav class="navbar navbar-expand-lg navbar-light">
  <div id="header" class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#menuItems" aria-controls="menuItems" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="<?php echo config::get('base_url'); ?>homepage.php" id="title"><img src="data:image/png;base64,<?php echo base64_encode(file_get_contents('img/logo transparent.png')); ?>" alt="Logo" height="50"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div id="menuItems" class="collapse navbar-collapse justify-content-end">
      <ul  class="nav navbar-nav navbar-right">
        <?php echo $username . $notifs . $moderator . $adminButton; ?>
        <li id="menu" class="dropdown m-1">
          <a href="#" class="dropdown-toggle solucracy_btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="problems.php"><?php echo _("Problems") ?></a></li>
            <li><a href="solutions.php"><?php echo _("Solutions") ?></a></li>
            <li><a href="#" onclick="ajax('buildform.php',{type:'newProblem'},'form')"><?php echo _("Add a problem") ?></a></li><!-- add a problem -->
            <li><a href="#" onclick="ajax('buildform.php',{type:'newSolution'},'form')"><?php echo _("Add a solution") ?></a></li><!-- add a solution -->
            <li><a href="#" onclick="ajax('buildform.php',{type:'invite'},'form')"><?php echo _("Invite people on Solucracy") ?></a></li><!-- Invite people on Democracia -->
            <li><a href="#" onclick="ajax('buildform.php',{type:'getCommunityUrl'},'form')"><?php echo _("Look for a community") ?></a></li><!--search for a community -->
          </ul>
        </li>
        <li class="dropdown m-1">
          <a href="#" class="dropdown-toggle solucracy_btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo _("Language") ?><span class="caret"></span></a><!-- langage -->
          <ul class="dropdown-menu language">
            <li class="lang" data-name="fr_FR">FR</li>
            <li class="lang" data-name="en_US">EN</li>
          </ul>
        </li>
        <li class="m-1"><a href="<?php echo config::get('blogAddress'); ?>"  class="solucracy_btn" target="blank">Blog</a></li><!-- Blog -->

        <li class="m-1"><a href="#" class="solucracy_btn loginButton" onclick="<?php echo $login ?>"><i class="fa fa-power-off"></i></a></li><!-- connexion -->
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div id="formModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
    </div>
  </div>
</div>
<script type="text/javascript">
    tarteaucitron.init({
      "hashtag": "#tarteaucitron", /* Automatically open the panel with the hashtag */
      "highPrivacy": false, /* disabling the auto consent feature on navigation? */
      "orientation": "top", /* the big banner should be on 'top' or 'bottom'? */
      "adblocker": false, /* Display a message if an adblocker is detected */
      "showAlertSmall": false, /* show the small banner on bottom right? */
      "cookieslist": true, /* Display the list of cookies installed ? */
      "removeCredit": false, /* remove the credit link? */
      "handleBrowserDNTRequest": false, /* Deny everything if DNT is on */
      //"cookieDomain": ".example.com" /* Domain name on which the cookie for the subdomains will be placed */
    });
    </script>
</header>
