<?php
require 'core/ini.php';
helper::loadHeader(
  'header.php',
  array(
  'TITLE' => _("Profile")
  , 'DESCRIPTION' => _("Profile"))
);

$displayCommunityAdmins        = "";
$displayCommunitySubscriptions = "";
$spamScore                     = '';
$validateAccount               = '';
if (!input::defined('userId') || session::get('user') === (input::get('userId'))) {
  $form            = new form();
  $user            = new user(session::get('user'));
  $validateAccount = "";
  if (!$user->checkRole('verified')) {
    $validateAccount = '<button type="button" class="solucracy_btn m-3" onclick="resendEmail()">' . _("Resend Validation Email") . '</button>'; //Resend validation email
  }
  $hello       = _("Hello");
  $communities = $user->getCommunities();
  if (!empty($communities)) {
    $displayCommunitySubscriptions .= $form->createField('select', 'communitySubscriptions', _("Communities you're following"), '', $communities);
  }
  $communityAdmins = $user->getuserCommunityAdmins();
  if (!empty($communityAdmins)) {
    $displayCommunityAdmins .= $form->createField('select', 'communityAdmins', _("Communities you manage"), '', $communityAdmins);
  }
} elseif (input::defined('userId') && session::get('user') !== (input::get('userId'))) {
  $user      = new user(input::get('userId'));
  $dt        = new DateTime($user->get('createdOn'));
  $createdOn = $dt->format('j m Y');
  $map       = 0;
  $hello     = false;
  $spamScore = $user->getNbSpams();
  if ($spamScore >= 0) {
    $spamScore = '<div class="text-center redRound m-1" data-toggle="tooltip" data-placement="top" data-original-title="' . _("Proportion of this user's publications considered as spam by the moderators") . '"><span>' . $spamScore . '% </span><span>Spam</span></div>'; //Nb of publications from this user considered as spam by the admins :
  }
}
$relatedRecords = $user->getUserRelatedRecords();
$items          = newsitem::getNewsItems('user', $user->get('userId'));
// //prepare problem list
//   $displayProblems = helper::displayList('problemList',$relatedRecords['problems']);
//   //prepare solution list
//   $displaySolutions = helper::displayList('solutionList',$relatedRecords['solutions']);
//   //prepare vote list
//   $displayVoted = helper::displayList('problemList',$relatedRecords['voted']);
//   //prepare proposition list
//   $displayPropositions = helper::displayList('proposition',$relatedRecords['propositions']);
//get all badges from user
$displayBadges = [0 => "", 1 => "", 2 => "", 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => ""];
$badges        = badge::getBadges(session::get('user'));
$badgeCount    = count($badges);
foreach ($badges as $badge) {
  if (is_null($badge->categoryId)) {
    $displayBadges[0] .= helper::render('badge', $badge);
  } else {
    $displayBadges[$badge->categoryId] .= helper::render('badge', $badge);
  }
}
?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <h3><?php echo $hello . ' ' . $user->get('userName'); ?></h3>
          <!-- Trigger the modal with a button -->
          <div class ="row p-3">
<?php if ($hello) {
  ?>


            <button type="button" class="solucracy_btn m-3" onclick="ajax('buildform.php',{type:'userSettings'},'form')"><i class="fas fa-user-cog"></i> <?php echo _("Settings") ?></button><!-- Settings -->
            <button type="button" class="solucracy_btn m-3" onclick="ajax('buildform.php',{type:'newCommunity'},'form')"><i class="fas fa-plus"></i> <?php echo _("Create a community") ?></button><!-- Create a community -->

<?php
echo $validateAccount;
  echo $displayCommunitySubscriptions;
  echo $displayCommunityAdmins;
} else {
  ?>
          <h5><?php echo _("Member since") . " " . $createdOn; ?></h5>

<?php
}

?>
          </div>
        </div>
        <div class="col-md-4 d-flex align-items-end">
          <?php
          echo $spamScore;
?>
          <h3 class=""><?php echo _("Latest news"); ?></h3>

      </div>
    </div>
      <div class="row">
        <div class="col-md-8">
            <div class="text-center ">
              <ul class="nav nav-tabs m-2 faded_green_bkgd" role="tablist">
                <li  class="nav-item"><a class="nav-link active" href=".badges" data-toggle="tab" role="tab" class="font_white green whiteBorder"><?php echo _("Achievements") ?> (<?php echo $badgeCount ?>)</a></li><!-- Achievements -->
                <li  class="nav-item"><a class="nav-link" href=".topics" data-toggle="tab" role="tab"  class="font_white green whiteBorder"><?php echo _("Reported problems") ?>  (<?php echo count($relatedRecords['problems']) ?>)</a></li><!-- Reported problems -->
                <li  class="nav-item"><a class="nav-link" href=".solutions" data-toggle="tab" role="tab"  class="font_white green whiteBorder"><?php echo _("Solutions added") ?> (<?php echo count($relatedRecords['solutions']) ?>)</a></li>
                <li  class="nav-item"><a class="nav-link" href=".voted" data-toggle="tab" role="tab"  class="font_white green whiteBorder"><?php echo _("Voted") ?> (<?php echo count($relatedRecords['voted']) ?>)</a></li><!-- Voted -->
                <li  class="nav-item"><a class="nav-link" href=".propositions" data-toggle="tab" role="tab"  class="font_white green whiteBorder"><?php echo _("Proposals made") ?> (<?php echo count($relatedRecords['propositions']) ?>)</a></li><!-- Proposals made -->
              </ul>

            <div class="row">
              <div class="w-100 d-flex justify-content-md-around">
                <div id='start' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
              <div id='previous' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
              <div id='current' class="pages font_green" data-nb="0"></div>
              <div id='next' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
              <div id='end' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
              </div>
              <div id="badges" class="tab-pane fade show active badges" role="tabpanel">
                <div class="d-flex justify-content-between p-1 text-center">
                  <?php $categories = helper::getCategories();
                  echo '<div class="d-flex align-items-start flex-column"><H5>' . _("Special badges") . '</H5>' . $displayBadges[0] . '</div>';
                  foreach ($categories as $category) {
                                      echo '<div class="d-flex align-items-start flex-column"><H5>' . $category->name . '</H5>' . $displayBadges[$category->id] . '</div>';
                  }
?>
                </div>
              </div>
              <div id="topics" class="tab-pane fade list topics row w-100 p-3" role="tabpanel" data-type="problem">
              </div>
              <div id="solutions" class="tab-pane fade list solutions row w-100 p-3" role="tabpanel" data-type="solution">
              </div>
              <div id="votes" class="tab-pane fade list voted row w-100 p-3" role="tabpanel" data-type="votes">
              </div>
              <div id="propositions" class="tab-pane fade list propositions row w-100 p-3" role="tabpanel"  data-type="proposition">
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 gray">
  <?php
  foreach ($items as $item) {
      echo helper::render('newsItem', $item);
  }
?>
        </div>
      </div>
    </div>
<?php
require "inc/footer.php";
?>
<script type="text/javascript">
  $("a[data-toggle='tab']").click(function(){
    var target = $(this).attr('href');
    var elements = $('.tab-pane');
    for (var i = elements.length - 1; i >= 0; i--) {
      elements[i].id = i;
    }
    $(target).attr('id','itemList');
    displayItems(this.id,$('#itemList').data('type'))
  })
$(document).ready(function() {
  searchAJAX(<?php echo $user->get('userId') ?>);
    });
</script>
