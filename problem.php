<?php
require 'core/ini.php';
//create a problem object
$problem = new problem(input::get('id'));
//get the data
$problemDetails = $problem->data();
//acceptable colors for the graph
$colors = ['#a6cee3', '#1f78b4', '#b2df8a', '#33a02c', '#fb9a99', '#e31a1c', '#fdbf6f', '#ff7f00', '#cab2d6', '#6a3d9a', '#ffff99', '#b15928'];
//Problem position
$location = $problemDetails->latitude . "," . $problemDetails->longitude;
//If the problem has been created less than 2 weeks prior, let the author modify the title
$editTitle  = false;
$nbDays     = strtotime(date('Y-m-d')) - strtotime($problemDetails->createdOn);
$timePassed = floor($nbDays / (60 * 60 * 24));
$timeLeft   = 14 - $timePassed;
if ($problemDetails->userId === session::get(config::get('session/session_name')) && $timePassed < 14) {
  $editTitle = true;
}

//Load the header for the page
helper::loadHeader(
  'header.php',
  array(
  'TITLE' => $problemDetails->title
  , 'DESCRIPTION' => $problemDetails->description)
);

//catch the date for the community responsible for this problem
$community           = $problem->currentOwner();
$contactProblemUsers = "";
$notMyProblem        = "";
$handledBy           = "";
//if the user is connected as a community and that problem is related to that community
if (session::exists('communityAdmin') && $community !== false && $community->communityId === session::get('communityAdmin')) {
  $contactProblemUsers = "<div onclick='ajax(\"buildform.php\",{type:\"contactProblemUsers\",problemId:" . $problemDetails->problemId . "},\"form\")' class='clickable'>" . _("Contact users impacted") . " <i class='fa fa-warning'></i></div> <!-- contact users impacted -->";
  $notMyProblem        = "<div onclick='ajax(\"buildform.php\",{type:\"notMyProblem\",problemId:" . $problemDetails->problemId . "},\"form\")' class='clickable'>" . _("Delegate this problem") . " <i class='fa fa-warning'></i></div> <!-- Déléguer ce problème -->";
}
if ($problemDetails->communityName !== null) {
  $handledBy = '<span class="font-weight-light col-12 text-left"">' . _("This problem is currently managed by") . '<a class="font-weight-bold" href="communityprofile.php?communityId=' . $problemDetails->communityId . '"><u>' . $problemDetails->communityName . '</u></a></span>'; //this problem is currently handled by
}
$displayPropositions = "</br>" . _("No solution has been logged yet") . "."; //No solution has been logged yet
//Get the problem facets
$facets        = $problem->getFacets();
$k             = 0;
$displayFacets = '';
foreach ($facets as $facet) {
  $newFacets[$facet->facetId]        = $facet;
  $newFacets[$facet->facetId]->order = $k;
  $displayFacets .= ',["' . $facet->description . '",' . $facet->nbVotes . ']';
  $k++;
}
//If there's a solution ofor this problem
if ($problemDetails->nbSolutions > 0) {
  $displayPropositions = "";
  $canVote             = false;
  //define if they got positive votes and display
  $positiveVotes = $problemDetails->solutionsPertinence;
  foreach ($positiveVotes as $positiveVote) {
    //calculate pertinence : number of positive votes divided by the number of total votes for that facet
    $pertinence = round(($positiveVote->nbVotes / $positiveVote->facetVotes) * 100, 2);
    //Filter only on facets this proposition deals with
    $facetIds = explode(',', $positiveVote->facetIds);
    $displayPropositions .= '<div class="blackBorder margin5 padding5">';
    $i        = 0;
    $barGraph = "";
    foreach ($facetIds as $facetId) {
      //calculate the bar on the graph based on the number of votes
      $size = round(($newFacets[$facetId]->nbVotes / $problemDetails->nbVotes) * 100);
      $barGraph .= '<div style="min-width: 30%;height: ' . $size . 'px; background-color: ' . $colors[$newFacets[$facetId]->order] . '; display: inline-block;" data-toggle="tooltip" title="' . $newFacets[$facetId]->description . '">&nbsp;</div>';
      //If the user has voted for one of those facets, let him evaluate the proposition
      if ($newFacets[$facetId]->voted > 0) {
        $canVote = true;
      }
      $i++;
    }
    $buttons = "";
    //Not supported by community, admin not connected
    $ribbon = "";
    if (session::exists('communityAdmin') && $community !== false && $community->communityId === session::get('communityAdmin')) {
      //Not supported by community, admin connected
      if (empty($positiveVote->endorsed)) {
        $ribbon = '<div class="col-1 p-1"><i class="fa fa-2x fa-plus font_green clickable" onclick="ajax(\'buildform.php\',{type:\'supportProposition\',propositionId:' . $positiveVote->propositionId . '},\'form\')" data-toggle="tooltip" data-placement="top" title="' . _("Show your support for this proposition !") . '"></i></div>';
        //supported by community, admin connected
      } elseif (!empty($positiveVote->endorsed)) {
        $ribbon = '<div class="col-1"><img src="img/support.png" height="64" width="64" class="clickable font_green float-right mt-0 mr-0" onclick="stopSupport(' . $positiveVote->propositionId . ')" data-toggle="tooltip" data-placement="top" title="' . _("Do not support this proposition anymore :") . ' ' . $positiveVote->comment . '"></div>';
      }
      //Supported by community, admin not connected
    } elseif (!empty($positiveVote->endorsed)) {
      $ribbon = '<div class="col-1"><img src="img/support.png" height="64" width="64" data-toggle="tooltip" data-placement="top" title="' . _("This proposition is supported by the community in charge of the problem :") . ' ' . $positiveVote->comment . '"></div>';
    }

    //if the proposition isn't supported by the community but an admin of this community is logged in, add the support button
    if (empty($positiveVote->endorsed) && session::exists('communityAdmin') && $community !== false && $community->communityId === session::get('communityAdmin')) {
      //if he can vote, add buttons on propositions
      if ($canVote && $positiveVote->voted < 1) {
        $buttons = '<i class="fa fa-thumbs-up font_green clickable" onclick="ajax(\'ajax/buildform.php\',{type:\'votePropositions\',problemId:' . $problemDetails->problemId . '},\'form\')"></i><i class="fa fa-thumbs-down font_red clickable" onclick="ajax(\'ajax/buildform.php\',{type:\'votePropositions\',problemId:' . $problemDetails->problemId . '},\'form\')"></i>';
      }
    }

    $displayPropositions .= '<div class="row"><div class="col-7"><h4><a href="solution-' . $positiveVote->solutionId . '.html"><i class="fas fa-info-circle font_green"></i></a> ' . $positiveVote->title . ' ' . $buttons . '</h4></div><div class="whiteBorder font_white gradient col-3 text-center" style="background-image: linear-gradient(to right, rgb(91, 200, 57) 0%, rgb(91, 200, 57) ' . $pertinence . '%, rgb(218, 50, 50) ' . $pertinence . '%, rgb(218, 50, 50) 100%);">
                  ' . $positiveVote->allVotes . ' votes sur ' . $positiveVote->facetVotes . ' utilisateurs impactés</div><div class="col-1 ">' . $barGraph . '</div>' . $ribbon . '</div><span class="col-6 col-offset-1">"' . $positiveVote->description . '"</span></div>';
  }
}
$propsLeft      = $problem->countPropsToEvaluate();
$evaluateButton = "";

$user = new user();

$voteButton = "<div id='newVote' onclick='ajax(\"buildform.php\",{type:\"vote\",problemId:" . $problemDetails->problemId . "},\"form\")' class='clickable'>" . _("Vote") . " <i class='fa fa-thumbs-up'></i></div>";
$newProp    = "<button type='button' onclick='ajax(\"buildform.php\",{type:\"selectSolution\",problemId:" . $problemDetails->problemId . "},\"form\")' class='clickable font_red white noBorder'><i class='fa fa-plus-circle'></i></button>";
$reportSpam = "<div onclick='ajax(\"buildform.php\",{type:\"spam\",entityId:" . $problemDetails->problemId . ",entityType:\"problem\"},\"form\")' class='clickable'>" . _("Spam") . " <i class='fa fa-warning'></i></div> <!-- report -->";

if ($problem->hasVoted()) {
  $voteButton = "<div id='newVote'>" . _("Already voted") . "<i class='fa fa-check'></i></div>"; //already voted
  if ($propsLeft > 0) {
    $evaluateButton .= "<div class='clickable' onclick='ajax(\"buildform.php\",{type:\"votePropositions\",problemId:" . $problemDetails->problemId . "},\"form\")'>" . $propsLeft . " " . _("Solution") . helper::plural($propsLeft) . " " . _("to evaluate") . "<i class='fa fa-gavel'></i></div>";
  }
}

?>

<div class="container-fluid map-height">
    <div class="row">
        <div itemscope class="col-md-8" itemtype="http://schema.org/ItemPage">
            <span id="infoWindow" style="padding-left: 25vw; color: red; font-weight: bold;"></span>
            <div class="row mbottom text-center" id="infos" data-problemid="<?php echo $problemDetails->problemId; ?>" d-lat="<?php echo $problemDetails->latitude ?>" d-lng="<?php echo $problemDetails->longitude ?>">
                <div class="col-md-2">
                    <img itemprop="image" src="img/<?php echo $problemDetails->categoryIcon ?>">
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <h3  itemprop="name" id="title"><?php echo $problemDetails->title; ?></h3>
        <?php if ($editTitle) {?>
                        <a href="#"  title="Il vous reste <?php echo $timeLeft; ?> jours pour éditer le titre" onclick="changeOption('.topic_title');"><i class="fa fa-edit"></i></a>
        <?php }?>
                    </div>
                    <div id="topic_descr" itemprop="description" class="row">
                      <span id="description" class="col-12 text-left"><?php echo $problemDetails->description ?></span>
                <?php if ($problem->isCreator()) {?>
                      <a href="#" class="topic_btn" title="<?php echo _("Click here to edit description") ?>" onclick="changeOption('#description');"><i class="fa fa-edit"></i></a>
                <?php }?>
                      <span class="col-12 text-left"><span itemprop="datePublished"><?php echo _("Created on") . " " . $problemDetails->createdOn; ?> |
                      <a href="profile.php?userId=<?php echo $problemDetails->userId; ?>" itemprop="author"><?php echo $problemDetails->userName; ?></a></span> |
                      <?php echo _("Scope") . ' : ' . _($problemDetails->scopeId); ?></span> <!-- added on -->
<?php
echo $handledBy;
?>
                    </div>
                    <div id="topic_tags" class="row">
                    <span class="col-12 text-left"><strong><?php echo _("Keywords") ?> :  </strong> <!-- keywords -->
                            <?php
                            $i        = 0;
                            $tagsSize = count($problemDetails->tags);
                            foreach ($problemDetails->tags as $tag) {
                                                          echo "<a class='font-italic' href=\"problems.php?tag=" . $tag . "\"><u>" . $tag . "</u></a>";
                              if ($i < ($tagsSize - 1)) {
                                echo ", ";
                              }

                                                          $i++;
                            }
                            echo '.';
?>
                    </span>
                    </div>
                </div>
                <div class="col-md-2">
                     <div class='redRound text-center'><span id="nbVotes"><?php echo $problemDetails->nbVotes ?></span><span id='text'><?php echo "vote" . helper::plural($problemDetails->nbVotes) ?></span></div>
                </div>
            </div>
            <div class="row font_white red mbottom">
                <div class="col-md-1">
                </div>
                <div class="col-md-2 whiteBorder text-center sideBorder">
<?php
echo $notMyProblem;
?>
                </div>
                <div class="col-md-2 whiteBorder text-center sideBorder">
<?php
echo $voteButton;
?>
                </div>
                <div class="col-md-2 whiteBorder text-center sideBorder">
<?php if ($problemDetails->statusId === '1') {
  echo $reportSpam;
}
?>
                </div>
                <div class="col-md-3 whiteBorder text-center sideBorder">
<?php
echo $contactProblemUsers;
?>
                </div>
                <div class="col-md-2">
<?php
echo $evaluateButton;
?>
                </div>
            </div>
            <div id='propositions' class="row">
              <div class="col-md-8">
                    <!-- Trigger the modal with a button -->
                    <h1><?php echo _("Proposals") . "(" . $problemDetails->nbSolutions . ") " . $newProp ?></h1><!-- Propositions -->
              </div>
              <div class='col-md-4 d-flex justify-content-around'>
                <a href="http://www.facebook.com/sharer.php?u=<?php echo "www.solucracy.com/problem-" . $problemDetails->problemId . ".html&title=" . $problemDetails->title; ?>"><i class="fab fa-2x fa-facebook-square font_blue"></i></a>
                <a href="http://reddit.com/submit?url=<?php echo "www.solucracy.com/problem-" . $problemDetails->problemId . ".html&title=" . $problemDetails->title; ?>"><i class="fab fa-2x fa-reddit-square font_blue"></i></a>
                <a href="https://twitter.com/intent/tweet?url=<?php echo "www.solucracy.com/problem-" . $problemDetails->problemId . ".html&TEXT=" . $problemDetails->title; ?>"><i class="fab fa-2x fa-twitter-square font_blue"></i></a>
                <a href="https://plusone.google.com/_/+1/confirm?hl=fr&url=<?php echo "www.solucracy.com/problem-" . $problemDetails->problemId . ".html"; ?>"><i class="fab fa-2x fa-google-plus-square font_blue"></i></a>
              </div>
            </div>
<?php
echo $displayPropositions;
?>
        </div>
        <div class="col-md-4">
          <div id="mapContainer">
            <div id="mapContainerShow" class="problemMap"><div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="<?php echo config::get('GMap_API_Key') ?>" width="605px" height="200px" ></div></div>
              <div id="expandBtn">
                <p id="expandMap"><?php echo _("Expand"); ?></p>
            </div>
          </div>
          <div class="row">
            <h3><?php echo _("Which need isn't fulfilled ?"); ?> ?</h3>
            <script type="text/javascript">
              google.charts.load("current", {packages:["corechart"]});
              google.charts.setOnLoadCallback(drawChart);
              function drawChart() {
                var data = google.visualization.arrayToDataTable([
                  ['<?php echo _("Reason"); ?>', '<?php echo _("# of votes"); ?>']
  <?php
  echo $displayFacets;
?>
                ]);

                var options = {
                  colors: ['#a6cee3','#1f78b4','#b2df8a','#33a02c','#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#cab2d6','#6a3d9a','#ffff99','#b15928'],
                  chartArea: {  width: "100%", height: "95%" },
                };

                var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                chart.draw(data, options);
              }
            </script>
            <div id="donutchart" class="full-height"></div>
          </div>
        </div>
    </div>
</div>

<?php
require config::get('root_path') . "inc/footer.php";
?>

<script type="text/javascript">

function ProcessVote() {
  ajax('processnew.php',$( "#newVoteForm" ).serialize(),'newVote');
}

$('#newText1').focus(function(){
  $('#new').prop('checked',true);
})

function expandMap(){
  if($('#expandMap').text() === ('<?php echo _("Expand"); ?>')){
    $("#mapContainer").height('70%');
    $("#mapContainerShow").height('100%');
    google.maps.event.trigger(d.map, 'resize');
    $('#expandMap').text('<?php echo _("Collapse"); ?>');
  } else {
    $("#mapContainer").height('35%');
    $("#mapContainerShow").height('100%');
    google.maps.event.trigger(d.map, 'resize');
    $('#expandMap').text('<?php echo _("Expand"); ?>');
  }
}

$('#expandMap').click(expandMap);

function processProps() {
  // add logic to check that children of swap newtext are not empty if they're checked
  var itemsChecked = $('form :checked');
  var result = true;
  for (var i = itemsChecked.length - 1; i >= 0; i--) {
    var itemId = itemsChecked[i].id;
    if(itemId.indexOf("new")>= 0){
      var inputId = [itemId.slice(0, 3), 'Text', itemId.slice(3)].join('');
      if($('#'+inputId).val().length < 1){
        result = false;
      }
    }
  }
  if(result){
    ajax('processnew.php',$( "#propositionsForm" ).serialize(),'showAndReload');
  }else{
    showMessage("<?php echo _("Please define the new reason"); ?>",false);
  }
}
</script>

