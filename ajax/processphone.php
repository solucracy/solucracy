<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';
$data = input::get('full_array');
$user = new user();
// check if user is logged
if (!$user->isLoggedIn()) {
  echo helper::outcome(_("Hello ! You need to login to do that"), false);
  return;
}
if (isset($data['code']) && $data['code'] !== '') {
  if ($user->checkPhoneCode($data['code'])) {
    $user->updateStatus(1);
    $user->addUserRole(session::get('user'), 'verified');
    badge::createUnique(3, session::get('user'), session::get('user'));
    echo helper::outcome(_("Your account is now active !"), true); //Your account is now active !
    exit();
  } else {
    echo helper::outcome(_("This code is incorrect"), false); //This code is incorrect
    exit();
  }
} else {
  echo helper::outcome(_("Please enter the validation code sent to your phone"), false); //Please enter the validation code sent to your phone
  exit();
}
