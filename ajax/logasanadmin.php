<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';

$user = new user();
$id   = input::get('id');
if ($id === '-1') {
  session::delete('communityName');
  session::delete('communityAdmin');
  echo helper::outcome(_("You are not logged in as a community anymore"), true, 'profile.php');
  return;
}
if ($user->loginAsAdmin($id)) {
  echo helper::outcome(_("Hello"), true, 'communityprofile.php?communityId=' . $id);
  return;
} else {
  echo helper::outcome(_("There's been a problem"), false);
  return;
}
;
