<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';

$input = input::get('full_array');
// echo helper::outcome($input,true);
// exit();
$group          = '';
$searchterms    = '';
$solutionSearch = '';
$scope          = '';
//filter by status
if ($input['statusId'] == '') {
  $criteria = 'AND p.statusId in (1,2,6)';
} else {
  $criteria = 'AND p.statusId = ' . $input['statusId'];
}
//only problems for which the community is looking for a solution
if ($input['solutionSearch'] === 'true') {
  $solutionSearch = ' AND cp.needHelp = 1';
}
if ($input['scope'] == '') {
  $scope = ' AND 1=1';
} else {
  $scope = ' AND scopeId =' . helper::test_input($input['scope']);
}

//define order of problems
switch ($input['sort']) {
case '1':
  $sort = 'ORDER BY nbvotes desc';
  break;
case '2':
  $sort = 'ORDER BY p.categoryId desc';
  break;
case '3':
  $sort = 'ORDER BY p.createdOn desc';
  break;
case '4':
  $sort = 'ORDER BY nbSolutions desc';
  break;
default:
  $sort = 'ORDER BY NULL';
  break;
}
if (isset($input['group']) && $input['group'] > 0) {
  $group = ' AND cp.communityId = ' . $input['group'];
}
if (isset($input['search']) && $input['search'] != '') {
  $searchterms = '(MATCH (p.title,p.description) AGAINST ("' . helper::test_input($input['search']) . '")OR MATCH (t.name) AGAINST ("' . helper::test_input($input['search']) . '")) AND';
}
if ($input['cat'] === '') {
  $processedcat = '';
} else {
  $processedcat = "(p.categoryId IN (" . $input['cat'] . ")) AND";
}
$coordinates = $input['coordinates'];
$db          = db::getInstance();
//query optimized with https://www.eversql.com/sql-query-optimizer/
$sql = $db->query(
  "CREATE TEMPORARY TABLE IF NOT EXISTS temp1 AS SELECT
    COUNT(DISTINCT v.voteId) AS count,
    f.problemId
  FROM
    vote AS v
  INNER JOIN
    facet AS f
      ON f.facetId = v.facetId
  WHERE
    1 = 1
  GROUP BY
    f.problemId
  ORDER BY
    NULL;"
);
$sql = $db->query(
  "ALTER TABLE
  `temp1`
ADD
  INDEX `temp1_idx_problemid_count` (`problemId`, `count`);"
);
$sql = "SELECT
    p.problemId as itemId,
    p.title,
    p.userId,
    u.userName,
    p.categoryId AS category,
    (case
    when cp.needHelp = 1 then 1
    when cp.needHelp = 0 then 0
    when cp.needHelp IS NULL then 0
    end) as needHelp,
    c.icon,
    concat(l.FR,
    ': ',
    (SELECT
      COUNT(DISTINCT pe.propositionId)
    FROM
      pertinence AS pe
    INNER JOIN
      facet AS f
        ON f.facetId = pe.facetId
    WHERE
      f.problemId = p.problemId)) AS nbItems,
    temp1.count,
    DATE_FORMAT(p.createdOn,
    '%d/%m/%Y') AS age,
    p.latitude,
    p.longitude
  FROM
    problem AS p
  LEFT JOIN
    user AS u
      ON p.userId = u.userId
  LEFT JOIN
    category AS c
      ON c.categoryId = p.categoryId
  LEFT JOIN
    problem_tag AS pt
      ON pt.problemId = p.problemId
  LEFT JOIN
    tag AS t
      ON t.tagId = pt.tagId
  LEFT JOIN
    communityproblem AS cp
      ON cp.problemId = p.problemId
      AND cp.statusId = 1
  LEFT JOIN
    language AS l
      ON l.id = 109
  LEFT JOIN
    temp1
      ON temp1.problemId = p.problemId
  WHERE";
$sql .= $searchterms;
$sql .= $processedcat;
$sql .= " (p.latitude > ? AND p.latitude < ? AND p.longitude > ? AND p.longitude < ?) " . $criteria . $group . $solutionSearch . $scope . " GROUP BY p.problemId, needhelp, count " . $sort;

$db->query($sql, array($coordinates[0], $coordinates[2], $coordinates[1], $coordinates[3]));
if ($db->count() >= 0) {
  echo helper::outcome($db->results(), true);
  exit();
} else {
  error_log(json_encode($db->debug()), 3, "my-errors.log");
  echo helper::outcome(_("Oups ! We were not able to access the database"), false);
  exit();
}
