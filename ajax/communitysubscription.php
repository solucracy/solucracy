<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';
if (input::defined('term')) {
  $data          = input::get('term');
  $communityList = helper::getCommunityList($data, 1);
  $jsonfile      = json_encode($communityList);
  echo $jsonfile;
} elseif (input::defined('communityId')) {
  $community = new community(input::get('communityId'));
  if (input::get('value') > 0) {
    if ($community->subscribe()) {
      echo helper::outcome(_("You just subscribed to this community"), true); //You just subscribed to this community
      exit();
    } else {
      echo helper::outcome(_("There's been a problem"), false); //There's been a problem
      exit();
    }
  } else {
    if ($community->unsubscribe()) {
      echo helper::outcome(_("You just unsubscribed from this community"), true); // you unsubscribed from this community
      exit();
    } else {
      echo helper::outcome(_("There's been a problem"), false); //There's been a problem
      exit();
    }
  }
}
