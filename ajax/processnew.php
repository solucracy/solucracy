<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';

$isHelogged = new user();

$data = input::get('full_array');
// echo helper::outcome($data,FALSE); //uncomment this to test if the data sent is correct
// exit();
if (!in_array($data['type'], array("newUser", "getCommunityUrl", "contactEmail", "reloadCaptcha", "passwordRequest", "setNewPassword"))) {
  if (!$isHelogged->isLoggedIn()) {
    echo helper::outcome(_("Hello ! You need to login to do that"), false);
    return;
  }
}

// input : data + type of data
switch ($data['type']) {

  case 'passwordRequest':
    $validate   = new validate();
    $validation = $validate->check(
      $data,
    array(
    'captcha' => array(
    'captcha'  => true,
    'required' => true,
    'min'      => 6),
      'email'   => array(
      'required'    => true,
      'valid_email' => true),
    )
    );
    if ($validation->passed()) {
        $user = new user();
      if ($user->newPasswordRequest($data['email'])) {
        echo helper::outcome(_("You will receive an email to reset your password shortly. If you didn't get anything, please check your spam folder, and if the email isn't there, please contact us."), true); //you're going to receive an email
        exit();
      } else {
          echo helper::outcome(_("We couldn't find a user with this email"), false); //We couldn't find anyone with this email
          exit();
      }
    } else {
        $message = "";
      foreach ($validate->errors() as $error) {
        $message = $message . '<br>' . $error;
      }
      echo helper::outcome($message, false);
      exit();
    }
    break;

  case 'setNewPassword':
    $validate   = new validate();
    $validation = $validate->check(
      $data,
      array(
      'requestId'     => array(
      'required' => true),
      'password'      => array(
      'required' => true,
      'min'      => 6),
      'passwordCheck' => array(
      'required' => true,
      'matches'  => 'password'),
      )
    );
    if ($validation->passed()) {
      $password = md5(helper::test_input($data["password"]) . config::get('salt'));
      $id       = input::get('requestId');
      $db       = db::getInstance();
      $db->query("UPDATE `user` SET `password`=?, passwordRequestId= NULL, requestTime = NULL WHERE passwordRequestId = ?", array($password, $id));
      $output['outcome'] = true;
      $output['message'] = _("Your password has been updated");
      $output['url']     = 'homepage.php';
      echo json_encode($output);
      exit();
    } else {
      $output['outcome'] = false;
      $output['message'] = "";
      foreach ($validate->errors() as $error) {
        $output['message'] = $output['message'] . '<br>' . $error;
      }
      echo json_encode($output);
      exit();
    }
    break;

  case 'sendNewsletter':
    $user = new user();
    if (!$user->checkRole('administrator')) {
      echo helper::outcome(_("You don't have the required privileges to do this."), false); //You don't have the necessary privs to do that
      exit();
    } else {
      $email = new email();
      $list  = helper::getNewsletterSubscriptions();
      foreach ($list as $item) {
        $email->sendNewsletter(array('email' => $item->email, 'text' => $data['text'], 'link' => $data['link']));
      }
      echo helper::outcome("tout a été envoyé !", true);
      exit();
    }
    break;

  case 'reloadCaptcha':
    $file         = helper::createCaptcha();
    $fileLocation = config::get("root_path") . '/img/' . $file . '.jpeg';
    echo helper::outcome($fileLocation, true);
    exit();
    break;
  case 'problem':
    if (!$isHelogged->checkRole('verified')) {
      echo helper::outcome(_("Please verify your account to be able to do that : Click on the link on the validation email or ask for a new one by clicking on the button below"), false);
      exit();
    }
    // validate the data
    $validate   = new validate();
    $validation = $validate->check(
      $data,
      array(
        'title'       => array(
        'required' => true,
        'min'      => 6),
        'location'    => array(
        'required' => true),
        'vote'        => array(
        'required' => true,
        'min'      => 6),
        'description' => array(
        'required' => true),
        'problemTags' => array(
        'required' => true),
      )
    );

    if ($validation->passed()) {
      unset($data['location'], $data['type']);
      $problem = new problem();
      if ($problem->create($data)) {
        $output['outcome']   = true;
        $output['message']   = _("Congratulations ! Your problem has been saved !");
        $output['problemId'] = $problem->get('problemId');
        echo json_encode($output);
        //send notifications to users if there's a new problem in the community
        $problemDetails = $problem->data();
        $_db            = db::getInstance();
        $_db->query(
        "SELECT u.userId
        from  notif_subscription as ns
        inner join user as u on u.userId = ns.userId
        inner join address as a on u.latitude = a.latitude and u.longitude = a.longitude
        where a.locality = ? and ns.notificationTypeId = 5 group by u.userId order by NULL",
          array($problemDetails->city)
        );
        //build notifications that need to be sent
        $data['userList']           = $_db->results();
        $data['statusId']           = 7;
        $data['title']              = _("A problem has been logged in your city"); //A problem has been logged in your city
        $data['notificationTypeId'] = 5;
        $data['description']        = $problemDetails->title;
        $data['link']               = 'problem-' . $problemDetails->problemId . '.html';
        notification::insertList($data);
        notification::sendPending();
        //check if user requires a badge
        badge::evaluate('newProblem');
        //create the newsItem
        newsitem::create(array('newsItemTypeId' => 3, 'problemId' => $problemDetails->problemId));
        exit();
      } else {
        echo helper::outcome(_("There's been a problem"), false); //there's been a problem
        exit();
      }
    } else {
        $output['outcome'] = false;
        $output['message'] = "";
      foreach ($validate->errors() as $error) {
        $output['message'] = $output['message'] . '<br>' . $error;
      }
      echo json_encode($output);
      exit();
    }
    break;

  case 'proposition':
    // validate the data
    $checkboxes = array_filter(
      $data,
      function ($var) {
        return (strpos($var, "facet") === 0);
      },
      ARRAY_FILTER_USE_KEY
    );
    if (empty($checkboxes)) {
      echo helper::outcome(_("You need to select at least one facet"), false); //You need to select at least one facet
      exit();
    }
    $validate   = new validate();
    $validation = $validate->check(
      $data,
      array(
        'title' => array(
        'required' => true,
        'min'      => 6,
        'max'      => 200),
      )
    );

    if ($validation->passed()) {
      //if it has been added by a community, prepare the link and the comment
      if (isset($data['support'])) {
        $comment = $data['comment'];
      }
      //If the proposition has been logged as implemented
      if (isset($data['implemented'])) {
        $data['implemented'] = 1;
      } else {
        $data['implemented'] = 0;
      }
      unset($data['type'], $data['comment']);
      //prepare the pertinence to create
      $facetIds    = "";
      $pertinences = array();
      foreach ($checkboxes as $checkbox => $value) {
          $new             = array();
          $new['facetId']  = substr($checkbox, 5);
          $new['positive'] = 1;
          $facetIds .= $new['facetId'];
          end($checkboxes);
        if ($checkbox === key($checkboxes)) {
        } else {
          $facetIds .= ",";
        }
        array_push($pertinences, $new);
      }
      //Verify if the proposition already exists
      if (helper::propositionExists($data['solutionId'], $facetIds)) {
        echo helper::outcome(_("This problem and that solution have already been linked."), false); //This solution and that problem have already been linked.
        exit();
      }
      //create the proposition
      $proposition = new proposition();
      $message     = $proposition->create($data);
      if (isset($comment)) {
        $proposition->addCommunityLink(session::get('communityAdmin'), $comment);
      }
      //create the pertinences
      foreach ($pertinences as $pertinence) {
        $proposition->addPertinence($pertinence);
      }
      //create the newsItem
      newsitem::create(array('newsItemTypeId' => 1, 'propositionId' => $message));
      //send the notifications to users who voted for facets
      $_db = db::getInstance();
      $_db->query("SELECT userId from  vote  where facetId in (" . $facetIds . ")");
      //Build the notifications that need to be sent
      $notifications['userList']           = $_db->results();
      $notifications['statusId']           = 7;
      $notifications['title']              = _("A solution has been added to one of the problems you voted on"); //A solution has been added to one of the problems you voted on
      $notifications['notificationTypeId'] = 1;
      $notifications['description']        = $data['title'];
      $notifications['link']               = 'solution.php?id=' . $data['solutionId'];
      $text                                = notification::insertList($notifications);
      //Send the notifications to users who want to know if a solution has been added to a problem in your community
      $_db->query(
        "SELECT distinct(ns.userId)
          from notif_subscription as ns
          inner join communityproblem as cp on cp.communityId = ns.entityId
          inner join facet as f on f.problemId = cp.problemId
          where f.facetId in (" . $facetIds . ") and ns.notificationTypeId = 5"
      );
      //Build the notifications that need to be sent
      $notifications['userList']           = $_db->results();
      $notifications['title']              = _("There's a new solution for your community"); //There is a new solution for your community
      $notifications['notificationTypeId'] = 5;
      $text                                = notification::insertList($notifications);
      $output['outcome']                   = true;
      $output['url']                       = "problem-" . session::get('problemId') . ".html";
      echo json_encode($output);
      notification::sendPending();
      exit();
    } else {
      $message = "";
      foreach ($validate->errors() as $error) {
        $message = $message . '<br>' . $error;
      }
      echo helper::outcome($message, false);
      exit();
    }
    break;

  case 'improveProposition':
    // validate the data
    $checkboxes = array_filter(
    $data,
    function ($var) {
      return (strpos($var, "facet") === 0);
    },
    ARRAY_FILTER_USE_KEY
    );
    if (empty($checkboxes)) {
      $message = _("You need to select at least one facet"); //You need to select at least one facet
      echo helper::outcome($message, false);
      exit();
    }
    if ($validation->passed()) {
      unset($data['type']);
      //catch the proposition
      $proposition        = new proposition($data['propositionId']);
      $propositionDetails = $proposition->data();
      //create the pertinences
      $facetIds = "";
      foreach ($checkboxes as $checkbox => $value) {
        $new             = array();
        $new['facetId']  = substr($checkbox, 5);
        $new['positive'] = 1;
        $proposition->addPertinence($new);
        $facetIds .= $new['facetId'];
        if (!next($checkboxes)) {
        } else {
          $facetIds .= ",";
        }
      }
      //Send the notifications to users who voted for the facets
      $_db = db::getInstance();
      $_db->query("SELECT userId from  vote  where facetId in (" . $facetIds . ")");
      //build what needs to be sent as notifications
      $data['userList']           = $_db->results();
      $data['statusId']           = 7;
      $data['title']              = _("A problem has been logged in your city"); //A solution has been added to one of the problems you voted on
      $data['notificationTypeId'] = 1;
      $data['description']        = $propositionDetails->title;
      $data['link']               = 'solution.php?id=' . $propositionDetails->solutionId;
      notification::insertList($data);
      notification::sendPending();
      //create the newsItem
      newsitem::create(array('newsItemTypeId' => 1, 'propositionId' => $id));
      echo helper::outcome($data['propositionId'], true);
      exit();
    } else {
      $message = "";
      foreach ($validate->errors() as $error) {
        $message = $message . '<br>' . $error;
      }
      echo helper::outcome($message, false);
      exit();
    }
    break;

  case 'newSolution':
    // validate the data
    $validate   = new validate();
    $validation = $validate->check(
      $data,
      array(
        'title'         => array(
        'required' => true,
        'min'      => 6,
        'max'      => 200),
        'description'   => array(
        'required' => true,
        'max'      => 600),
        'solutionTags'  => array(
        'required' => true),
        'avantages'     => array(
        'required' => true,
        'max'      => 600),
        'inconvenients' => array(
        'required' => true,
        'max'      => 600),
      )
    );

    if ($validation->passed()) {
      unset($data['type']);
      $solution = new solution();
      if (isset($data['singleProblem']) && $data['singleProblem'] === 'on') {
        unset($data['singleProblem']);
        $data['oneOff'] = 1;
      }
      $id                = $solution->create($data);
      $output['outcome'] = true;
      //create the newsItem
      newsitem::create(array('newsItemTypeId' => 7, 'solutionId' => $id));
      if (session::exists('problemId')) {
        $output['url'] = 'addproposition.php';
        $output['id']  = $id;
      } else {
        $output['url'] = false;
        $output['id']  = $id;
      }
      echo json_encode($output);
      exit();
    } else {
      $message = "";
      foreach ($validate->errors() as $error) {
        $message = $message . '<br>' . $error;
      }
      echo helper::outcome($message, false);
      exit();
    }
    break;

  case 'invite':
    // Check if emails are valid
    $emails = explode(";", $data['emails']);
    $errors = "";
    foreach ($emails as $email) {
      if (!helper::check_email_address($email)) {
        $errors .= $email . ";";
      }
    }
    //if all email adresses are valid then move forward
    if (empty($errors)) {
      //check if people are already in the newsletter list or in the user list, if they are don't send anything
      $newPeople = array_diff($emails, helper::checkEmails($emails));
      if (!empty($newPeople)) {
        $email = new email();
        badge::createUnique(3, session::get('user'), session::get('user'));
        foreach ($newPeople as $newPerson) {
          if (session::exists('communityName')) {
            if ($email->inviteCommunity($newPerson)) {
              notification::newsletter($newPerson, 0);
            }
          } else {
            if ($email->invite($newPerson)) {
              notification::newsletter($newPerson, 0);
            }
          }
        }
        echo helper::outcome(_("Invitations have been sent to the following addresses :") . " " . implode(";", $newPeople), true); //Invitations have been sent to the following addresses
      } else {
        echo helper::outcome(_("All the people mentionned have already received an invitation"), false); //All the people mentionned have already received an invitation
      }
    } else {
      echo helper::outcome(_("Please enter the following addresses in the right format : ") . $errors, false); //Please enter the following addresses in the right format :
    }
    break;

  case 'propositions':
    // validate the data
    $proposition = new proposition();
    $facetId     = $data['facetId'];
    //Check that the user has voted for the facet
    $problem = new problem();
    if ($problem->hasVoted($facetId)) {
      // if the slider is off, it's a new pertinence, we add one
      $newOnes = array_filter(
        $data,
        function ($key) {
          return strpos($key, 'new') !== false;
        },
        ARRAY_FILTER_USE_KEY
      );
      foreach ($newOnes as $key => $value) {
        if (!empty($value)) {
          $newPertinence['propositionId'] = str_replace('new', '', $key);
          $newPertinence['facetId']       = $facetId;
          $newPertinence['reason']        = $value;
          $newPertinence['positive']      = 0;
          $newPertinence['id']            = $proposition->addPertinence($newPertinence);
          // Then add the vote
          $proposition->addPertinenceVote($newPertinence['id']);
        } else {
          continue;
        }
      }
      //if it's not the new one just add the vote
      $existingOnes = array_filter(
        $data,
        function ($key) {
          return strpos($key, 'prop') !== false;
        },
        ARRAY_FILTER_USE_KEY
      );
      foreach ($existingOnes as $key => $value) {
        $proposition->addPertinenceVote($value);
      }
      //if the slider is on, add a pertinence vote to the right solution for the pertinence
      $positives = array_filter(
        $data,
        function ($key) {
          return $key === 'on';
        }
      );

      foreach ($positives as $key => $value) {
        $pertinenceId = str_replace('clauseSlide', '', $key);
        $proposition->addPertinenceVote($pertinenceId);
      }
      echo helper::outcome(_("Thanks for your help !"), true); //thank you for your help
      exit();
    }
    break;

  case 'vote':
    if (!$isHelogged->checkRole('verified')) {
      echo helper::outcome(_("Please verify your account to be able to do that : Click on the link on the validation email or ask for a new one by clicking on the button below"), false); //Please verify your account to be able to vote : Click on your name on the right, then click on settings.
      exit();
    }
    if (input::defined('facetId')) {
      $problem = new problem($data['problemId']);
      if ($data['facetId'] === 'on') {
        if (input::defined('newText1') && !empty($data['newText1'])) {
          $newFacet = array('description' => $data['newText1'], 'problemId' => $data['problemId']);
          $facetId  = $problem->addFacet($newFacet);
        } else {
          echo helper::outcome(_("Please define the need that isn't satisfied"), false); //Please define how you are impacted by this issue
          exit();
        }
      } else {
        $facetId = $data['facetId'];
      }
      $vote = new vote();
      if (!$vote->alreadyVoted($data['problemId'])) {
        if (!$vote->create($facetId)) {
          echo helper::outcome(_("Oups, sorry there was a problem processing your vote"), false); //Oups, sorry there was a problem processing your vote
          exit();
        }
      } else {
        echo helper::outcome(_("You have already voted for this problem"), false); //You have already voted for this problem
        exit();
      }
      badge::evaluate('newProblemVote');
      //check if there are any solutions that need to be voted on
      $output['props']     = $problem->countPropsToEvaluate();
      $output['problemId'] = $data['problemId'];
      $output['outcome']   = true;
      $output['message']   = _("Thanks for your help !"); //Congratulations ! You have voted for a problem !
      echo json_encode($output);
    } else {
      echo helper::outcome(_("Oups, sorry there was a problem processing your vote"), false); //Oups, sorry there was a problem processing your vote
      exit();
    }
    break;

  case 'newUser':
    // if something has been submitted, do all the checks
    $validate   = new validate();
    $validation = $validate->check(
      $data,
      array(
        'userName'      => array(
        'required' => false,
        'min'      => 2,
        'max'      => 20,
        'unique'   => 'user'),
        'password'      => array(
        'required' => false,
        'min'      => 6),
        'captcha'       => array(
        'captcha'  => true,
        'required' => true,
        'min'      => 6),
        'passwordCheck' => array(
        'required' => false,
        'matches'  => 'password'),
        'email'         => array(
        'required'    => false,
        'valid_email' => true,
        'unique'      => 'user'),
      )
    );
    if ($validation->passed()) {
      if (isset($data['newsletter']) && $data['newsletter'] === 'on') {
        notification::newsletter($data['email'], 1);
      }
        unset($data['captcha'], $data['type'], $data['newsletter']);
        $user    = new user();
        $message = $user->create($data);
      if ($message) {
          $user->login($data['email'], $data['password']);
          $email = new email();
          $email->sendValidationEmail($data['email'], $data['userName']);
          echo helper::outcome(_("You will receive a validation email, please click on the link to validate your account. If you didn't get anything, please check your spam folder, and if the email isn't there, please contact us."), true); //Vous allez recevoir un email de validation, merci de cliquer sur le lien qu'il contient
          exit();
      } else {
          echo helper::outcome(_("There's been a problem"), false); //there's been a problem
          exit();
      }
    } else {
        $message = "";
      foreach ($validate->errors() as $error) {
        $message = $message . '<br>' . $error;
      }
      echo helper::outcome($message, false);
      exit();
    }
    break;

  case 'usercomplete':
    $user = new user();
    // did we get the coordinates from the form ?
    if (input::defined('coord')) {
      //process the coordinates
      $coordinates          = explode(',', trim($data['coord'], '()'));
      $address              = (array) json_decode($data['address']);
      $address['latitude']  = $coordinates[0];
      $address['longitude'] = $coordinates[1];

      $city = address::create($address);
      //subscribe to notifications

      $communityId = $user->findCommunityId($city);
      if (input::defined('myTown')) {
        notification::subscribe($user->data()->userId, 6, $communityId);
      } else {
        notification::unsubscribe($user->data()->userId, 6, $communityId);
      }
      if (input::defined('newProblems')) {
        notification::subscribe($user->data()->userId, 5, $communityId);
      } else {
        notification::unsubscribe($user->data()->userId, 5, $communityId);
      }
      //update user with the phone number, the activation code and the coordinates and add coordinates to the session
      if ($user->update(array('latitude' => $coordinates[0], 'longitude' => $coordinates[1], 'communityId' => $communityId, 'statusId' => 1))) {
        $user->addUserRole(session::get('user'), 'verified');
        badge::createUnique(3, session::get('user'), session::get('user'));
        //create the newsItem
        newsitem::create(array('newsItemTypeId' => 6, 'userId' => $user->data()->userId));
        session::put('latitude', $coordinates[0]);
        session::put('longitude', $coordinates[1]);
        $_SESSION['userInfo']->statusId = 1;
      }
      $output['outcome'] = true;
      $output['message'] = _("Your account is now active !"); //your account is now active
      $output['url']     = 'homepage.php';
      echo json_encode($output);
      exit();
    } else {
      echo helper::outcome(_("Please enter an address or click on the map"), false); //Please enter an address or click on the map
      exit();
    }
    break;

  case 'resendValidation':
    $email = new email();
    $email->sendValidationEmail($isHelogged->get('email'), $isHelogged->get('userName'));
    echo helper::outcome(_("You will receive a validation email, please click on the link to validate your account. If you didn't get anything, please check your spam folder, and if the email isn't there, please contact us."), true);
    break;

  case 'newCommunity':
    // validate the data
    if (!isset($data['terms_of_use'])) {
      echo helper::outcome(_("Please check that you have read the terms of use"), false); //please check that you have read the terms of use
    } else {
      unset($data['terms_of_use'], $data['type']);
      $community = new community();
      $result    = $community->create($data);
      if ($result === 'created') {
        echo helper::outcome($data['name'] . ' ' . _("has been created and will be activated once the payment has come through"), true); //has been created and will be activated when the payment has come through
      } elseif ($result === 'problem') {
        echo helper::outcome($data['name'] . ' ' . _("couldn't be created, a problem has occured..."), false); // couldn't be created, a problem has occured
      } elseif ($result === 'nameNotAvailable') {
        echo helper::outcome($data['name'] . ' ' . _("is already taken"), false); // couldn't be created, a problem has occured
      }
    }
    break;

  case 'solutionVote':
    $solution = new solution(input::get('solutionId'));
    if ($solution->addVote(helper::test_input(input::get('value')), helper::test_input(input::get('comment')))) {
      echo helper::outcome(_("Thank you for your vote !"), true);
    } else {
      echo helper::outcome(_("Oups, sorry there was a problem processing your vote"), false);
    }
    break;

  case 'getCommunityUrl':
    $community = new community();
    $community->find('name(departmentId)', $data['name']);
    $output['outcome'] = true;
    $output['url']     = 'communityprofile.php?communityId=' . $community->get('communityId');
    echo json_encode($output);
    break;

  case 'spam':
    // validate the data
    $report  = new spam();
    $message = $report->report($data['entity'], $data['entityId'], $data['comment']);
    echo helper::outcome($message, true);
    break;

  case 'manageAdmins':
    $community = new community($data['communityId']);
    //Take out the type and the communityId from the array
    unset($data['type'], $data['communityId']);
    //remove duplicates
    $data = array_unique($data);
    //for each email, add the admin and log the results
    $results = "";
    if (count($data) > 0) {
      foreach ($data as $type => $email) {
        $results .= "</br>" . $community->addAdmin($email);
      }
    } else {
      $results = _("No new admin has been added"); //no new admin has been added
    }
    echo helper::outcome($results, true);
    break;

  case 'contactUsers':
    $community = new community($data['communityId']);
    //get the list of subscribers who accept to be contacted
    $list = $community->getUserContactList($data['list']);
    //create the notification for each
    $data['userList']           = $list;
    $data['statusId']           = 7;
    $data['title']              = _("A community you subscribed to sent you a message"); //A community you subscribed to sent you a message
    $data['notificationTypeId'] = 7;
    $data['description']        = $data['text'];
    $data['link']               = 'communityprofile.php?communityId=' . $data['communityId'];
    notification::insertList($data);
    echo helper::outcome(_("Your message has been sent !"), true); //Your message has been sent !
    break;

  case 'contactProblemUsers':
    $problem = new problem($data['problemId']);
    //take the list of subscribers who accept to be contacted
    $list = $problem->getUserContactList();
    //create the notification for each
    $data['userList']           = $list;
    $data['statusId']           = 7;
    $data['title']              = _("A community you subscribed to sent you a message"); //A community you subscribed to sent you a message
    $data['notificationTypeId'] = 9;
    $data['description']        = $data['text'];
    $data['link']               = 'problem.php?problemId=' . $data['problemId'];
    notification::insertList($data);
    echo helper::outcome(_("Your message has been sent !"), true); //Your message has been sent !
    break;

  case 'notMyProblem':
    $community = new community();
    $community->find('name(departmentId)', $data['name']);
    if ($community->get('communityId') === session::get('communityAdmin')) {
      echo helper::outcome(_("You can't delegate problems to yourself (you're risking a burnout...) "), false); //You can't delegate problems to yourself
      exit();
    }
    $problem = new problem($data['problemId']);
    $problem->addCommunityLink($community->get('communityId'));
    //create the newsItem
    newsitem::create(array('newsItemTypeId' => 4, 'problemId' => $data['problemId']));
    echo helper::outcome(_("It's not your problem anymore !"), true); //It's not your problem anymore !
    break;

  case 'supportProposition':
    //take the id of the logged community
    $proposition = new proposition($data['propositionId']);
    $proposition->addCommunityLink(session::get('communityAdmin'), $data['comment']);
    //create the newsItem
    newsitem::create(array('newsItemTypeId' => 9, 'propositionId' => $data['propositionId']));
    //check if the author deserves a badge
    badge::evaluate('propositionSupport', $data['propositionId']);
    echo helper::outcome(_("Your support has been recorded"), true); //your support has been recorded
    break;

  case 'contactEmail':
    $validate   = new validate();
    $validation = $validate->check(
      $data,
      array(
        'captcha'   => array(
        'captcha'  => true,
        'required' => true,
        'min'      => 6),
        'email'     => array(
        'required'    => true,
        'valid_email' => true),
        'emailText' => array(
        'required' => true),
      )
    );
    if ($validation->passed()) {
      $email = new email();
      $email->sendContactEmail(helper::test_input($data["name"]), helper::test_input($data["email"]), helper::test_input($data["emailText"]));
      echo helper::outcome(_("Thanks for the email! We'll be in touch shortly!"), true); //Thank you for your message !
    } else {
      $message = "";
      foreach ($validate->errors() as $error) {
        $message = $message . '<br>' . $error;
      }
      echo helper::outcome($message, false);
      exit();
    }
    break;

  default:
    echo helper::outcome(_("There's been a problem"), false); //There's been a problem
    break;
}
