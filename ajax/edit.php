<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';

$user = new user();
$data = input::get('full_array');
// echo helper::outcome($data,FALSE);//uncomment this to test the data received on the page.
// exit();
if (!$user->isLoggedIn()) {
  echo helper::outcome(_("Hello ! You need to login to do that"), false);
  return;
}

$data = input::get('full_array');
// input : data + type of data
switch ($data['type']) {
  case 'editUser':
    // validate the data
    if (!$user->checkPassword($data['oldPassword'])) {
      echo helper::outcome(_("Please fill in the password field properly to validate your modifications"), false); //The current password is incorrect
      exit();
    }
    if (isset($data['delete']) && $data['delete'] === 'on') {
      if ($user->delete()) {
        echo helper::outcome('delete', true);
        exit();
      } else {
        echo helper::outcome(_("There's been a problem"), false);
        exit();
      }
    }
    $validate   = new validate();
    $validation = $validate->check(
      $_POST,
      array(
      'userName'              => array(
      'required' => false,
      'min'      => 2,
      'max'      => 20,
      'unique'   => 'user'),
        'oldPassword'           => array(
        'required' => true,
        'min'      => 6),
        'newPassword'           => array(
        'required' => false,
        'min'      => 6),
        'settingsPasswordCheck' => array(
        'required' => false,
        'matches'  => 'newPassword'),
        'email'                 => array(
        'required'    => false,
        'valid_email' => true,
        'unique'      => 'user'),
      )
    );
    if ($validation->passed()) {
      $updateData = array();
      if (!empty($data['userName'])) {
        $updateData['userName'] = $data['userName'];
      }
      if (!empty($data['coord'])) {
        $coordinates             = explode(',', trim($data['coord'], '()'));
        $updateData['latitude']  = $coordinates[0];
        $updateData['longitude'] = $coordinates[1];
        $address                 = (array) json_decode($data['address']);
        $address['latitude']     = $coordinates[0];
        $address['longitude']    = $coordinates[1];
        address::create($address);
      }
      if (!empty($data['newPassword'])) {
        $updateData['password'] = md5(helper::test_input($data['newPassword']) . config::get('salt'));
      }
      if (!empty($data['email'])) {
        $updateData['email'] = $data['email'];
      }

      try {
        $result = $user->update($updateData);
      } catch (Exception $e) {
        echo helper::outcome(_('oh oh problem...'), false);
        exit();
      }
      if (!empty($data['email'])) {
        $email = new email();
        if ($email->sendValidationEmail($data['email'], $user->get('userName'))) {
          $user->refreshSession();
          echo helper::outcome(_("You will receive a validation email shortly.If you didn't get anything, please check your spam folder, and if the email isn't there, please contact us."), true); //You will receive shortly a validation email for your account, please click on the provided link.
          exit();
        } else {
          echo helper::outcome($email->error, false);
          exit();
        }
      } else {
        $user->refreshSession();
        echo helper::outcome(_("Your modifications have been saved"), true); //Your modifications have been saved.
        exit();
      }
    } else {
      $output = "";
      foreach ($validate->errors() as $error) {
        $output = $output . '<br>' . $error;
      }
      echo helper::outcome($output, false);
      exit();
    }
break;
  default:
    echo helper::outcome(json_encode($data), true);
break;
}
