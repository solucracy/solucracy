<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';
//Check if person is logged
$isHelogged = new user();
if (!$isHelogged->isLoggedIn()) {
  echo helper::outcome(_("Hello ! You need to login to do that"), false);
  return;
}
//Is the checkbox checked
if (input::defined('cancel') && input::get('cancel') === 'on') {
  //Check if the person is acommunity admin
  $community = new community(session::get('communityAdmin'));
  if ($community->isAdmin(session::get('user'))) {
    //Prepare the code to send
    //Log code on the community field and deactivate account
    if ($community->deactivate()) {
      echo helper::outcome(_("We're sad to see you go but will always be there if you change your mind !"), true);
      return;
    }
  } else {
    echo helper::outcome(_("You don't have the required privileges to do this."), false);
  }
} else {
  echo helper::outcome(_("Please check the box to confirm your action"), false);
  return;
}
