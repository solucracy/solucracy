<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';
//Check if user is logged
$isHelogged = new user();
if (!$isHelogged->isLoggedIn()) {
  echo helper::outcome(_("Hello ! You need to login to do that"), false);
  return;
}
if (!$isHelogged->checkRole('moderator')) {
  echo helper::outcome(_("You don't have the required privileges to do this."), false);
  return;
}
$data = input::get('full_array');

$spam    = new spam();
$message = $spam->process($data['flaggedBy'], $data['spamId'], $data['outcome'], $data['entity'], $data['entityId']);
echo helper::outcome(_("This spam has been processed !"), true); //This spam has been processed !
exit();
