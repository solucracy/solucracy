<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';

//define order of problems
switch (input::get('sort')) {
case '1':
  $sort = 'ORDER BY nbproblems desc';
  break;
case '2':
  $sort = 'ORDER BY s.categoryId desc';
  break;
case '3':
  $sort = 'ORDER BY s.createdOn desc';
  break;
case '4':
  $sort = 'ORDER BY u.reputation desc';
  break;
default:
  $sort = 'ORDER BY Null';
  break;
}

if (input::defined('search')) {
  $searchTerms = '(MATCH (s.title,s.description) AGAINST ("' . helper::test_input(input::get('search')) . '")OR MATCH (t.name) AGAINST ("' . helper::test_input(input::get('search')) . '")) AND ';
} else {
  $searchTerms = "";
}
if (input::defined('problemId')) {
  $problemFilter = ' s.solutionId not in (SELECT pr.solutionId FROM `problem` as p inner join facet as f on f.problemId = p.problemId inner join pertinence as pe on pe.facetId = f.facetId inner join proposition as pr on pe.propositionId = pr.propositionId WHERE pe.positive = 1 and p.problemId = ' . helper::test_input(input::get('problemId')) . ' group by pr.solutionId) AND ';
} else {
  $problemFilter = "";
}
if (input::get('cat') != "") {
  $processedcat = " (s.categoryId IN (" . input::get('cat') . "))";
} else {
  $processedcat = " 1=1";
}

$sql = 'SELECT s.solutionId as itemId, s.title,s.userId, u.userName, s.categoryId as category, c.icon, (SELECT COUNT(DISTINCT p.propositionId)) AS nbItems, DATE_FORMAT(s.createdOn,"%d/%m/%Y") AS age, s.description
  FROM solution AS s
  INNER JOIN user AS u ON s.userId = u.userId
  LEFT JOIN proposition as p ON p.solutionId = s.solutionId AND p.statusId != 5
  LEFT JOIN solution_tag as st on s.solutionId = st.solutionId
  LEFT JOIN tag as t on t.tagId = st.tagId
  LEFT JOIN category as c on c.categoryId = s.categoryId
  WHERE s.statusId !=5 AND';
$sql = $sql . $problemFilter;
$sql = $sql . $searchTerms;
$sql = $sql . $processedcat;
$sql = $sql . ' GROUP BY s.solutionId ' . $sort;

$db = db::getInstance();
$db->query($sql);
if ($db->count() >= 0) {
  echo helper::outcome($db->results(), true);
  exit();
} else {
  helper::errorLog($db->debug());
  echo helper::outcome(_("Oups ! We were not able to access the database"), false);
  exit();
}
