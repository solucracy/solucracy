<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';
$isHelogged = new user();
if (!$isHelogged->isLoggedIn()) {
  echo helper::outcome(_("Hello ! You need to login to do that"), false);
}
$data               = input::get('full_array');
$userId             = session::get('user');
$notificationTypeId = $data['notificationTypeId'];
$action             = $data['action'];

if (isset($notificationTypeId)) {

  switch ($action) {
  case 'true':
    notification::subscribe($userId, $notificationTypeId, $data['entityId']);
    $message = _("You just subscribed to a notification"); //You just subscribed to a notification
    break;
  case 'false':
    notification::unsubscribe($userId, $notificationTypeId, $data['entityId']);
    $message = _("You just unsubscribed from this type of notification"); //You just unsubscribed from this type of notification
    break;

  default:
    echo helper::outcome(_("There's been a problem"), false); //there was a problem
    exit();
  }
} else {
  echo helper::outcome(_("There's been a problem"), false); //there was a problem
  exit();
}

echo helper::outcome($message, true);
