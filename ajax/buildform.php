<?php
require '../core/ini.php';
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/

//using the connected userId if there's one
$user = new user();
//Getting data to build the form
$data = input::get('full_array');
// helper::outcome($data,FALSE);
// exit();
//Log history to know which popup was loaded
helper::logHistory($data['type']);
$formContent = '';
//Which type of form is it ?
//If it requires the user to be logged in, check if he is
//if not, send the login form
if (in_array($data['type'], array('vote', 'newProblem', 'newProposition', 'newSolution', 'spam', 'votePropositions', 'login', 'invite', 'moderation', 'solutionVote', 'userSettings', 'newCommunity', 'cancelSubscription', 'communityChoice', 'manageAdmins', 'contactUsers', 'contactProblemUsers', 'communitySettings', 'notMyProblem', 'supportProposition', 'selectSolution', 'userComplete'))) {
  if (!$user->isLoggedIn()) {
    $data['type'] = 'notConnected';
  }
  //If it's the moderation page and they don't have the right role, prevent access
  if ($data['type'] === 'moderation') {
    if (!$user->checkRole('moderator')) {
      helper::outcome(_("You don't have access to this page"), false);
      exit();
    }
  }
  if (session::exists('userInfo') && session::get('userInfo')->statusId != 1 && $data['type'] != 'userComplete') {
    $data['type'] = 'notComplete';
  }
}
$form = new form();
//Prepare variables if something need to be added before or after the form
$before = "";
$after  = "";
$header = "";
$footer = "";
//Based on type, prepare the data
switch ($data['type']) {
case 'setNewPassword':
  $db = db::getInstance();
  if (input::defined('code')) {
    $result = $db->query(
      'SELECT * FROM user
    WHERE passwordRequestId = ? AND requestTime < TIME( DATE_SUB( NOW( ) , INTERVAL 24 HOUR ) )',
    array(input::get('code'))
      );

    if (!$result->count() > 0) {
        $title       = _("Your request has expired"); //your request has expired
        $formId      = '';
        $onclick     = "$('#formModal').modal('hide')";
        $submitValue = _("Close");
        $fields      = '';
    } else {
        $title  = _("Change the password");
        $formId = 'newPasswordForm';

        $onclick     = "processForm('newPasswordForm','url')";
        $submitValue = _("Send");
        $fields      = '';
        $fields .= $form->createField('hidden', 'requestId', '', '', input::get('code'));
        $fields .= $form->createField('hidden', 'type', '', '', 'setNewPassword');
        $fields .= $form->createField('password', 'password', _("Password"), _("Password"));
        $fields .= $form->createField('password', 'passwordCheck', _("Password check"), _("Password check"));
    }
  }
    break;
case 'notComplete':
  $title       = _("Please verify your account to be able to do that : Click on the link on the validation email or ask for a new one by clicking on the button below");
  $formId      = '';
  $onclick     = "resendEmail()";
  $submitValue = _("Resend Validation Email");
  $fields      = '';
    break;
case 'vote':
  $title   = _("Which need isn't fulfilled ?");
  $formId  = "newVoteForm";
  $problem = new problem($data['problemId']);
  $facets  = $problem->getFacets();
  $fields  = '';
  foreach ($facets as $facet) {
    $fields .= '<div class="row"><input id="' . $facet->facetId . '" name="facetId" type="radio" value="' . $facet->facetId . '"><label for="' . $facet->facetId . '">' . $facet->description . '</label></div>';
  }
  $fields .= '<div class="row"><input id="new" name="facetId" type="radio"><label for="new"><input id="newText1" name="newText1" type="text" placeholder="' . _("Add a new reason") . '" maxlength="140"></label></div>'; //add another reason
  $fields .= $form->createField('swap', 'newSolution', _("I would like to be informed of new solutions for this problem"), '', 'data-notificationtypeid="1" data-entityid="' . $data['problemId'] . '" onclick="subscribe(this)"'); // I would like to be informed of new solutions for this problem
  $fields .= ' <input type="hidden" name="type" id="type" value="vote"/><input type="hidden" name="problemId" id="problemId" value="' . $data['problemId'] . '"/>';
  $submitValue = _("Vote");
  $onclick     = 'ProcessVote();return false;';
    break;
case 'forgotPassword':
  $title       = _("Please enter your email address and we'll send you a password reset email ");
  $formId      = 'passwordRequestForm';
  $onclick     = "processForm('passwordRequestForm','showAndReload')";
  $submitValue = _("Send");
  $fields      = '';
  $fields .= $form->createField('email', 'email', _("Email"), '');
  $fields .= $form->createField('hidden', 'type', '', '', 'passwordRequest');
  $fields .= $form->createField('captcha', 'captcha', _("Are you a really smart algorithm ?"), _("Please copy here the text in the image"));
    break;
case 'selectSolution':
  $formContent .= '<div class="modal-header">
            <h3 class="modal-title text-center">' . _("What would you like to do") . ' ?</h3><!-- What would you like to do -->
            <button id="modalClose" type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
          <div class="modal-body d-flex justify-content-around"><a href="solutions.php?problemId=' . $data['problemId'] . '" class="solucracy_btn padding5">' . _("Find an existing solution") . '</a><!-- Find an existing solution --><a href="#" class="solucracy_btn padding5" onclick="ajax(\'buildform.php\',{type:\'newSolution\',problemId:\'' . $data['problemId'] . '\'},\'form\')">' . _("Add a solution") . '</a><!-- Add a solution --></div>';
  echo helper::outcome($formContent, true);
    exit();
  break;
case 'userComplete':
  $user   = new user();
  $check  = false;
  $formId = "userCompleteForm";
  $fields = '';
  if ($user->get('statusId') == 1) {
    $title       = _("Your account has been activated and your email has been confirmed !"); //Your account has already been activated
    $submitValue = _("Back to homepage"); //back to homepage
    $onclick     = "window.location.replace('homepage.php');";
  } else {
    // if they are check the salted email against the link
    if (md5($user->get('email') . config::get('salt')) === input::get("code")) {
      //if it's ok, display the form for the rest
      $title   = _("Complete your profile"); // Complete your profile
      $check   = true;
      $onclick = "processForm('userCompleteForm','showAndRedirect')";
      $fields  = '<div class="row"><div class="col-6 col-lg-6">';
      $fields .= $form->createField('hidden', 'type', '', '', 'usercomplete');
      $fields .= $form->createField('hidden', 'address', '', '');
      $fields .= $form->createField('hidden', 'coord', '', '');
      $fields .= $form->createField('text', 'location', _("Location"), _("Search"));
      $fields .= $form->createField('link', 'why', _("Why is this important ?"), _("Why is this important ?"), 'faq' . session::get('language') . '.php#why');
      $fields .= $form->createUnvalidatedField('swap', 'newProblems', _("I want to be notified of new problems in my town"), '', 'data-entityid="0" data-notificationtypeid="5" onclick="subscribe(this)"');
      $fields .= '</div><div id="mapContainerEdit" class="col-6 col-lg-6" style="min-height: 400px;"><div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="' . config::get('GMap_API_Key') . '" width="378px" height="400px" ></div></div></div>';
      $submitValue = _("Participate");
    } else {
      //if it's not, let them know that they have nothing to do here and send them back to the index
      $title       = _("The validation link doesn't point to an existing account, please try clicking on the link again."); //The validation link doesn't point to an existing....
      $submitValue = _("Back to homepage"); //back to homepage
      $onclick     = "window.location.replace('homepage.php');";
    }
  }
    break;
case 'spam':
  $title  = _("I think this problem should be deactivated because :"); //I think it should be deactivated because
  $formId = "spam";
  $fields = '';
  $fields .= $form->createField('text', 'comment', _("Post your comment here"), _("Post your comment here"));
  $fields .= $form->createField('hidden', 'entity', '', '', $data['entityType']);
  $fields .= $form->createField('hidden', 'entityId', '', '', $data['entityId']);
  $fields .= $form->createField('hidden', 'type', '', '', 'spam');
  $submitValue = _("Send");
  $onclick     = "processForm('spam','showAndReload')";
    break;
  case 'votePropositions':
    $title        = _("Here are the proposals that can solve your side of the problem. Do you think they are pertinent ?");
    $formId       = "propositionsForm";
    $problem      = new problem($data['problemId']);
    $propositions = $problem->getUserRelevantPropositions();
    $fields       = '';
    foreach ($propositions as $prop) {
      //get all the positive pertinences
      $a = array_filter(
      $prop,
      function ($var) {
        return $var->positive === '1';
      }
      );
      $positive = array_shift($a);
      $facetId  = $positive->facetId;
      $fields .= $form->createField('swap', 'clauseSlide' . $positive->pertinenceId, $positive->title, $positive->title, 'checked');
      $fields .= '<div id="props';
      $fields .= $positive->pertinenceId;
      $fields .= '" style="display: none;" class="offset-2"><p>' . _("Why")
      . ' ?</p>';
      foreach ($prop as $pertinence) {
        if ($pertinence->positive === '1') {
          continue;
        }
        ;
        $fields .= '<div class="row margin5"><input id="per';
        $fields .= $pertinence->pertinenceId;
        $fields .= '" name="prop';
        $fields .= $pertinence->propositionId;
        $fields .= '" type="radio" value="';
        $fields .= $pertinence->pertinenceId;
        $fields .= '"><label for="per';
        $fields .= $pertinence->pertinenceId;
        $fields .= '">';
        $fields .= $pertinence->reason;
        $fields .= '</label></div>';
      }
      $fields .= '<div class="row"><input id="new';
      $fields .= $positive->propositionId;
      $fields .= '" name="prop';
      $fields .= $positive->propositionId;
      $fields .= '" type="radio" class="hidden"><label for="new';
      $fields .= $positive->propositionId;
      $fields .= '"><input id="newText';
      $fields .= $positive->propositionId;
      $fields .= '" name="new';
      $fields .= $positive->propositionId;
      $fields .= '" type="text" placeholder="' . _("Add a new reason") . '" maxlength="140"></label></div></div>';
    }
    $fields .= $form->createField('hidden', 'facetId', '', '', $facetId);
    $fields .= $form->createField('hidden', 'type', '', '', 'propositions');
    $submitValue = _("Send");
    $onclick     = 'processProps();return false;';
break;
  case 'notConnected':
    $title  = _("Login"); //Connexion
    $formId = "connect";
    $fields = '';
    $fields .= $form->createField('hidden', 'action', '', '', 'login');
    $fields .= $form->createField('email', 'email', _("Email"), _("Email"));
    $fields .= $form->createField('password', 'password', _("Password"), _("Password"));
    $fields .= '<a name="requestPassword" id="requestPassword" href="#" onclick="ajax(\'buildform.php\',{type:\'forgotPassword\'},\'form\')" class="font_green text-underline"><u>' . _("Forgot password") . '</u></a>';
    $fields .= '</br>';
    $fields .= '<a name="communityAccount" id="communityAccount" href="#" onclick="ajax(\'buildform.php\',{type:\'newAccount\'},\'form\')" class="font_green text-underline"><u>' . _("You don't have an account ? Register for free") . '</u></a>';
    $submitValue = _("Login");
    $onclick     = 'login()';
break;
  case 'newAccount':
    $title  = _("Participate in Solucracy"); //Participate in Solucracy
    $formId = "newUserForm";
    $fields = '';
    $fields .= $form->createField('text', 'userName', _("Login"), _("Login"));
    $fields .= $form->createField('hidden', 'type', '', '', 'newUser');
    $fields .= $form->createField('email', 'email', _("Email"), _("Email"));
    $fields .= $form->createField('password', 'password', _("Password"), _("Password"));
    $fields .= $form->createField('password', 'passwordCheck', _("Password check"), _("Password check"));
    $fields .= $form->createField('captcha', 'captcha', _("Are you a really smart algorithm ?"), _("Please copy here the text in the image"));
    $fields .= $form->createField('link', 'terms_of_use', _(" By clicking on Save, I confirm having read and accepted the terms and conditions of this website."), _(" By clicking on Save, I confirm having read and accepted the terms and conditions of this website."), 'faq' . session::get('language') . '.php#terms');
    $fields .= $form->createField('swap', 'newsletter', _("I'd like to subscribe to the newsletter"), _("I'd like to subscribe to the newsletter"));
    $submitValue = _("Save");
    $onclick     = 'processNewUser()';
break;
  case 'notifications':
    $title         = _("Unread notifications");
    $formId        = "blank";
    $notifications = notification::getUnread($user->data()->userId);
    $fields        = '<div id="notifications" class="list-group">';
    foreach ($notifications as $notif) {
      $fields .= '<a href="#" data-link="';
      $fields .= $notif->link;
      $fields .= '" id="';
      $fields .= $notif->notificationId;
      $fields .= '" class="list-group-item clickable card"><h5 class="list-group-item-heading">';
      $fields .= $notif->title;
      $fields .= '</h5><p class="list-group-item-text">';
      $fields .= $notif->description;
      $fields .= '</p><span class="badge">';
      $fields .= $notif->createdOn;
      $fields .= '</span></a>';
    }
    $fields .= '</div>';
    $submitValue = _("Close");
    $onclick     = '$("#formModal").modal("hide")';
break;
  case 'invite':
    $title  = _("Invite people on Solucracy");
    $formId = "inviteForm";
    $fields = '';
    $fields .= $form->createField('textarea', 'emails', _("Email Addresses"), _("separated by a colon"));
    $fields .= $form->createField('hidden', 'type', '', '', 'invite');
    $submitValue = _("Send invitation");
    $onclick     = "processForm('inviteForm','showAndReload')";
break;
  case 'moderation':
    $title  = _("Moderation page");
    $spam   = new spam();
    $list   = $spam->getList();
    $fields = "";
    foreach ($list as $item) {
      switch ($item->entity) {
        case 'problem':
          $entityName = _("Problem");
          $link       = config::get("base_url") . $item->entity . '.php?id=' . $item->entityId;
          break;
        case 'solution':
          $entityName = _("Solution");
          $link       = config::get("base_url") . $item->entity . '.php?' . $item->entity . 'Id=' . $item->entityId;
          break;
        case 'proposition':
          $entityName = _("Proposal");
          $link       = config::get("base_url") . $item->entity . '.php?' . $item->entity . 'Id=' . $item->entityId;
          break;

        default:
          // code...
          break;
      }
      $fields .= '<div class="row margin5 greenBorder"><div class="col-md-2">' . $entityName . '</div><div class="col-md-2">' . $item->userName . '</div><div class="col-md-2">' . $item->flaggedOn . '</div><div class="col-md-2"><a href="' . $link . '">' . $item->title . '</a></div><div class="col-md-2 p-1">' . $item->comment . '</div><div class="col-md-2"><i class="fa fa-thumbs-up font_green clickable margin5" onclick="processSpam(' . $item->flaggedBy . ',' . $item->spamId . ',1,\'' . $item->entity . '\',' . $item->entityId . ')"></i><i class="fa fa-thumbs-down font_red clickable margin5" onclick="processSpam(' . $item->flaggedBy . ',' . $item->spamId . ',0,\'' . $item->entity . '\',' . $item->entityId . ')"></i></div></div>';
    }

    $submitValue = _("Close");
    $onclick     = "$('#formModal').modal('hide')";
break;
  case 'newSolution':
    if (null !== input::get('problemId') && is_numeric(input::get('problemId'))) {
      session::put('problemId', input::get('problemId'));
    }
    $categories = helper::getCategories();
    $title      = _("Add a solution"); //Add a new solution
    $formId     = "newSolutionForm";
    $fields     = "";
    $fields     = $form->createField('text', 'title', _("Title"), _("Title"));
    $fields .= $form->createField('textarea', 'description', _("Description"), _("Description"));
    $fields .= $form->createField('text', 'solutionTags', _("Keywords"), _("Press Enter to validate each keyword"));
    $fields .= $form->createField('select', 'categoryId', _("Category"), _("Category"), $categories);
    $fields .= $form->createUnvalidatedField('swap', 'singleProblem', _("Can this solution solve other problems ?"), _("Can this solution solve other problems ?"), 'checked');
    $fields .= $form->createField('text', 'avantages', _("Upsides"), _("Upsides"));
    $fields .= $form->createField('hidden', 'type', '', '', 'newSolution');
    $fields .= $form->createField('text', 'inconvenients', _("Downsides"), _("Downsides"));
    $submitValue = _("Add");
    $onclick     = "processForm('newSolutionForm','newSolution')";
break;
  case 'solutionVote':
    if ($data['voteValue'] == 1) {
      $title = _("I'm voting this up because");
    } else {
      $title = _("I'm voting this down because");
    }

    $formId = "solutionVote";
    $fields = '';
    $fields .= $form->createField('textarea', 'comment', _("Comment"), _("Post your comment here"));
    $fields .= $form->createField('hidden', 'value', '', '', $data['voteValue']);
    $fields .= $form->createField('hidden', 'solutionId', '', '', $data['solutionId']);
    $fields .= $form->createField('hidden', 'type', '', '', 'solutionVote');
    $submitValue = _("Save");
    $onclick     = "processForm('solutionVote','showAndReload')";
break;
  case 'userSettings':
    $title            = _("Update my profile");
    $formId           = "settingsForm";
    $notifications    = notification::loadSubscriptions(session::get('user'));
    $commentReply     = (in_array(2, $notifications)) ? 'checked' : "";
    $problemVote      = (in_array(4, $notifications)) ? 'checked' : "";
    $problemComment   = (in_array(3, $notifications)) ? 'checked' : "";
    $newSolution      = (in_array(1, $notifications)) ? 'checked' : "";
    $myTown           = (in_array(5, $notifications)) ? 'checked' : "";
    $premiumCity      = (in_array(6, $notifications)) ? 'checked' : "";
    $communityContact = (in_array(10, $notifications)) ? 'checked' : "";
    $fields           = '<div class="row"><div class="col-6 col-lg-6 text-center">';
    $fields .= $form->createUnvalidatedField('text', 'userName', _("Login"), $user->data()->userName);
    $fields .= $form->createField('hidden', 'type', '', '', 'editUser');
    $fields .= $form->createField('hidden', 'address', '', '');
    $fields .= $form->createField('hidden', 'coord', '', '');
    $fields .= $form->createUnvalidatedField('email', 'email', _("Email"), $user->data()->email);
    $fields .= $form->createField('password', 'oldPassword', _("Password") . '<div class="input_caption">' . _("Please enter your password before validating the changes") . '</div>', _("Password"));
    $fields .= $form->createField('password', 'newPassword', _("Change the password"), _("Change the password"));
    $fields .= $form->createField('password', 'settingsPasswordCheck', _("Password check"), _("Password check"));
    $fields .= $form->createUnvalidatedField('text', 'location', _("Address"), _("Search"));
    $fields .= $form->createUnvalidatedField('swap', 'delete', _("Delete my account"), _("Delete my account"), '');
    $fields .= '</div><div id="mapContainerEdit" class="col-6 col-lg-6" style="min-height: 400px;"><div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="' . config::get('GMap_API_Key') . '" width="378px" height="400px" ></div></div></div>';
    $fields .= '<div class="row margin5"><h3>' . _("I would like to receive an email when :") . '</h3></div>'; //I would like to receive an email when...
    $after .= $form->createField('swap', 'problemVote', _("Someone voted on my problem"), '', $problemVote . ' data-notificationtypeid="4" data-entityid="0" onclick="subscribe(this)"');
    $after .= $form->createField('swap', 'newSolution', _("Someone found a solution to my problem"), '', $newSolution . ' data-notificationtypeid="1" data-entityid="0" onclick="subscribe(this)"');
    $after .= $form->createField('swap', 'MyTown', _("I want to be notified of new problems in my town"), '', $myTown . ' data-notificationtypeid="5" data-entityid="0" onclick="subscribe(this)"');
    $after .= $form->createField('swap', 'comContact', _("Allow communities I follow to contact me"), '', $communityContact . ' data-notificationtypeid="10" data-entityid="0" onclick="subscribe(this)"');
    $submitValue = _("Update");
    $onclick     = 'processChanges()';
break;
  case 'newProblem':
    $title              = _("Add a problem"); //Add a problem
    $formId             = "newProblemForm";
    $relatedCommunities = $user->getCommunities();
    $scopeList          = helper::scope();
    $categories         = helper::getCategories();
    $fields             = '<div class="row"><div class="col-6 col-lg-6 text-center">';
    $fields .= $form->createField('text', 'title', _("Title"), _("Title"));
    $fields .= $form->createField('textarea', 'description', _("Description"), _("Description"));
    $fields .= $form->createField('text', 'problemTags', _("Keywords"), _("Press Enter to validate each keyword"));
    $fields .= '<i class="ml-4">' . _("Press Enter to validate each keyword") . '</i>';
    $fields .= $form->createField('select', 'categoryId', _("Category"), _("Category"), $categories);
    $fields .= $form->createField('text', 'vote', _("How does this problem impact you ?"), _("How does this problem impact you ?"));
    if (count($relatedCommunities) > 0) {
      $fields .= $form->createUnvalidatedField('select', 'communityId', _("Link to an account"), _("Link to an account"), $relatedCommunities);
      // $fields .= $form->createUnvalidatedField('swap','private',66,66,'');
    }
    $fields .= $form->createField('hidden', 'type', '', '', 'problem');
    $fields .= $form->createField('hidden', 'address', '', '');
    $fields .= $form->createField('hidden', 'coord', '', '');
    $fields .= $form->createField('text', 'location', _("Location"), _("Search"));
    $fields .= $form->createField('select', 'scopeId', _("Problem's scope"), _("Problem's scope"), $scopeList);
    $fields .= '</div><div id="mapContainerEdit" class="col-6  col-lg-6" style="min-height: 400px;"><div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="' . config::get('GMap_API_Key') . '" width="378px" height="400px" ></div></div></div>';

    $submitValue = _("Add");
    $onclick     = "processForm('newProblemForm','newProblem')";
break;
  case 'problemShare':
    $title          = _("Why not share it with your friends right now so that we can find a solution together");
    $formId         = "ShareProblemForm";
    $problem        = new problem(input::get('id'));
    $problemDetails = $problem->data();
    $fields         = '<div itemscope class="row text-center" itemtype="http://schema.org/ItemPage">
      <div class="faded_green_bkgd2 font_white w-100">
      <H5>' . _("Let's find a solution together") . '</H5>
      </div>
    </div>
    <div class="row text-center">
      <div class="topic_pix col-md-2 text-right">
      <img itemprop="image" src="img/' . $problemDetails->categoryIcon . '">
      </div>
      <div class="col-md-9">
        <span itemprop="name">' . $problemDetails->title . '</span></br>
        <span itemprop="description">' . $problemDetails->description . '</span>
      </div>
      <div class="row w-25">
      <div class="col-12 d-flex justify-content-around">
        <a href="http://www.facebook.com/sharer.php?u=www.solucracy.com/problem-' . $problemDetails->problemId . ".html&title=" . $problemDetails->title . '"><i class="fab fa-2x fa-facebook-square font_blue"></i></a>
        <a href="http://reddit.com/submit?url=www.solucracy.com/problem-' . $problemDetails->problemId . '.html&title=' . $problemDetails->title . '"><i class="fab fa-2x fa-reddit-square font_blue"></i></a>
        <a href="https://twitter.com/intent/tweet?url=www.solucracy.com/problem-' . $problemDetails->problemId . '.html&TEXT=' . $problemDetails->title . '"><i class="fab fa-2x fa-twitter-square font_blue"></i></a>
        <a href="https://plusone.google.com/_/+1/confirm?hl=fr&url=www.solucracy.com/problem-' . $problemDetails->problemId . '.html"><i class="fab fa-2x fa-google-plus-square font_blue"></i></a>
      </div>
      </div>
    </div>';
    $submitValue = _("Close");
    $onclick     = "window.location.replace('problem-" . $problemDetails->problemId . ".html');";
break;
  case 'solutionShare':
    $title           = _("Why not share it with your friends right now to see if it solves their problems ?"); //Why not share it now ?
    $formId          = "ShareSolutionForm";
    $solution        = new solution(input::get('id'));
    $solutionDetails = $solution->data();
    $fields          = '<div itemscope class="row text-center" itemtype="http://schema.org/ItemPage">
      <div class="faded_green_bkgd2 font_white w-100">
      <H5>' . _("I've added a solution on Solucracy!") . '</H5>
      </div>
    </div>
    <div class="row text-center">
      <div class="topic_pix col-md-2 text-right">
      <img itemprop="image" src="img/' . $solutionDetails->categoryIcon . '">
      </div>
      <div class="col-md-9">
        <span itemprop="name">' . $solutionDetails->title . '</span></br>
        <span itemprop="description">' . $solutionDetails->description . '</span>
      </div>
      <div class="row w-25">
      <div class="col-12 d-flex justify-content-around">
        <a href="http://www.facebook.com/sharer.php?u=www.solucracy.com/solution-' . $solutionDetails->solutionId . ".html&title=" . $solutionDetails->title . '"><i class="fab fa-2x fa-facebook-square font_blue"></i></a>
        <a href="http://reddit.com/submit?url=www.solucracy.com/solution-' . $solutionDetails->solutionId . '.html&title=' . $solutionDetails->title . '"><i class="fab fa-2x fa-reddit-square font_blue"></i></a>
        <a href="https://twitter.com/intent/tweet?url=www.solucracy.com/solution-' . $solutionDetails->solutionId . '.html&TEXT=' . $solutionDetails->title . '"><i class="fab fa-2x fa-twitter-square font_blue"></i></a>
        <a href="https://plusone.google.com/_/+1/confirm?hl=fr&url=www.solucracy.com/solution-' . $solutionDetails->solutionId . '.html"><i class="fab fa-2x fa-google-plus-square font_blue"></i></a>
      </div>
      </div>
    </div>';
    $submitValue = _("Close");
    $onclick     = "window.location.replace('solution-" . $solutionDetails->solutionId . ".html');";
break;
  case 'newCommunity':
    //Send back to contact form for now
    $title  = _("We're currently looking for a beta site to test the community account, please send us an email if you're interested in this project");
    $formId = "contactForm";
    $fields = '';
    $fields .= $form->createField('hidden', 'type', '', '', 'contactEmail');
    $fields .= $form->createField('hidden', 'address', '', '');
    $fields .= $form->createField('text', 'name', _("Name"), '');
    $fields .= $form->createField('email', 'email', _("Email"), '');
    $fields .= $form->createField('textarea', 'emailText', _("Comment"), _("Comment"));
    $fields .= $form->createField('captcha', 'captcha', _("Are you a really smart algorithm ?"), _("Please copy here the text in the image"));
    $submitValue = _("Send");
    $onclick     = "processForm('contactForm','showAndReload')";

    //Once the logic is a little bit clearer, uncomment the code related to communities
    //   $title = _("Create a community"); //Create a community

    // $formId = "newCommunityForm";
    // $communityTypes = helper::getCommunityTypes();
    // $fields = '<div class="ml-4 p-2"><label for="communityTypeId" class="row">'._("Community type").'</label>
    //       <div class="row">
    //       <select type="select" name="communityTypeId" id="communityTypeId" class="validate grayBorder" ><option class="text-muted" value="2" selected>'._("Collectivity").'</option><option class="text-muted" value="1"  disabled>'._("Non profit").'</option><option class="text-muted" value="3" disabled>'._("Company").'</option></select>
    //       </div></div>';
    // $fields .= $form->createField('hidden','type','','','newCommunity');
    // $fields .= $form->createField('text','name',_("Name").' <a id="popUpTitle" style="display:none;" href="#" data-toggle="popover" data-trigger="hover" title="" data-html="true" data-content="<b>'._("Why can't I find my community ?").'</b></br>'._("The account might already be taken. If it doesn't appear in the results, you can contact the admins by clicking on Contact at the bottom of the page.").'" width="75" height="75" data-original-title=""><i class="far fa-question-circle font_green"></i></a>',_("Name"));
    // $submitValue = _("Proceed to payment");
    // $onclick = 'processNewCommunity()';
break;
  case 'getCommunityUrl':
    $title = _("Look for a community amongst the activated accounts"); //Look for a community amongst the activated accounts
    $name  = "";
    if (isset($data['communityName'])) {
      $name = 'value="' . $data['communityName'] . '"';
    }
    $formId = "findCommunityForm";
    $fields = '';
    $fields .= $form->createField('text', 'name', _("Name"), _("Name"));
    $fields .= $form->createField('hidden', 'type', '', '', 'getCommunityUrl');
    $submitValue = _("Let's go !");
    $onclick     = "processForm('findCommunityForm','url')";
break;
  case 'communityChoice':
    $title = _("You administer multiple communities, which one would you like to log in with ?"); //You are admin for multiple communities please select one

    $formId      = "communityChoice";
    $communities = $user->getUserCommunityAdmins();
    $fields      = '';
    $fields .= $form->createField('select', 'communities', _("Communities"), '', $communities);
    if (session::exists('communityName')) {
      $fields .= $form->createField(
        'submit',
      'user',
      _("User Profile"),
      '',
      'logAsAdmin(-1)'
        );
    }
    $submitValue = _("Login");
    $onclick     = 'logAsAdmin($(\'#communities option:checked\' ).val())';
break;
  case 'manageAdmins':
    $title     = _("Manage admins"); //manage admins
    $formId    = "adminManagementForm";
    $community = new community($data['communityId']);
    $admins    = $community->getAdmins();
    $fields    = '';
    $fields .= '<table id="admins" class="table table-bordered">
          <thead>
            <tr class="table-hov">
            <th></th>
            <th>' . _("Name") . '</th>
            <th>' . _("Admin since :") . '</th>
            </tr>
          </thead>
          <tbody>';
    //Display existing admins
    foreach ($admins as $admin) {
      $fields .= '<tr id="admin' . $admin->userId . '">
      <td class="text-center clickable" onclick="deleteAdmin(' . $admin->userId . ')"><i class="fa fa-trash-alt"></i></td>
      <td>' . $admin->userName . '</td>
      <td>' . $admin->since . '</td>
      </tr>';
    }
    $fields .= '<tr id="newAdmin" class="table-hover">
            <td colspan="3" class="table-success table-hover text-center"><i class="fas fa-plus"></i></td>
          </tr>
            </tbody>
          </table><input type="hidden" name="type" id="type" value="manageAdmins"/><input type="hidden" name="communityId" id="communityId" value="' . $data['communityId'] . '"/>';
    $submitValue = _("Save");
    $onclick     = "processForm('adminManagementForm','showAndReload')";
break;
  case 'contactUsers':
    $title     = _("Contact followers"); //manage admins
    $formId    = "userContactForm";
    $community = new community($data['communityId']);
    $fields    = '';
    $fields .= $form->createField('textarea', 'text', _("Please enter the message you would like to send"), _("Please enter the message you would like to send"));
    $fields .= $form->createField('hidden', 'type', '', '', 'contactUsers');
    $fields .= $form->createField('hidden', 'communityId', '', '', $data['communityId']);
    $fields .= '
    <div class="row"><input id="list" name="list" type="radio" value="1"><label for="list">' . _("To all your followers") . '</label></div><!--  to all your followers -->
    <div class="row"><input id="inhabitants" name="list" type="radio" value="2"><label for="inhabitants">' . _("Only to inhabitants") . '</label></div> <!-- only to inhabitants -->';
    $submitValue = _("Send");
    $onclick     = "processForm('userContactForm','showAndReload')";
break;
  case 'contactProblemUsers':
    $title   = _("Contact users impacted"); //contact problem users
    $formId  = "problemUsersContactForm";
    $problem = new problem($data['problemId']);
    $fields  = '';
    $fields .= $form->createField('textarea', 'text', _("Message"), _("Please enter the message you would like to send"));
    $fields .= $form->createField('hidden', 'type', '', '', 'contactProblemUsers');
    $fields .= $form->createField('hidden', 'problemId', '', '', $data['problemId']);
    $submitValue = _("Send");
    $onclick     = "processForm('problemUsersContactForm','showAndReload')";
break;
  case 'newProposition':
    $problem       = new problem(session::get('problemId'));
    $community     = $problem->currentOwner();
    $solution      = new solution(input::get('solutionId'));
    $facets        = $problem->getFacets(input::get('solutionId'));
    $fields        = '';
    $displayFacets = "";
    $form          = new form();
    foreach ($facets as $facet) {
      $displayFacets .= '<div class="ml-4 p-2"><div class="row">' . $facet->description . '</div><div class="row swap greenBorder"><input type="checkbox" name="facet' . $facet->facetId . '" id="facet' . $facet->facetId . '" class="hidden"><label for="facet' . $facet->facetId . '" class="slider green"></label></div></div>';
    }
    $pertinenceVotes = $problem->getPertinenceVotes('negative');
    if (count($pertinenceVotes) > 0) {
      $chart = "";
      foreach ($pertinenceVotes as $pertinenceVote) {
        $chart .= ',["' . $pertinenceVote->reason . '", ' . $pertinenceVote->nbVotes . ']';
      }
    }
    $title       = '<div class="row m-1"><h2>' . _("Link this problem") . ' :</h2></div> <div class="row m-1"><h3 class="font-italic redBorder">' . $problem->get('title') . '</h3></div> <div class="row m-1"> <h2>' . _("And this solution") . ' :</h2></div> <div class="row m-1"><h3  class="font-italic greenBorder">' . $solution->get('title') . '</h3></div>';
    $formId      = "newPropositionForm";
    $submitValue = _("Add");
    $onclick     = "processForm('newPropositionForm','url')";
    if ($solution->propositionExists(session::get('problemId')) === 0) {
      $fields .= $form->createField('hidden', 'type', '', '', 'proposition');
      $fields .= $form->createField('hidden', 'solutionId', '', '', $solution->get('solutionId'));
      $fields .= $form->createField('text', 'title', _("Why is it relevant"), '');
      $fields .= $form->createField('swap', 'implemented', _("This solution has already been implemented here"), '');

      if (session::exists('communityAdmin') && $community !== false && $community->communityId === session::get('communityAdmin')) {
        $fields .= $form->createField('swap', 'support', _("Show your support for this proposition !"), '');
        $fields .= $form->createField('text', 'comment', _("Post your comment here"), '');
      }
    } else {
      $title = _("This problem and that solution have already been linked.");
      $fields .= $form->createField('hidden', 'type', '', 'improveProposition');
      $fields .= $form->createField('hidden', 'propositionId', '', $solution->propositionExists(session::get('problemId')));
    }
    if (count($facets) > 0) {
      $fields .= '<div class="col-md-6 font_white red"><label for="description">' . _("Which facets of the problem does this solve") . ' ? <!-- Which facets of the problem does this solve --></label></div>' . $displayFacets;
      if (count($pertinenceVotes) > 0) {
        $fields .= '<script type="text/javascript">
            google.charts.load(\'current\', {packages: [\'corechart\', \'bar\']});
      google.charts.setOnLoadCallback(drawRightY);

      function drawRightY() {
        var data = google.visualization.arrayToDataTable([
        ["Raison", "Nb de votes"]';
        $fields .= $chart;
        $fields .= ']);

        var options = {
        chart: {
          subtitle: "';
        $fields .= _("Main reasons why the previous proposals were not relevant");
        $fields .= '"//Main reasons why the previous propositions were not pertinent
        },
        title: \'';
        $fields .= _("Reasons");
        $fields .= '\',
        hAxis: {
          textPosition: \'in\',
          title: \'Nb de votes\',
          minValue: 0,
        },
        vAxis: {
          title: \'Raison\'
        },
        bars: \'horizontal\',
        axes: {
          y: {
          0: {side: \'right\'}
          }
        },
        legend: {position: \'none\'}
        };
        var material = new google.charts.Bar(document.getElementById(\'chart_div\'));
        material.draw(data, options);
      }
      </script>
      <div id="chart_div" style="width: 80%; height: 30%;" ></div>';
      }
    } else {
      $title       = _("All facets of this problem are already linked");
      $submitValue = _("Back to the solution list");
      $onclick     = "window.location.replace('solutions.php')";
    }
break;
  case 'communitySettings':
    $title                     = _("Notification settings"); //Notification settings
    $formId                    = "communitySettingsForm";
    $notifications             = notification::loadCommunitySubscriptions(session::get('user'), session::get('communityAdmin'));
    $newProblem                = (in_array(5, $notifications)) ? 'checked' : "";
    $newProposition            = (in_array(12, $notifications)) ? 'checked' : "";
    $highPertinenceProposition = (in_array(13, $notifications)) ? 'checked' : "";
    $community                 = new community($data['communityId']);
    $fields                    = '';
    $before                    = '<div class="row">';
    $after                     = '<div class="row margin5">
        <h3>' . _("I would like to receive an email when :") . '</h3> <!-- I would like to receive an email when -->
        </div>';
    $after .= $form->createField('swap', 'newProblem', _("I want to be notified of new problems in my town"), '', $newProblem . ' data-entityid="' . $data['communityId'] . '" data-notificationtypeid="5" onclick="subscribe(this)"');
    $after .= $form->createField('swap', 'newProposition', _("A new proposition has been added to problem in my community"), '', $newProposition . ' data-entityid="' . $data['communityId'] . '" data-notificationtypeid="12" onclick="subscribe(this)"');
    $after .= $form->createField('swap', 'highPertinenceProposition', _("A proposition on one of my community's problems reached at least 75% of pertinence"), '', $highPertinenceProposition . ' data-entityid="' . $data['communityId'] . '" data-notificationtypeid="13" onclick="subscribe(this)"');
    $after .= '</div>';
    $submitValue = false;
    $onclick     = '';
break;
  case 'notMyProblem':
    $title = _("Delegate this problème"); //Delegate the problem

    $formId = "notMyProblemForm";
    $fields = '';
    $fields .= $form->createField('hidden', 'type', '', '', 'notMyProblem');
    $fields .= $form->createField('hidden', 'problemId', '', '', $data['problemId']);
    $fields .= $form->createField('text', 'name', _("Name"), _("Name"));
    $submitValue = _("Send");
    $onclick     = "processForm('notMyProblemForm','showAndReload')";
break;
  case 'supportProposition':
    $title  = _("Show your support for this proposition !"); //Show your support for this proposition !
    $formId = "supportPropositionForm";
    $fields = '';
    $fields .= $form->createField('hidden', 'type', '', '', 'supportProposition');
    $fields .= $form->createField('hidden', 'propositionId', '', '', $data['propositionId']);
    $fields .= $form->createField('text', 'comment', _("Post your comment here"), _("Post your comment here"));
    $submitValue = _("Send");
    $onclick     = "processForm('supportPropositionForm','showAndReload')";
break;
  case 'cancelSubscription':
    $title  = _("Cancel my subscription"); //Show your support for this proposition !
    $formId = "confirmationForm";
    $fields = '';
    $fields .= '<div class="row m-1">' . _("Are you sure you want to cancel your subscription ? For the next 10 days, you can reactivate it using the link sent by email and the payment information.") . '</div>';
    $fields .= $form->createField('swap', 'cancel', _("I understand and still want to cancel my subscription"), _("I understand and still want to cancel my subscription"));
    $fields .= $form->createField('textarea', 'text', _("Message"), _("Can you let us know why you're leaving us ?"));
    $submitValue = _("Cancel my subscription");
    $onclick     = 'confirmCancelSubscription()';
break;
  case 'contactAdmins':
    $title  = _("Hello, what can we do for you ?"); //Hello, what can we do for you ?
    $formId = "contactForm";
    $fields = '';
    $fields .= $form->createField('hidden', 'type', '', '', 'contactEmail');
    $fields .= $form->createField('hidden', 'address', '', '');
    $fields .= $form->createField('text', 'name', _("Name"), '');
    $fields .= $form->createField('email', 'email', _("Email"), '');
    $fields .= $form->createField('textarea', 'emailText', _("Comment"), _("Comment"));
    $fields .= $form->createField('captcha', 'captcha', _("Are you a really smart algorithm ?"), _("Please copy here the text in the image"));
    $submitValue = _("Send");
    $onclick     = "processForm('contactForm','showAndReload')";
break;
  case 'newsletterSubscription':
    $email = "";
    if (isset($data['email'])) {
      $email = $data['email'];
    } elseif ($user->isLoggedIn()) {
      $email = $user->get('email');
    }
    $fields = '';
    $fields .= '<div class="modal-header">
            <h3 class="modal-title text-center">' . _("Please enter your email address and click on the button you like the most") . ' :-)</h3><!-- Please enter your email address and click on the button you like the most -->
            <button id="modalClose" type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>
          <div class="modal-body">';
    $fields .= '<form id="subscribeForm" method="post">';
    $fields .= $form->createField('email', 'email', _("Email"), '', $email);
    $fields .= '</div></div></form></div><div class="modal-footer">';
    $fields .= '<div class="row mx-auto p-1">';
    $fields .= $form->createField('submit', 'subscribe', _("Subscribe"), '', 'newsletterSubscription(true)');
    $fields .= '<div class="m-1"></div>';
    $fields .= $form->createField('submit', 'subscribe', _("Unsubscribe"), '', 'newsletterSubscription(false)');
    $fields .= '<div id="alerts" class="ml-0"></div></div>';
    echo helper::outcome($fields, true);
exit();
  default:
break;
}
//Get the form title and format it
$title = (is_numeric($title)) ? _($title) : $title;
$formContent .= '<div class="modal-header">
            <h3 class="modal-title text-center">' . $title . '</h3>
            <button id="modalClose" type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
$formContent .= $header;
$formContent .= '</div>
          <div class="modal-body">';
//Add whatever goes before the form
$formContent .= $before;
//Get the form fields and build them with the right structure
$formContent .= '<form id="' . $formId . '" method="post">' . $fields;
$formContent .= '</form>';
//Add whatever goes after the form
$formContent .= $after;
$formContent .= '<div class="modal-footer">';
//Add a footer if necessary
$formContent .= $footer;
//Create the validation button
if ($submitValue) {
  $formContent .= $form->createField('submit', 'submit', $submitValue, '', $onclick);
}
$formContent .= '<div id="alerts" class="ml-0"></div></div>';
//and send.
echo helper::outcome($formContent, true);
exit();
