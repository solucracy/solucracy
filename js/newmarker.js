/*
coord : google.maps.LatLng/boolean
if coord is false, then erase the marker on the map,
otherwise draw a new marker at position defined by coord
and center the map on this point
*/
d.DrawMarker = function (coord) {
    if (!coord && d.marker != undefined) {
        d.marker.setMap(null)
    } else if ($.type(coord) === "object") {
        if (!d.marker) {
            d.marker = new google.maps.Marker()
            d.marker.setPosition(coord)
            d.marker.setMap(d.map)
            d.map.setCenter(coord)
        }
    }
}
/*
coord : google.maps.LatLng/boolean
*/
d.StoreCoord = function (coord) {
    if (!coord) $('#coord').val('')
    else if ($.type(coord) === "object") {
        $('#coord').val(coord)
    }
}
/*
city : google.maps.city/string
*/
d.StoreAddress = function (addressComponents) {
    if (!addressComponents) {
        $('#address').val('');
    } else {
        var addressArray = [];
        for (var i = addressComponents.length - 1; i >= 0; i--) {
            addressArray.push(
                {
                    'name': addressComponents[i].long_name,
                    'type': addressComponents[i].types[0]
                }
            );
        }
        addressArray = JSON.stringify(addressArray);
        $('#address').val(addressArray);
    }
}
/*
longAddress : Array of google.maps.GeocoderAddressComponent/boolean
*/
d.PrintAddress = function (longAddress) {
    if (!longAddress) {
        $('#location').val('')
    } else if ($.type(longAddress) === "array") {
        var address = findAddress(longAddress)
        $('#location').val(getShortAddress(address))
    } else if ($.type(longAddress) === "string") {
        $('#location').val(longAddress)
    }
}
/*
addresses : Array of google.maps.GeocoderResult/boolean
*/
d.PrintRadio = function (addresses) {
    if (!addresses) {
        $('#locationRadio').remove();
    } else {
        if (!$('#locationRadio').length) {
            $('#locationSearch').after($('<div id="locationRadio"></div>'))
            $('#locationRadio').empty()
            for (var i = 0; i < addresses.length; i++) {
                var address = findAddress(addresses[i].address_components);
                var radioChoice = $('<input type="radio" name="locationChoice" value="' + i + '" id="Choice' + i + '" />');
                var radioLabel = $('<label for="Choice' + i + '">' + getShortAddress(address) + '</label>');
                radioChoice.data('coord', addresses[i].geometry.location);
                var addressArray = [];
                var addressComponents = addresses[i].address_components;
                if (addressComponents) {
                    radioChoice.data('address', addressComponents);
                }
                $('#locationRadio').append(radioChoice, radioLabel, '<br>')
            }
        }
        SetRadioEvents()
    }
}
/*
isOk : boolean
*/
d.Signal = function (isOk) {
    if (isOk) {
        $('#location').css('background-color', 'rgba(91,200,57,0.5)'); //green
        $(".submit").css("background", '#5BC839');
        $(".submit").prop('disabled', false);
    } else {
        $('#location').css('background-color', 'rgba(255,113,112,0.5)'); //red
        $(".submit").css("background", 'gray');
        $(".submit").prop('disabled', true);
    }
}
$('#locationSearch span').click(SearchLocation);
/*
	addrComponents : Array of google.maps.GeocoderAddressComponent
	addrLatLng : google.maps.LatLng
	*/
function SingleAddressReturned(addrComponents, addrLatLng, city, country)
{
    d.DrawMarker(addrLatLng);
    d.StoreCoord(addrLatLng);
    d.StoreAddress(addrComponents);
    // d.StoreCountry(country);
    d.PrintAddress(addrComponents);
    d.Signal(true);
    if ($('#locationRadio').length) {
        d.PrintRadio(false);
    }
}
/*
addresses : Array of google.maps.GeocoderResult
*/
function SeveralAddressesReturned(addresses)
{
    d.DrawMarker(false);
    d.StoreCoord(false);
    d.StoreAddress(false);
    // d.StoreCity(false);
    // d.StoreCountry(false);
    d.PrintAddress(false);
    d.Signal(false);
    d.PrintRadio(addresses);
}

function SearchLocation()
{
    var search = $('#location').val();
    if (!$.trim(search)) {
        return;
    }
    d.geocoder.geocode(
        {
            'address': search
        },
        function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results.length == 1) {
                    var address = findAddress(results[0].address_components);
                    SingleAddressReturned(results[0].address_components, results[0].geometry.location);
                } else {
                    SeveralAddressesReturned(results);
                }
            } else {
                d.Signal(false);
            }
            console.log("Error : " + status)
        }
    );
}

function SetRadioEvents()
{
    $('input[name="locationChoice"]').change(
        function () {
            d.DrawMarker($(this).data('coord'));
            d.StoreCoord($(this).data('coord'));
            d.StoreAddress($(this).data('address'));
            // d.StoreCountry($(this).data('country'));
            d.PrintAddress($('label[for=' + $(this).attr('id') + ']').text());
            d.Signal(true);
        }
    );
}
/*
nrAddress : Array of google.maps.GeocoderAddressComponent
*/
function findAddress(nrAddress)
{
    var address = {};
    for (var i = 0; i < nrAddress.length; i++) {
        for (var j = 0; j < nrAddress[i].types.length; j++) {
            if (nrAddress[i].types[j] == 'locality') {
                address.locality = nrAddress[i].long_name;
            } else if (nrAddress[i].types[j] == 'country') {
                address.country = nrAddress[i].short_name;
            } else if (nrAddress[i].types[j] == 'route') {
                address.route = nrAddress[i].long_name;
            } else if (nrAddress[i].types[j] == 'postal_code') {
                address.zipCode = nrAddress[i].long_name;
            } else if (nrAddress[i].types[j] == 'administrative_area_level_1') {
                address.area_level_1 = nrAddress[i].long_name;
            } else if (nrAddress[i].types[j] == 'administrative_area_level_2') {
                address.area_level_2 = nrAddress[i].long_name;
            } else if (nrAddress[i].types[j] == 'administrative_area_level_3') {
                address.area_level_3 = nrAddress[i].long_name;
            }
        }
    }
    return address;
}

function getShortAddress(address)
{
    var short_addr = "";
    if (address.route) {
        short_addr += (address.route + ", ");
    };
    if (address.locality) {
        short_addr += (address.locality + ", ");
    }
    if (address.zipCode) {
        short_addr += (address.zipCode + ", ");
    }
    if (address.area_level_2) {
        short_addr += (address.area_level_2 + ", ");
    } else if (address.area_level_3) {
        short_addr += (address.area_level_3 + ", ");
    }
    if (address.area_level_1) {
        short_addr += (address.area_level_1 + ", ");
    }
    if (address.country) {
        short_addr += address.country;
    }
    return short_addr;
}
// function getCountry(address){
// 	var country= address.country ;
// 	return country ;
// }
// function getCity(address){
// 	var city = '';
// 	if(address.locality)
// 		city += (address.locality+", ") ;
// 	if(address.zipCode)
// 		city += address.zipCode ;
// 	return city ;
// }
function validateResult(address, location)
{
    var short_addr = getShortAddressName(address);
    $('#location').val(short_addr);
    $('#location').css('background-color', '#5BC839');
    var marker = new google.maps.Marker(
        {
            'map': d.map,
            'position': location
        }
    );
    d.map.setCenter(location);
}

