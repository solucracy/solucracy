/*
 * display clusters based on an Array of markers
 */
function loadClusters(markers)
{
    if (typeof d.mc !== 'undefined') {
        d.mc.clearMarkers();
    }
    var clusterStyles = {
        minimumClusterSize: 20,
        styles: [{
            textColor: 'black',
            url: 'img/cluster.png',
            backgroundPosition: '50% 50%',
            height: 32,
            width: 32
        }, {
            textColor: 'black',
            url: 'img/cluster.png',
            backgroundPosition: '50% 50%',
            height: 32,
            width: 32
        }, {
            textColor: 'black',
            url: 'img/cluster.png',
            backgroundPosition: '50% 50%',
            height: 32,
            width: 32
        }]
    };
    d.mc = new MarkerClusterer(d.map, '', clusterStyles);
    d.mc.clearMarkers();
    for (i = 0; i < markers.length; i++) {
        d.mc.addMarker(markers[i]);
    }
}

function setMarkers(result)
{
    d.markers = [];
    if (!result) {
        return;
    }
    if ($.isArray(result)) {
        $.each(
            result,
            function (i, item) {
                var lat = this.latitude;
                var lng = this.longitude;
                var iconbase;
                if (baseName == "problem.php") {
                    iconbase = "img/Marqueur vote 32.png";
                } else {
                    iconbase = "img/Marqueur_probleme 32.png";
                }
                var title = this.title;
                var point = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                var marker = new google.maps.Marker(
                    {
                        position: point,
                        title: title,
                        icon: iconbase
                    }
                );
                marker.ID = item.problemId;
                google.maps.event.addListener(
                    marker,
                    'click',
                    function () {
                        if (d.infoWindow) {
                            d.infoWindow.close();
                        }
                        d.infoWindow = new google.maps.InfoWindow(
                            {
                                content: "<a href=\"" + baseUrl + "problem-" + marker.ID + ".html\">" + marker.title + "</a>",
                                disableAutoPan: true
                            }
                        );
                        d.infoWindow.open(d.map, marker);
                    }
                );
                d.markers.push(marker);
            }
        );
    }
    loadClusters(d.markers);
}
$("#myModal").on(
    "shown.bs.modal",
    function () {
        var currentCenter = d.map.getCenter(); // Get current center before resizing
        google.maps.event.trigger(d.map, "resize");
        d.map.setCenter(currentCenter); // Re-set previous center
    }
);
/*
 * event : what causes the request
 * retrieve the values of :
 * the bounds of the map : from google.maps.LatLngBounds
 * the categories selected : Array of string
 * the keywords in the search bar : string
 * the page to display : int
 * then proceed a AJAX request to upload new topics
 */
// load all the votes
function searchVotes(problemId)
{
    // retrieve the bounds
    var mapboundaries = d.map.getBounds().toUrlValue().split(',');
    ajax(
        'votemarkers.php',
        {
            'coordinates[]': mapboundaries,
            'problemId': problemId
        },
        'searchVotes'
    );
}
//set a single marker
function setSingleMarker()
{
    var singleIcon = 'img/Marqueur_probleme 32.png';
    var marker = new google.maps.Marker(
        {
            position: myLatlng,
            icon: singleIcon,
            map: d.map
        }
    );
}

function loadMap(zoom = 7)
{
    var customStyle = [{
        "featureType": "landscape.natural",
        "stylers": [{
            "hue": "#ff7700"
        }]
    }, {
        "featureType": "landscape.man_made",
        "stylers": [{
            "saturation": 19
        }, {
            "hue": "#ff2b00"
        }, {
            "lightness": -11
        }]
    }, {
        "featureType": "poi",
        "stylers": [{
            "hue": "#ff6e00"
        }, {
            "saturation": 5
        }, {
            "lightness": 23
        }]
    }, {
        "featureType": "water",
        "stylers": [{
            "hue": "#00ccff"
        }, {
            "saturation": 43
        }, {
            "hue": "#00ccff"
        }, ]
    }, {}];
    if (baseName == "custommap.php") {
        customStyle = [{
            "featureType": "landscape",
            "elementType": "labels",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "landscape.man_made",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#ffbf80"
            }]
        }, {
            "featureType": "landscape.man_made",
            "elementType": "geometry.stroke",
            "stylers": [{
                "color": "#000000"
            }, {
                "weight": 1.5
            }]
        }, {
            "featureType": "landscape.man_made",
            "elementType": "labels",
            "stylers": [{
                "visibility": "simplified"
            }]
        }, {
            "featureType": "landscape.natural",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#ffffff"
            }]
        }, {
            "featureType": "poi",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "road",
            "stylers": [{
                "color": "#c0c0c0"
            }]
        }, {
            "featureType": "road",
            "elementType": "geometry.stroke",
            "stylers": [{
                "visibility": "simplified"
            }]
        }, {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [{
                "visibility": "simplified"
            }]
        }, {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#000000"
            }]
        }];
    }
    var mapOptions = {
        zoom: zoom,
        center: myLatlng,
        streetViewControl: false,
        zoomControl: true,
        gestureHandling: 'greedy',
        disableDefaultUI: true,
        styles: customStyle
    };
    if ($('#mapContainerEdit').length > 0) {
        d.map = new google.maps.Map(document.getElementById("mapContainerEdit"), mapOptions);
        d.mapType = "edit";
    } else if ($('#mapContainerShow').length > 0) {
        d.map = new google.maps.Map(document.getElementById("mapContainerShow"), mapOptions);
        d.mapType = "show";
    }
    /*
    * called when the user moves the map
    */
    google.maps.event.addListener(
        d.map,
        'idle',
        function () {
            if (baseName == "problem.php") {
                searchVotes(problemId);
            } else if (baseName != "problem.php" && d.mapType == "show") {
                searchAJAX($('#itemList').data('type'));
            }
        }
    );
    if (d.mapType == "edit") {
        google.maps.event.addListener(
            d.map,
            'click',
            function (event) {
                d.geocoder.geocode(
                    {
                        'location': event.latLng
                    },
                    function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            var biggest = results[0].address_components.length;
                            var result = 0;
                            for (var i = 0; i < results.length; i++) {
                                if (results[i].address_components.length > biggest) {
                                    result = i
                                    d.DrawMarker(results[result].geometry.location);
                                }
                            }
                            d.StoreCoord(results[result].geometry.location);
                            d.StoreAddress(results[result].address_components);
                            d.PrintAddress(results[result].address_components);
                            d.PrintRadio(false);
                            d.Signal(true);
                        } else {
                            d.Signal(false);
                            console.log("Error : " + status)
                        }
                    }
                );
            }
        );
    }
    if (baseName == "problem.php") {
        setSingleMarker();
    }
}

