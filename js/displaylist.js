// function searchSolutions(){
// 	var criteria;
// 	if(typeof problemId === 'undefined'){
// 		problemId = "";
// 	}
// 	//category
// 	var catId = $( "#category option:selected" ).val();
// 	// retrieve the keywords
// 	var search = $('#searchTerms').val();
// 	// define the last item of the page
// 	var lastitem = $(".page_nb_selected").text() * d.pageLength;
// 	//Get sort order
// 	var sort = $( "#sort option:selected" ).val();
// 	//Get group selected
// 	//store options in array
// 	var filters = {'search' : search,'cat' : catId,'criteria' : criteria,'status' : status,'sort' : sort, 'problemId' : problemId}
// 	//let's send a Solutions request here
// 	ajax("getsolutions.php",filters,"getSolutions");
// }
function searchAJAX(type) {
  switch (type) {
    case 'problem':
      // retrieve the bounds
      var mapboundaries = d.map.getBounds().toUrlValue().split(',');
      //retrieve the categories
      var catId = $("#category option:selected").val();
      var criteria;
      // retrieve the keywords
      var search = $('#topic_search').val();
      // Only those where we're actively looking for a solution ?
      var solutionSearch = $('#solutionSearch').is(":checked");
      // define the last item of the page
      var lastitem = $(".page_nb_selected").text() * d.pageLength;
      //Get problem status
      var status = $("#status option:selected").val();
      //Get problem scope
      var scope = $("#scopeId option:selected").val();
      //Get sort order
      var sort = $("#sort option:selected").val();
      //Get group selected
      var group = $("#group option:selected").val();
      //store options in array
      var filters = {
        'coordinates[]': mapboundaries,
        'search': search,
        'scope': scope,
        'solutionSearch': solutionSearch,
        'cat': catId,
        'criteria': criteria,
        'statusId': status,
        'sort': sort,
        'group': group
      };
      ajax(problemList, filters, "setMarkers");
      break;
    case 'solution':
      var criteria;
      if (typeof problemId === 'undefined') {
        problemId = "";
      }
      //category
      var catId = $("#category option:selected").val();
      // retrieve the keywords
      var search = $('#searchTerms').val();
      // define the last item of the page
      var lastitem = $(".page_nb_selected").text() * d.pageLength;
      //Get sort order
      var sort = $("#sort option:selected").val();
      //Get group selected
      //store options in array
      var filters = {
        'search': search,
        'cat': catId,
        'criteria': criteria,
        'status': status,
        'sort': sort,
        'problemId': problemId
      }
      //let's send a Solutions request here
      ajax("getsolutions.php", filters, "getSolutions");
      break;
    case 'proposition':
      break;
    default:
      ajax("useritems.php", {
        'type': type
      }, "getUserItems");
      break
  }
}
var pageNumber
var d = d || {};
$(function() {
  d.markers = [];
  d.pageLength = itemsPerPage;
  /*
   * return true if the object exists (and is not empty), false otherwise
   */
  $.fn.exists = function() {
    return this.length !== 0;
  }
  /*
   * called when users presses 'enter' in the search bar
   */
  $(document).keypress(function(event) {
    if (event.which == 13) {
      searchAJAX($('#itemList').data('type'));
      $('#current').text(pageNumber);
      event.preventDefault();
    }
  });
  // called when users sort by status
  $("#problemStatus").change(function() {
    searchAJAX($('#itemList').data('type'));
    $('#current').text(pageNumber);
  });
  // called when users sort the list
  $("#sort").change(function() {
    searchAJAX($('#itemList').data('type'));
    $('#current').text(pageNumber);
  });
});
