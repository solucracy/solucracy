<?php
require 'core/ini.php';
helper::loadHeader(
  'header.php',
  array(
  'TITLE' => 'test'
  , 'DESCRIPTION' => _("Add a problem"))
);

?>
  <div itemscope itemtype="http://schema.org/ItemPage" class="container-fluid full-height marginbt100 m-0">
  <div itemprop="description" style="display: none;">Nous avons tous les mêmes problèmes, trouvons ensemble les solutions</div>
    <div class="row full-height">
      <div class="col-12 full-height m-0 pl-0">
        <div id="mapContainerShow" class="full-height" style="position:relative">
          <div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="<?php echo config::get('GMap_API_Key') ?>" width="WIDTH" height="HEIGHT" ></div>
        </div>
      </div>
    </div>
  </div>
<?php
require "inc/footer.php";
?>
<script type="text/javascript">
  $('header').hide();
</script>
