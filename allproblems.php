<?php
require 'core/ini.php';
helper::loadHeader(
  'header.php',
  array(
  'TITLE' => 'Solucracy'
  , 'DESCRIPTION' => "All problems")
);

$_db = db::getInstance();

$new      = $_db->query("SELECT `problemId`, `title`, `description` FROM `problem` WHERE `statusId` IN (1,3,4)");
$problems = $new->results();

?>

<div>
  <ul>
  <?php
  foreach ($problems as $problem) {
      echo '<li><a href="' . config::get('base_url') . 'problem-' . $problem->problemId . '.html" class="topic_title">' . $problem->title . '</a></li>';
  }
?>

  </ul>

</div>


