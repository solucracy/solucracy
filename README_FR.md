Solucracy
===============

![Solucracy](img/readme.png?raw=true)

Qu'est-ce que c'est ?
-----------------------

> **Un outil collaboratif de priorisation et de résolution de problèmes**

Nous avons tous les mêmes problèmes, trouvons ensemble les solutions.

L’idée est de mettre en commun une méthode et une boite à outils qui permettent de faire remonter les besoins/manques/problèmes d’un groupe d’humains (quartier/village/copropriété/etc...) sous formes de données standardisées qui pourront ensuite être consolidées et rendues visibles grâce à un outil numérique ( www.solucracy.com ) en open source.

A cela s’ajoute des outils de facilitation d’intelligence collective pour organiser des ateliers participatifs pour faire émerger des projets/solutions qui rempliront ces besoins, et seront également renseignés dans l’outil, pour les partager avec les groupes d’humains faisant face aux mêmes besoins.

### Objectif

- Retransformer le citoyen consommateur en citoyen contributeur
- Clarifier les besoins et les laisser s’exprimer pour éviter les explosions type “Gilets Jaunes”
- Offrir aux élus et acteurs du territoire l’opportunité de partager les contraintes auxquelles ils doivent faire face et de communiquer sur la difficulté de leur travail.
- Mettre en action les solutions validées par le choix majoritaire et évaluer sa mise en application
- Utiliser les compétences et les moyens disponibles au plus près du besoin.
- Rendre ludique la participation pour répondre à des besoins collectifs

### Utilisateur type

Collectivité, association, entreprise, citoyens

### Principe

1. Créer un compte
2. Identifier un besoin/manque/problème collectif :
    - Titre
    - Description
    - Mots-clés
    - Catégorie (Urbanisme,Tourisme, Sport, Loisirs, Environnement, Education, Commerce, Administration, Autre)
    - Quel besoin n'est pas satisfait ?
    - Localisation
    - Périmètre du problème (Voisinage, arrondissement, ville, commune, departement, région, pays, état, continent, monde)
3. Suivre l’actualité grace aux filtres (géographique, mots clés, communautés,...)
4. Soutenir un problème
5. Proposer une solution
6. Evaluer la mise en application de la solution 

Historique
-------------

L'idée est née en 2014, de la frustration de ne pas savoir comment aider ma communauté. Il semblait que toutes les solutions existaient déjà, mais rien n'était documenté nulle part.

Pourquoi ne pas utiliser les processus de feuille de route que nous utilisons dans l'industrie du logiciel pour créer une feuille de route pour les communautés ? Laissons les besoins émerger, établissons les priorités et résolvons les problèmes ensemble.

En octobre 2018, après un atelier sur l'économie open source, nous avons pris la décision de développer d'abord une méthode et de la tester puis de retravailler le code pour créer un outil supportant cette méthode.
Et open sourceons tout ça :-) . Alors nous y voilà.

Initialement développé en privé, le code a été libéré en décembre 2018.

Enregistrer un problème
-----------

Il y a 2 types de problèmes qui peuvent être enregistrés

- Bug

Si vous trouvez un bogue, veuillez vérifier la liste des problèmes pour vous assurer qu'il n'a pas déjà été enregistré.
Si ce n'est pas le cas, merci de créer un nouveau numéro documentant la situation, comment vous y êtes arrivé, le navigateur utilisé et si possible une capture d'écran.

-Mise en valeur

Pour l'instant, les améliorations ne seront pas traitées en priorité car la méthode doit être perfectionnée avant que l'application puisse être entièrement conçue.
Veuillez ajouter des demandes de fonctionnalités à la page wiki : https://gitlab.com/solucracy/solucracy/wikis/Nouvelle-fonctionnalit%C3%A9


Copyright
-----------
Copyright (c) 2014-2018 Yannick Laignel
Available under GNU General Public license version 3, See LICENCE.md for more details
