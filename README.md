Solucracy
===============

![Solucracy](img/readme.png?raw=true)

What is it?
-------------

### Guiding principles

**A collaborative tool to prioritize and solve problems**

We all have the same problems, let's find the solutions together.

The goal is to create a common method and toolbox to highlight the needs/problems emerging from a group of humans living together (village, street, city, association) in a standard way so they can be consolidated and made visible thanks to a numeric tool (https://www.solucracy.com) in open source.

To that, we add tools to facilitate collective intelligence to organize participative workshops.

In addition, tools to facilitate collective intelligence will be provided to organize participatory workshops to bring out projects/solutions that will meet these needs, and will also be informed in the tool, to share them with groups of people facing the same needs.

#### Goals

- Transform the citizen consumer into a contributing citizen
- Clarify the needs and let them be expressed to avoid explosions such as the "Yellow Vests".
- Offer elected officials and stakeholders in the territory the opportunity to share the constraints they face and to communicate on the difficulty of their work.
- Put into action the solutions validated by the majority choice and evaluate its implementation
- Use the skills and resources available as close as possible to the need.
- Make participation fun to meet collective needs

#### User types

Community, association, company, citizens

#### Principles

1. Create an account
2. Identify a collective need/lack/issue:
    - Title
    - Description
    - Keywords
    - Category (Urban planning, Tourism, Sport, Leisure, Environment, Education, Commerce, Administration, Other)
    - Why is this a problem for you?
    - Location
    - Scope of the problem (Neighbourhood, borough, city, town, commune, department, region, country, state, continent, world)
3. Monitor current events thanks to filters (geographical, keywords, communities,...)
4. Support a problem
5. Propose a solution
6. Evaluate the implementation of the solution 

### Historical context

The idea came up in 2014, out of frustration from not knowing how to help in my community. It seemed that all the solutions already existed but nothing was documented anywhere.

So I thought why not use the roadmapping processes we use in the software industry to create a roadmap for communities? Let the needs emerge, prioritize and solve together.

In October 2018, after a workshop on the open source economy, we made the decision to develop a method first and test it then rework the code to create a tool supporting that method.

And open source the whole thing :-) . So here we are.

Initially developed privately, the code has been freed in December 2018.

Install
------------

You will need to have

- a web server (either Apache or Nginx) with php (either as module or php-fpm)
- a mysql/mariadb database

After uploading the code to the server, you need to tweak details in `core/config.php`:

- `cp core/config.{sample.,}php`

Edit that file and fill it up with credentials for your database. You will also need to add your [google map API key][gmap-key] in that file.

Then you need to populate the database:

- `mysql solucracy < db/seed.sql`

This may seem a little rough if you don't know much about server administration, but we'll work on some tooling for install automation and making things easy (maybe even some kind of web wizard, who knows: feel free to propose your help on that if you know that stuff).

Develop using docker
------------------------

We use a docker-compose setup to spawn nginx, php, mysql and run the codebase all in one go. But you need to first create a dev config file:

- `cp core/config{.sample,-dev}.php`

You will need to add your [google map API key][gmap-key] in there, and read teh docker-compose.yml to get the mysql credentials that we use by default in that setup.

Then launch the containers with

- `docker-compose up`

This will expose 3 ports locally:

- http://localhost:8001 -> solucracy
- http://localhost:8002 -> phpmyadmin
- http://localhost:8003 -> mysql

Coding standards
-----------------

To enforce some kind of consistency in the coding style, we use phpcs and phpcbf.

### phpcs - PHP-CodeSniffer

It is a static analyzer of php code. Launch with

    docker-compose run phpcs .

Roadmap
---------

Currently version 0.1.0 is the first public version. It's a proof of concept that has already a granted maturity but the codebase is, well, it is what it is: perfectible :D

Focus for 0.2.0

- code cleanup
- code normalization (file naming, coding style, linting)
- tools for install and onboarding
- stronger and easier internationalization

Focus for 0.3.0 and onwards

- refactoring for more code modularity
- switch away from google map towards openstreetmap
- database abstraction layer

Contributing
------------

- check existing issues, maybe you can find some feature request you feel like addressing
- fork the repo under your own space
- create a branch
- do some changes
- push the changes on your forked repo in the branch
- create a pull request

Logging an issue
-----------

There are 2 types of issues that can be logged

- Bug

If you find a bug, please check the issue list to make sure it hasn't already been logged.
If it hasn't, then please create a new issue documenting the situation, how you got there, the browser used and if possible a screenshot.

-Enhancement

For now, enhancements won't be handled as priorities since the method needs to be perfected before the application can be fully designed.
Please add feature requests to the wiki page : https://gitlab.com/solucracy/solucracy/wikis/Nouvelle-fonctionnalit%C3%A9

Copyright
-----------

Copyright (c) 2014-2018 Yannick Laignel  
Available under GNU General Public license version 3, See [LICENSE.txt](LICENSE.txt) for more details

[gmap-key]: https://developers.google.com/maps/documentation/javascript/tutorial
