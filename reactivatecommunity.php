<?php
require 'core/ini.php';
helper::loadHeader(
  'header.php',
  array(
  'TITLE' => _("Reactivate my subscription")
  , 'DESCRIPTION' => _("My community's account has been deactivated by mistake"))
);
//if the code is in the URL
if (input::defined('code')) {
  $community = new community();
  //and it matches a community, and it's not more than 30 days old
  if ($community->checkCode(input::get('code'))) {
    //redirect to a page to enter payment info
    echo '<p>Si Yannick avait fait son boulot, vous seriez redirigé(e) vers les infos de paiement...</p>';
  } else {
    //otherwise let the user know
    echo '<p>' . _("Hello, it looks like this code is wrong or expired. Please use the account creation page to reactivate your community.") . '</p>';
    return;
  }
} else {
  echo '<p>Oui ? vous cherchez quelque chose ?</p>';
}
