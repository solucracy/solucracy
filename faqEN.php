<?php
require 'core/ini.php';
helper::loadHeader(
  'header.php',
  array(
  'TITLE' => 'FAQ'
  , 'DESCRIPTION' => 'help')
);
?>
<div class="container-fluid">
  <div class="offset-3 col-md-6 list w-100 ">
<h1 id="terms">Terms and conditions</h1></br>
<p>Thank you for your interest in Solucracy.
This website is designed to be collaborative, and like in any community, we as members, need to follow some guidelines :
_Please don't do any advertisement for your forums, blogs or websites if they are not directly related to the problem or the ongoing conversation.
_The admins will delete any message or post they think is going against the website mindset.
_Please check the problems or solutions you are entering don't already exist by using the search tools we provide.
_Please be nice. The goal here is to list problems not to create new ones...
_What is a problem for you might not be for someone else. You will probably see problems you think ridiculous or insignificant, they might be very real for others. Please respect that.

And here's a bunch of legal mumbo jumbo...

This document is intended to define the terms and conditions under which Solucracy, hereinafter the EDITOR, provides its users the site and the services available on the website and Moreover, the manner in which the user accesses the site and uses its services.
Any connection to the site is subject to compliance with these conditions.
For the user, simply accessing the site EDITOR at the following URL www.solucracy.com constitutes acceptance of all the conditions described below.
Intellectual Property
All elements of this site, including downloadable documents, are free of charge. Except for iconography, the reproduction of the pages of this website is authorized to condition of mentioning the source. They can not be used for commercial and advertising purposes.
Hyperlinks
www.solucracy.com may contain hypertext links to other sites on the Internet. Links to these other resources make you leave www.solucracy.com
It is possible to create a link to the presentation page of this website without the express permission of the publisher. No prior authorization or request for information may be required by the publisher in respect of a site that wants to establish a link to the publisher's website. It should, however, view this site in a new browser window. However, the EDITOR reserves the right to request removal of a link that he believes not according to the purpose of the site www.solucracy.com.
Responsibility editor
Information and / or documents on this Site and / or accessed through this site are from sources considered reliable.
However, such information and / or documents may contain technical inaccuracies and typographical errors.
THE EDITOR reserves the right to correct errors as soon as they are brought to its attention.
It is strongly recommended to verify the accuracy and relevance of information and / or documents available on this site.
Information and / or documents available on this site are subject to change at any time, and may have been updated. In particular, they may have been updated between the time they are downloaded and when they are brought to the user's knowledge.
The use of information and / or documents available on this site is under the full and sole responsibility of the user, who assumes all the consequences arising therefrom, without the EDITOR can be sought in this respect, and without recourse against the latter.
EDITOR shall in no event be liable for any damages of any kind arising from the interpretation or use of information and / or documents available on this site.
Site Access
The editor strives to allow access to the site 24 hours over 24, 7/ 7 days, except in cases of force majeure or an event beyond the control of the EDITOR, and subject to any breakdowns and maintenance necessary for the proper functioning of the Site and the Services.
Therefore, the EDITOR can not guarantee availability of the site and / or services, transmission reliability and performance in terms of response time or quality. There is no provision for technical assistance with respect to the user either by mail or telephone means.
The responsibility of the EDITOR can not be held liable for inability to access this site and / or use of services.
Moreover, EDITOR may be required to discontinue the site or part of the services at any time without prior notice without any right to compensation. The user acknowledges and agrees that the EDITOR is not responsible for interruptions and the consequences that may result for the User or any third party.
Modification of Terms of Service
THE EDITOR reserves the right to modify at any time without notice these conditions in order to adapt them to the site and / or its operation.
Rules for use of the Internet
The user agrees to the characteristics and limitations of the Internet, and in particular recognizes that:
EDITOR assumes no liability for services accessible via the Internet and has no control of any kind whatsoever about the nature and characteristics of any data transferred via its server center.
The user acknowledges that the data circulating on the Internet are not protected especially against possible diversions. Disclosure of any information deemed by the user of sensitive or confidential nature at its own risk and peril.
The user acknowledges that the data circulating on the Internet can be regulated in terms of use or be protected by a property right.
The user is solely responsible for the use of data consulted, questions and transfers on the Internet.
The user acknowledges that the EDITOR has no way of controlling the content of services accessible on the Internet
Applicable Law
Both this site and the terms and conditions of use are governed by French law, whatever the place of use. In case of any dispute, and after the failure of any attempt to search for an amicable solution, the French courts will have jurisdiction to hear the dispute.
For any question relating to these Terms of Use, you can write to us at the following address: <img class="alignnone size-full wp-image-124" src="http://www.solucracy.com/blog/wp-content/uploads/2017/12/email.png" alt="" width="172" height="19" />
"The terms and conditions (Terms) are full or partial reproduction of the Model TOS (Terms of Service) Internet site, including the owner of the copyright is DROITISSIMO. COM website of large public legal information. "</p>

<h1 id="why">Why do you need my coordinates ?</h1></br>
Solucracy's objective is to define how many people are impacted by a particular problem on a geographical area.
When you vote for a problem, it will add a point on the problem's map.
Then, if this problem impacts your town for example, you'll be able to tell how many people think it is a problem.
It will allow the administrators of your town to get a list of problems to solve in their jurisdiction
<strong>IMPORTANT</strong> : We will not transmit anyone's coordinates to anyone else and other users won't see them.
</div>
</div>
<?php
require "inc/footer.php";
?>
