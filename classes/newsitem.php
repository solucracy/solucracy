<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class newsitem
{
  //newsItemTypes requiring a notification
  private $requiresNotification = array(1, 3, 4, 5, 11),
  //newsItemTypes requiring a badge evaluation
  $requiresBadgeEvaluation = array(1, 2, 3, 6, 7),
  $_db;

  public function __construct()
  {
    $_db = db::getInstance();
  }
  //create a new event
  public static function create($data)
  {
    $_db         = db::getInstance();
    $result      = "";
    $description = "";
    switch ($data['newsItemTypeId']) {
      //proposition added or reaching more than 75% of pertinence
    case '1':
    case '2':
      $new = $_db->query(
        "SELECT distinct(s.title) as solutionTitle, pb.title as problemTitle, pb.problemId, p.title as propositionTitle, u.userName, p.userId, (SELECT " . session::get('language') . " FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM proposition as p
inner join solution as s on s.solutionId = p.solutionId
inner join pertinence as pt on pt.propositionId = p.propositionId
inner join facet as f on f.facetId = pt.facetId
inner join problem as pb on pb.problemId = f.problemId
INNER join user as u on p.userId = u.userId
WHERE p.propositionId = ?",
      array($data['newsItemTypeId'], $data['propositionId'])
          );
        $result            = $new->first();
        $data['problemId'] = $result->problemId;
        $data['userId']    = $result->userId;
        break;
      //problem added
    case '3':
      $new = $_db->query(
      "SELECT pb.title as problemTitle,pb.userId, pb.problemId, (SELECT " . session::get('language') . " FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM problem as pb
WHERE pb.problemId = ?",
      array($data['newsItemTypeId'], $data['problemId'])
        );
        $result            = $new->first();
        $data['problemId'] = $result->problemId;
        $data['userId']    = $result->userId;
        break;
      //problem transferred
      case '4':
        $new = $_db->query(
          "SELECT distinct(pb.title) as problemTitle, pb.problemId, pb.userId,
(SELECT " . session::get('language') . " FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description,
(SELECT c.name from communityproblem as cp inner join community as c on c.communityId = cp.communityId where cp.problemId = pb.problemId and cp.statusId = 1) as name,
(SELECT c.communityId from communityproblem as cp inner join community as c on c.communityId = cp.communityId where cp.problemId = pb.problemId and cp.statusId = 0 order by cp.statusUpdate desc limit 1) as communityId,
(SELECT c.name from communityproblem as cp inner join community as c on c.communityId = cp.communityId where cp.problemId = pb.problemId and cp.statusId = 0 order by cp.statusUpdate desc limit 1) as oldCommunityName
FROM problem as pb
WHERE pb.problemId = ?",
        array($data['newsItemTypeId'], $data['problemId'])
          );
        $result            = $new->first();
        $data['problemId'] = $result->problemId;
        $data['userId']    = $result->userId;
        break;
      //community just created an account
      case '5':
        $new = $_db->query(
          "SELECT name,
(SELECT " . session::get('language') . " FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM community as c
WHERE c.communityId = ?",
        array($data['newsItemTypeId'], $data['communityId'])
          );
        $result = $new->first();
        break;
      //user created an account
      case '6':
        $new = $_db->query(
          "SELECT u.userName,
(SELECT " . session::get('language') . " FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description, u.userId
FROM user as u
WHERE u.userId = ?",
        array($data['newsItemTypeId'], $data['userId'])
          );
        $result = $new->first();
        break;
      case '7':
      //a solution has been added
        $new = $_db->query(
        "SELECT u.userName, u.userId, s.title as solutionTitle,
(SELECT " . session::get('language') . " FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM user as u
inner join solution as s on s.userId = u.userId
WHERE s.solutionId = ?",
        array($data['newsItemTypeId'], $data['solutionId'])
        );
        $result = $new->first();
        break;
      case '8':
      case '10':
        //a community is asking for help, doesn't need help anymore
        $new = $_db->query(
        "SELECT c.name, c.communityId, pb.title as problemTitle, pb.problemId,
(SELECT " . session::get('language') . " FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM problem as pb
inner join communityproblem as cp on cp.problemId = pb.problemId
inner join community as c on c.communityId = cp.communityId
WHERE pb.problemId = ? and cp.statusId = 1",
        array($data['newsItemTypeId'], $data['problemId'])
        );
        $result              = $new->first();
        $data['problemId']   = $result->problemId;
        $data['communityId'] = $result->communityId;
        break;
      case '9':
        //a community supports a proposition
        $new = $_db->query(
          "SELECT distinct(s.title) as solutionTitle,p.userId,c.communityId, pb.title as problemTitle, pb.problemId, c.name,
(SELECT " . session::get('language') . " FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = ?) as description
FROM proposition as p
inner join solution as s on s.solutionId = p.propositionId
inner join pertinence as pt on pt.propositionId = p.propositionId
inner join facet as f on f.facetId = pt.facetId
inner join problem as pb on pb.problemId = f.problemId
INNER JOIN communityproposition as cp on cp.propositionId = p.propositionId
INNER JOIN community as c on c.communityId = cp.communityId
WHERE p.propositionId = ?",
        array($data['newsItemTypeId'], $data['propositionId'])
          );
        $result              = $new->first();
        $data['problemId']   = $result->problemId;
        $data['communityId'] = $result->communityId;
        $data['userId']      = $result->userId;
        break;
      case '11':
        //a badge has been unlocked by a user
        $new = $_db->query(
          "SELECT u.userName,
(SELECT " . session::get('language') . " FROM `language` as l inner join newsitemtype as nit on nit.translationId = l.id WHERE nit.newsItemTypeId = 11) as description,
(SELECT " . session::get('language') . " FROM `language` as l inner join badge as b on b.name = l.id WHERE b.badgeId = ?) as name,
ub.userId
FROM userbadge as ub
inner join user as u on ub.userId = u.userId
where ub.badgeId = ? and ub.scope = ?",
        array($data['badgeId'], $data['badgeId'], $data['scope'])
          );
        $result         = $new->first();
        $data['userId'] = session::get('user');
        break;
      default:
        echo "Ce type de News n'existe pas";
        break;
    }

    // if(in_array($data['newsItemTypeId'],$this->requiresNotification){
    //   //create notification
    // }
    // if(in_array($data['newsItemTypeId'],$this->requiresBadgeEvaluation){
    //   //evaluate badge
    // }
    // replace description depending on news type
    if (in_array($data['newsItemTypeId'], array(2, 1, 3, 4, 8, 9, 10))) {
      $result->description = str_replace('%problemTitle%', $result->problemTitle, $result->description);
    }
    if (in_array($data['newsItemTypeId'], array(2, 1, 7, 9))) {
      $result->description = str_replace('%solutionTitle%', $result->solutionTitle, $result->description);
    }
    if (in_array($data['newsItemTypeId'], array(6, 1, 7, 11))) {
      $result->description = str_replace('%userName%', $result->userName, $result->description);
    }
    if (in_array($data['newsItemTypeId'], array(1))) {
      $result->description = str_replace('%propositionTitle%', $result->propositionTitle, $result->description);
    }
    if (in_array($data['newsItemTypeId'], array(9, 10, 8, 4, 5))) {
      $result->description = str_replace('%communityName%', $result->name, $result->description);
    }
    if (in_array($data['newsItemTypeId'], array(4))) {
      $result->description = str_replace('%communityName2%', $result->oldCommunityName, $result->description);
    }
    if (in_array($data['newsItemTypeId'], array(11))) {
      $result->description = str_replace('%badgeName%', $result->name, $result->description);
    }
    $data['description'] = $result->description;
    unset($data['scope']);
    $query = $_db->insert('newsitem', $data);
    return $query;
  }
  public static function render($data)
  {
    $card = '<div class="card m-2">
        <div class="card-header">
          %formattedDate%
        </div>
        <div class="card-body">
          <h5 class="card-title">%cardTitle%</h5>
          <p class="card-text">%cardText%</p>
          <a href="%url%" class="btn solucracy_btn">' . _("More information") . '</a>
        </div>
      </div>';
    $card = str_replace('%formattedDate%', $data->formattedDate, $card);
    $card = str_replace('%cardTitle%', _($data->title), $card);
    $card = str_replace('%cardText%', $data->description, $card);
    $card = str_replace('%url%', $data->url, $card);
    return $card;
  }
  public static function getNewsItems($scope, $id = null)
  {
    $_db = db::getInstance();
    switch ($scope) {
      case 'community':
        $new = $_db->query(
          "SELECT
    *
  FROM
    ((SELECT
      DISTINCT (ni.newsItemId),
      DATE_FORMAT(ni.createdOn,
      '%d %m %Y %T') AS formattedDate,
      ni.description,
        ni.createdOn,
      nit.titleTranslationId AS title,
      (CASE
        WHEN ni.newsItemTypeId = 1
        OR ni.newsItemTypeId = 2
        OR ni.newsItemTypeId = 3
        OR ni.newsItemTypeId = 4
        OR ni.newsItemTypeId = 8
        OR ni.newsItemTypeId = 9
        OR ni.newsItemTypeId = 10 THEN concat('problem-',
        ni.problemId,
        '.html')
        WHEN ni.newsItemTypeId = 5 THEN concat('communityprofile.php?communityId=',
        ni.communityId)
        WHEN ni.newsItemTypeId = 6
        OR ni.newsItemTypeId = 11 THEN concat('profile.php?userId=',
        ni.userId)
        WHEN ni.newsItemTypeId = 7 THEN concat('solution-',
        ni.problemId,
        '.html')
      END) AS url
    FROM
      `newsitem` AS ni
    INNER JOIN
      communityproblem AS cp
        ON cp.problemId = ni.problemId
    INNER JOIN
      newsitemtype AS nit
        ON nit.newsItemTypeId = ni.newsItemTypeId
    WHERE
      ni.communityId = ?
    ORDER BY
      ni.createdOn DESC)
  UNION
  DISTINCT (SELECT
    DISTINCT (ni.newsItemId),
    DATE_FORMAT(ni.createdOn,
    '%d %m %Y %T') AS formattedDate,
    ni.description,
    ni.createdOn,
    nit.titleTranslationId AS title,
    (CASE
      WHEN ni.newsItemTypeId = 1
      OR ni.newsItemTypeId = 2
      OR ni.newsItemTypeId = 3
      OR ni.newsItemTypeId = 4
      OR ni.newsItemTypeId = 8
      OR ni.newsItemTypeId = 9
      OR ni.newsItemTypeId = 10 THEN concat('problem-',
      ni.problemId,
      '.html')
      WHEN ni.newsItemTypeId = 5 THEN concat('communityprofile.php?communityId=',
      ni.communityId)
      WHEN ni.newsItemTypeId = 6
      OR ni.newsItemTypeId = 11 THEN concat('profile.php?userId=',
      ni.userId)
      WHEN ni.newsItemTypeId = 7 THEN concat('solution-',
      ni.problemId,
      '.html')
    END) AS url
  FROM
    `newsitem` AS ni
  INNER JOIN
    communityproblem AS cp
      ON cp.problemId = ni.problemId
  INNER JOIN
    newsitemtype AS nit
      ON nit.newsItemTypeId = ni.newsItemTypeId
  WHERE
    (cp.communityId = ?
    AND cp.statusId = 1)
  ORDER BY
    ni.createdOn DESC)
) AS union1
ORDER BY
union1.createdOn DESC limit 10",
        array($id, $id)
          );
        $result = $new->results();
        break;

      case 'user':
        $communityFilters = '';
        //if the user is logged in, display the news for the communities to which they subscribed
        if ($id === session::get('user')) {
          $communityFilters = 'or ni.communityId in (select communityId from communitysubscription where userId = ni.userId)';
        }
        $new = $_db->query(
          "SELECT DATE_FORMAT(ni.createdOn,'%d %m %Y %T') as formattedDate, ni.*, nit.titleTranslationId as title,
 (CASE
  WHEN ni.newsItemTypeId = 1 OR ni.newsItemTypeId = 2 OR ni.newsItemTypeId = 3 OR ni.newsItemTypeId = 4 OR ni.newsItemTypeId = 8 OR ni.newsItemTypeId = 9 OR ni.newsItemTypeId = 10 THEN concat('problem-',ni.problemId,'.html')
  WHEN ni.newsItemTypeId = 5 THEN concat('communityprofile.php?communityId=',ni.communityId)
  WHEN ni.newsItemTypeId = 6 OR ni.newsItemTypeId = 11 THEN concat('profile.php?userId=',ni.userId)
  WHEN ni.newsItemTypeId = 7 THEN concat('solution-',ni.problemId,'.html')
  END) as url
FROM `newsitem` as ni
INNER JOIN newsitemtype as nit on nit.newsItemTypeId = ni.newsItemTypeId
WHERE ni.userId = ? " . $communityFilters . " order by ni.createdOn desc limit 10",
        array($id)
          );
        $result = $new->results();
        break;

      case 'none':
        // code...
        break;

      default:
        // code...
        break;
    }

    return $result;
  }

  //find an existing event
  private function find($id)
  {
  }
}
