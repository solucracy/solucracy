<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class address
{

  public static function exists()
  {
  }

  public static function create($data)
  {
    $newAddress = array('latitude' => $data['latitude'], 'longitude' => $data['longitude'], 'other' => null);
    unset($data['latitude'], $data['longitude']);
    foreach ($data as $item) {
      if (in_array($item->type, array('postal_code', 'country', 'administrative_area_level_1', 'administrative_area_level_2', 'administrative_area_level_3', 'locality', 'route', 'street number'))) {
        $newAddress[$item->type] = $item->name;
      } else {
        $newAddress['other'] .= $item->type . ':' . $item->name . ',';
      }
    }
    if (!in_array('locality', $newAddress)) {
      $newAddress['locality'] = $newAddress['administrative_area_level_2'];
    }
    if (!in_array('postal_code', $newAddress)) {
      $newAddress['postal_code'] = '00000';
    }
    $_db = db::getInstance();
    $_db->insert('address', $newAddress);
    return $newAddress['locality'] . ',' . $newAddress['postal_code'];
  }
}
