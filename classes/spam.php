<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class spam
{

  private $_db;
  private $_data;

  public function __construct()
  {
    $this->_db = db::getInstance();
  }

  public function report($entity, $entityId, $comment)
  {
    $spam = new $entity($entityId);
    if ($spam->get('statusId') == 6) {
      return _("This item has already been processed and is not considered as a spam"); //This item has already been processed and is not considered as a spam
    }
    if ($spam->get('statusId') == 1) {
      if ($spam->get('userId') != session::get('user')) {
        $spam->updateStatus(2);
        $this->create($entity, $entityId, $comment);
        // create new spam record and notification for admins
        $email = new email();
        $email->sendContactEmail('Admin', 'webmaster@solucracy.com', $entity . ' ' . _("has been reported by") . ' ' . session::get('userInfo')->userName . ' : ' . $comment);
        return _("Thank you for reporting this ! A moderator will process this shortly");
      } else {
        return _("You cannot report an item that you created yourself");
      }
    } else {
      return _("This item has already been reported by another user, thanks for your help !");
    }
  }

  public function getSpam($id)
  {
    $new = $this->_db->query("select * from spam where spamId = ?", array($id));
    return $new->first();
  }

  public function create($entity, $entityId, $comment)
  {
    $data = $this->_db->insert('spam', array('entity' => $entity, 'entityId' => $entityId, 'comment' => $comment, 'flaggedBy' => session::get('user')));
  }

  public function getList()
  {
    $new = $this->_db->query(
      "SELECT s.*,DATE_FORMAT(s.flaggedOn,'%d %m %Y') as flaggedOn, p.title, u.userName, s.flaggedBy FROM spam as s inner join problem as p on p.problemId = s.entityId inner join user as u on s.flaggedBy = u.userId WHERE approved is NULL and entity = 'problem' and p.userId <> ?
    union ALL
    SELECT s.*,DATE_FORMAT(s.flaggedOn,'%d %m %Y') as flaggedOn, p.title, u.userName, s.flaggedBy FROM spam as s inner join solution as p on p.solutionId = s.entityId inner join user as u on s.flaggedBy = u.userId WHERE approved is NULL and entity = 'solution' and p.userId <> ?
    union ALL
    SELECT s.*,DATE_FORMAT(s.flaggedOn,'%d %m %Y') as flaggedOn, p.title, u.userName, s.flaggedBy FROM spam as s inner join proposition as p on p.propositionId = s.entityId inner join user as u on s.flaggedBy = u.userId WHERE approved is NULL and entity = 'proposition' and p.userId <> ?",
      array(session::get('user'), session::get('user'), session::get('user'))
    );
    return $new->results();
  }

  public static function count()
  {
    $db  = db::getInstance();
    $new = $db->query("SELECT count(distinct spamId) as count FROM spam as s WHERE approved is NULL", array(session::get('user')));
    return $new->first()->count;
  }

  public function process($flaggedBy, $id, $outcome, $entity, $entityId)
  {

    if ($outcome == 1) {
      //if positive, flag as spam
      $entity = helper::test_input($entity);
      $query  = $this->_db->query("UPDATE " . $entity . " SET statusId=5,statusUpdate=now() WHERE " . $entity . "Id = ?", array($entityId));
    } elseif ($outcome == 0) {
      //otherwise, flag it as non spam so that nobody else can flag it as a spam
      $entity = helper::test_input($entity);
      $query  = $this->_db->query("UPDATE " . $entity . " SET statusId=6,statusUpdate=now() WHERE " . $entity . "Id = ?", array($entityId));
    }
    $new = $this->_db->query("UPDATE spam SET processedBy=?,processedOn=now(),approved=? WHERE spamId = ?", array(session::get('user'), $outcome, $id));
    badge::evaluate('newSpamProcessed', $flaggedBy);
    //should remove badges for the user who logged the spam
  }
}
