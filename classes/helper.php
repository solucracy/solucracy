<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class helper
{

  public static function test_input($data)
  {
    if (is_array($data)) {
      return $data;
    }
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
  public static function countActive($entity)
  {
    $entity = helper::test_input($entity);
    $_db    = db::getInstance();
    $_db->query(
      "SELECT count(*) as count
    FROM  " . $entity . "
    WHERE statusId in (1,2,4,3,9)"
    );
    $result = $_db->first();

    return $result->count;
  }

  public static function check_email_address($email)
  {
    // First, we check that there's one @ symbol, and that the lengths are right
    if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
      // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
      return false;
    }
    // Split it into sections to make life easier
    $email_array = explode("@", $email);
    $local_array = explode(".", $email_array[0]);
    for ($i = 0; $i < sizeof($local_array); $i++) {
      if (!preg_match("/^(([A-Za-z0-9!#$%&'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&'*+\/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
        return false;
      }
    }
    if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) {
      // Check if domain is IP. If not, it should be valid domain name
      $domain_array = explode(".", $email_array[1]);
      if (sizeof($domain_array) < 2) {
        return false; // Not enough parts to domain
      }
      for ($i = 0; $i < sizeof($domain_array); $i++) {
        if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
          return false;
        }
      }
    }

    return true;
  }

  public static function generateRandomString($length)
  {
    $characters   = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
  }

  public static function plural($nb)
  {
    return $nb > 1 ? 's' : '';
  }

  public static function loadLanguage()
  {
    $_db = db::getInstance();
    //get country of user and if not already set, set language

    if (!session::exists('language')) {
      self::userIpLocation();

      if (session::exists('country')) {
        if ($_SESSION['country'] === 'France' or $_SESSION['country'] === 'Belgium' or $_SESSION['country'] === 'Switzerland') {
          session::put('language', 'FR');
        } else {
          session::put('language', 'EN');
        }
      } else {
        session::put('language', 'FR');
      }
    }

    switch ($_SESSION['language']) {
    case 'FR':
      session::put('FR', 'lang_selected');
      session::put('EN', "");
        break;

    case 'EN':
      session::put('EN', 'lang_selected');
      session::put('FR', "");
        break;

    default:
      session::put('FR', 'lang_selected');
      session::put('EN', "");
        break;
    }

    //delete this once all translation stuff has been cleaned
    // //get language from db
    // $_db->query("SELECT id," . $_SESSION['language'] . "  FROM language");
    // $results = $_db->results();
    // foreach ($results as $result) {
    //   $truc               = get_object_vars($result);
    //   $words[$truc['id']] = $truc[$_SESSION['language']];
    // }
    // session::put('words', $words);
  }

  //get user IP and location ( limited to 1000 per day !) / another service : echo var_export(unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR'])));
  public static function userIpLocation()
  {
    if (isset($GLOBALS['config']['base_geoloc'])) {
      session::put('latitude', $GLOBALS['config']['base_geoloc']['latitude']);
      session::put('longitude', $GLOBALS['config']['base_geoloc']['longitude']);
      session::put('country', $GLOBALS['config']['base_geoloc']['country']);
    }
    if (!isset($_SESSION['latitude'])) {
      if ($_SERVER['REMOTE_ADDR'] == "::1" or $_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
        session::put('ipaddress', '78.205.32.93');
      } else {
        session::put('ipaddress', $_SERVER['REMOTE_ADDR']);
      }
      $servers = array('http://ip-api.com', 'http://www.geoplugin.net', 'http://freegeoip.net');

      foreach ($servers as $server) {
        if (self::ping($server)) {
          $working = $server;
          break;
        }
      }

      switch ($working) {
        // case 'http://freegeoip.net':
        //  $details = json_decode(@file_get_contents("http://freegeoip.net/json/".$_SESSION['ipaddress']));
        // session::put('latitude',$details->latitude);
        // session::put('longitude',$details->longitude);
        // session::put('country',$details->country_name);
        //  break;

      case 'http://ip-api.com':
        $details = json_decode(@file_get_contents("http://ip-api.com/json/" . $_SESSION['ipaddress']));
        session::put('latitude', $details->lat);
        session::put('longitude', $details->lon);
        session::put('country', $details->country);
          break;

      case 'http://www.geoplugin.net':
        $details = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $_SESSION['ipaddress']));
        session::put('latitude', $details['geoplugin_latitude']);
        session::put('longitude', $details['geoplugin_longitude']);
        session::put('country', $details['geoplugin_countryName']);
          break;

      default:
        session::put('latitude', 6.0833);
        session::put('longitude', 46.299999);
        session::put('country', 'France');
          break;
      }
    }
    if ($_SESSION['latitude'] === '') {
      session::put('latitude', 6.0833);
      session::put('longitude', 46.299999);
      session::put('country', 'France');
    }
    $loc = $_SESSION['latitude'] . ',' . $_SESSION['longitude'];
    return $loc;
  }

  //display all spams
  public static function loadSpams()
  {
    $_db = db::getInstance();
    $_db->query(
      "SELECT  `commentId` ,  `name` ,  `content` , u.userId, p.title, u.userName,  `spam`
    FROM  `comment` AS c
    LEFT JOIN problem AS p ON c.problemId = p.problemId
    LEFT JOIN user AS u ON c.userId = u.userId
    WHERE c.spam = 1"
    );
    $results = $_db->results();

    return $results;
  }

  //Check that the database contains the user with name and password
  public static function databaseContainsUser($email, $password)
  {
    $_db   = db::getInstance();
    $query = $_db->query(
      "SELECT userId, latitude, longitude, userName FROM user
      WHERE email = ? AND password = ? AND (statusId = 1 OR (statusId = 0 AND  DATE_FORMAT(CreatedOn, '%Y-%m-%d')= CURDATE()))",
      array($email, $password)
    );

    if ($query->count() > 0) {
      return true;
    }
    return false;
  }

  public static function propositionExists($solutionId, $facetIds)
  {
    $_db   = db::getInstance();
    $query = $_db->query(
      "SELECT p.propositionId
FROM `proposition` as p
inner join pertinence as per on p.propositionId = per.propositionId
WHERE per.facetId in (?) and p.solutionId = ?",
      array($facetIds, $solutionId)
    );
    if ($query->count() > 0) {
      return true;
    } else {
      return false;
    }
  }

  public static function ping($domain)
  {
    //check, if a valid url is provided
    if (!filter_var($domain, FILTER_VALIDATE_URL)) {
      return false;
    }

    //initialize curl
    $curlInit = curl_init($domain);
    curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($curlInit, CURLOPT_HEADER, true);
    curl_setopt($curlInit, CURLOPT_NOBODY, true);
    curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);

    //get answer
    $response = curl_exec($curlInit);

    curl_close($curlInit);

    if ($response) {
      return true;
    }

    return false;
  }

  public static function loadHeader($headerType, $items = array())
  {
    ob_start();
    include "inc/header/" . $headerType;
    $buffer = ob_get_contents();
    ob_end_clean();
    $buffer = str_replace("%URL%", "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], $buffer);
    foreach ($items as $key => $value) {
      $buffer = str_replace("%" . $key . "%", $value, $buffer);
    }
    echo $buffer;
  }
  public static function map()
  {
    if (in_array(basename($_SERVER['PHP_SELF']), config::get('map_forms'))) {
      return true;
    }
    return false;
  }

  public static function scope()
  {
    $_db = db::getInstance();
    $_db->query(
      'SELECT scopeId as id, l.' . session::get('language') . ' as name
    FROM  `scope`  as s inner join language as l on l.id = s.scopeId'
    );
    $results = $_db->results();

    return $results;
  }

  public static function outcome($message, $outcome, $url = null)
  {
    $output['outcome'] = $outcome;
    $output['message'] = $message;
    if (!null == $url) {
      $output['url'] = $url;
    }
    return json_encode($output);
  }

  public static function replace($item)
  {
    $replaced1 = str_replace(config::get('base_url'), " ", $item);
    $replaced2 = str_replace('_', " ", $replaced1);
    $replaced3 = str_replace('.php', "", $replaced2);
    $result    = substr($replaced3, 0, -2);

    return $result;
  }

  public static function checkEmails($data)
  {
    $data = join("', '", $data);
    $_db  = db::getInstance();
    $_db->query("select distinct email from newsletter where email IN ('$data') union select distinct email from user where email IN ('$data')");
    $emails  = $_db->results();
    $results = array();
    foreach ($emails as $email) {
      array_push($results, $email->email);
    }
    return $results;
  }
  public static function placeholders($text, $count = 0, $separator = ",")
  {
    $result = array();
    if ($count > 0) {
      for ($x = 0; $x < $count; $x++) {
        $result[] = $text;
      }
    }
    return implode($separator, $result);
  }

  public static function logError($text)
  {
    if (is_array($text)) {
      $text = json_encode($text);
    }
    $file = config::get("root_path") . 'logs/error.txt';
    file_put_contents($file, "\n" . date('d/m/Y h:i:s a', time()) . " " . $text, FILE_APPEND | LOCK_EX);
  }

  public static function logHistory($page)
  {
    if (session::get('user') !== 0) {
      $session = session::get('user');
    } else {
      $session = substr(session::get('ipaddress'), 3);
    }
    $text = 'session=' . $session . ',currentPage=' . $page . ',time=' . date('d/m/Y h:i:s a', time()) . ',browser=' . helper::get_browser_name($_SERVER['HTTP_USER_AGENT']);
    $file = config::get("root_path") . 'logs/metrics.txt';
    file_put_contents($file, "\n" . $text, FILE_APPEND | LOCK_EX);
  }

  public static function getCommunityTypes()
  {
    $_db = db::getInstance();
    $_db->query('SELECT communityTypeId as id,  l.' . session::get('language') . ' as name FROM  `communitytype` as c inner join language as l on l.id = c.translationId');
    $results = $_db->results();

    return $results;
  }

  public static function getCommunityList($text, $statusId = null)
  {
    $text = '%' . $text . '%';
    if (is_null($statusId)) {
      $statusSearch = "statusId is not ? and";
    } else {
      $statusSearch = "statusId = ? and";
    }
    $_db     = db::getInstance();
    $new     = $_db->query('SELECT concat(name,"(",departmentId,")") as name FROM  community where ' . $statusSearch . ' name like ? limit 25', array($statusId, $text));
    $data    = $new->results();
    $results = array();
    foreach ($data as $item) {
      array_push($results, $item->name);
    }
    return $results;
  }

  public static function activeUserExists($email)
  {
    $_db     = db::getInstance();
    $new     = $_db->query('SELECT count(*) as total FROM  user where email = ? and statusId = 1', array($email));
    $results = $new->first();
    if ($results->total > 0) {
      return true;
    } else {
      return false;
    }
  }

  public static function createCaptcha()
  {
    $captchaCode = 'abcdefghijkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789';
    $captchaCode = substr(str_shuffle($captchaCode), 0, 6);
    session::put('captchaCode', $captchaCode);
    $image = imagecreate(200, 60);
    imagecolorallocate($image, 91, 200, 57);
    $textColor = imagecolorallocate($image, 218, 50, 50);
    imagettftext($image, 30, 0, 15, 35, $textColor, config::get("root_path") . 'font/PenguinAttack.ttf', $captchaCode);
    $fileName = trim($_SERVER['REMOTE_ADDR'], ';:.');
    imagejpeg($image, config::get("root_path") . '/img/' . $fileName . '.jpeg');
    return $fileName;
  }

  public static function checkCaptcha($text)
  {
    if (session::get('captchaCode') === $text) {
      $fileName = trim($_SERVER['REMOTE_ADDR'], ';:.');
      // unlink(config::get("root_path").'img/'.$fileName.'.jpeg');
      return true;
    } else {
      return false;
    }
  }

  public static function toggle($type, $entityId)
  {
    $_db = db::getInstance();
    switch ($type) {
    case 'help':
      $query = $_db->query(
        'UPDATE communityproblem
          SET needHelp = !needHelp
          WHERE problemId = ?',
      array($entityId)
          );
        $query = $_db->query(
          'select count(*) as nb from communityproblem
          WHERE problemId = ? and needHelp = 1',
        array($entityId)
          );
        $result = $query->first();
        //create the newsitems that goes with it whether we're asking for help or not
      if ($result->nb > 0) {
        newsitem::create(array('newsItemTypeId' => 8, 'problemId' => $entityId));
      } else {
          newsitem::create(array('newsItemTypeId' => 10, 'problemId' => $entityId));
      }
        break;

    default:
      // code...
        break;
    }
  }

  public static function render($type, $data)
  {
    switch ($type) {
    case 'newsItem':
      $card = '<div class="card m-2">
          <div class="card-header">
            %formattedDate%
          </div>
          <div class="card-body">
            <h5 class="card-title">%cardTitle%</h5>
            <p class="card-text">%cardText%</p>
            <a href="%url%" class="btn solucracy_btn">' . _("More information") . '</a>
          </div>
          </div>';
      $card = str_replace('%formattedDate%', $data->formattedDate, $card);
      $card = str_replace('%cardTitle%', _($data->title), $card);
      $card = str_replace('%cardText%', $data->description, $card);
      $card = str_replace('%url%', $data->url, $card);
        break;
      case 'problemListCommunityAdmin':
        $callForHelp = '<i class="fas fa-hand-paper" onclick="toggle(\'help\',' . $data->id . ')" data-toggle="tooltip" data-placement="top" title="' . _("We don't need help anymore") . '" ></i>'; //we don't need help anymore;
        if (!$data->needHelp) {
          $callForHelp = '<i class="fas fa-bullhorn" onclick="toggle(\'help\',' . $data->id . ')" data-toggle="tooltip" data-placement="top" title="' .   _("Ask users for help") . '" ></i>'; //ask users for help
        }
        $card = '<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 p-1">
                    <div class="card redBorder h-100" data-id="p%id%">
                      <div class="card-header d-flex justify-content-between">
                      %helpNeeded%
                        <div class="topic_pix"><img src="img/%icon%"></div>
                        <div class="topic_sharenvote p-2">
                          <span>%votes%</span></span>
                        </div>
                      </div>
                      <div class="card-body">
                        <h5 class="card-title text-center">%problemTitle%</h5><br>
                      </div>
                      <div class="card-footer d-flex justify-content-md-around p-0">
                        <small class="text-muted col-8 col-md-6 p-0">
                          %addedOn%
                        </small>
                        <small class="text-muted col-8 col-md-6 p-0">%solutionsAdded%</small>
                      </div>
                    </div>
                  </div>';
        $card = str_replace('%helpNeeded%', $callForHelp, $card);
        $card = str_replace('%id%', $data->id, $card);
        $card = str_replace('%icon%', $data->icon, $card);
        $card = str_replace('%votes%', $data->count . '<br><span class="word">' . _("Votes") . '</span>', $card);
        $card = str_replace('%problemTitle%', $data->title, $card);
        $card = str_replace('%addedOn%', _("Created on") . ' ' . $data->createdOn, $card);
        $card = str_replace('%solutionsAdded%', _("Solutions added") . ': ' . $data->nbItems, $card);
        break;
      case 'problemList':
        $card = '<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 p-1">
                    <div class="card redBorder h-100" data-id="p%id%">
                      <div class="card-header d-flex justify-content-between">
                        <div class="topic_pix"><img src="img/%icon%"></div>
                        <div class="topic_sharenvote p-2">
                          <span>%votes%</span></span>
                        </div>
                      </div>
                      <div class="card-body">
                        <h5 class="card-title text-center">%problemTitle%</h5><br>
                      </div>
                      <div class="card-footer d-flex justify-content-md-around p-0">
                        <small class="text-muted col-8 col-md-6 p-0">
                          %addedOn%
                        </small>
                        <small class="text-muted col-8 col-md-6 p-0">%solutionsAdded%</small>
                      </div>
                    </div>
                  </div>';
        $card = str_replace('%problemTitle%', $data->title, $card);
        $card = str_replace('%id%', $data->id, $card);
        $card = str_replace('%icon%', $data->icon, $card);
        $card = str_replace('%addedOn%', _("Created on") . ' ' . $data->createdOn, $card);
        $card = str_replace('%votes%', $data->count . '<br><span class="word">' . _("Votes") . '</span>', $card);
        $card = str_replace('%solutionsAdded%', _("Solutions added") . ': ' . $data->nbItems, $card);
        break;
      case 'badge':
        $card = '<img src="img/%img%" class="img-responsive center-block topic_content" data-toggle="popover" data-trigger="hover" title="" data-html="true" data-content="<b>%name%</b></br> %quote% </br><i>%author%</i>" width="75" height="75">';
        $card = str_replace('%img%', $data->image, $card);
        $card = str_replace('%name%', $data->name, $card);
        $card = str_replace('%author%', $data->author, $card);
        $card = str_replace('%quote%', $data->quote, $card);
        break;
      case 'proposition':
        $card = '<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6"><div class="card greenBorder text-center p-1" data-id="p%problemId%">
              <div class="whiteBorder font_white gradient" style="background-image: linear-gradient(to right, rgb(91, 200, 57) 0%, rgb(91, 200, 57)%count%%, rgb(218, 50, 50) %count%%, rgb(218, 50, 50) 100%)">
                ' . _("Pertinence") . ' : %count%%
              </div>
            <div class="row">
              <div class="offset-md-1 col-md-5 red no-padding font_white propThumbnail">
                <div class="triangleTopRight"></div>
                %title%
              </div>
              <div class="col-md-5 green no-padding font_white propThumbnail">
                <div class="triangleBottomLeft"></div>
              %description%
              </div>
            </div>
            <div class="card-footer">
              <small class="text-muted">' . _("Created on") . ' %createdOn%</small>
              </div>
          </div></div>';
        $card = str_replace('%problemId%', $data->id, $card);
        $card = str_replace('%count%', $data->count, $card);
        $card = str_replace('%title%', $data->title, $card);
        $card = str_replace('%description%', $data->description, $card);
        $card = str_replace('%createdOn%', $data->createdOn, $card);
        break;
      case 'solutionList':
        $card = '<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 p-1">
                    <div class="card greenBorder h-100" data-id="s%id%">
                      <div class="card-header d-flex justify-content-between">
                        <div class="topic_pix"><img src="img/%icon%"></div>
                      </div>
                      <div class="card-body">
                        <h5 class="card-title text-center">%solutionTitle%</h5><br>
                      </div>
                      <div class="card-footer d-flex justify-content-md-around p-0">
                        <small class="text-muted col-8 col-md-6 p-0">
                          %addedOn%
                        </small>
                        <small class="text-muted col-8 col-md-6 p-0">%nbproblems%</small>
                      </div>
                    </div>
                  </div>';
        $card = str_replace('%solutionTitle%', $data->title, $card);
        $card = str_replace('%icon%', $data->icon, $card);
        $card = str_replace('%id%', $data->id, $card);
        $card = str_replace('%addedOn%', _("Created on") . ' ' . $data->createdOn, $card);
        $card = str_replace('%nbproblems%', _("# of problems linked") . ': ' . $data->nbItems, $card);
        break;
      case 'topBadges':
        $card = '<div class="card h-10"><a href="%url%">
                <div class="card-body text-center">
                  <h5 class="card-title">%name% : %userName%</h5>
                  <img class="card-img-bottom homeImage" src="img/%img%" alt="%name%">
                </div>

                </a></div>';
        $card = str_replace('%img%', $data->image, $card);
        $card = str_replace('%name%', $data->name, $card);
        $card = str_replace('%url%', 'profile.php?userId=' . $data->userId, $card);
        $card = str_replace('%userName%', $data->userName, $card);
        break;
      case 'selectValues':
        $card = '<option value="%id%">%name%</option>';
        $card = str_replace('%id%', $data->id, $card);
        $card = str_replace('%name%', $data->name, $card);
        break;
      default:
        // code...
        break;
    }
    return $card;
  }
  public static function displayList($type, $data)
  {
    $result = "";
    $i      = 0;
    if (count($data) > 0) {
      foreach ($data as $item) {
        if ($i === 0) {
          $result .= '<div class="itemPage displayed card-deck" data-pageNb="1">';
        }
        if (is_integer($i / 12) && $i / 12 > 0) {
          $pageNb = ($i / 12) + 1;
          $result .= '</div><div class="itemPage hidden card-deck" data-pageNb="' . $pageNb . '">';
        }
        $result .= helper::render($type, $item);
        $i++;
      }
      $result .= "</div>";
    }
    return $result;
  }

  public static function getCategories()
  {
    $_db     = db::getInstance();
    $query   = $_db->query('SELECT c.categoryId as id, c.icon, c.translationId, l.' . session::get('language') . ' as name FROM `category` as c inner join language as l on l.id = c.translationId order by icon desc', array());
    $results = $query->results();
    return $results;
  }

  public static function getNewsletterSubscriptions()
  {
    $_db     = db::getInstance();
    $query   = $_db->query('SELECT email from newsletter where statusId = 1');
    $results = $query->results();
    return $results;
  }

  public static function get_browser_name($user_agent)
  {
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) {
      return 'Opera';
    } elseif (strpos($user_agent, 'Edge')) {
      return 'Edge';
    } elseif (strpos($user_agent, 'Chrome')) {
      return 'Chrome';
    } elseif (strpos($user_agent, 'Safari')) {
      return 'Safari';
    } elseif (strpos($user_agent, 'Firefox')) {
      return 'Firefox';
    } elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) {
      return 'Internet Explorer';
    }

    return 'Other';
  }
  public static function newTransaction($data)
  {
    $_db   = db::getInstance();
    $query = $_db->insert('transaction', $data);
  }
}
