<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class problem
{

  private $_db, $_data, $comments, $markers;

  function __construct($id = null)
  {
    $this->_db = db::getInstance();
    if (isset($id) and is_numeric($id)) {
      $this->find($id);
      $this->_data->tags = explode(',', $this->_data->tags);
    }
  }

  public function update($fields)
  {
    $this->_db->update('problem', $this->_data->problemId, $fields);
  }

  public function updateStatus($value)
  {
    $this->update(
      array(
      'statusId' => $value,
      'statusUpdate' => date('Y-m-d H:i:s')
      )
    );
  }

  public function get($arg)
  {
    return $this->_data->$arg;
  }

  public function find($id)
  {
    $new = $this->_db->query(
      "set @problemId = ?;",
      array(
      $id
      )
    );
    $new = $this->_db->query(
      "CREATE TEMPORARY TABLE IF NOT EXISTS find1 AS SELECT
        COUNT(DISTINCT v.voteId) AS nbVotes,
        f.problemId
    FROM
        vote AS v
    INNER JOIN
        facet AS f
            ON f.facetId = v.facetId
    WHERE
        1 = 1
    GROUP BY
        f.problemId
    ORDER BY
        NULL;"
    );


    $new = $this->_db->query(
      "CREATE TEMPORARY TABLE IF NOT EXISTS find2 AS SELECT
        COUNT(DISTINCT pe.propositionId) AS nbSolutions,
        f.problemId
    FROM
        pertinence AS pe
    INNER JOIN
        facet AS f
            ON f.facetId = pe.facetId
    INNER JOIN
        proposition AS pro
            ON pro.propositionId = pe.propositionId
    WHERE
        1 = 1
        AND pro.statusId <> 5
    GROUP BY
        f.problemId
    ORDER BY
        NULL;"
    );

    $new = $this->_db->query(
      "ALTER TABLE
  find1
ADD
  INDEX find1_idx_problemid_nbvotes (problemId, nbVotes);"
    );
    $new = $this->_db->query(
      "
ALTER TABLE
  find2
ADD
  INDEX find2_idx_problemid_nbsolutions (problemId, nbSolutions);"
    );

    $new         = $this->_db->query(
      "SELECT
        p.problemId,
        p.statusId,
        p.statusUpdate,
        p.latitude,
        p.longitude,
        p.title,
        p.description,
        p.userId,
        u.userName,
        a.locality as city,
        find1.nbVotes,
        find2.nbSolutions,
        p.scopeId,
        DATE_FORMAT(p.createdOn,
        '%d %m %Y') AS createdOn,
        ca.icon AS categoryIcon,
        (SELECT
            GROUP_CONCAT(t.name)
        FROM
            problem AS pro
        LEFT JOIN
            problem_tag AS pt
                ON pro.problemId = pt.problemId
        LEFT JOIN
            tag AS t
                ON t.tagId = pt.tagId
        WHERE
            pro.problemId = p.problemId) AS tags,
        (SELECT
            c.name AS communityName
        FROM
            community AS c
        WHERE
            c.communityId = cp.communityId LIMIT 1) AS communityName,
        (SELECT
            c.communityId
        FROM
            community AS c
        WHERE
            c.communityId = cp.communityId LIMIT 1) AS communityId
    FROM
        problem AS p
    INNER JOIN
        user AS u
            ON p.userId = u.userId
    LEFT JOIN
        facet AS f
            ON f.problemId = p.problemId
    LEFT JOIN
        vote AS v
            ON v.facetId = f.facetId
    LEFT JOIN
        address as a
            on p.latitude = a.latitude
            AND p.longitude = p.longitude
    INNER JOIN
        category AS ca
            ON p.categoryId = ca.categoryId
    LEFT JOIN
        communityproblem AS cp
            ON cp.problemId = p.problemId
            AND cp.statusId = 1
    LEFT JOIN
        find1
            ON find1.problemId = p.problemId
    LEFT JOIN
        find2
            ON find2.problemId = p.problemId
    WHERE
        p.problemId = @problemId
    GROUP BY
        p.problemId
    ORDER BY
        NULL"
    );
    $this->_data = $new->first();
    if ($this->_data->nbSolutions > 0) {
      $this->_data->pertinenceVotes = $this->getPertinenceVotes('');
      $this->getSolutionPertinence();
    }
    return $new->results();
  }

  public function data()
  {
    return $this->_data;
  }

  public function exists()
  {
    return (!empty($this->_data)) ? true : false;
  }

  public function create($data)
  {

    $keywords             = explode(",", helper::test_input($data['problemTags']));
    $coordinates          = explode(',', trim($data['coord'], '()'));
    $facet['description'] = $data['vote'];
    if (isset($data['communityId']) && $data['communityId'] > 0) {
      $communityId = $data['communityId'];
      if (isset($data['private']) && $data['private'] === "on") {
        $data['private'] = 1;
      } else {
        unset($data['private']);
      }
    }
    $address              = (array) json_decode($data['address']);
    $address['latitude']  = $coordinates[0];
    $address['longitude'] = $coordinates[1];
    unset($data['vote'], $data['coord'], $data['problemTags'], $data['communityId'], $data['address']);
    $data['latitude']  = $coordinates[0];
    $data['longitude'] = $coordinates[1];
    $data['userId']    = session::get('user');
    $data['createdOn'] = date('Y-m-d H:i:s');
    $data['statusId']  = 1;
    $this->_db->insert('problem', $data);
    $facet['problemId'] = $this->_db->lastInsertId();
    address::create($address);
    if (isset($facet['problemId'])) {
      $this->find($facet['problemId']);
      if (isset($communityId)) {
        $this->addCommunityLink($communityId);
      }
      $facet['facetId'] = $this->addFacet($facet);
      new vote($facet['facetId']);
      $tags = new tags();
      $tags->add($keywords);

      // Link each tag to the problem
      foreach ($keywords as $keyword) {
        $this->addTag($keyword, $facet['problemId']);
      }
      return true;
    }

    return false;
  }

  public function addCommunityLink($communityId)
  {
    if (null !== $this->_data->problemId) {
      //void the old communitylink
      $query = $this->_db->query(
        "UPDATE communityproblem SET statusId=0, statusUpdate=now(), updatedBy = ? WHERE statusId = 1 and problemId = ?",
        array(
        session::get('user'),
        $this->_data->problemId
        )
      );
      //clear the support for any proposition for that problem
      $this->clearSupport();
      $data['problemId']   = $this->_data->problemId;
      $data['communityId'] = $communityId;
      $data['statusId']    = 1;
      $data['updatedBy']   = session::get('user');
      $this->_db->insert('communityproblem', $data);
    }
  }

  public function currentOwner()
  {
    $query = $this->_db->query(
      "SELECT c.communityId, c.communityTypeId, c.name, c.statusId, ct.translationId
FROM communityproblem as cp
inner join community as c on cp.communityId = c.communityId
inner join communitytype as ct on ct.communityTypeId = c.communityTypeId
WHERE cp.problemId = ? and cp.statusId = 1",
      array(
      $this->_data->problemId
      )
    );
    if ($query->count() > 0) {
      return $query->first();
    } else {
      return false;
    }
  }
  //void all communitypropositions for that problem
  public function clearSupport()
  {
    $query = $this->_db->query(
      "UPDATE communityproposition as cp
inner join pertinence as per on per.propositionId = cp.propositionId
inner join facet as f on per.facetId = f.facetId
SET statusId=0, statusUpdate=now(), updatedBy = ?

WHERE statusId = 1 and f.problemId = ?",
      array(
      session::get('user'),
      $this->_data->problemId
      )
    );
  }


  //get impacted users who agreed to be contacted
  public function getUserContactList()
  {
    $new     = $this->_db->query(
      "SELECT distinct(v.userId)
      from vote as v
      inner join facet as f on f.facetId = v.facetId
      inner join notif_subscription as ns on ns.userId = v.userId
      where ns.notificationTypeId = 9 and f.problemId = ?",
      array(
      $this->_data->problemId
      )
    );
    $results = $new->results();
    return $results;
  }

  public function getFacets($solutionId = null)
  {
    if (null !== $solutionId) {
      $query = $this->_db->query(
        "set @userId = ?, @problemId = ?, @solutionId= ?;",
        array(
        session::get('user'),
        $this->_data->problemId,
        $solutionId
        )
      );
      $query = $this->_db->query(
        "CREATE TEMPORARY TABLE IF NOT EXISTS facets1 AS SELECT
        count(DISTINCT v.voteId) AS voted,
        v.facetId
    FROM
        vote AS v
    INNER JOIN
        facet
            ON facet.facetId = v.facetId
    WHERE
        v.userId = @userId
        AND 1 = 1
    GROUP BY
        v.facetId
    ORDER BY
        NULL;"
      );


      $query = $this->_db->query(
        "ALTER TABLE
      'facets1'
    ADD
      INDEX 'facets1_idx_facetid_voted' ('facetId', 'voted');"
      );

      $query = $this->_db->query(
        "SELECT
            f.*,
            COUNT(DISTINCT v.voteId) AS nbVotes,
            facets1.voted
        FROM
            facet AS f
        INNER JOIN
            vote AS v
                ON v.facetId = f.facetId
        LEFT JOIN
            facets1
                ON facets1.facetId = f.facetId
        WHERE
            f.problemId = @problemId
            AND NOT EXISTS (
                SELECT
                    1
                FROM
                    pertinence AS pe
                INNER JOIN
                    proposition AS p
                        ON p.propositionId = pe.propositionId
                WHERE
                    p.solutionId = @solutionId
                    AND f.facetId = pe.facetId
            )
        GROUP BY
            f.facetId
        ORDER BY
        f.facetId ASC"
      );
    } else {
      $query = $this->_db->query(
        "set @userId = ?, @problemId = ?;",
        array(
        session::get('user'),
        $this->_data->problemId
        )
      );
      $query = $this->_db->query(
        "CREATE TEMPORARY TABLE IF NOT EXISTS facets1 AS SELECT
        count(DISTINCT v.voteId) AS voted,
        v.facetId
    FROM
        vote AS v
    INNER JOIN
        facet
            ON facet.facetId = v.facetId
    WHERE
        v.userId = @userId
        AND 1 = 1
    GROUP BY
        v.facetId
    ORDER BY
        NULL;"
      );

      $query = $this->_db->query(
        "ALTER TABLE
  'facets1'
ADD
  INDEX 'facets1_idx_facetid_voted' ('facetId', 'voted');"
      );
      $query = $this->_db->query(
        "
SELECT
        f.*,
        COUNT(DISTINCT v.voteId) AS nbVotes,
        facets1.voted
    FROM
        facet AS f
    INNER JOIN
        vote AS v
            ON v.facetId = f.facetId
    LEFT JOIN
        facets1
            ON facets1.facetId = f.facetId
    WHERE
        f.problemId = @problemId
    GROUP BY
        f.facetId
    ORDER BY
        f.facetId ASC"
      );
    }
    return $query->results();
  }

  public function addFacet($data)
  {
    $data['userId'] = session::get('user');
    $this->_db->insert('facet', $data);
    return $this->_db->lastInsertId();
  }

  public function getPertinenceVotes($positive, $propositionId = null)
  {
    switch ($positive) {
    case 'positive':
      $outcome = 'p.positive = 1 AND ';
        break;
    case 'negative':
      $outcome = 'p.positive = 0 AND ';
        break;
    default:
      $outcome = '';
        break;
    }
    if ($propositionId) {
      $propositionFilter = 'pr.propositionId = ' . helper::test_input($propositionId) . ' AND ';
    } else {
      $propositionFilter = '';
    }
    $query = $this->_db->query(
      "SELECT p.*,f.description, COUNT(DISTINCT pv.pertinenceVoteId) AS nbVotes, s.title FROM pertinence as p LEFT JOIN pertinencevote as pv on p.pertinenceId = pv.pertinenceId INNER JOIN proposition as pr on p.propositionId = pr.propositionId INNER JOIN facet as f on f.facetId = p.facetId INNER JOIN solution as s on s.solutionId = pr.solutionId WHERE " . $outcome . $propositionFilter . "f.problemId = ? group by p.pertinenceId",
      array(
      $this->_data->problemId
      )
    );
    return $query->results();
  }
  //Positive pertinence for each proposition : facetIds = facets of that problem solved by this proposition, nbVotes = number of positive votes for this pertinence, facetvotes = total number of people impacted by this problem's facets, voted = 1 if the user has already voted for this proposition, allVotes = all cumulated votes for this proposition
  public function getSolutionPertinence()
  {
    $query                            = $this->_db->query(
      "set @userId = ?, @problemId = ?;",
      array(
      session::get('user'),
      $this->_data->problemId
      )
    );
    $query                            = $this->_db->query(
      "
CREATE TEMPORARY TABLE IF NOT EXISTS temp1 AS SELECT
        count(DISTINCT pev.pertinenceVoteId) AS voted,
        pertinence.propositionId
    FROM
        pertinencevote AS pev
    INNER JOIN
        pertinence
            ON pertinence.pertinenceId = pev.pertinenceId
    WHERE
        pev.userId = @userId
        AND 1 = 1
    GROUP BY
        pertinence.propositionId
    ORDER BY
        NULL;"
    );
    $query                            = $this->_db->query(
      "CREATE TEMPORARY TABLE IF NOT EXISTS temp2 AS SELECT
        count(DISTINCT pev.pertinenceVoteId) AS allVotes,
        pertinence.propositionId
    FROM
        pertinencevote AS pev
    INNER JOIN
        pertinence
            ON pertinence.pertinenceId = pev.pertinenceId
    WHERE
        1 = 1
    GROUP BY
        pertinence.propositionId
    ORDER BY
        NULL;"
    );
    $query                            = $this->_db->query(
      "ALTER TABLE
  temp1
ADD
  INDEX temp1_idx_propositionid_voted (propositionId, voted);"
    );
    $query                            = $this->_db->query(
      "ALTER TABLE
  temp2
ADD
  INDEX temp2_idx_propositionid_allvotes (propositionId, allVotes);"
    );
    $query                            = $this->_db->query(
      "SELECT
        GROUP_CONCAT(DISTINCT f.facetId
    ORDER BY
        f.facetId ASC) AS facetIds,
        pr.propositionId,
        s.solutionId,
        s.title,
        pr.title AS description,
        COUNT(DISTINCT pv.pertinenceVoteId) AS nbVotes,
        COUNT(DISTINCT v.voteId) AS facetVotes,
        temp1.voted,
        temp2.allVotes,
        (SELECT
            cp.communityId
        FROM
            communityproposition AS cp
        WHERE
            cp.statusId = 1
            AND cp.propositionId = pr.propositionId) AS endorsed,
        (SELECT
            cp.comment
        FROM
            communityproposition AS cp
        WHERE
            cp.statusId = 1
            AND cp.propositionId = pr.propositionId) AS comment
    FROM
        pertinence AS p
    LEFT JOIN
        pertinencevote AS pv
            ON p.pertinenceId = pv.pertinenceId
    INNER JOIN
        proposition AS pr
            ON p.propositionId = pr.propositionId
    INNER JOIN
        facet AS f
            ON f.facetId = p.facetId
    INNER JOIN
        vote AS v
            ON f.facetId = v.facetId
    INNER JOIN
        solution AS s
            ON s.solutionId = pr.solutionId
    LEFT JOIN
        temp1
            ON temp1.propositionId = p.propositionId
    LEFT JOIN
        temp2
            ON temp2.propositionId = p.propositionId
    WHERE
        f.problemId = @problemId
        AND p.positive = 1
        AND pr.statusId <> 5
    GROUP BY
        pr.propositionId
    ORDER BY
        nbVotes DESC"
    );
    $this->_data->solutionsPertinence = $query->results();
    return $query->debug();
  }

  public function getUserRelevantPropositions()
  {
    $query  = $this->_db->query(
      "set @userId = ?, @problemId = ?;",
      array(
      session::get('user'),
      $this->_data->problemId
      )
    );
    $query  = $this->_db->query(
      "SELECT
        p.pertinenceId,
        p.reason,
        s.title,
        pr.propositionId,
        p.positive,
        f.facetId
    FROM
        facet AS f
    LEFT JOIN
        pertinence AS p
            ON f.facetId = p.facetId
    INNER JOIN
        vote AS v
            ON f.facetId = v.facetId
    INNER JOIN
        proposition AS pr
            ON p.propositionId = pr.propositionId
    LEFT JOIN
        solution AS s
            ON s.solutionId = pr.solutionId
    WHERE
        f.problemId = @problemId
        AND v.userId = @userId
        AND pr.statusId <> 5
        AND NOT EXISTS (
            SELECT
                1
            FROM
                pertinence AS pe
            INNER JOIN
                pertinencevote AS pv
                    ON pe.pertinenceId = pv.pertinenceId
            WHERE
                pv.userId = @userId
                AND pr.propositionId = pe.propositionId
        )
        group by p.pertinenceId order by NULL"
    );
    $result = $query->results();
    $arr    = array();
    foreach ($result as $key => $item) {
      $arr[$item->propositionId][$key] = $item;
    }
    return $arr;
  }

  public function countPropsToEvaluate()
  {
    $query = $this->_db->query(
      'SET @user = ?, @problem = ?;',
      array(
      session::get('user'),
      $this->_data->problemId
      )
    );
    $query = $this->_db->query(
      "SELECT
        count(DISTINCT pr.propositionId) AS count
    FROM
        proposition AS pr
    INNER JOIN
        pertinence AS p
            ON p.propositionId = pr.propositionId
    WHERE
        pr.statusId <> 5
        AND NOT EXISTS (
            SELECT
                1
            FROM
                pertinencevote AS pv
            INNER JOIN
                pertinence AS p
                    ON p.pertinenceId = pv.pertinenceId
            WHERE
                pv.userid = @user
                AND p.facetId = (
                    SELECT
                        v.facetId
                    FROM
                        vote AS v
                    INNER JOIN
                        facet AS f
                            ON f.facetId = v.facetId
                    WHERE
                        v.userId = @user
                        AND f.problemId = @problem
                        GROUP BY v.facetId
                ORDER BY NULL
                )
                AND pr.propositionId = p.propositionId
            )
            AND p.facetId = (
                SELECT
                    v.facetId
                FROM
                    vote AS v
                INNER JOIN
                    facet AS f
                        ON f.facetId = v.facetId
                WHERE
                    v.userId = @user
                    AND f.problemId = @problem
                GROUP BY v.facetId
                ORDER BY NULL
            )"
    );
    return $query->first()->count;
  }

  public function hasVoted($facetId = null)
  {
    if (!is_null($facetId)) {
      $filter = 'f.facetId = ' . $facetId;
    } else {
      $filter = 'f.problemId = ' . $this->_data->problemId;
    }
    $query = $this->_db->query(
      "SELECT v.voteId from vote AS v INNER JOIN facet as f ON f.facetId = v.facetId where " . $filter . " AND
      v.userId = ?",
      array(
      session::get('user')
      )
    );
    if ($query->count() > 0) {
      return true;
    }
    return false;
  }

  public function addTag($tag, $problemId)
  {
    $this->_db->query(
      'INSERT into problem_tag  set
      problemId = ?,
      tagId = ( select tagId from tag where name = ? )',
      array(
      $problemId,
      $tag
      )
    );
  }

  // //get problem info for markers within map boundaries
  // public function problem_get_markersubset($maplimits,$userid,$search,$cat){

  //     $output = array();
  //     if(isset($search)and $search != ''){
  //         $searchterms = '(MATCH (p.title,p.description) AGAINST ("'.test_input($search).'")OR MATCH (t.name) AGAINST ("'.test_input($search).'")) AND';
  //     } else {
  //         $searchterms = "";
  //     }
  //     $processedcat = "'".implode("','",$cat)."'" ;

  //     $query = $this->_db->query("SELECT p.ProblemId, p.title, u.userName, p.categoryId as category, COUNT(distinct v.voteId) AS nbvotes, p.createdOn AS age, p.latitude, p.longitude
  //                         FROM problem AS p
  //                         INNER JOIN user AS u ON p.userId = u.userId
  //                         LEFT JOIN vote AS v ON v.problemId = p.problemId
  //                         LEFT JOIN problem_tag as pt on p.problemId = pt.problemId
  //                         LEFT JOIN tag as t on t.tagId = pt.tagId
  //                         LEFT JOIN category as c on c.categoryId = p.categoryId
  //                         WHERE".$searchterms."(p.categoryId IN (".$processedcat.")) AND (p.latitude > ? AND p.latitude < ? AND p.longitude > ? AND p.longitude < ?) and p.statusId != 0 GROUP BY p.problemId ORDER BY nbvotes desc", array($userid,$maplimits[0],$maplimits[2],$maplimits[1],$maplimits[3]));
  //         $this->markers = $query->results();

  //     $output['outcome'] = true;
  //     $output['message'] = $this->markers;
  //     return json_encode($output);
  //     exit();
  // }

  public function isCreator()
  {
    if (session::get("userId") === $this->_data->userId) {
      return true;
    }
    return false;
  }

  // public function commonProblems($user1,$user2){
  //     $query = $this->_db->query("SELECT count(*), v.problemId, p.title, p.description, (SELECT c.icon FROM category AS c WHERE c.categoryId = p.categoryId ) as categoryIcon, DATE(p.createdOn) AS age FROM vote AS v
  //         right join problem as p on p.problemId = v.problemId
  //         WHERE v.userId = ? OR v.userId = ? group by v.problemId having count(*)>1", array($user1,$user2));
  //     return $query->results();
  // }

  public function spam()
  {
    $query = $this->_db->query("SELECT * FROM problem WHERE statusId = 2");
    return $query->results();
  }

  public function getVoteCoordinates($mapCoordinates)
  {
    $query = $this->_db->query(
      "SELECT f.description as title, v.voteId, v.latitude, v.longitude from vote as v inner join facet as f on f.facetId = v.facetId inner join problem as p on p.problemId = f.problemId where f.problemId = ? AND v.latitude > ? AND v.latitude < ? AND v.longitude > ? AND v.longitude < ? group by v.voteId ORDER BY NULL",
      array(
      $this->_data->problemId,
      $mapCoordinates[0],
      $mapCoordinates[2],
      $mapCoordinates[1],
      $mapCoordinates[3]
      )
    );
    return $query->results();
  }
}
