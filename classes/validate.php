<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class validate
{
  private $_passed = false, $_errors = array(), $_db = null;

  public function __construct()
  {
    $this->_db = db::getInstance();
  }

  public function check($source, $items = array())
  {
    foreach ($items as $item => $rules) {
      foreach ($rules as $rule => $rule_value) {
        $value = trim($source[$item]);

        if ($rule === 'required' && $rule_value === true && empty($value)) {
          $this->addError('{$item} {_("is a required field")}.'); //is required
        } elseif (!empty($value)) {
          switch ($rule) {
          case 'min':
              if (strlen($value) < $rule_value) {
                $this->addError('{$item} {_("must be a minimum of")} {$rule_value} {_("characters")}.'); //minimum of X characters
              }
              break;
          case 'max':
              if (strlen($value) > $rule_value) {
                $this->addError('{$item} {_("must be a maximum of")} {$rule_value} {_("characters")}.'); //maximum of X characters
              }
              break;
          case 'matches':
              if ($value != $source[$rule_value]) {
                $this->addError('{$rule_value} {_("must match")} {$item}.'); //must match
              }
              break;
          case 'captcha':
              if (!helper::checkCaptcha($value)) {
                $this->addError('{_("Please fill in the text displayed in the image otherwise everyone will think you\'re an algorithm")}');
              }
              break;
          case 'unique':
            $check = $this->_db->get(
            $rule_value,
            array(
            $item,
            '=',
            $value
            )
              );
            if ($check->count()) {
                $this->addError('{$item} {_("is already taken")}'); //is laready taken
            }
              break;
          case 'empty':
              if ($value != "") {
                $this->addError("Caught you, Winnie !");
              }
              break;
          case 'header_injection':
              if (stripos($value, 'Content-Type:') !== $rule_value) {
                $this->addError(_("There was a problem with the information you entered."));
              }
              break;
          case 'valid_email':
              if (helper::check_email_address($value) !== $rule_value) {
                $this->addError(_("invalid email address"));
              }
              break;
          case 'checked':
              if (helper::check_email_address($value) !== $rule_value) {
                $this->addError(_("invalid email address"));
              }
              break;
          }
        }
      }
    }

    if (empty($this->_errors)) {
      $this->_passed = true;
    }

    return $this;
  }

  protected function addError($error)
  {
    $this->_errors[] = $error;
  }

  public function passed()
  {
    return $this->_passed;
  }

  public function errors()
  {
    return $this->_errors;
  }
}
