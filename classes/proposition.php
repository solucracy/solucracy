<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class proposition
{

  private $_db;
  private $_data;
  private $_list;

  public function __construct($id = null)
  {
    $this->_db = db::getInstance();

    if (isset($id) and !is_array($id)) {
      $this->find($id);
    } elseif (isset($id) and is_array($id)) {
      try {
        $this->create($id);
      } catch (Exception $e) {
        $this->_data = $e;
      }
    }
  }

  public function find($id = null)
  {

    $query = $this->_db->get('proposition', array('propositionId', '=', $id));
    if ($query->count()) {
      $this->_data = $query->first();
      return true;
    }
    return false;
  }

  public function getPertinenceList()
  {
    $query = $this->_db->get('pertinence', array('propositionId', '=', $id));
    if ($query->count()) {
      $this->_list = $query->results();
      return true;
    }
    return false;
  }

  public function addPertinence($data)
  {
    if (isset($data['propositionId'])) {
      $propositionId = $data['propositionId'];
    } else {
      $propositionId = $this->_data->propositionId;
    }
    if (isset($data['positive']) && $data['positive'] === 1) {
      $new = $this->_db->query("SELECT pertinenceId FROM `pertinence` WHERE positive = 1 and facetId = ? and propositionId = ?", array($data['facetId'], $propositionId));
      if ($new->count() > 0) {
        return false;
      } else {
        $data['reason'] = _("This solution is relevant");
      }
    }
    $keys = array(
    'propositionId' => $propositionId,
    'reason'        => $data['reason'],
    'positive'      => $data['positive'],
    'facetId'       => $data['facetId'],
    'userId'        => session::get('user'));
    $this->_db->insert('pertinence', $keys);
    $pertinenceId = $this->_db->lastInsertId();
    $new          = $this->_db->query("SELECT * FROM `vote` WHERE facetId = ? and userId = ?", array($data['facetId'], session::get('user')));
    if ($new->count() > 0) {
      $this->addPertinenceVote($pertinenceId);
    }
  }
  public function addPertinenceVote($pertinenceId)
  {
    //might want to add a stored procedure here
    $userId = session::get('user');
    $query  = $this->_db->query('SET @pertinenceId = ?,@userId = ?;', array($pertinenceId, $userId));
    $new    = $this->_db->query("SELECT (SELECT count(*) FROM pertinencevote as pv INNER JOIN pertinence as p on pv.pertinenceId = p.pertinenceId WHERE p.propositionId = (select propositionId where p.pertinenceId = @pertinenceId) and pv.userId = @userId) as count, (SELECT p.propositionId FROM  pertinence as p WHERE p.propositionId = (select propositionId where p.pertinenceId = @pertinenceId)) as id,(SELECT pr.userId FROM  pertinence as p inner join proposition as pr on p.propositionId = pr.propositionId WHERE p.propositionId = (select p.propositionId where p.pertinenceId = @pertinenceId)) as propUserId");
    $result = $new->first();
    if ($result->count == 0) {
      $keys = array(
      'pertinenceId' => $pertinenceId,
      'userId'       => $userId);
      $propositionId = $result->id;
      $debug         = $this->_db->insert('pertinencevote', $keys);
      //check if it brings the proposition to more than 75% of pertinence
      if ($this->calculatePertinenceScore($propositionId) > 0.75) {
          //create the newsItem
          newsitem::create(array('newsItemTypeId' => 2, 'propositionId' => $result->id));
          //Evaluate if we need a new badge
          badge::evaluate('newPertinenceVote', $result->propUserId);
      }
    }
  }

  public function update($fields)
  {
    if (!$this->_db->update('proposition', $this->_data->propositionId, $fields)) {
      throw new Exception('There was a problem updating.');
    }
  }

  public function updateStatus($value)
  {
    $this->update(array('status' => $value, 'statusUpdate' => date('Y-m-d H:i:s')));
  }

  public function getProblemId()
  {
    $new = $this->_db->query("SELECT f.problemId FROM `facet` as f LEft JOIN pertinence as per on per.facetId = f.facetId LEFT JOIN proposition as p on per.propositionId = p.propositionId WHERE p.propositionId = ? and per.positive = 1 group by f.problemId", array($this->_data->propositionId));
    return $new->first()->problemId;
  }

  public function get($arg)
  {
    return $this->_data->$arg;
  }

  public function create($data)
  {
    $keys = array(
    'solutionId'  => $data['solutionId'],
    'title'       => $data['title'],
    'implemented' => $data['implemented'],
    'userId'      => session::get('user'));
    $debug = $this->_db->insert('proposition', $keys);
    $last  = $this->_db->lastInsertId();
    $this->find($last);
    return $last;
  }

  public function data()
  {
    return $this->_data;
  }

  public function addCommunityLink($communityId, $comment)
  {
    if (null !== $this->_data->propositionId) {
      $query                 = $this->_db->query("UPDATE `communityproposition` SET `statusId`=0,`statusUpdate`=now(), updatedBy = ? WHERE statusId = 1 and propositionId = ?", array(session::get('user'), $this->_data->propositionId));
      $data['propositionId'] = $this->_data->propositionId;
      $data['communityId']   = $communityId;
      $data['statusId']      = 1;
      $data['comment']       = $comment;
      $data['updatedBy']     = session::get('user');
      $this->_db->insert('communityproposition', $data);
    }
  }
  public function stopSupport()
  {
    if (null !== $this->_data->propositionId) {
      $query = $this->_db->query("UPDATE `communityproposition` SET `statusId`=0,`statusUpdate`=now(), updatedBy = ? WHERE statusId = 1 and propositionId = ?", array(session::get('user'), $this->_data->propositionId));
    }
  }

  public function calculatePertinenceScore($propositionId)
  {
    $new = $this->_db->query(
      "SELECT
    100 * SUM(positive) / SUM(total) as pertinenceScore
    FROM
        (SELECT
            count(DISTINCT pertinencevoteId) AS positive,
            p.propositionId,
            NULL AS total

        FROM
            pertinenceVote AS pv
        INNER JOIN
            pertinence AS p
            ON p.pertinenceId = pv.pertinenceId
        INNER JOIN
          proposition AS pro
          ON p.propositionId = pro.propositionId
        WHERE
          p.positive = 1
        AND
          p.propositionId = ?
        UNION
        SELECT
            NULL AS positive,
            p.propositionId,
            count(DISTINCT pertinencevoteId) AS total
        FROM
            pertinenceVote AS pv
        INNER JOIN
            pertinence AS p
                ON p.pertinenceId = pv.pertinenceId
        INNER JOIN
            proposition AS pro
                ON p.propositionId = pro.propositionId
        WHERE
            p.propositionId = ?
    ) AS counts",
      array($propositionId, $propositionId)
    );
    return $new->first()->pertinenceScore;
  }
}
