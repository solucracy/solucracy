<?php
// copyright (c) 2018 - Yannick machin <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class email
{

  private $senderName,
          $senderEmail,
          $recipientName,
          $error,
          $recipientEmail,
          $mailer;


  public function setArg($arg, $value)
  {
    $this->$arg = $value;
  }

  public function error()
  {
    echo $this->error;
  }

  public function initiateMailer()
  {
    include_once config::get('root_path') . 'inc/Swift-5.0.3/lib/swift_required.php';
    // Create the Transport the call setUsername() and setPassword()
     $transport = Swift_SmtpTransport::newInstance(config::get('smtp'), config::get('smtpPort'))
      ->setUsername(config::get('sendAddress'))
      ->setPassword(config::get('sendAddressPwd'));

    // Create the Mailer using your created Transport
    $this->mailer = Swift_Mailer::newInstance($transport);
  }

  public function sendContactEmail($name, $email, $message)
  {
    $this->initiateMailer();


    $email_body = "";
    $email_body = $email_body . "Name: " . $name . "<br>";
    $email_body = $email_body . "Email: " . $email . "<br>";
    $email_body = $email_body . "Message: " . $message;

            // Create the message
    $feedback = Swift_Message::newInstance()

      // Give the message a subject
      ->setSubject('Solucracy')

      // Set the From address with an associative array
      ->setFrom(array($email => $name))

      // Set the To addresses with an associative array
      ->setTo(array(config::get('sendAddress') => 'solucracy'))

      // Give it a body
      ->setBody($email_body, 'text/html');



    // if the email is sent successfully, return true;
    // otherwise, set a new error message
    if (!$this->mailer->send($feedback, $failures)) {
        $this->error = $failures;
        return false;
    } else {
        return true;
    }
  }
  public function invite($email)
  {
    $this->initiateMailer();
    $user = new user();
    $this->recipientEmail = $email;
    $this->recipientName = "Invité";
    $emailLink = '<a href="https://solucracy.com/homepage.php" target="_blank">'._("View it in your browser").'.</a>';

    $email_body = file_get_contents('../templates/invitemail.html', FILE_USE_INCLUDE_PATH);

       // Create the message
    $feedback = Swift_Message::newInstance()

      // Give the message a subject
      ->setSubject($user->data()->userName." vous a envoyé une invitation sur Solucracy")

      //make it an html message
      ->setContentType("text/html")

      // Set the From address with an associative array
      ->setFrom(array(config::get('noReply') => 'Solucracy'))

      // Set the To addresses with an associative array
      ->setTo(array($this->recipientEmail => $this->recipientName))

      // Give it a body
      ->setBody($email_body, 'text/html');



            //log the result for each email
    if (!$this->mailer->send($feedback, $failures)) {
      $this->error = $failures;
      return false;
    } else {
        return true;
    }
  }

  public function inviteCommunity($email)
  {
    $this->initiateMailer();
    $user = new user();
    $this->recipientEmail = $email;
    $this->recipientName = "Invité";
    $emailLink = '<a href="https://solucracy.com/homepage.php" target="_blank">'._("View it in your browser").'.</a>';

    $email_body = file_get_contents('../templates/communityinvitemail.html', FILE_USE_INCLUDE_PATH);

    $variables = array('{{communityName}}' => session::get('communityName'),'{{newsLetter}}' => 'https://solucracy.com/homepage.php?email='.$email,'{{link}}' => 'https://solucracy.com/communityprofile.php?communityId='.session::get('communityAdmin'));
    foreach ($variables as $key => $value) {
          $email_body = str_replace($key, $value, $email_body);
    }

       // Create the message
    $feedback = Swift_Message::newInstance()

      // Give the message a subject
      ->setSubject(session::get('communityName'). _("sent you an invitation on Solucracy.com"))

      //make it an html message
      ->setContentType("text/html")

      // Set the From address with an associative array
      ->setFrom(array(config::get('noReply') => 'Solucracy'))

      // Set the To addresses with an associative array
      ->setTo(array($this->recipientEmail => $this->recipientName))

      // Give it a body
      ->setBody($email_body, 'text/html');



            //log the result for each email
    if (!$this->mailer->send($feedback, $failures)) {
      $this->error = $failures;
      return false;
    } else {
       return true;
    }
  }

  public function deactivateCommunity($code, $email)
  {
      $this->initiateMailer();
      $user = new user();
      $this->recipientEmail = $email;
      $this->recipientName = _("Administration");
      $emailLink = '<a href="https://solucracy.com/homepage.php" target="_blank">'._("View it in your browser").'.</a>';

    $email_body = file_get_contents('../templates/cancelCommunity.html', FILE_USE_INCLUDE_PATH);

    $variables = array('{{communityName}}' => session::get('communityName'),'{{userName}}' => $user->get('userName'),'{{url}}'=>'https://solucracy.com/reactivatecommunity.php?code='.$code,'{{newsLetter}}' => 'https://solucracy.com/homepage.php?email='.$email);
    foreach ($variables as $key => $value) {
          $email_body = str_replace($key, $value, $email_body);
    }

       // Create the message
    $feedback = Swift_Message::newInstance()

      // Give the message a subject
      ->setSubject(session::get('communityName')._("View it in your browser"))

      //make it an html message
      ->setContentType("text/html")

      // Set the From address with an associative array
      ->setFrom(array(config::get('noReply') => 'Solucracy'))

      // Set the To addresses with an associative array
      ->setTo(array($this->recipientEmail => $this->recipientName))

      // Give it a body
      ->setBody($email_body, 'text/html');



            //log the result for each email
    if (!$this->mailer->send($feedback, $failures)) {
      $this->error = $failures;
      return false;
    } else {
       return true;
    }
  }

  public function sendNewsletter($data)
  {

    $this->initiateMailer();
    $this->recipientEmail = $data['email'];
    $this->recipientName = 'un-e gentil-le Solucrate';
    $email_body = file_get_contents(config::get('root_path').'templates/newsletter.html', FILE_USE_INCLUDE_PATH);
    $variables = array('{{header}}' => _("Solucracy : New blog article"),'{{article_intro}}' => $data['text'],'{{_("View it in your browser")}}' => $data['link'],'{{newsLetter}}' => 'https://solucracy.com/homepage.php?email='.$data['email'],'{{article_link}}'=>$data['link']);
    foreach ($variables as $key => $value) {
      $email_body = str_replace($key, $value, $email_body);
    }
     // Create the message
    $feedback = Swift_Message::newInstance()

    // Give the message a subject
    ->setSubject(_("Solucracy : New blog article"))

    //make it an html message
    ->setContentType("text/html")

    // Set the From address with an associative array
    ->setFrom(array(config::get('noReply') => 'Solucracy'))

    // Set the To addresses with an associative array
    ->setTo(array($this->recipientEmail => $this->recipientName))

    // Give it a body
    ->setBody($email_body, 'text/html');



          //log the result for each email
    if (!$this->mailer->send($feedback, $failures)) {
        $this->error = $failures;
        return false;
    } else {
        return true;
    }
  }

  public function sendNotification($data)
  {
    $this->initiateMailer();
    $user = new user($data->userId);
    $this->recipientEmail = $user->data()->email;
    $this->recipientName = $user->data()->userName;
    $emailLink = '<a href="https://solucracy.com/homepage.php" target="_blank">'._("View it in your browser").'.</a>';

    $email_body = file_get_contents(config::get('root_path').'templates/notifemail.html', FILE_USE_INCLUDE_PATH);

    $notificationText = '<div style="border: 2px solid #5BC839; margin: 5px;"><h3 class="h3" style="margin: 5px;">'.$data->title.'</h3><p style="margin: 5px;">"'.$data->description.'"</p><br/><br/><a href="https://solucracy.com/'.$data->link.'" target="_blank" style="text-decoration: none; color: #FFF; background-color: #5BC839; padding: 5px;">'._("Click here").'</a><br /></div>';

    $variables = array('{{header}}' => _("Here's what might require your attention"),'{{notificationText}}' => $notificationText,'{{view in your browser}}' => $emailLink,'{{newsLetter}}' => 'https://solucracy.com/homepage.php?email='.$email);
    foreach ($variables as $key => $value) {
          $email_body = str_replace($key, $value, $email_body);
    }

     // Create the message
    $feedback = Swift_Message::newInstance()

    // Give the message a subject
    ->setSubject(_("A few things happened on Solucracy"))

    //make it an html message
    ->setContentType("text/html")

    // Set the From address with an associative array
    ->setFrom(array(config::get('noReply') => 'solucracy'))

    // Set the To addresses with an associative array
    ->setTo(array($this->recipientEmail => $this->recipientName))

    // Give it a body
    ->setBody($email_body, 'text/html');



          //log the result for each email
    if (!$this->mailer->send($feedback, $failures)) {
      $this->error = $failures;
      return false;
    } else {
        return true;
    }
  }

  public function sendNotifications($notifications)
  {
      $this->initiateMailer();
      $this->recipientEmail = $notifications[0]->email;
      $this->recipientName = $notifications[0]->userName;
      $emailLink = '<a href="https://solucracy.com/homepage.php" target="_blank">'._("View it in your browser").'.</a>';

      $email_body = file_get_contents(config::get('root_path').'templates/notifemail.html', FILE_USE_INCLUDE_PATH);
      $notificationText = "";

    foreach ($notifications as $notification) {
      $notificationText .= '<div style="border: 2px solid #5BC839; margin: 5px;"><h3 class="h3" style="margin: 5px;">'.$notification->title.'</h3><p style="margin: 5px;">"'.$notification->description.'"</p><br/><br/><a href="https://solucracy.com/'.$notification->link.'" target="_blank" style="text-decoration: none; color: #FFF; background-color: #5BC839; padding: 5px;">'._("Click here").'</a><br /></div>';
    }

      $variables = array('{{header}}' => _("Here's what might require your attention"),'{{notificationText}}' => $notificationText,'{{view in your browser}}' => $emailLink,'{{newsLetter}}' => 'https://solucracy.com/homepage.php?email='.$notifications[0]->email);
    foreach ($variables as $key => $value) {
      $email_body = str_replace($key, $value, $email_body);
    }

       // Create the message
      $feedback = Swift_Message::newInstance()

      // Give the message a subject
      ->setSubject(_("A few things happened on Solucracy"))

      //make it an html message
      ->setContentType("text/html")

      // Set the From address with an associative array
      ->setFrom(array(config::get('noReply') => 'Solucracy'))

      // Set the To addresses with an associative array
      ->setTo(array($this->recipientEmail => $this->recipientName))

      // Give it a body
      ->setBody($email_body, 'text/html');
            //log the result for each email
    if (!$this->mailer->send($feedback, $failures)) {
        $this->error = $failures;
        return $this->error;
    } else {
       return true;
    }
  }

  public function sendValidationEMail($email, $userName)
  {
    $this->initiateMailer();

    // the form has fields for name, email, and message
    $saltedEmail = md5($email . config::get('salt'));
    $this->recipientEmail = $email;
    $this->recipientName = $userName;
    $emailLink = '<a href="https://solucracy.com/homepage.php?validationEmail='.$saltedEmail.'" target="_blank">'._("View it in your browser").'.</a>';

    $email_body = file_get_contents(config::get('root_path').'templates/validateemail.html', FILE_USE_INCLUDE_PATH);
    $notificationText = '<div style="margin: 5px;"><h3 class="h3" style="margin: 5px;"><a href="https://solucracy.com/homepage.php?validationEmail='.$saltedEmail.'" target="_blank" style="text-decoration: none; color: #FFF; background-color: #5BC839; padding: 5px;">'._("Click here").'</a></h3></div>';

    $variables = array('{{header}}' => _("Welcome to Solucracy! Please confirm your email."),'{{notificationText}}' => $notificationText,'{{view in your browser}}' => $emailLink,'{{newsLetter}}' => 'https://solucracy.com/homepage.php?email='.$email);
    foreach ($variables as $key => $value) {
      $email_body = str_replace($key, $value, $email_body);
    }
    $subject = _("Welcome to Solucracy! Please confirm your email.");

    // Create the message
    $feedback = Swift_Message::newInstance()
    //make it an html message
      ->setContentType("text/html")
    // Give the message a subject
      ->setSubject($subject)
    // Set the From address with an associative array
      ->setFrom(array(config::get('noReply') => 'solucracy'))
    // Set the To addresses with an associative array
      ->setTo($email)
    // Give it a body
      ->setBody($email_body, 'text/html');

    // if the email is sent successfully, return True
      // otherwise, set a new error message and return False
    if (!$this->mailer->send($feedback, $failures)) {
      helper::logError($failures);
      return false;
    } else {
      return true;
    }
  }

  public function passwordRequest($email, $passwordRequestId)
  {
    $this->initiateMailer();
    $user = new user();
    $user->find($email);
    $this->recipientEmail = $email;
    $this->recipientName = $user->get('userName');
    $emailLink = '<a href="https://solucracy.com/homepage.php?passwordId='. $passwordRequestId .'" target="_blank">'._("View it in your browser").'.</a>';

    $email_body = file_get_contents(config::get('root_path').'templates/resetpassword.html', FILE_USE_INCLUDE_PATH);

    $variables = array('{{header}}' => _("Here's what might require your attention"),'{{title}}' => _("Password request"),'{{view in your browser}}' => $emailLink,'{{button}}'=>_("Click here"),'{{url}}'=>'https://solucracy.com/homepage.php?passwordId='. $passwordRequestId,'{{newsLetter}}' => 'https://solucracy.com/homepage.php?email='.$email);
    foreach ($variables as $key => $value) {
          $email_body = str_replace($key, $value, $email_body);
    }

     // Create the message
    $feedback = Swift_Message::newInstance()

    // Give the message a subject
    ->setSubject(_("A few things happened on Solucracy"))

    //make it an html message
    ->setContentType("text/html")

    // Set the From address with an associative array
    ->setFrom(array(config::get('noReply') => 'Solucracy'))

    // Set the To addresses with an associative array
    ->setTo(array($this->recipientEmail => $this->recipientName))

    // Give it a body
    ->setBody($email_body, 'text/html');



          //log the result for each email
    if (!$this->mailer->send($feedback, $failures)) {
      $this->error = $failures;
      return false;
    } else {
        return true;
    }
  }
}
