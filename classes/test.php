<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class test
{

  private $_db, $adresses = array('[{"name":"54110","type":"postal_code"},{"name":"France","type":"country"},{"name":"Grand Est","type":"administrative_area_level_1"},{"name":"Meurthe-et-Moselle","type":"administrative_area_level_2"},{"name":"Champenoux","type":"locality"},{"name":"Unnamed Road","type":"route"}]', '[{"name":"39600","type":"postal_code"},{"name":"France","type":"country"},{"name":"Bourgogne Franche-Comté","type":"administrative_area_level_1"},{"name":"Jura","type":"administrative_area_level_2"},{"name":"La Châtelaine","type":"locality"},{"name":"D469","type":"route"},{"name":"2","type":"street_number"}]', '[{"name":"38250","type":"postal_code"},{"name":"France","type":"country"},{"name":"Auvergne-Rhône-Alpes","type":"administrative_area_level_1"},{"name":"Isère","type":"administrative_area_level_2"},{"name":"Villard-de-Lans","type":"locality"},{"name":"Unnamed Road","type":"route"}]', '[{"name":"10060","type":"postal_code"},{"name":"Italie","type":"country"},{"name":"Piemonte","type":"administrative_area_level_1"},{"name":"Città Metropolitana di Torino","type":"administrative_area_level_2"},{"name":"Inverso Pinasca","type":"administrative_area_level_3"},{"name":"Vivian","type":"locality"},{"name":"Borgata Don","type":"route"},{"name":"10","type":"street_number"}]', '[{"name":"27020","type":"postal_code"},{"name":"Italie","type":"country"},{"name":"Lombardia","type":"administrative_area_level_1"},{"name":"Provincia di Pavia","type":"administrative_area_level_2"},{"name":"Zerbolò","type":"administrative_area_level_3"},{"name":"Strada Provinciale 3","type":"route"},{"name":"3","type":"street_number"}]', '[{"name":"41045","type":"postal_code"},{"name":"Italie","type":"country"},{"name":"Emilia-Romagna","type":"administrative_area_level_1"},{"name":"Provincia di Modena","type":"administrative_area_level_2"},{"name":"Montefiorino","type":"administrative_area_level_3"},{"name":"Unnamed Road","type":"route"}]', '[{"name":"69126","type":"postal_code"},{"name":"France","type":"country"},{"name":"Auvergne-Rhône-Alpes","type":"administrative_area_level_1"},{"name":"Rhône","type":"administrative_area_level_2"},{"name":"Brindas","type":"locality"},{"name":"Chemin des Vignes Rouges","type":"route"},{"name":"52","type":"street_number"}]', '[{"name":"16500","type":"postal_code"},{"name":"France","type":"country"},{"name":"Nouvelle-Aquitaine","type":"administrative_area_level_1"},{"name":"Charente","type":"administrative_area_level_2"},{"name":"Saint-Maurice-des-Lions","type":"locality"},{"name":"D948","type":"route"}]', '[{"name":"40420","type":"postal_code"},{"name":"France","type":"country"},{"name":"Nouvelle-Aquitaine","type":"administrative_area_level_1"},{"name":"Landes","type":"administrative_area_level_2"},{"name":"Brocas","type":"locality"},{"name":"Unnamed Road","type":"route"}]'), $keywords = '{{test|key|word|pourquoi|rhododendron|George|Moustaki|epingle|rogntudju}}', $template = '{{une {{incroyable|magnifique|improbable}} méthode|un moyen infaillible|Le cas {{le plus simple|le moins bleu|hallucinant|hypocondriaque}} |Ma {{technique|methode|ruse}}}} pour tester ce {{putain de site|sacré problème|truc}}', $voteText = '{{{{parce que|à cause que|malgré que}} {{ca chatouille|ça gratouille|ca piquouille}} {{mes pieds|mes genoux|mon cou}}}} ', $reasons = '{{{{tout le monde|je|Mon médecin}} {{pense|sait|croit}} {{que ca se passe comme ça|que plus y en a mieux c\est|que ta maman déteste ça}}}} ', $name = '{{{{jean|Yves|Daniel}}-{{Benoit|Paul|Bob}}}} ', $email = '{{{{bisounours|Jayce|truc}}@{{machin|pouet|bof}}.{{com|fr|cul}}}} ';

  function __construct()
  {
  $this->_db = db::getInstance();
  }

  public function create($type, $amount)
  {
  $result = '';
  $i      = 0;
  $data   = array();
  while ($i <= $amount) {
    $userId = $this->selectRandom('user', 'userId', 1);
    $user   = new user($userId);
    session::put('user', $userId);
    session::put('latitude', $user->get('latitude'));
    session::put('longitude', $user->get('longitude'));
    switch ($type) {
    case 'problem':
      $problem             = new problem();
      $data['title']       = $this->makeSentenceFromTemplate($this->template);
      $data['description'] = $this->makeSentenceFromTemplate($this->template);
      $data['problemTags'] = $this->makeSentenceFromTemplate($this->keywords) . ',' . $this->makeSentenceFromTemplate($this->keywords);
      //de temps en temps ajouter une communityId et de temps en temps, mettre le problème en private
      if ($i % 2 == 0) {
      $data['communityId'] = rand(3, 35000);
      if ($data['communityId'] % 2 == 0) {
        $data['private'] = 'on';
      } else {
        $data['private'] = 'on';
      }
      }
      $data['vote']       = $this->makeSentenceFromTemplate($this->voteText);
      $data['categoryId'] = rand(1, 10);
      $lat                = rand(-90, +90);
      $lon                = rand(-180, +180);
      $data['coord']      = '(' . $lat . ',' . $lon . ')';
      $data['address']    = $this->adresses[substr($i, -1)];
      $result .= var_dump($problem->create($data));
      break;
    case 'solution':
      $solution              = new solution();
      $data['solutionTags']  = $this->makeSentenceFromTemplate($this->keywords) . ',' . $this->makeSentenceFromTemplate($this->keywords);
      $data['title']         = $this->makeSentenceFromTemplate($this->template);
      $data['description']   = $this->makeSentenceFromTemplate($this->template);
      $data['avantages']     = $this->makeSentenceFromTemplate($this->voteText);
      $data['inconvenients'] = $this->makeSentenceFromTemplate($this->voteText);
      if ($i % 2 == 0) {
      $data['oneOff'] = 1;
      } else {
      $data['oneOff'] = 0;
      }
      $data['categoryId'] = rand(1, 10);
      $result .= $solution->create($data);
      break;
    case 'proposition':
      $proposition        = new proposition();
      $data['solutionId'] = $this->selectRandom('solution', 'solutionId', 1);
      $data['title']      = $this->makeSentenceFromTemplate($this->template);
      $facetIds           = $this->selectRandom('facet', 'facetId', 2);
      $array              = explode(',', $facetIds);
      if (helper::propositionExists($data['solutionId'], $facetIds)) {
      } else {
      $proposition->create($data);
      foreach ($array as $facetId) {
        $proposition->addPertinence(array(
        'facetId' => $facetId,
        'positive' => 1
        ));
      }
      }

      break;

    case 'problemVote':
      $problemId = $this->selectRandom('problem', 'problemId', 1);
      $problem   = new problem($problemId);
      $facets    = $problem->getFacets();
      $vote      = new vote();
      if (!$vote->alreadyVoted($problemId)) {
      if ($i % 2 != 0) {
        $text     = $this->makeSentenceFromTemplate($this->voteText);
        $newFacet = array(
        'description' => $text,
        'problemId' => $problemId
        );
        $facetId  = $problem->addFacet($newFacet);
      } else {
        $facets  = $problem->getFacets();
        $nb      = rand(1, count($facets));
        $facetId = $facets[$nb - 1]->facetId;
      }
      $vote->create($facetId);
      }
      break;
    case 'propositionVote':
      $query  = $this->_db->query("select DISTINCT per.propositionId, f.facetId from pertinence as per inner Join facet as f on f.facetId = per.facetId inner join vote as v on v.facetId = f.facetId and v.userId = ?", array(
      session::get('user')
      ));
      $result = $query->results();
      if (count($result) > 0) {
      foreach ($result as $item) {
        $proposition                    = new proposition();
        $newPertinence['propositionId'] = $item->propositionId;
        $newPertinence['facetId']       = $item->facetId;
        $newPertinence['reason']        = $this->makeSentenceFromTemplate($this->reasons);
        if ($i % 2 == 0) {
        $newPertinence['positive'] = 0;
        } else {
        $newPertinence['positive'] = 1;
        }
        $proposition->addPertinence($newPertinence);
      }

      }
      break;
    case 'user':
      $user     = new user();
      // if checks are ok, create all variables
      $password = md5('password123' . config::get('salt'));

      // insert new user in db
      $last = $this->_db->insert('user', array(
      'userName' => $this->makeSentenceFromTemplate($this->name) . rand(0, 4200),
      'password' => $password,
      'language' => 'FR',
      'latitude' => rand(-90, +90),
      'longitude' => rand(-180, +180),
      'email' => rand(0, 4200) . $this->makeSentenceFromTemplate($this->email),
      'statusId' => 1
      ));
      $last = $this->_db->lastInsertId();
      $user->addUserRole($last, 'verified');
      $user->addUserRole($last, 'utilisateur');
    //
    default:
      # code...
      break;
    }
    $i++;
  }
  return $result;
  }
  //from https://github.com/lastguest/DynamicString
  public function makeSentenceFromTemplate($template)
  {
  while (strpos($template, '{{') !== false) {
    $template = preg_replace_callback('({{([^{}]+)}})', function($m)
    {
    $choice = array_rand($options = explode('|', $m[1]), 1);
    return $options[$choice];
    }, $template);
  }
  return $template;
  }

  public function selectRandom($table, $column, $nb)
  {
  $query   = $this->_db->query("SELECT " . $column . " FROM " . $table . " WHERE 1 ORDER BY RAND() LIMIT " . $nb);
  $results = $query->results();
  $data    = array();
  foreach ($results as $key) {
    array_push($data, $key->$column);
  }
  return implode(',', $data);
  }
}
