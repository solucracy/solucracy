<?php

// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/

class db
{
  public static $instance = null;

  private $_pdo = null, $_query = null, $_error = false, $_debug = null, $_results = null, $_count = 0, $_lastInsertId = null;
  private function __construct()
  {
    try {
      $this->_pdo = new PDO('mysql:host=' . config::get('mysql/host') . ';charset=UTF8;dbname=' . config::get('mysql/db'), config::get('mysql/username'), config::get('mysql/password'));
    } catch (PDOExeption $e) {
      die($e->getMessage());
    }
  }

  public static function getInstance()
  {

    // Already an instance of this? Return, if not, create.

    if (!isset(self::$instance)) {
      self::$instance = new db();
    }

    return self::$instance;
  }

  public function query($sql, $params = array())
  {
    $this->_error = false;
    if ($this->_query = $this->_pdo->prepare($sql)) {
      $x = 1;
      if (count($params)) {
        foreach ($params as $param) {
          $this->_query->bindValue($x, $param);
          $x++;
        }
      }

      if ($this->_query->execute()) {
        $this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
        $this->_count = $this->_query->rowCount();
      } else {
        $this->_error = true;
      }

      $this->_debug = array(
      'query' => $sql,
      'parameters' => implode("|", $params)
      );
    }

    return $this;
  }

  public function lastInsertId()
  {
    return $this->_lastInsertId;
  }

  public function debug()
  {
    return $this->_debug;
  }

  public function get($table, $where)
  {
    return $this->action('SELECT *', $table, $where);
  }

  public function delete($table, $where)
  {
    return $this->action('DELETE', $table, $where);
  }

  public function action($action, $table, $where = array())
  {
    if (count($where) === 3) {
      $operators = array(
      '=',
      '>',
      '<',
      '>=',
      '<='
      );
      $field = $where[0];
      $operator = $where[1];
      $value = $where[2];
      if (in_array($operator, $operators)) {
          $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";
        if (!$this->query(
          $sql,
          array(
            $value
          )
        )->error()
        ) {
            return $this;
        }
      }

      return false;
    }
  }

  public function insert($table, $fields = array())
  {
    $keys = array_keys($fields);
    $values = null;
    $x = 1;
    foreach ($fields as $value) {
      $values.= "?";
      if ($x < count($fields)) {
        $values.= ', ';
      }

      $x++;
    }

    $sql = "INSERT IGNORE INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES ({$values})";
    if (!$this->query($sql, $fields)->error()) {
      $this->_lastInsertId = $this->_pdo->lastInsertId();
      return true;
    } else {
      return $this->debug();
    }

    return false;
  }

  public function update($table, $id, $fields = array())
  {
    $set = null;
    $x = 1;
    foreach ($fields as $name => $value) {
      $set.= "{$name} = ?";
      if ($x < count($fields)) {
        $set.= ', ';
      }

      $x++;
    }

    $sql = "UPDATE {$table} SET {$set} WHERE {$table}Id = {$id}";
    if (!$this->query($sql, $fields)->error()) {
      return true;
    }

    return $fields;
  }

  public function results()
  {

    // Return result object

    return $this->_results;
  }

  public function first()
  {
    return $this->_results[0];
  }

  public function count()
  {

    // Return count

    return $this->_count;
  }

  public function error()
  {
    return $this->_error;
  }
}
