<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class form
{

  private $_db;

  public function __construct($id = null)
  {
    $this->_db = db::getInstance();
  }

  public function createField($type, $name, $translationId, $placeholder, $values = null)
  {
    $field         = "";
    $translationId = (is_numeric($translationId)) ? _($translationId) : $translationId;
    $placeholder   = (is_numeric($placeholder)) ? _($placeholder) : $placeholder;
    if ($type === 'swap') {
      $field = '<div class="ml-4 p-2"><div class="row">' . $translationId . '</div><div class="row swap greenBorder"><input type="checkbox" ' . $values . ' name="' . $name . '" id="' . $name . '" class="hidden"><label for="' . $name . '" class="slider green"></label></div></div>';
    } elseif ($type === 'hidden') {
      $field = '<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $values . '"/>';
    } elseif ($type === 'submit') {
      $field = '<input type="button" id="' . $name . '" value="' . $translationId . '" class="solucracy_btn submit" onclick="' . $values . '" />';
    } elseif ($type === 'link') {
      $field = '<a name="' . $name . '" id="' . $name . '" href="' . $values . '" target="_blank" class="link ml-4"><u>' . $translationId . '</u></a>';
    } elseif ($type === 'captcha') {
      $text  = helper::createCaptcha();
      $field = '<div class="ml-4 p-2"><label for="' . $name . '" class="row">' . $translationId . '</label>
              <div class="row">
              <img id="captchaImage" src="img/' . $text . '.jpeg"><input type="text" name="' . $name . '" id="' . $name . '" class="validate grayBorder" data-toggle="tooltip" data-placement="bottom" title="' . $placeholder . '" placeholder="' . $placeholder . '"> <i class="fas fa-2x fa-sync-alt font_green m-2 clickable" onclick="ajax(\'processnew.php\',{type : \'reloadCaptcha\'},\'reloadCaptcha\');" data-toggle="tooltip" data-placement="bottom" title="' . _("Refresh the picture") . '"></i>
              </div></div>';
    } elseif ($type === 'button') {
      $field = '<div class="d-flex ml-4 p-2"><button id="' . $name . '" class="solucracy_btn align-self-end" onclick="' . $values . '">' . $translationId . '</button></div>';
    } else {
      $field = '<div class="ml-4 p-2"><label for="%name%" class="row">%translation%</label>
              <div %wrapperStart% class="row">
              <%input% type="%type%" name="%name%" id="%name%" class="validate grayBorder w-75" placeholder="%placeholder%" %extra% >%values%</%input%>%wrapperEnd%
              </div></div>';
      $field = str_replace('%name%', $name, $field);
      $field = str_replace('%type%', $type, $field);
      $field = str_replace('%translation%', $translationId, $field);
      $field = str_replace('%placeholder%', $placeholder, $field);
      if ($type === 'textarea') {
        $field = str_replace('%extra%', 'rows="4" cols="50"', $field);
        $field = str_replace('%input%', 'textarea', $field);
        $field = str_replace('%values%', '', $field);
        $field = str_replace('%wrapperStart%', '', $field);
        $field = str_replace('%wrapperEnd%', '', $field);
      } elseif ($type === 'select') {
        $field       = str_replace('%input%', 'select', $field);
        $field       = str_replace('%wrapperStart%', '', $field);
        $field       = str_replace('%extra%', '', $field);
        $field       = str_replace('%wrapperEnd%', '', $field);
        $selectItems = '<option class="text-muted" value="" selected>' . _("Please select here") . '</option>';
        foreach ($values as $value) {
          $selectItems .= helper::render('selectValues', $value);
        }
        $field = str_replace('%values%', $selectItems, $field);
      } else {
        $field = str_replace('%input%', 'input', $field);
        $field = str_replace('%extra%', '', $field);
        $field = str_replace('%values%', '', $field);
        if ($name === 'location') {
          $field = str_replace('%wrapperStart%', 'id="locationSearch"', $field);
          $field = str_replace('%wrapperEnd%', '<span><i class="fa fa-search fa-2x font_white green p-1"></i></span>', $field);
        } else {
          $field = str_replace('%wrapperStart%', '', $field);
          $field = str_replace('%wrapperEnd%', '', $field);
        }
      }
    }

    return $field;
  }

  public function createUnvalidatedField($type, $name, $translationId, $placeholder, $values = null)
  {
    $field         = "";
    $translationId = (is_numeric($translationId)) ? _($translationId) : $translationId;
    $placeholder   = (is_numeric($placeholder)) ? _($placeholder) : $placeholder;
    if ($type === 'swap') {
      $field = '<div class="ml-4 p-2"><div class="row ">' . $translationId . '</div><div class="row swap greenBorder"><input type="checkbox"  ' . $values . ' name="' . $name . '" id="' . $name . '" class="hidden"><label for="' . $name . '" class="slider green"></label></div></div>';
    } elseif ($type === 'hidden') {
      $field = '<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $values . '"/>';
    } elseif ($type === 'submit') {
      $field = '<input type="button" id="' . $name . '" value="' . $translationId . '" class="solucracy_btn submit" onclick="' . $values . '" />';
    } elseif ($type === 'link') {
      $field = '<a name="' . $name . '" id="' . $name . '" href="' . $values . '" target="_blank" class="ml-4"><u>' . $translationId . '</u></a>';
    } elseif ($type === 'button') {
      $field = '<div class="d-flex ml-4 p-2"><button id="' . $name . '" class="solucracy_btn align-self-end" onclick="' . $values . '">' . $translationId . '</button></div>';
    } else {
      $field = '<div class="ml-4 p-2"><label for="%name%" class="row">%translation%</label>
              <div %wrapperStart% class="row">
              <%input% type="%type%" name="%name%" id="%name%" class="grayBorder w-75" placeholder="%placeholder%" %extra% >%values%</%input%>%wrapperEnd%
              </div></div>';
      $field = str_replace('%name%', $name, $field);
      $field = str_replace('%type%', $type, $field);
      $field = str_replace('%translation%', $translationId, $field);
      $field = str_replace('%placeholder%', $placeholder, $field);
      if ($type === 'textarea') {
        $field = str_replace('%extra%', 'rows="4" cols="50"', $field);
        $field = str_replace('%input%', 'textarea', $field);
        $field = str_replace('%values%', '', $field);
        $field = str_replace('%wrapperStart%', '', $field);
        $field = str_replace('%wrapperEnd%', '', $field);
      } elseif ($type === 'select') {
        $field       = str_replace('%input%', 'select', $field);
        $field       = str_replace('%wrapperStart%', '', $field);
        $field       = str_replace('%extra%', '', $field);
        $field       = str_replace('%wrapperEnd%', '', $field);
        $selectItems = '<option class="text-muted" value="" selected disabled hidden >' . _("Please select here") . '</option>';
        foreach ($values as $value) {
          $selectItems .= helper::render('selectValues', $value);
        }
        $field = str_replace('%values%', $selectItems, $field);
      } else {
        $field = str_replace('%input%', 'input', $field);
        $field = str_replace('%extra%', '', $field);
        $field = str_replace('%values%', '', $field);
        if ($name === 'location') {
          $field = str_replace('%wrapperStart%', 'id="locationSearch"', $field);
          $field = str_replace('%wrapperEnd%', '<span><i class="fa fa-search font_red"></i></span>', $field);
        } else {
          $field = str_replace('%wrapperStart%', '', $field);
          $field = str_replace('%wrapperEnd%', '', $field);
        }
      }
    }

    return $field;
  }
}
