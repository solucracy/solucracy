<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class solution
{

  private $_db, $_data, $comments, $_list;

  function __construct($id = null)
  {
    $this->_db = db::getInstance();
    if (isset($id) and is_numeric($id)) {
      $this->find($id);
      $this->_data->tags = explode(',', $this->_data->tags);
    }
  }

  public function update($fields)
  {
    $query = $this->_db->update('solution', $this->_data->solutionId, $fields);
  }

  public function updateStatus($value)
  {
    $this->update(
      array(
      'statusId' => $value,
      'statusUpdate' => date('Y-m-d H:i:s')
      )
    );
  }

  public function get($arg)
  {
    return $this->_data->$arg;
  }

  public function find($id)
  {
    $new         = $this->_db->query(
      "SELECT s.*,DATE_FORMAT(s.createdOn,'%d %m %Y') as createdOn, u.userName, ca.icon as categoryIcon,
          (SELECT GROUP_CONCAT(t.name) from solution as sol
          LEFT JOIN solution_tag as st on sol.solutionId = st.solutionId
          LEFT JOIN tag as t on t.tagId = st.tagId where sol.solutionId = s.solutionId) as tags
          FROM solution AS s
            INNER JOIN user AS u ON s.userId = u.userId
            INNER JOIN category as ca  on s.categoryId = ca.categoryId
            where s.solutionId =  ?",
      array(
      $id
      )
    );
    $this->_data = $new->first();
  }

  public function data()
  {
    return $this->_data;
  }

  public function exists()
  {
    return (!empty($this->_data)) ? true : false;
  }

  public function create($data)
  {
    $keywords = explode(",", helper::test_input($data['solutionTags']));
    unset($data['solutionTags']);
    $data['userId'] = session::get('user');
    $debug          = $this->_db->insert('solution', $data);
    $last           = $this->_db->lastInsertId();
    $this->find($last);
    $tags = new tags();
    $tags->add($keywords);

    // Link each tag to the solution
    foreach ($keywords as $keyword) {
      $this->addTag($last, $keyword);
    }
    return $last;
  }

  public function addVote($val, $comment)
  {
    if (!$this->hasVoted()) {
      $data = array(
      'createdOn' => date('Y-m-d H:i:s'),
      'solutionId' => $this->_data->solutionId,
      'userId' => session::get('user'),
      'comment' => $comment,
      'value' => $val
      );
      $this->_db->insert('solutionvote', $data);
      $last = $this->_db->lastInsertId();
      if (isset($last)) {
          return true;
      }
      return false;
    } else {
      $this->_db->query(
        "update solutionvote  set value = ?, createdOn = CURRENT_TIMESTAMP, comment = ? WHERE
          solutionId = ? and userId = ?",
        array(
        $val,
        $comment,
        $this->_data->solutionId,
        session::get('user')
        )
      );
      if ($this->_db->error()) {
        return false;
      } else {
        return true;
      }
    }
  }

  public function hasVoted()
  {
    $query = $this->_db->query(
      "SELECT * FROM solutionvote WHERE solutionId = ? AND
      userId = ?",
      array(
      $this->_data->solutionId,
      session::get('user')
      )
    );
    if ($query->count() > 0) {
      return true;
    }
    return false;
  }

  public function getComments()
  {
    $query = $this->_db->query(
      "select sv.*, DATE_FORMAT(sv.createdOn,'%d %m %Y') as createdOn, u.userName from solutionvote as sv inner Join user as u on sv.userId = u.userId
      where solutionId = ?
      group by solutionVoteId",
      array(
      $this->_data->solutionId
      )
    );
    return $query->results();
  }


  public function addTag($solutionId, $tag)
  {
    $this->_db->query(
      'INSERT into solution_tag  set
      solutionId = ?,
      tagId = ( select tagId from tag where name = ? )',
      array(
      $solutionId,
      $tag
      )
    );
  }


  public function getList($id = null)
  {

    $query = $this->_db->query(
      "SELECT s.solutionId, s.title, s.description, s.problemId, s.userId, u.userName, DATE_FORMAT(s.createdOn,'%d %m %Y') as createdOn,
      sum(if(sv.value = '1',1,0)) AS positive, sum(if(sv.value = '-1',1,0)) AS negative, sum(if(sv.value = '0',1,0)) AS blank
        FROM solution as s
        INNER JOIN user as u on u.userId = s.userId
        LEFT JOIN solutionvote AS sv ON sv.solutionId = s.solutionId
        WHERE problemId = ?
        GROUP BY s.solutionId
        ORDER BY sv.value DESC , s.createdOn DESC",
      array(
      $id
      )
    );
    if ($query->count()) {
      $this->_list = $query->results();
      return true;
    }
    return false;
  }
  public function returnList()
  {
    return $this->_list;
  }
  public function getProblemList()
  {
    $query = $this->_db->query(
      "SELECT p.problemId, p.title,p.description,(SELECT COUNT(DISTINCT v.voteId) from vote AS v INNER JOIN facet as f ON f.facetId = v.facetId where f.problemId = p.problemId) AS nbVotes, COUNT(DISTINCT pv.pertinenceVoteId) AS pertinenceVotes FROM `problem` as p inner join facet as f on f.problemId = p.problemId inner join pertinence as pe on pe.facetId = f.facetId inner join proposition as pr on pe.propositionId = pr.propositionId left join pertinencevote as pv on pe.pertinenceId = pv.pertinenceId WHERE pr.solutionId = ? and pe.positive = 1 and pr.statusId !=5 group by problemId",
      array(
      $this->_data->solutionId
      )
    );
    return $query->results();
  }
  public function propositionExists($problemId)
  {
    $query = $this->_db->query(
      "SELECT distinct pr.propositionId FROM `problem` as p inner join facet as f on f.problemId = p.problemId inner join pertinence as pe on pe.facetId = f.facetId inner join proposition as pr on pe.propositionId = pr.propositionId  WHERE pr.solutionId = ? and p.problemId = ?",
      array(
      $this->_data->solutionId,
      $problemId
      )
    );
    if ($query->count() > 0) {
      return $query->first()->propositionId;
    } else {
      return 0;
    }
  }
}
