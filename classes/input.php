<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
class input
{
  public static function exists($type = 'post')
  {
    switch ($type) {
      case 'post':
        return (!empty($_POST)) ? true : false;
        break;
      case 'get':
        return (!empty($_GET)) ? true : false;
        break;
      default:
        return false;
        break;
    }
  }

  public static function defined($param = null)
  {
    $item = input::get($param);

    return (!empty($item)) ? true : false;
  }

  public static function get($item)
  {
    if (isset($_POST[$item])) {
      return helper::test_input($_POST[$item]);
    } else if (isset($_GET[$item])) {
      return helper::test_input($_GET[$item]);
    }
    if ($item === "full_array") {
      if (isset($_POST)) {
        return $_POST;
      }
      if (isset($_GET)) {
        return $_GET;
      }
    }
    return '';
  }
}
