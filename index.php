<?php
require 'core/ini.php';
helper::loadHeader(
  'header.php',
  array(
  'TITLE' => 'Solucracy'
  , 'DESCRIPTION' => _("We all have the same problems, let's find a solution together."))
);
?>
<style type="text/css">

section {
  min-height: 110%;
  overflow: hidden;
}
.video-container {
  position: relative;
  overflow: hidden;
  top: 0;
  height: 0;
  padding-bottom: 56.25%; /* calculate by aspect ratio (h / w * 100%) */
}
 .video {
  position: absolute;
  margin-left: 5%;
  width: 90%;
  height: 90%;
}
.go{
  top: 0;
}

.fa-caret-down:hover,.fa-caret-up:hover{
  -webkit-transform: scale(1.3);
  -ms-transform: scale(1.3);
  transform: scale(1.3);
}
.fa-caret-down,.fa-caret-up{
  -webkit-animation: glowing 1500ms infinite;
  -moz-animation: glowing 1500ms infinite;
  -o-animation: glowing 1500ms infinite;
  animation: glowing 1500ms infinite;
}

@-webkit-keyframes glowing {
  0% { -webkit-transform: scale(1); }
  50% { -webkit-transform: scale(1.3); }
  100% { -webkit-transform: scale(1); }
}

@-moz-keyframes glowing {
  0% { -ms-transform: scale(1); }
  50% { -ms-transform: scale(1.3); }
  100% { -ms-transform: scale(1); }
}

@-o-keyframes glowing {
  0% { transform: scale(1); }
  50% { transform: scale(1.3); }
  100% { transform: scale(1); }
}

@keyframes glowing {
  0% { transform: scale(1); }
  50% { transform: scale(1.3); }
  100% { transform: scale(1); }
}


</style>
    <a href="homepage.php" class="solucracy_btn float-right m-3 p-2 go" data-toggle="tooltip" data-placement="bottom" title="<?php echo _("You convinced me ! Now let me play !") ?>"><h4> <?php echo _("Let's go !") ?> </h4></a>
    <section id="about" class="d-flex align-items-center flex-column w-100">
          <h2 ><?php echo _("A collaborative tool to prioritize and solve problems") ?></h2>

          <p class="lead"><?php echo _("We all have the same problems, let's find a solution together.") ?></p>
          <img class="img-fluid" src="data:image/png;base64,<?php echo base64_encode(file_get_contents('img/sol+prob-icon.png')); ?>" alt="probleme" width="235" height="150">
          <ul class="list-inline self-align-center ">
            <li class="button list-inline-item"><a class="nav-link js-scroll-trigger link" href="#addProblem"><?php echo _("Add a problem") ?></a></li>
            <li class="button list-inline-item"><a class="nav-link js-scroll-trigger link" href="#problemList"><?php echo _("Overview of the problems") ?></a></li>
            <li class="button list-inline-item"><a class="nav-link js-scroll-trigger link" href="#solutions"><?php echo _("Potential solutions") ?></a></li>
            <li class="button list-inline-item"><a class="nav-link js-scroll-trigger link" href="#communities"><?php echo _("For your community") ?></a></li>
          </ul>
        <a class="nav-link js-scroll-trigger row" href="#addProblem"><i class='fas fa-5x fa-caret-down font_green clickable'></i></a>
    </section>

    <section id="addProblem" class="faded_red_bkgd w-100">
        <div class="row">
          <div class="col-3">
            <img class="img-fluid" src="data:image/png;base64,<?php echo base64_encode(file_get_contents('img/sample-map.png')); ?>" alt="map and marker" width="600" height="600">
          </div>
          <div class="col-6 mx-auto">
            <div class=row>
              <h2><?php echo _("Did you identify a need ? A problem") ?></h2>
              <p class="lead"><?php echo _("Let's say that a crossroad is dangerous...Log the problem in Solucracy and explain which need isn't met") ?></p>
              <i id="typeWrite"></i>
            </div>
            <div class="row">
              <h3><?php echo _("Share it on social networks so that other people can vote as well") ?></h3>
              <p class="lead"><?php echo _("Other people will be able to vote and add information. The amount of votes will allow to define how important this is for the community ") ?>
            </div>
            <div class="row text-right d-flex">
              <img class="float-right" src="data:image/png;base64,<?php echo base64_encode(file_get_contents('img/vote-graph.png')); ?>" alt="vote graph">
            </div>
          </div>
        </div>
        <a class="nav-link js-scroll-trigger text-center" href="#problemList"><i class='fas fa-5x fa-caret-down font_green  clickable'></i></a>
    </section>

    <section id="problemList" class="faded_green_bkgd w-100">
        <div class="col-8 mx-auto">
          <h2><?php echo _("Overview of the problems") ?></h2>
          <p class="lead"><?php echo _("Thanks to the information provided by the inhabitants, businesses,etc... a list of needs and improvment opportunities will start to emerge") ?></p>
        </div>
        <a class="nav-link js-scroll-trigger text-center" href="#solutions"><i class='fas fa-5x fa-caret-down font_green  clickable'></i></a>
        <div id="myVideo">
          <div class="video-container">
            <video class="video" autoplay muted loop >
              <source  src="videos/sample.mp4" type="video/mp4">
            </video>
          </div>
        </div>
    </section>

    <section id="solutions" class="faded_red_bkgd d-flex align-items-center flex-column p-2 w-100">
      <h2><?php echo _("Potential solutions") ?></h2>
      <p class="lead"><?php echo _("Everyone will be able to offer solutions and evaluate their pertinence. We might require multiple solutions to fully address the needs behind a single problem") ?></p>
      <div class="img-container text-center">
        <img class="img-fluid w-75" src="data:image/png;base64,<?php echo base64_encode(file_get_contents('img/prop1.png')); ?>" alt="solution-sample1">
        <img class="img-fluid w-75" src="data:image/png;base64,<?php echo base64_encode(file_get_contents('img/prop2.png')); ?>" alt="solution-sample2">
        <img class="img-fluid w-75" src="data:image/png;base64,<?php echo base64_encode(file_get_contents('img/prop3.png')); ?>" alt="solution-sample2">
      </div>
      <a class="nav-link js-scroll-trigger text-center" href="#communities"><i class='fas fa-5x fa-caret-down font_green clickable'></i></a>
    </section>

    <section id="communities" class="faded_green_bkgd w-100">
      <a class="nav-link js-scroll-trigger text-center" href="#about"><i class='fas fa-5x fa-caret-up font_green  clickable'></i></a>
      <div class="row">
        <div class="col-8 mx-auto">
          <h2><?php echo _("For your community") ?></h2>
          <p class="lead"><?php echo _("Activate your community account to display the solutions you support, highlight the ones already implemented, call for solutions, and communicate easily with your users") ?></p>
          <p class="lead"><?php echo _("Easily manage your administrators to keep track of your activity and mutualize need management with other communities") ?></p>
        </div>
      </div>
      <div class="row p-2">
      <img src="data:image/png;base64,<?php echo base64_encode(file_get_contents('img/news-sample.png')); ?>" alt="news">
      <img class="img-fluid offset-md-3" src="data:image/png;base64,<?php echo base64_encode(file_get_contents('img/admin-sample.png')); ?>" alt="admin-management">
    </div>
    </section>

    <!--  <section id="companies" class="bg-light">
      <div class="container">
        <div class="row">
          <div class="col-8 mx-auto">
            <h2>pour les entreprises</h2>
            <p class="lead">Activez un compte pour votre entreprise pour proposer vos solutions, être tenu au courant des besoins que vos produits peuvent combler et améliorer la visibilité de vos activités.</p>
          </div>
        </div>
      </div>
    </section> -->

<?php
require "inc/footer.php";
?>


    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

    <!-- Custom JavaScript for this theme -->
    <script>
      (function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav'
  });

})(jQuery); // End of use strict

var i = 0;
var txt = ' "<?php echo _("My kids use that road every day and have no visibility over what is coming.") ?>"      '; /* The text */
var speed = 50; /* The speed/duration of the effect in milliseconds */

function typeWriter() {
  if (i < txt.length+10) {
    i++;
  }else{
    $("#typeWrite").empty();
    i=0;
  }
    document.getElementById("typeWrite").innerHTML += txt.charAt(i);

    setTimeout(typeWriter, speed);

}
typeWriter();

    </script>
