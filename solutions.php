<?php
require 'core/ini.php';
helper::loadHeader(
  'header.php',
  array(
  'TITLE' => _("Solutions")
  , 'DESCRIPTION' => _("list of solutions logged"))
);
//If we're coming from "find an existing solution", record the problem ID for later
if (null !== input::get('problemId') && is_numeric(input::get('problemId'))) {
  session::put('problemId', input::get('problemId'));
}
$form           = new form();
$categories     = helper::getCategories();
$filters        = '';
$sortOptions[0] = (object) array('id' => '1', 'name' => _("# of problems linked"));
$sortOptions[1] = (object) array('id' => '2', 'name' => _("Category"));
$sortOptions[2] = (object) array('id' => '3', 'name' => _("Creation date"));
$filters .= $form->createField('select', 'sort', _("Sort by"), '', $sortOptions);
$filters .= $form->createField('select', 'category', _("Category"), '', $categories);
$filters .= $form->createField('text', 'searchTerms', _("Keywords"), '');
$filters .= $form->createField('button', 'Search', _("Search"), '', "searchAJAX($('#itemList').data('type'))");
?>
<div itemscope itemtype="http://schema.org/ItemPage" class="container-fluid">
<div itemprop="description" style="display: none;"><?php echo _("Solution's list") ?></div>
  <div class="row">
    <div class="w-100 d-flex flex-wrap faded_gray_bkgd p-2">
            <h3 class="w-100"><?php echo _("Solutions") ?></h3><!-- solutions -->
  <?php
  echo $filters;
?>
    </div>
    <div class="w-100 d-flex justify-content-md-around m-1">
      <div id='start' class="pages font_green" onclick='displayItems(this.id,"solution")'></div>
      <div id='previous' class="pages font_green" onclick='displayItems(this.id,"solution")'></div>
      <div id='current' class="pages font_green" data-nb="0"></div>
      <div id='next' class="pages font_green" onclick='displayItems(this.id,"solution")'></div>
      <div id='end' class="pages font_green" onclick='displayItems(this.id,"solution")'></div>
    </div>
  </div>
  <div class="row list" id="itemList" data-type="solution">
  </div>
</div>





<?php
require "inc/footer.php";
?>
<script type="text/javascript">
$(document).ready(function() {

  });
$(".pages").show();
</script>
