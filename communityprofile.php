<?php
require 'core/ini.php';
helper::loadHeader(
  'header.php',
  array(
  'TITLE' => _("Community")
  , 'DESCRIPTION' => _("Community"))
);
$adminPage = false;
if (session::exists('communityAdmin') && input::defined('communityId') && input::get('communityId') == session::get('communityAdmin')) {
  $adminPage = true;
  $community = new community(session::get('communityAdmin'));
} elseif (input::defined('communityId')) {
  $community = new community(input::get('communityId'));
  if ($community->get('follower') != 1) {
    $follow = '<button id="follow" class="solucracy_btn padding5" onclick="follow(1,' . input::get('communityId') . ')">' . _("Subscribe") . '</button><!-- subscribe -->';
  } else {
    $follow = '<button id="follow" class="solucracy_btn padding5" onclick="follow(0,' . input::get('communityId') . ')">' . _("Unsubscribe") . '</button><!-- unsubscribe -->';
  }
}
$problems = $community->getCommunityProblems();
$items    = newsitem::getNewsItems('community', $community->get('communityId'));

//prepare problem list
$displayProblems = "";
if ($adminPage) {
  $displayProblems = helper::displayList('problemListCommunityAdmin', $problems);
} else {
  $displayProblems = helper::displayList('problemList', $problems);
}
?>
<div class="container-fluid">
  <div class="row m-2">
    <div class="col-md-8">
      <h3><?php echo _("Official page for") . ' ' . $community->get('name'); ?></h3>
<?php
if ($adminPage) {
  ?>
<button id="manageAdmins" class="solucracy_btn padding5" onclick="ajax('buildform.php',{type:'manageAdmins',communityId:<?php echo session::get('communityAdmin'); ?>},'form')"><?php echo _("Manage admins"); ?></button><!-- manage admins -->
      <button id="contactUsers" class="solucracy_btn padding5" onclick="ajax('buildform.php',{type:'contactUsers',communityId:<?php echo session::get('communityAdmin'); ?>},'form')"><?php echo _("Contact followers"); ?></button><!-- contact your followers -->
      <button id="communitySettings" class="solucracy_btn padding5" onclick="ajax('buildform.php',{type:'communitySettings',communityId:<?php echo session::get('communityAdmin'); ?>},'form')"><?php echo _("Notification settings"); ?></button><!-- notification settings -->
      <button id="cancel" class="solucracy_btn padding5" onclick="cancelSubscription()"><?php echo _("Cancel my subscription"); ?></button><!-- notification settings -->
<?php
} else {
  echo $follow;
}
?>

    </div>
    <div class="col-md-4">
      <h3><?php echo _("Latest news"); ?></h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-8 list w-100 ">
      <div class="row d-flex justify-content-md-around">
        <div id='start' class="pages font_green" onclick='displayProblems(this.id)'></div>
        <div id='previous' class="pages font_green" onclick='displayProblems(this.id)'></div>
        <div id='current' class="pages font_green" data-nb="0"></div>
        <div id='next' class="pages font_green" onclick='displayProblems(this.id)'></div>
        <div id='end' class="pages font_green" onclick='displayProblems(this.id)'></div>
      </div>

      <?php
      echo $displayProblems;
?>
    </div>
    <div class="col-md-4 gray">
        <?php
        foreach ($items as $item) {
                  echo helper::render('newsItem', $item);
        }
?>
    </div>
  </div>
</div>




<?php
require "inc/footer.php";
?>

