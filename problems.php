<?php
require 'core/ini.php';

helper::loadHeader(
  'header.php',
  array(
  'TITLE' => _("Problems")
  , 'DESCRIPTION' => _("We all have the same problems, let's find a solution together."))
);
//where to get the markers
$getProblemList = 'problemmarkers.php';
$categories     = helper::getCategories();
$form           = new form();
$user           = new user();
if ($user->isLoggedIn()) {
  $groups = $user->getCommunities();
}
$tag = "";
if (input::defined('tag')) {
  $tag = input::get('tag');
}
$scopeList = helper::scope();
$filters   = '';
$status[0] = (object) array('id' => '1', 'name' => _("Active"));
$status[1] = (object) array('id' => '3', 'name' => _("Voting for resolution"));
$status[2] = (object) array('id' => '4', 'name' => _("Solved"));
$filters .= $form->createField('select', 'status', _("Status"), '', $status);
$filters .= $form->createField('swap', 'solutionSearch', _("In need of a solution"), '');
$sortOptions[0] = (object) array('id' => '1', 'name' => _("# of votes"));
$sortOptions[1] = (object) array('id' => '2', 'name' => _("Category"));
$sortOptions[2] = (object) array('id' => '3', 'name' => _("Creation date"));
$sortOptions[4] = (object) array('id' => '4', 'name' => _("# of solutions"));
$filters .= $form->createField('select', 'sort', _("Sort by"), '', $sortOptions);
$filters .= $form->createField('select', 'category', _("Category"), '', $categories);
$filters .= $form->createField('select', 'scopeId', _("Problem's scope"), _("Problem's scope"), $scopeList);
if ($user->isLoggedIn() && count($groups) > 0) {
  $filters .= $form->createField('select', 'group', _("Communities"), '', $groups);
}
$filters .= $form->createField('text', 'topic_search', _("Keywords"), '', $tag);
$filters .= $form->createField('button', 'Search', _("Search"), '', "searchAJAX($('#itemList').data('type'))");
?>
  <div itemscope itemtype="http://schema.org/ItemPage" class="container-fluid full-height marginbt100 m-0">
  <div itemprop="description" style="display: none;"><?php echo _("We all have the same problems, let's find a solution together."); ?></div>
    <div class="row full-height">
      <div itemscope itemtype="http://schema.org/ItemPage" class="col-md-6">
        <div class="row">
          <div class="w-100 d-flex flex-wrap faded_gray_bkgd p-2">
            <h3 class="w-100"><?php echo _("Problems") ?></h3><!-- problems -->
  <?php
  echo $filters;
?>
          </div>
          <div class="w-100 d-flex justify-content-md-around">
              <div id='start' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
              <div id='previous' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
              <div id='current' class="pages font_green" data-nb="0"></div>
              <div id='next' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
              <div id='end' class="pages font_green" onclick="displayItems(this.id,$('#itemList').data('type'))"></div>
          </div>
          <div id="itemList" class="row w-100 list p-3" data-type="problem">
          </div>
        </div>
      </div>
      <div class="col-md-6 col-12 full-height m-0 pl-0">
        <div id="mapContainerShow" class="full-height" style="position:relative">
          <div class="googlemapssearch" data-search="SEARCHWORDS" data-api-key="<?php echo config::get('GMap_API_Key') ?>" width="WIDTH" height="HEIGHT" ></div>
        </div>
      </div>
    </div>
  </div>
<?php
require "inc/footer.php";
?>
<script type="text/javascript">
// $(document).ready(function() {
// searchAJAX($('#itemList').data('type'));
// 	});
(tarteaucitron.job = tarteaucitron.job || []).push('googlemapssearch');
</script>
