<?php
/**
 * code below found here : https://www.toptal.com/php/build-multilingual-app-with-gettext
 * Verifies if the given $locale is supported in the project
 * @param string $locale
 * @return bool
 */
function valid($locale) {
  return in_array($locale, ['en_US', 'en', 'pt_BR', 'pt', 'es_ES', 'es','fr_FR','fr']);
}

//setting the source/default locale, for informational purposes
$lang = 'en_US';

if (isset($_GET['lang']) && valid($_GET['lang'])) {
  // the locale can be changed through the query-string
  $lang = Helper::test_input($_GET['lang']);
  setcookie('lang', $lang); //it's stored in a cookie so it can be reused
} elseif (isset($_COOKIE['lang']) && valid($_COOKIE['lang'])) {
  // if the cookie is present instead, let's just keep it
  $lang = Helper::test_input($_COOKIE['lang']);
} elseif (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
  // default: look for the languages the browser says the user accepts
  $langs = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
  array_walk($langs, function (&$lang) { $lang = strtr(strtok($lang, ';'), ['-' => '_']); });
  foreach ($langs as $browser_lang) {
    if (valid($browser_lang)) {
      $lang = $browser_lang;
      echo $lang;
      break;
    }
  }
}

// here we define the global system locale given the found language
putenv("LANG=$lang");

// this might be useful for date functions (LC_TIME) or money formatting (LC_MONETARY), for instance
setlocale(LC_ALL, $lang);

// this will make Gettext look for ../locales/<lang>/LC_MESSAGES/main.mo
bindtextdomain('main', '../locale');

// indicates in what encoding the file should be read
bind_textdomain_codeset('main', 'UTF-8');

// if your application has additional domains, as cited before, you should bind them here as well
// bindtextdomain('forum', '../locale');
// bind_textdomain_codeset('forum', 'UTF-8');

// here we indicate the default domain the gettext() calls will respond to
textdomain('main');

// this would look for the string in forum.mo instead of main.mo
// echo dgettext('forum', 'Welcome back!');
?>
