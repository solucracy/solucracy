-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 03 déc. 2018 à 15:22
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `solucracy`
--

-- --------------------------------------------------------

--
-- Structure de la table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `latitude` decimal(11,8) NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `postal_code` varchar(140) NOT NULL,
  `country` varchar(140) NOT NULL,
  `administrative_area_level_1` varchar(140) DEFAULT NULL,
  `administrative_area_level_2` varchar(140) DEFAULT NULL,
  `administrative_area_level_3` varchar(140) DEFAULT NULL,
  `locality` varchar(140) DEFAULT NULL,
  `route` varchar(200) DEFAULT NULL,
  `street_number` int(4) DEFAULT NULL,
  `other` text,
  UNIQUE KEY `position` (`latitude`,`longitude`) USING BTREE,
  KEY `address_idx_latitude_longitude_locality` (`latitude`,`longitude`,`locality`),
  KEY `address_idx_latitude_longitude_country` (`latitude`,`longitude`,`country`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `address`
--

INSERT INTO `address` (`latitude`, `longitude`, `postal_code`, `country`, `administrative_area_level_1`, `administrative_area_level_2`, `administrative_area_level_3`, `locality`, `route`, `street_number`, `other`) VALUES
('46.96608990', '4.41449290', '71360', 'France', 'Bourgogne Franche-Comté', 'Saône-et-Loire', NULL, 'Sully', 'Unnamed Road', NULL, NULL),
('46.94185670', '3.07834220', '58470', 'France', 'Bourgogne Franche-Comté', 'Nièvre', NULL, 'Gimouille', 'Maison du Pont Canal', NULL, NULL),
('37.68176880', '-95.45158280', '66720', 'États-Unis', 'Kansas', 'Neosho County', NULL, 'Chanute', 'East Main Street', NULL, '18651153'),
('48.86526300', '1.79490300', '78770', 'France', 'Île-de-France', 'Yvelines', NULL, 'Thoiry', NULL, NULL, NULL),
('46.58574310', '4.59522840', '71460', 'France', 'Bourgogne Franche-Comté', 'Saône-et-Loire', NULL, 'Burzy', 'D188', NULL, NULL),
('36.07097220', '-95.89179020', '74133', 'États-Unis', 'Oklahoma', NULL, NULL, 'Tulsa', '76th East Avenue', NULL, NULL),
('45.27540300', '6.34488600', '73300', 'France', 'Auvergne-Rhône-Alpes', 'Savoie', NULL, 'Saint-Jean-de-Maurienne', NULL, NULL, NULL),
('46.30754450', '5.88753190', '39310', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'Bellecombe', 'Les Mouillés', NULL, NULL),
('46.29082780', '6.07558470', '01170', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Ségny', 'Chemin des Landes', NULL, '493'),
('46.29079590', '5.53622580', '39240', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'Cornod', 'Turgon', NULL, NULL),
('46.26750420', '5.90806340', '01410', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Lélex', 'D991', NULL, NULL),
('45.87594240', '5.65938280', '01260', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Belmont-Luthézieu', 'Bioleaz Hameau', NULL, NULL),
('46.30788260', '5.71587180', '39360', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'Viry', 'La Pommeraie', NULL, NULL),
('46.32287000', '5.77232470', '39360', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'Vulvoz', 'La Vignette', NULL, NULL),
('46.86481080', '5.81181310', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'Route de Champagnole', NULL, NULL),
('46.47849870', '5.82189900', '39150', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'Les Crozets', 'La Grande Pièce', NULL, NULL),
('45.95545980', '5.54879660', '01230', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Chaley', 'D21', NULL, NULL),
('46.08907530', '5.68717220', '01260', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Le Grand-Abergement', 'Combe de la Manche', NULL, NULL),
('45.98646120', '5.69434920', '01260', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Songieu', 'D9A', NULL, NULL),
('45.92109280', '5.63752980', '01260', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Sutrieu', 'D8C', NULL, NULL),
('45.96813990', '5.71145930', '01260', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Songieu', 'Unnamed Road', NULL, NULL),
('46.01750310', '5.82958250', '74910', 'France', 'Auvergne-Rhône-Alpes', 'Haute-Savoie', NULL, 'Challonges', 'Rue de la Combe', NULL, '155'),
('46.03549410', '5.65781330', '01260', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Le Petit-Abergement', 'D57A', NULL, NULL),
('46.01384260', '5.59808820', '01110', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Champdor', 'Champdor', NULL, '370'),
('46.00338220', '5.50842490', '01110', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Aranc', 'Le Bourg', NULL, '235'),
('45.96262820', '5.47371740', '01230', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Oncieu', 'D34', NULL, NULL),
('45.89101260', '5.47462200', '01230', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Conand', 'D73', NULL, '3'),
('46.29782830', '5.94791960', '01170', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Sergy', 'Rue du Vieux Bourg', NULL, 'street_number:167,'),
('46.24699450', '5.85518580', '01410', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Chézery-Forens', 'Unnamed Road', NULL, NULL),
('45.77491250', '4.96379270', '69150', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Décines-Charpieu', 'Avenue Edouard Herriot', NULL, 'street_number:45,'),
('10.00000000', '124.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('53.00000000', '137.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-63.00000000', '-137.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-79.00000000', '-100.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-79.00000000', '-150.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-24.00000000', '-103.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-10.00000000', '71.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-51.00000000', '18.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-13.00000000', '43.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-76.00000000', '133.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('86.00000000', '-154.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('5.00000000', '37.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('49.00000000', '-82.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-81.00000000', '175.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('73.00000000', '54.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('63.00000000', '-52.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-45.00000000', '-159.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('54.00000000', '-18.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-11.00000000', '126.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('86.00000000', '139.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('70.00000000', '-55.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('47.00000000', '-134.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-31.00000000', '-32.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('26.00000000', '87.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('83.00000000', '132.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('15.00000000', '-35.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('13.00000000', '180.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-43.00000000', '-92.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('14.00000000', '136.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-25.00000000', '-99.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-31.00000000', '-111.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('78.00000000', '-176.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('76.00000000', '170.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-19.00000000', '-120.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('32.00000000', '37.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-73.00000000', '-129.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('30.00000000', '35.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-9.00000000', '62.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('14.00000000', '-72.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('89.00000000', '-93.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('55.00000000', '-175.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('82.00000000', '94.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('49.00000000', '125.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-23.00000000', '-6.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-4.00000000', '99.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-19.00000000', '-18.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('51.00000000', '-107.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-11.00000000', '-4.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('30.00000000', '125.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('80.00000000', '-95.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('86.00000000', '2.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-61.00000000', '32.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('57.00000000', '-113.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-62.00000000', '6.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('22.00000000', '-114.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('71.00000000', '173.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('11.00000000', '-17.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('18.00000000', '80.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('26.00000000', '-178.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('69.00000000', '142.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-34.00000000', '-37.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-37.00000000', '-177.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-59.00000000', '82.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-6.00000000', '-12.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-59.00000000', '51.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-39.00000000', '-170.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-71.00000000', '49.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-85.00000000', '-40.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-48.00000000', '84.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-1.00000000', '37.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-75.00000000', '48.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-64.00000000', '114.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-66.00000000', '156.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('70.00000000', '3.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-77.00000000', '-152.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-72.00000000', '-166.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('62.00000000', '80.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-38.00000000', '-145.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('55.00000000', '171.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-2.00000000', '-169.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-75.00000000', '105.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-46.00000000', '67.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-8.00000000', '-63.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('0.00000000', '98.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('81.00000000', '67.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('34.00000000', '-109.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-71.00000000', '-35.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('63.00000000', '70.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('41.00000000', '13.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-13.00000000', '-25.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('88.00000000', '158.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-11.00000000', '-179.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('42.00000000', '-41.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-48.00000000', '32.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('12.00000000', '51.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('40.00000000', '165.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-71.00000000', '-14.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('86.00000000', '-86.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('86.00000000', '156.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-77.00000000', '-17.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('63.00000000', '162.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('32.00000000', '35.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-75.00000000', '11.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('39.00000000', '101.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('13.00000000', '12.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('56.00000000', '-125.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('13.00000000', '148.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-33.00000000', '107.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-46.00000000', '-101.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('45.00000000', '72.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('16.00000000', '91.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-61.00000000', '51.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('12.00000000', '101.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-4.00000000', '140.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('82.00000000', '-71.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-48.00000000', '-25.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-33.00000000', '-10.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('71.00000000', '78.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('63.00000000', '-11.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-60.00000000', '146.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('38.00000000', '52.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-81.00000000', '127.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-1.00000000', '-36.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('51.00000000', '170.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('85.00000000', '-31.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-22.00000000', '150.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-29.00000000', '-82.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-56.00000000', '-168.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('58.00000000', '26.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-38.00000000', '-122.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('55.00000000', '10.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('4.00000000', '-177.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('40.00000000', '73.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('29.00000000', '180.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('35.00000000', '-66.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-12.00000000', '0.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('83.00000000', '164.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('59.00000000', '-155.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-17.00000000', '161.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-71.00000000', '21.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('82.00000000', '27.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('53.00000000', '-101.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-72.00000000', '-154.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('37.00000000', '-77.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('21.00000000', '102.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('33.00000000', '-18.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-23.00000000', '142.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-71.00000000', '18.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('71.00000000', '155.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('41.00000000', '-141.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-18.00000000', '6.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('67.00000000', '-152.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-88.00000000', '122.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('24.00000000', '133.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-9.00000000', '-176.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-30.00000000', '72.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-84.00000000', '103.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('35.00000000', '-83.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('11.00000000', '-107.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-83.00000000', '2.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-11.00000000', '-9.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-46.00000000', '75.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-54.00000000', '-136.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-37.00000000', '-69.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('38.00000000', '60.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-12.00000000', '167.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('88.00000000', '48.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('52.00000000', '75.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('0.00000000', '78.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('9.00000000', '163.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('66.00000000', '-128.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('47.00000000', '158.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-7.00000000', '106.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-8.00000000', '52.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('13.00000000', '74.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-29.00000000', '86.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('6.00000000', '28.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('41.00000000', '28.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('86.00000000', '25.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-62.00000000', '-30.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('22.00000000', '91.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('21.00000000', '-104.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-23.00000000', '61.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-2.00000000', '-119.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-68.00000000', '-58.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-77.00000000', '-54.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-42.00000000', '-124.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-80.00000000', '133.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('75.00000000', '73.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('19.00000000', '-117.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('32.00000000', '-42.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-61.00000000', '-11.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-5.00000000', '20.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-75.00000000', '176.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('31.00000000', '162.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('47.00000000', '70.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('23.00000000', '-154.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-80.00000000', '116.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('75.00000000', '-52.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('74.00000000', '-123.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-50.00000000', '126.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('53.00000000', '51.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('10.00000000', '46.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('80.00000000', '99.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-40.00000000', '98.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-83.00000000', '-75.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('38.00000000', '-50.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('53.00000000', '10.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-32.00000000', '52.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-26.00000000', '-16.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('50.00000000', '-28.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-60.00000000', '-80.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-87.00000000', '-92.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-28.00000000', '74.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-28.00000000', '-164.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-90.00000000', '114.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-21.00000000', '76.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-78.00000000', '-172.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-25.00000000', '0.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-30.00000000', '-43.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-26.00000000', '-90.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-4.00000000', '163.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('71.00000000', '137.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-6.00000000', '25.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('0.00000000', '75.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-84.00000000', '-3.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('8.00000000', '57.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('14.00000000', '51.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('32.00000000', '79.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-42.00000000', '-51.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-9.00000000', '32.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-13.00000000', '132.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-9.00000000', '-148.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-26.00000000', '-42.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-83.00000000', '-37.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('62.00000000', '-122.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-71.00000000', '-116.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('32.00000000', '108.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('37.00000000', '26.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-49.00000000', '52.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-2.00000000', '0.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-37.00000000', '34.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-30.00000000', '-120.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('5.00000000', '-163.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('34.00000000', '8.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-29.00000000', '-8.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-3.00000000', '-168.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-50.00000000', '-69.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-84.00000000', '-28.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('80.00000000', '179.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('77.00000000', '-69.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('8.00000000', '86.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-34.00000000', '6.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('15.00000000', '130.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-24.00000000', '164.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-54.00000000', '-170.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('56.00000000', '-166.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('67.00000000', '156.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('49.00000000', '-174.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('69.00000000', '-37.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('82.00000000', '-165.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-86.00000000', '136.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-25.00000000', '-73.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-83.00000000', '-113.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('69.00000000', '174.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-1.00000000', '120.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('0.00000000', '-19.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('31.00000000', '105.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-88.00000000', '-13.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('23.00000000', '100.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-48.00000000', '-48.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('10.00000000', '-33.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('88.00000000', '-115.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('24.00000000', '122.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-77.00000000', '134.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('21.00000000', '-102.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('20.00000000', '-47.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('50.00000000', '48.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('16.00000000', '45.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('39.00000000', '-31.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-15.00000000', '-179.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('57.00000000', '-90.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-40.00000000', '172.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-80.00000000', '-84.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-29.00000000', '121.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('59.00000000', '-25.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('72.00000000', '162.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-60.00000000', '-60.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('74.00000000', '-126.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-71.00000000', '59.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('69.00000000', '65.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('65.00000000', '94.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('85.00000000', '173.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('25.00000000', '161.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-31.00000000', '-20.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('39.00000000', '82.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-34.00000000', '-114.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('6.00000000', '-110.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-80.00000000', '97.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-45.00000000', '16.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('46.13204120', '5.59231810', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('48.00000000', '-48.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('40.00000000', '-25.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('72.00000000', '-26.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('65.00000000', '144.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('71.00000000', '88.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-18.00000000', '-155.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('43.00000000', '-18.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('38.00000000', '-138.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-37.00000000', '67.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-49.00000000', '133.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('5.00000000', '-28.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('14.00000000', '-143.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-31.00000000', '21.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('47.00000000', '-145.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('62.00000000', '-129.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('53.00000000', '-92.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-76.00000000', '-125.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('0.00000000', '13.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('68.00000000', '147.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL);
INSERT INTO `address` (`latitude`, `longitude`, `postal_code`, `country`, `administrative_area_level_1`, `administrative_area_level_2`, `administrative_area_level_3`, `locality`, `route`, `street_number`, `other`) VALUES
('-69.00000000', '-35.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('66.00000000', '62.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('61.00000000', '-134.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-45.00000000', '-19.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-27.00000000', '-29.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('32.00000000', '44.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('84.00000000', '-151.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-12.00000000', '-159.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-56.00000000', '-12.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-32.00000000', '118.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-2.00000000', '-104.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-26.00000000', '98.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('29.00000000', '11.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-40.00000000', '-62.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-41.00000000', '4.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('36.00000000', '-28.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-87.00000000', '-113.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('45.00000000', '7.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-24.00000000', '-3.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-57.00000000', '141.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('41.00000000', '-78.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-83.00000000', '71.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-56.00000000', '94.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-71.00000000', '-170.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-73.00000000', '130.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('77.00000000', '109.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('46.00000000', '98.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('83.00000000', '-41.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('78.00000000', '26.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('86.00000000', '37.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('65.00000000', '-29.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('67.00000000', '-56.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-37.00000000', '16.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('30.00000000', '77.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('75.00000000', '88.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('61.00000000', '-113.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-5.00000000', '-9.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-13.00000000', '-110.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('45.00000000', '51.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-8.00000000', '176.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-82.00000000', '-28.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-57.00000000', '-9.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('73.00000000', '-152.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('81.00000000', '-89.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('42.00000000', '-62.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-54.00000000', '-91.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-65.00000000', '-99.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('41.00000000', '-10.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-57.00000000', '55.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('50.00000000', '-43.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-81.00000000', '113.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('88.00000000', '-69.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-43.00000000', '133.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('7.00000000', '25.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('67.00000000', '-171.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-28.00000000', '118.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('55.00000000', '-157.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('46.00000000', '-171.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-48.00000000', '54.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('79.00000000', '165.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('33.00000000', '149.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('5.00000000', '-130.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('49.00000000', '-108.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('69.00000000', '27.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-61.00000000', '40.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('83.00000000', '-118.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-16.00000000', '-59.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-57.00000000', '-3.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-24.00000000', '-19.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('62.00000000', '94.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('47.00000000', '18.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-66.00000000', '65.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('60.00000000', '63.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-75.00000000', '106.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-28.00000000', '-128.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('2.00000000', '99.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('88.00000000', '37.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('73.00000000', '-73.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('69.00000000', '123.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('41.00000000', '56.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('28.00000000', '-57.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-80.00000000', '52.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('31.00000000', '47.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('72.00000000', '-17.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-69.00000000', '26.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-49.00000000', '137.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-74.00000000', '-34.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-17.00000000', '32.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-5.00000000', '-68.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-16.00000000', '-134.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('57.00000000', '-87.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-29.00000000', '165.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-86.00000000', '-180.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-63.00000000', '-26.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-21.00000000', '139.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('36.00000000', '20.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('11.00000000', '33.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-55.00000000', '-22.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('27.00000000', '-5.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('70.00000000', '155.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('3.00000000', '-174.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('88.00000000', '134.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('71.00000000', '23.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-51.00000000', '-28.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('73.00000000', '73.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-74.00000000', '-116.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('36.00000000', '117.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-27.00000000', '-117.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-18.00000000', '114.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-29.00000000', '140.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-87.00000000', '-54.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('86.00000000', '108.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-14.00000000', '102.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('78.00000000', '158.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-81.00000000', '151.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-49.00000000', '172.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('28.00000000', '-1.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-57.00000000', '43.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('4.00000000', '-2.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('33.00000000', '-106.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('62.00000000', '-23.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('90.00000000', '154.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-45.00000000', '-122.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-28.00000000', '-84.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('22.00000000', '162.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-3.00000000', '-138.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('15.00000000', '23.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('30.00000000', '-30.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('35.00000000', '52.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-66.00000000', '150.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('8.00000000', '136.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('89.00000000', '28.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('35.00000000', '178.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-64.00000000', '-102.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-45.00000000', '35.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-41.00000000', '-133.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('34.00000000', '12.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('25.00000000', '147.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-87.00000000', '-131.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('75.00000000', '-20.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('69.00000000', '-44.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('5.00000000', '6.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('2.00000000', '-154.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('43.00000000', '-143.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-56.00000000', '-106.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('9.00000000', '117.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('40.00000000', '67.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('16.00000000', '51.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-33.00000000', '146.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('73.00000000', '115.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('57.00000000', '81.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-25.00000000', '-58.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('84.00000000', '-111.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-30.00000000', '-163.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-4.00000000', '50.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('40.00000000', '169.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-5.00000000', '41.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('53.00000000', '-13.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-5.00000000', '151.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('58.00000000', '105.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-77.00000000', '19.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('66.00000000', '153.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('83.00000000', '-115.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('1.00000000', '102.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-27.00000000', '-23.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('42.00000000', '-144.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-46.00000000', '-139.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-56.00000000', '119.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('88.00000000', '146.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-74.00000000', '54.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('53.00000000', '14.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('45.00000000', '-161.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('19.00000000', '-148.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-39.00000000', '48.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-88.00000000', '64.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-7.00000000', '-59.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-90.00000000', '-110.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('11.00000000', '139.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('11.00000000', '136.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-35.00000000', '12.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('11.00000000', '4.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('23.00000000', '115.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('48.00000000', '136.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('16.00000000', '139.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-84.00000000', '139.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-5.00000000', '-71.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('77.00000000', '15.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-89.00000000', '140.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-55.00000000', '-18.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('39.00000000', '58.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-12.00000000', '-138.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-48.00000000', '5.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-41.00000000', '121.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('75.00000000', '-88.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('52.00000000', '-19.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('63.00000000', '-69.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-56.00000000', '163.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-19.00000000', '-99.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-81.00000000', '-10.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('9.00000000', '-126.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-67.00000000', '88.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-78.00000000', '162.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-74.00000000', '-178.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('8.00000000', '95.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('6.00000000', '42.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-52.00000000', '-106.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-2.00000000', '-17.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-39.00000000', '-25.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('73.00000000', '59.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('65.00000000', '-2.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('54.00000000', '36.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-2.00000000', '155.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('1.00000000', '-107.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('72.00000000', '-85.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('39.00000000', '-160.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-73.00000000', '-85.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-80.00000000', '33.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('58.00000000', '65.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('3.00000000', '1.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-74.00000000', '133.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('66.00000000', '-49.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-17.00000000', '103.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('39.00000000', '-127.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('71.00000000', '109.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-88.00000000', '101.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-88.00000000', '43.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-80.00000000', '-157.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-40.00000000', '153.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-56.00000000', '147.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('13.00000000', '-87.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-28.00000000', '-176.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-57.00000000', '-74.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-26.00000000', '85.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('47.00000000', '26.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('30.00000000', '-138.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-67.00000000', '-170.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-70.00000000', '48.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('35.00000000', '-168.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('75.00000000', '-50.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-69.00000000', '103.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('0.00000000', '-67.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-83.00000000', '169.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-65.00000000', '-76.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-18.00000000', '27.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-72.00000000', '158.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-64.00000000', '-38.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('45.00000000', '93.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-24.00000000', '33.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('17.00000000', '-146.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-62.00000000', '-167.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-5.00000000', '-83.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-46.00000000', '119.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('35.00000000', '51.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('42.00000000', '-51.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-1.00000000', '-72.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-6.00000000', '157.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('83.00000000', '-9.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-15.00000000', '-85.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-29.00000000', '-58.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('75.00000000', '-58.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('47.00000000', '-68.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-46.00000000', '-123.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('20.00000000', '-180.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-90.00000000', '35.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-4.00000000', '-108.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-33.00000000', '157.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('73.00000000', '-81.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-54.00000000', '-63.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-83.00000000', '83.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('53.00000000', '43.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('41.00000000', '147.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-87.00000000', '81.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-25.00000000', '-41.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-25.00000000', '-169.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('33.00000000', '-150.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-4.00000000', '31.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-52.00000000', '-127.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-30.00000000', '-91.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('54.00000000', '-67.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-65.00000000', '-124.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-54.00000000', '-156.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('23.00000000', '-116.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('9.00000000', '139.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('61.00000000', '-11.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-69.00000000', '88.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('39.00000000', '103.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('78.00000000', '-47.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('50.00000000', '84.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('10.00000000', '66.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-79.00000000', '53.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('66.00000000', '168.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('45.00000000', '64.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-59.00000000', '97.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-35.00000000', '154.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-21.00000000', '-152.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('76.00000000', '-162.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-34.00000000', '-172.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('69.00000000', '-80.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('10.00000000', '-87.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-77.00000000', '97.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('75.00000000', '-167.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('31.00000000', '-118.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-7.00000000', '-53.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-66.00000000', '127.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('47.00000000', '-101.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('51.00000000', '58.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-90.00000000', '-130.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('15.00000000', '153.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-81.00000000', '80.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-66.00000000', '-6.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-30.00000000', '168.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-29.00000000', '-161.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('23.00000000', '29.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('9.00000000', '-113.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('59.00000000', '-67.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('31.00000000', '30.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('13.00000000', '168.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-79.00000000', '30.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('6.00000000', '92.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('2.00000000', '107.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('10.00000000', '-45.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-49.00000000', '-136.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('75.00000000', '-137.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('39.00000000', '105.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('68.00000000', '24.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('24.00000000', '-76.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-81.00000000', '136.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('19.00000000', '-112.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('28.00000000', '160.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('47.00000000', '110.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-44.00000000', '43.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('39.00000000', '152.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-69.00000000', '-62.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('75.00000000', '-21.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('9.00000000', '121.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-42.00000000', '-107.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('64.00000000', '-61.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('82.00000000', '-66.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('80.00000000', '29.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('28.00000000', '35.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('0.00000000', '34.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('55.00000000', '87.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('30.00000000', '-45.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('40.00000000', '11.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-52.00000000', '-64.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-63.00000000', '32.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-70.00000000', '121.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,');
INSERT INTO `address` (`latitude`, `longitude`, `postal_code`, `country`, `administrative_area_level_1`, `administrative_area_level_2`, `administrative_area_level_3`, `locality`, `route`, `street_number`, `other`) VALUES
('-15.00000000', '126.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-34.00000000', '-78.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-25.00000000', '-173.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-3.00000000', '93.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('82.00000000', '-57.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('2.00000000', '46.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('61.00000000', '-58.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('10.00000000', '-93.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('61.00000000', '60.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-10.00000000', '-73.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-58.00000000', '60.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-66.00000000', '-152.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-87.00000000', '117.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('83.00000000', '-171.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('83.00000000', '-138.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-21.00000000', '165.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-59.00000000', '-23.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-72.00000000', '27.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-7.00000000', '-132.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('1.00000000', '89.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-80.00000000', '-49.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('73.00000000', '-52.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-28.00000000', '148.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('77.00000000', '157.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('66.00000000', '113.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-41.00000000', '60.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('38.00000000', '50.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-75.00000000', '-16.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('78.00000000', '21.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-60.00000000', '-104.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('89.00000000', '16.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('15.00000000', '33.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-5.00000000', '-145.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('74.00000000', '155.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-65.00000000', '176.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('32.00000000', '22.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('27.00000000', '65.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-15.00000000', '76.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('64.00000000', '51.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-4.00000000', '175.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-48.00000000', '90.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-7.00000000', '180.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('35.00000000', '99.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-89.00000000', '-40.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('11.00000000', '54.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-62.00000000', '-54.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('12.00000000', '31.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-38.00000000', '95.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('59.00000000', '-176.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-30.00000000', '-22.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-60.00000000', '-71.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-64.00000000', '-132.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('70.00000000', '-132.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-71.00000000', '66.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('14.00000000', '60.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('4.00000000', '-60.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-89.00000000', '154.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('52.00000000', '-108.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-58.00000000', '88.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-41.00000000', '73.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('45.00000000', '-21.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-41.00000000', '96.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('35.00000000', '155.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('36.00000000', '6.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-11.00000000', '-30.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('4.00000000', '105.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-83.00000000', '-114.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('6.00000000', '36.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('88.00000000', '18.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('26.00000000', '-139.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('87.00000000', '-43.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('21.00000000', '51.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('49.00000000', '57.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('60.00000000', '70.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-20.00000000', '-174.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('20.00000000', '-170.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-41.00000000', '48.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('48.00000000', '-29.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-20.00000000', '156.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('67.00000000', '121.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('84.00000000', '64.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('38.00000000', '-180.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-2.00000000', '113.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('58.00000000', '-169.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-30.00000000', '160.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('46.00000000', '-104.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('77.00000000', '142.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-55.00000000', '-157.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-41.00000000', '45.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('49.00000000', '-103.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('7.00000000', '-127.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-12.00000000', '-144.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('24.00000000', '176.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-86.00000000', '14.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-67.00000000', '59.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('28.00000000', '-76.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('67.00000000', '-17.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('63.00000000', '-123.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('47.00000000', '51.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-19.00000000', '101.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-66.00000000', '-37.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-32.00000000', '-9.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('64.00000000', '59.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-13.00000000', '-3.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-42.00000000', '124.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-54.00000000', '111.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-40.00000000', '-75.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-60.00000000', '140.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-86.00000000', '-116.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-71.00000000', '-91.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-70.00000000', '28.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-63.00000000', '99.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-1.00000000', '7.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('29.00000000', '7.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-5.00000000', '-180.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('53.00000000', '31.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('5.00000000', '116.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-20.00000000', '-80.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('45.00000000', '-172.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-16.00000000', '-111.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-87.00000000', '-173.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-20.00000000', '-40.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-16.00000000', '-173.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-27.00000000', '107.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-58.00000000', '-64.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-37.00000000', '-65.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('55.00000000', '129.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-56.00000000', '38.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('16.00000000', '-82.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('55.00000000', '174.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('47.00000000', '3.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('72.00000000', '30.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-13.00000000', '-150.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('74.00000000', '-76.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('74.00000000', '-176.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('31.00000000', '170.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('63.00000000', '50.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('64.00000000', '-124.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('84.00000000', '-101.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-62.00000000', '140.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('61.00000000', '-147.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('40.00000000', '-72.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-30.00000000', '38.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('33.00000000', '-97.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-64.00000000', '91.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('71.00000000', '-117.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('90.00000000', '-142.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-88.00000000', '70.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('56.00000000', '-15.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('72.00000000', '-62.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('-48.00000000', '-106.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('32.00000000', '-147.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-22.00000000', '35.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('2.00000000', '155.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-83.00000000', '-154.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-34.00000000', '74.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-58.00000000', '113.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('20.00000000', '158.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-64.00000000', '-87.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('31.00000000', '-139.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('-88.00000000', '10.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('90.00000000', '-65.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('63.00000000', '21.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('83.00000000', '38.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-11.00000000', '11.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('69.00000000', '-167.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('88.00000000', '-6.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-79.00000000', '-175.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('78.00000000', '-88.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('50.00000000', '-46.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('86.00000000', '146.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('-88.00000000', '-166.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('71.00000000', '-138.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('77.00000000', '127.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('65.00000000', '91.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-83.00000000', '-4.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('14.00000000', '180.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('81.00000000', '115.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('43.00000000', '-30.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('-30.00000000', '76.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('42.00000000', '-153.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-43.00000000', '113.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-40.00000000', '-41.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('-11.00000000', '-80.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-12.00000000', '114.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('20.00000000', '-105.00000000', '10060', 'Italie', 'Piemonte', 'Città Metropolitana di Torino', 'Inverso Pinasca', 'Vivian', 'Borgata Don', NULL, 'street_number:10,'),
('89.00000000', '-95.00000000', '27020', 'Italie', 'Lombardia', 'Provincia di Pavia', 'Zerbolò', NULL, 'Strada Provinciale 3', NULL, 'street_number:3,'),
('50.00000000', '178.00000000', '41045', 'Italie', 'Emilia-Romagna', 'Provincia di Modena', 'Montefiorino', NULL, 'Unnamed Road', NULL, NULL),
('27.00000000', '-150.00000000', '69126', 'France', 'Auvergne-Rhône-Alpes', 'Rhône', NULL, 'Brindas', 'Chemin des Vignes Rouges', NULL, 'street_number:52,'),
('61.00000000', '75.00000000', '16500', 'France', 'Nouvelle-Aquitaine', 'Charente', NULL, 'Saint-Maurice-des-Lions', 'D948', NULL, NULL),
('-50.00000000', '-3.00000000', '40420', 'France', 'Nouvelle-Aquitaine', 'Landes', NULL, 'Brocas', 'Unnamed Road', NULL, NULL),
('-29.00000000', '-59.00000000', '54110', 'France', 'Grand Est', 'Meurthe-et-Moselle', NULL, 'Champenoux', 'Unnamed Road', NULL, NULL),
('77.00000000', '32.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('90.00000000', '46.00000000', '38250', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Villard-de-Lans', 'Unnamed Road', NULL, NULL),
('-15.00000000', '2.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-29.00000000', '1.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('80.00000000', '129.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('24.00000000', '141.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-72.00000000', '-49.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-21.00000000', '-35.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-69.00000000', '97.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('29.00000000', '-144.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('13.00000000', '-169.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-51.00000000', '110.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('85.00000000', '17.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('38.00000000', '117.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-17.00000000', '-83.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-37.00000000', '14.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('80.00000000', '-78.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('5.00000000', '145.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-29.00000000', '71.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-29.00000000', '21.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('66.00000000', '-123.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-5.00000000', '-24.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-2.00000000', '165.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-21.00000000', '32.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('3.00000000', '171.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('38.00000000', '59.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('89.00000000', '-43.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('44.00000000', '2.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('30.00000000', '165.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('59.00000000', '-43.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-48.00000000', '163.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-78.00000000', '95.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('29.00000000', '120.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('51.00000000', '-7.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-64.00000000', '29.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('66.00000000', '-110.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('25.00000000', '-117.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('41.00000000', '-89.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('14.00000000', '-64.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('24.00000000', '18.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-17.00000000', '-91.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('87.00000000', '-28.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-24.00000000', '85.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-3.00000000', '-30.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-54.00000000', '117.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('18.00000000', '173.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-55.00000000', '30.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('85.00000000', '149.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('50.00000000', '-117.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-58.00000000', '35.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-67.00000000', '-143.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('56.00000000', '-49.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('43.00000000', '-16.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('62.00000000', '124.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('6.00000000', '-144.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-1.00000000', '-95.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('69.00000000', '-174.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('68.00000000', '-47.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-80.00000000', '-166.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('58.00000000', '-37.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('11.00000000', '25.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-23.00000000', '-161.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('28.00000000', '48.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-14.00000000', '164.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('78.00000000', '-84.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('73.00000000', '-132.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-4.00000000', '-49.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('-16.00000000', '96.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('82.00000000', '160.00000000', '39600', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'La Châtelaine', 'D469', NULL, 'street_number:2,'),
('43.33292900', '5.45228370', '13013', 'France', 'Provence-Alpes-Côte d\'Azur', 'Bouches-du-Rhône', NULL, 'Marseille', 'Rue Paul Preboist', NULL, 'street_number:38,'),
('45.46771450', '5.53099720', '38850', 'France', 'Auvergne-Rhône-Alpes', 'Isère', NULL, 'Paladru', 'Chemin de la Pierre Qui Danse', NULL, 'street_number:149,'),
('46.25417640', '6.01091060', '01630', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Sergy', 'Chemin du marais', NULL, NULL),
('46.25028690', '6.00551980', '01630', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Sergy', 'Avenue du Jura', NULL, 'street_number:677,'),
('46.34545080', '5.79067390', '39360', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'Larrivoire', 'Rue Saint-Georges', NULL, 'street_number:34,'),
('46.14363790', '6.05164450', '74160', 'France', 'Auvergne-Rhône-Alpes', 'Haute-Savoie', NULL, 'Saint-Julien-en-Genevois', 'Chemin Rural de Soral à la Voie communale N°1', NULL, NULL),
('46.09123440', '5.94417990', '74520', 'France', 'Auvergne-Rhône-Alpes', 'Haute-Savoie', NULL, 'Dingy-en-Vuache', 'Chemin Rural dit de la Mairie', NULL, 'street_number:68,'),
('46.27922720', '6.05979410', '01170', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Chevry', 'D78', NULL, 'street_number:574,'),
('47.22948030', '6.02943370', '25000', 'France', 'Bourgogne Franche-Comté', 'Doubs', NULL, 'Besançon', 'Faubourg Tarragnoz', NULL, 'street_number:2A,'),
('46.36881620', '5.91465570', '39310', 'France', 'Bourgogne Franche-Comté', 'Jura', NULL, 'Septmoncel', 'Route de Genève', NULL, 'street_number:34,'),
('46.33080950', '6.06350980', '01170', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Gex', 'Impasse des Jardins', NULL, 'street_number:20,'),
('46.13077510', '5.93825420', '01550', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Collonges', 'Unnamed Road', NULL, NULL),
('46.32602360', '6.05280420', '01170', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Gex', 'Rue des Vertes Campagnes', NULL, 'street_number:153,'),
('46.25210340', '6.07577620', '01280', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Prévessin-Moëns', 'Route de la Fontaine', NULL, 'street_number:523,'),
('46.23323110', '5.97773930', '01710', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Thoiry', 'Rue des Tamaris', NULL, 'street_number:60,'),
('0.00000000', '0.00000000', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('46.33081630', '6.05586010', '01170', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Gex', 'Rue des Abattoirs', NULL, 'street_number:229,'),
('46.19987840', '6.07636000', '1233', 'Suisse', 'Genève', 'Genève', NULL, 'Bernex', 'Route de Chèvres', NULL, 'street_number:30,'),
('46.24698970', '6.09424690', '01210', 'France', 'Auvergne-Rhône-Alpes', 'Ain', NULL, 'Ferney-Voltaire', 'Rue de Meyrin', NULL, 'street_number:182B,'),
('47.83666860', '8.08767860', '79859', 'Allemagne', 'Baden-Württemberg', 'Freiburg', 'Breisgau-Hochschwarzwald', 'Schluchsee', 'Unnamed Road', NULL, NULL),
('43.02146610', '-2.01734170', '31891', 'Espagne', 'Navarra', 'Navarra', NULL, NULL, 'Calle San Martín', NULL, NULL),
('42.34849760', '-1.17369940', '50678', 'Espagne', 'Aragón', 'Zaragoza', NULL, 'Uncastillo', NULL, NULL, ':,'),
('41.95759270', '-3.99369740', '34248', 'Espagne', 'Castilla y León', 'Palencia', NULL, 'Espinosa De Cerrato', 'P-141', NULL, 'street_number:22,:,'),
('41.82549880', '1.40387800', '', 'Espagne', 'Catalunya', 'Lleida', NULL, 'Torà', 'Unnamed Road', NULL, ':,'),
('43.01099160', '-6.98920290', '27658', 'Espagne', 'Galicia', 'Lugo', NULL, 'Galicia', 'Unnamed Road', NULL, NULL),
('46.23700970', '6.10915640', '1215', 'Suisse', 'Genève', 'Genève', NULL, 'Genève', 'Route de l\'Aéroport', NULL, 'street_number:21,airport:Genève Aéroport,'),
('32.68750780', '-2.77798130', '', 'Maroc', 'Oriental', 'Province de Figuig', NULL, 'Province de Figuig', NULL, NULL, NULL),
('34.56819650', '-5.26418900', '', 'Maroc', 'Tanger-Tétouan-Al Hoceïma', 'Province de Sidi Kacem', NULL, 'Province de Sidi Kacem', 'Unnamed Road', NULL, NULL),
('31.90512750', '-4.72775280', '00000', 'Maroc', 'Drâa-Tafilalet', 'Errachidia Province', NULL, 'Errachidia Province', NULL, NULL, NULL),
('13.34307510', '-15.98721320', '00000', 'Gambie', 'Lower River', 'Kiang West', NULL, 'Kiang West', 'Unnamed Road', NULL, NULL),
('13.34928570', '-16.01699710', '00000', 'Gambie', 'Lower River', 'Kiang West', NULL, 'Kiang West', NULL, NULL, NULL),
('46.70177760', '6.86668570', '00000', 'Suisse', 'Vaud', 'Broye-Vully', NULL, 'Broye-Vully', 'Prassy', NULL, 'street_number:3,'),
('12.24290210', '-2.39634760', '00000', 'Burkina Faso', 'Centre-Ouest', 'Boulkiemdé', NULL, 'Boulkiemdé', NULL, NULL, 'premise:ENS,'),
('12.24904320', '-2.32510100', '00000', 'Burkina Faso', 'Centre-Ouest', 'Boulkiemdé', NULL, 'Boulkiemdé', 'Unnamed Road', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `badge`
--

DROP TABLE IF EXISTS `badge`;
CREATE TABLE IF NOT EXISTS `badge` (
  `badgeId` int(4) NOT NULL,
  `name` varchar(140) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(140) NOT NULL,
  `categoryId` int(1) DEFAULT NULL,
  `quote` int(4) DEFAULT NULL,
  `author` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`badgeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `badge`
--

INSERT INTO `badge` (`badgeId`, `name`, `createdOn`, `image`, `categoryId`, `quote`, `author`) VALUES
(2791, '279', '2017-06-12 05:50:22', 'usager.png', 1, 342, 'Charles F Kettering'),
(2801, '280', '2017-06-12 05:50:22', 'loupe.png', 1, 343, 'Mark Twain'),
(3, '281', '2017-06-12 05:50:59', 'human.png', NULL, 358, 'Anthony Rapp'),
(4, '282', '2017-06-12 05:50:59', 'badge2.png', NULL, 359, 'Hervé Le Tellier'),
(5, '283', '2017-06-15 05:34:19', 'badge2.png', NULL, 360, 'Julius Goupil'),
(6, '284', '2017-06-15 05:34:19', 'pupitre.png', NULL, 361, 'Henri-Frédéric Amiel'),
(7, '285', '2017-06-15 05:35:19', 'bug.png', NULL, 362, 'Apollo 13'),
(8, '286', '2017-06-16 08:43:10', 'badge1.png', NULL, 369, 'Jacques Chaban Delmas'),
(2871, '287', '2017-06-19 05:38:52', 'bougie.png', 1, 345, 'Les Shadoks'),
(2881, '288', '2017-06-19 05:38:52', 'badge1.png', 1, 346, 'Jacques de Bourbon Busset'),
(2891, '289', '2017-06-19 05:38:52', 'badge1.png', 1, 347, 'Rava Bakou'),
(3091, '309', '2017-06-21 05:49:27', 'badge1.png', 1, 344, 'Joseph Sugarman'),
(2792, '279', '2017-06-12 05:50:22', 'usager.png', 2, 342, 'Charles F Kettering'),
(2802, '280', '2017-06-12 05:50:22', 'loupe.png', 2, 343, 'Mark Twain'),
(2872, '287', '2017-06-19 05:38:52', 'bougie.png', 2, 345, 'Les Shadoks'),
(2882, '288', '2017-06-19 05:38:52', 'badge1.png', 2, 346, 'Jacques de Bourbon Busset'),
(2892, '289', '2017-06-19 05:38:52', 'badge1.png', 2, 347, 'Rava Bakou'),
(3092, '309', '2017-06-21 05:49:27', 'badge1.png', 2, 344, 'Joseph Sugarman'),
(2793, '279', '2017-06-12 05:50:22', 'usager.png', 3, 342, 'Charles F Kettering'),
(2803, '280', '2017-06-12 05:50:22', 'loupe.png', 3, 343, 'Mark Twain'),
(2873, '287', '2017-06-19 05:38:52', 'bougie.png', 3, 345, 'Les Shadoks'),
(2883, '288', '2017-06-19 05:38:52', 'badge1.png', 3, 346, 'Jacques de Bourbon Busset'),
(2893, '289', '2017-06-19 05:38:52', 'badge1.png', 3, 347, 'Rava Bakou'),
(3093, '309', '2017-06-21 05:49:27', 'badge1.png', 3, 344, 'Joseph Sugarman'),
(2794, '279', '2017-06-12 05:50:22', 'usager.png', 4, 342, 'Charles F Kettering'),
(2804, '280', '2017-06-12 05:50:22', 'loupe.png', 4, 343, 'Mark Twain'),
(2874, '287', '2017-06-19 05:38:52', 'bougie.png', 4, 345, 'Les Shadoks'),
(2884, '288', '2017-06-19 05:38:52', 'badge1.png', 4, 346, 'Jacques de Bourbon Busset'),
(2894, '289', '2017-06-19 05:38:52', 'badge1.png', 4, 347, 'Rava Bakou'),
(3094, '309', '2017-06-21 05:49:27', 'badge1.png', 4, 344, 'Joseph Sugarman'),
(2795, '279', '2017-06-12 05:50:22', 'usager.png', 5, 342, 'Charles F Kettering'),
(2805, '280', '2017-06-12 05:50:22', 'loupe.png', 5, 343, 'Mark Twain'),
(2875, '287', '2017-06-19 05:38:52', 'bougie.png', 5, 345, 'Les Shadoks'),
(2885, '288', '2017-06-19 05:38:52', 'badge1.png', 5, 346, 'Jacques de Bourbon Busset'),
(2895, '289', '2017-06-19 05:38:52', 'badge1.png', 5, 347, 'Rava Bakou'),
(3095, '309', '2017-06-21 05:49:27', 'badge1.png', 5, 344, 'Joseph Sugarman'),
(2796, '279', '2017-06-12 05:50:22', 'usager.png', 6, 342, 'Charles F Kettering'),
(2806, '280', '2017-06-12 05:50:22', 'loupe.png', 6, 343, 'Mark Twain'),
(2876, '287', '2017-06-19 05:38:52', 'bougie.png', 6, 345, 'Les Shadoks'),
(2886, '288', '2017-06-19 05:38:52', 'badge1.png', 6, 346, 'Jacques de Bourbon Busset'),
(2896, '289', '2017-06-19 05:38:52', 'badge1.png', 6, 347, 'Rava Bakou'),
(3096, '309', '2017-06-21 05:49:27', 'badge1.png', 6, 344, 'Joseph Sugarman'),
(2797, '279', '2017-06-12 05:50:22', 'usager.png', 7, 342, 'Charles F Kettering'),
(2807, '280', '2017-06-12 05:50:22', 'loupe.png', 7, 343, 'Mark Twain'),
(2877, '287', '2017-06-19 05:38:52', 'bougie.png', 7, 345, 'Les Shadoks'),
(2887, '288', '2017-06-19 05:38:52', 'badge1.png', 7, 346, 'Jacques de Bourbon Busset'),
(2897, '289', '2017-06-19 05:38:52', 'badge1.png', 7, 347, 'Rava Bakou'),
(3097, '309', '2017-06-21 05:49:27', 'badge1.png', 7, 344, 'Joseph Sugarman'),
(2798, '279', '2017-06-12 05:50:22', 'usager.png', 8, 342, 'Charles F Kettering'),
(2808, '280', '2017-06-12 05:50:22', 'loupe.png', 8, 343, 'Mark Twain'),
(2878, '287', '2017-06-19 05:38:52', 'bougie.png', 8, 345, 'Les Shadoks'),
(2888, '288', '2017-06-19 05:38:52', 'badge1.png', 8, 346, 'Jacques de Bourbon Busset'),
(2898, '289', '2017-06-19 05:38:52', 'badge1.png', 8, 347, 'Rava Bakou'),
(3098, '309', '2017-06-21 05:49:27', 'badge1.png', 8, 344, 'Joseph Sugarman'),
(2799, '279', '2017-06-12 05:50:22', 'usager.png', 9, 342, 'Charles F Kettering'),
(2809, '280', '2017-06-12 05:50:22', 'loupe.png', 9, 343, 'Mark Twain'),
(2879, '287', '2017-06-19 05:38:52', 'bougie.png', 9, 345, 'Les Shadoks'),
(2889, '288', '2017-06-19 05:38:52', 'badge1.png', 9, 346, 'Jacques de Bourbon Busset'),
(2899, '289', '2017-06-19 05:38:52', 'badge1.png', 9, 347, 'Rava Bakou'),
(3099, '309', '2017-06-21 05:49:27', 'badge1.png', 9, 344, 'Joseph Sugarman'),
(13, '300', '2017-06-19 05:38:52', 'badge1.png', NULL, 363, 'Desperate housewives, Mary Alice'),
(14, '301', '2017-06-19 05:38:52', 'drapeau.png', NULL, 364, 'Paul Carvel'),
(15, '302', '2017-06-21 05:49:27', 'monde.png', NULL, 365, 'Yannick Laignel'),
(2971, '297', '2017-06-26 05:44:51', 'badge1.png', 1, 355, 'Albert Einstein'),
(2972, '297', '2017-06-26 05:44:51', 'badge1.png', 2, 355, 'Albert Einstein'),
(2973, '297', '2017-06-26 05:44:51', 'badge1.png', 3, 355, 'Albert Einstein'),
(2974, '297', '2017-06-26 05:44:51', 'badge1.png', 4, 355, 'Albert Einstein'),
(2975, '297', '2017-06-26 05:44:51', 'badge1.png', 5, 355, 'Albert Einstein'),
(2976, '297', '2017-06-26 05:44:51', 'badge1.png', 6, 355, 'Albert Einstein'),
(2977, '297', '2017-06-26 05:44:51', 'badge1.png', 7, 355, 'Albert Einstein'),
(2978, '297', '2017-06-26 05:44:51', 'badge1.png', 8, 355, 'Albert Einstein'),
(2979, '297', '2017-06-26 05:44:51', 'badge1.png', 9, 355, 'Albert Einstein'),
(2981, '298', '2017-06-26 05:44:51', 'badge2.png', 1, 356, 'Jack Sparrow'),
(2982, '298', '2017-06-26 05:44:51', 'badge2.png', 2, 356, 'Jack Sparrow'),
(2983, '298', '2017-06-26 05:44:51', 'badge2.png', 3, 356, 'Jack Sparrow'),
(2984, '298', '2017-06-26 05:44:51', 'badge2.png', 4, 356, 'Jack Sparrow'),
(2985, '298', '2017-06-26 05:44:51', 'badge2.png', 5, 356, 'Jack Sparrow'),
(2986, '298', '2017-06-26 05:44:51', 'badge2.png', 6, 356, 'Jack Sparrow'),
(2987, '298', '2017-06-26 05:44:51', 'badge2.png', 7, 356, 'Jack Sparrow'),
(2988, '298', '2017-06-26 05:44:51', 'badge2.png', 8, 356, 'Jack Sparrow'),
(2989, '298', '2017-06-26 05:44:51', 'badge2.png', 9, 356, 'Jack Sparrow'),
(2991, '299', '2017-06-26 05:44:51', 'badge3.png', 1, 357, 'Indiana Jones'),
(2992, '299', '2017-06-26 05:44:51', 'badge3.png', 2, 357, 'Indiana Jones'),
(2993, '299', '2017-06-26 05:44:51', 'badge3.png', 3, 357, 'Indiana Jones'),
(2994, '299', '2017-06-26 05:44:51', 'badge3.png', 4, 357, 'Indiana Jones'),
(2995, '299', '2017-06-26 05:44:51', 'badge3.png', 5, 357, 'Indiana Jones'),
(2996, '299', '2017-06-26 05:44:51', 'badge3.png', 6, 357, 'Indiana Jones'),
(2997, '299', '2017-06-26 05:44:51', 'badge3.png', 7, 357, 'Indiana Jones'),
(2998, '299', '2017-06-26 05:44:51', 'badge3.png', 8, 357, 'Indiana Jones'),
(2999, '299', '2017-06-26 05:44:51', 'badge3.png', 9, 357, 'Indiana Jones'),
(16, '305', '2017-06-27 05:53:16', 'badge2.png', NULL, NULL, NULL),
(17, '306', '2017-06-27 06:01:56', 'badge3.png', NULL, NULL, NULL),
(18, '307', '2017-06-28 05:47:41', 'badge3.png', NULL, 372, 'Abraham Maslow'),
(19, '293', '2017-07-03 06:07:09', 'jumelles.png', NULL, 351, 'Gandalf'),
(20, '294', '2017-07-03 06:07:09', 'badge2.png', NULL, 352, 'Antoine de St Exupéry'),
(21, '295', '2017-07-03 06:07:52', 'badge2.png', NULL, 353, 'Antoine Loisel'),
(22, '296', '2017-07-03 06:07:52', 'badge2.png', NULL, 354, 'Benjamin Parker, Spiderman'),
(2901, '290', '2017-07-14 11:14:25', 'badge1.png', 1, 348, 'Étienne de Jouy'),
(2902, '290', '2017-07-14 11:14:25', 'badge1.png', 2, 348, 'Étienne de Jouy'),
(2903, '290', '2017-07-14 11:14:25', 'badge1.png', 3, 348, 'Étienne de Jouy'),
(2904, '290', '2017-07-14 11:14:25', 'badge1.png', 4, 348, 'Étienne de Jouy'),
(2905, '290', '2017-07-14 11:14:25', 'badge1.png', 5, 348, 'Étienne de Jouy'),
(2906, '290', '2017-07-14 11:14:25', 'badge1.png', 6, 348, 'Étienne de Jouy'),
(2907, '290', '2017-07-14 11:14:25', 'badge1.png', 7, 348, 'Étienne de Jouy'),
(2908, '290', '2017-07-14 11:14:25', 'badge1.png', 8, 348, 'Étienne de Jouy'),
(2909, '290', '2017-07-14 11:14:25', 'badge1.png', 9, 348, 'Étienne de Jouy'),
(2911, '291', '2017-07-14 11:16:31', 'badge1.png', 1, 349, 'Charles de Gaulle'),
(2912, '291', '2017-07-14 11:16:31', 'badge1.png', 2, 349, 'Charles de Gaulle'),
(2913, '291', '2017-07-14 11:16:31', 'badge1.png', 3, 349, 'Charles de Gaulle'),
(2914, '291', '2017-07-14 11:16:31', 'badge1.png', 4, 349, 'Charles de Gaulle'),
(2915, '291', '2017-07-14 11:16:31', 'badge1.png', 5, 349, 'Charles de Gaulle'),
(2916, '291', '2017-07-14 11:16:31', 'badge1.png', 6, 349, 'Charles de Gaulle'),
(2917, '291', '2017-07-14 11:16:31', 'badge1.png', 7, 349, 'Charles de Gaulle'),
(2918, '291', '2017-07-14 11:16:31', 'badge1.png', 8, 349, 'Charles de Gaulle'),
(2919, '291', '2017-07-14 11:16:31', 'badge1.png', 9, 349, 'Charles de Gaulle'),
(2921, '292', '2017-07-14 11:17:50', 'badge1.png', 1, 350, 'Aimé Césaire'),
(2922, '292', '2017-07-14 11:17:50', 'badge1.png', 2, 350, 'Aimé Césaire'),
(2923, '292', '2017-07-14 11:17:50', 'badge1.png', 3, 350, 'Aimé Césaire'),
(2924, '292', '2017-07-14 11:17:50', 'badge1.png', 4, 350, 'Aimé Césaire'),
(2925, '292', '2017-07-14 11:17:50', 'badge1.png', 5, 350, 'Aimé Césaire'),
(2926, '292', '2017-07-14 11:17:50', 'badge1.png', 6, 350, 'Aimé Césaire'),
(2927, '292', '2017-07-14 11:17:50', 'badge1.png', 7, 350, 'Aimé Césaire'),
(2928, '292', '2017-07-14 11:17:50', 'badge1.png', 8, 350, 'Aimé Césaire'),
(2929, '292', '2017-07-14 11:17:50', 'badge1.png', 9, 350, 'Aimé Césaire'),
(23, '303', '2017-07-14 11:20:56', 'badge1.png', NULL, 366, 'Antoine de St Exupéry'),
(24, '304', '2017-07-14 11:20:56', 'badge1.png', NULL, 367, 'Henry Ford'),
(25, '305', '2017-07-14 11:20:56', 'badge1.png', NULL, 368, 'Friedrich Nietzsche'),
(26, '308', '2017-07-14 11:20:56', 'badge-vote.png', NULL, 371, 'Amma'),
(31, '478', '2018-07-31 09:29:14', 'high5.png', NULL, 477, 'Tim Allen');

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `categoryId` int(4) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `icon` varchar(255) NOT NULL,
  `translationId` int(4) NOT NULL,
  PRIMARY KEY (`categoryId`),
  KEY `category_idx_categoryid_icon` (`categoryId`,`icon`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`categoryId`, `name`, `icon`, `translationId`) VALUES
(1, 'Tourism', 'tourisme.png', 74),
(2, 'Business', 'commerce.png', 75),
(3, 'Education', 'education.png', 76),
(4, 'Administration', 'admin.png', 77),
(5, 'Entertainment', 'loisir.png', 78),
(6, 'Sport', 'sport.png', 79),
(7, 'Other', '-autre.png', 86),
(8, 'Urbanism', 'urbanisme.png', 131),
(9, 'Environment', 'environnement.png', 132);

-- --------------------------------------------------------

--
-- Structure de la table `community`
--

DROP TABLE IF EXISTS `community`;
CREATE TABLE IF NOT EXISTS `community` (
  `communityId` int(4) NOT NULL AUTO_INCREMENT,
  `communityTypeId` int(4) NOT NULL,
  `name` varchar(140) NOT NULL,
  `statusId` int(4) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `departmentId` varchar(10) DEFAULT '00',
  `countryId` varchar(2) DEFAULT NULL,
  `statusUpdate` timestamp NULL DEFAULT NULL,
  `reactivationCode` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`communityId`),
  UNIQUE KEY `communityName` (`departmentId`,`name`),
  KEY `community_idx_communityid_name` (`communityId`,`name`)
) ENGINE=MyISAM AUTO_INCREMENT=35897 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `communityadmin`
--

DROP TABLE IF EXISTS `communityadmin`;
CREATE TABLE IF NOT EXISTS `communityadmin` (
  `userId` int(4) NOT NULL,
  `communityId` int(4) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `UK-communityadmin-userId-communityId` (`userId`,`communityId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `communityproblem`
--

DROP TABLE IF EXISTS `communityproblem`;
CREATE TABLE IF NOT EXISTS `communityproblem` (
  `communityId` int(4) DEFAULT NULL,
  `problemId` int(4) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `statusId` int(4) NOT NULL DEFAULT '1',
  `statusUpdate` timestamp NULL DEFAULT NULL,
  `updatedBy` int(4) NOT NULL,
  `needHelp` tinyint(1) NOT NULL DEFAULT '0',
  KEY `communityproblem_idx_statusid_problemid_needhelp` (`statusId`,`problemId`,`needHelp`),
  KEY `communityproblem_idx_problemid` (`problemId`),
  KEY `communityproblem_idx_communityi_statusid_problemid` (`communityId`,`statusId`,`problemId`),
  KEY `communityproblem_idx_statusid_problemid` (`statusId`,`problemId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `communityproposition`
--

DROP TABLE IF EXISTS `communityproposition`;
CREATE TABLE IF NOT EXISTS `communityproposition` (
  `communityId` int(4) DEFAULT NULL,
  `propositionId` int(4) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `statusId` int(4) NOT NULL DEFAULT '1',
  `statusUpdate` timestamp NULL DEFAULT NULL,
  `updatedBy` int(4) NOT NULL,
  `comment` varchar(250) DEFAULT NULL,
  KEY `communityproposition_idx_statusid_propositio_communityi` (`statusId`,`propositionId`,`communityId`),
  KEY `communityproposition_idx_statusid_propositio_comment` (`statusId`,`propositionId`,`comment`),
  KEY `communityproposition_idx_propositionid` (`propositionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `communitysubscription`
--

DROP TABLE IF EXISTS `communitysubscription`;
CREATE TABLE IF NOT EXISTS `communitysubscription` (
  `communitySubscriptionId` int(4) NOT NULL AUTO_INCREMENT,
  `userId` int(4) NOT NULL,
  `communityId` int(4) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`communitySubscriptionId`),
  UNIQUE KEY `UK-communityId-userId` (`communityId`,`userId`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `communitytype`
--

DROP TABLE IF EXISTS `communitytype`;
CREATE TABLE IF NOT EXISTS `communitytype` (
  `communityTypeId` int(4) NOT NULL AUTO_INCREMENT,
  `translationId` int(4) NOT NULL,
  PRIMARY KEY (`communityTypeId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `communitytype`
--

INSERT INTO `communitytype` (`communityTypeId`, `translationId`) VALUES
(1, 389),
(2, 388),
(3, 390);

-- --------------------------------------------------------

--
-- Structure de la table `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `countryId` varchar(2) NOT NULL,
  `name` varchar(140) NOT NULL,
  UNIQUE KEY `countryId` (`countryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `country`
--

INSERT INTO `country` (`countryId`, `name`) VALUES
('AF', 'Afghanistan'),
('AX', '  Åland Islands'),
('AL', '  Albania'),
('DZ', '  Algeria'),
('AS', '  American Samoa'),
('AD', '  Andorra'),
('AO', '  Angola'),
('AI', '  Anguilla'),
('AQ', '  Antarctica'),
('AG', '  Antigua and Barbuda'),
('AR', '  Argentina'),
('AM', '  Armenia'),
('AW', '  Aruba'),
('AU', '  Australia'),
('AT', '  Austria'),
('AZ', '  Azerbaijan'),
('BS', '  Bahamas'),
('BH', '  Bahrain'),
('BD', '  Bangladesh'),
('BB', '  Barbados'),
('BY', '  Belarus'),
('BE', '  Belgium'),
('BZ', '  Belize'),
('BJ', '  Benin'),
('BM', '  Bermuda'),
('BT', '  Bhutan'),
('BO', '  Bolivia (Plurinational State of)'),
('BQ', '  Bonaire, Sint Eustatius and Saba'),
('BA', '  Bosnia and Herzegovina'),
('BW', '  Botswana'),
('BV', '  Bouvet Island'),
('BR', '  Brazil'),
('IO', '  British Indian Ocean Territory'),
('BN', '  Brunei Darussalam'),
('BG', '  Bulgaria'),
('BF', '  Burkina Faso'),
('BI', '  Burundi'),
('CV', '  Cabo Verde'),
('KH', '  Cambodia'),
('CM', '  Cameroon'),
('CA', '  Canada'),
('KY', '  Cayman Islands'),
('CF', '  Central African Republic'),
('TD', '  Chad'),
('CL', '  Chile'),
('CN', '  China'),
('CX', '  Christmas Island'),
('CC', '  Cocos (Keeling) Islands'),
('CO', '  Colombia'),
('KM', '  Comoros'),
('CG', '  Congo'),
('CD', '  Congo (Democratic Republic of the)'),
('CK', '  Cook Islands'),
('CR', '  Costa Rica'),
('CI', '  Côte d\'Ivoire'),
('HR', '  Croatia'),
('CU', '  Cuba'),
('CW', '  Curaçao'),
('CY', '  Cyprus'),
('CZ', '  Czechia'),
('DK', '  Denmark'),
('DJ', '  Djibouti'),
('DM', '  Dominica'),
('DO', '  Dominican Republic'),
('EC', '  Ecuador'),
('EG', '  Egypt'),
('SV', '  El Salvador'),
('GQ', '  Equatorial Guinea'),
('ER', '  Eritrea'),
('EE', '  Estonia'),
('ET', '  Ethiopia'),
('FK', '  Falkland Islands (Malvinas)'),
('FO', '  Faroe Islands'),
('FJ', '  Fiji'),
('FI', '  Finland'),
('FR', '  France'),
('GF', '  French Guiana'),
('PF', '  French Polynesia'),
('TF', '  French Southern Territories'),
('GA', '  Gabon'),
('GM', '  Gambia'),
('GE', '  Georgia'),
('DE', '  Germany'),
('GH', '  Ghana'),
('GI', '  Gibraltar'),
('GR', '  Greece'),
('GL', '  Greenland'),
('GD', '  Grenada'),
('GP', '  Guadeloupe'),
('GU', '  Guam'),
('GT', '  Guatemala'),
('GG', '  Guernsey'),
('GN', '  Guinea'),
('GW', '  Guinea-Bissau'),
('GY', '  Guyana'),
('HT', '  Haiti'),
('HM', '  Heard Island and McDonald Islands'),
('VA', '  Holy See'),
('HN', '  Honduras'),
('HK', '  Hong Kong'),
('HU', '  Hungary'),
('IS', '  Iceland'),
('IN', '  India'),
('ID', '  Indonesia'),
('IR', '  Iran (Islamic Republic of)'),
('IQ', '  Iraq'),
('IE', '  Ireland'),
('IM', '  Isle of Man'),
('IL', '  Israel'),
('IT', '  Italy'),
('JM', '  Jamaica'),
('JP', '  Japan'),
('JE', '  Jersey'),
('JO', '  Jordan'),
('KZ', '  Kazakhstan'),
('KE', '  Kenya'),
('KI', '  Kiribati'),
('KP', '  Korea (Democratic People\'s Republic of)'),
('KR', '  Korea (Republic of)'),
('KW', '  Kuwait'),
('KG', '  Kyrgyzstan'),
('LA', '  Lao People\'s Democratic Republic'),
('LV', '  Latvia'),
('LB', '  Lebanon'),
('LS', '  Lesotho'),
('LR', '  Liberia'),
('LY', '  Libya'),
('LI', '  Liechtenstein'),
('LT', '  Lithuania'),
('LU', '  Luxembourg'),
('MO', '  Macao'),
('MK', '  Macedonia (the former Yugoslav Republic of)'),
('MG', '  Madagascar'),
('MW', '  Malawi'),
('MY', '  Malaysia'),
('MV', '  Maldives'),
('ML', '  Mali'),
('MT', '  Malta'),
('MH', '  Marshall Islands'),
('MQ', '  Martinique'),
('MR', '  Mauritania'),
('MU', '  Mauritius'),
('YT', '  Mayotte'),
('MX', '  Mexico'),
('FM', '  Micronesia (Federated States of)'),
('MD', '  Moldova (Republic of)'),
('MC', '  Monaco'),
('MN', '  Mongolia'),
('ME', '  Montenegro'),
('MS', '  Montserrat'),
('MA', '  Morocco'),
('MZ', '  Mozambique'),
('MM', '  Myanmar'),
('NA', '  Namibia'),
('NR', '  Nauru'),
('NP', '  Nepal'),
('NL', '  Netherlands'),
('NC', '  New Caledonia'),
('NZ', '  New Zealand'),
('NI', '  Nicaragua'),
('NE', '  Niger'),
('NG', '  Nigeria'),
('NU', '  Niue'),
('NF', '  Norfolk Island'),
('MP', '  Northern Mariana Islands'),
('NO', '  Norway'),
('OM', '  Oman'),
('PK', '  Pakistan'),
('PW', '  Palau'),
('PS', '  Palestine, State of'),
('PA', '  Panama'),
('PG', '  Papua New Guinea'),
('PY', '  Paraguay'),
('PE', '  Peru'),
('PH', '  Philippines'),
('PN', '  Pitcairn'),
('PL', '  Poland'),
('PT', '  Portugal'),
('PR', '  Puerto Rico'),
('QA', '  Qatar'),
('RE', '  Réunion'),
('RO', '  Romania'),
('RU', '  Russian Federation'),
('RW', '  Rwanda'),
('BL', '  Saint Barthélemy'),
('SH', '  Saint Helena, Ascension and Tristan da Cunha'),
('KN', '  Saint Kitts and Nevis'),
('LC', '  Saint Lucia'),
('MF', '  Saint Martin (French part)'),
('PM', '  Saint Pierre and Miquelon'),
('VC', '  Saint Vincent and the Grenadines'),
('WS', '  Samoa'),
('SM', '  San Marino'),
('ST', '  Sao Tome and Principe'),
('SA', '  Saudi Arabia'),
('SN', '  Senegal'),
('RS', '  Serbia'),
('SC', '  Seychelles'),
('SL', '  Sierra Leone'),
('SG', '  Singapore'),
('SX', '  Sint Maarten (Dutch part)'),
('SK', '  Slovakia'),
('SI', '  Slovenia'),
('SB', '  Solomon Islands'),
('SO', '  Somalia'),
('ZA', '  South Africa'),
('GS', '  South Georgia and the South Sandwich Islands'),
('SS', '  South Sudan'),
('ES', '  Spain'),
('LK', '  Sri Lanka'),
('SD', '  Sudan'),
('SR', '  Suriname'),
('SJ', '  Svalbard and Jan Mayen'),
('SZ', '  Swaziland'),
('SE', '  Sweden'),
('CH', '  Switzerland'),
('SY', '  Syrian Arab Republic'),
('TW', '  Taiwan, Province of China[a]'),
('TJ', '  Tajikistan'),
('TZ', '  Tanzania, United Republic of'),
('TH', '  Thailand'),
('TL', '  Timor-Leste'),
('TG', '  Togo'),
('TK', '  Tokelau'),
('TO', '  Tonga'),
('TT', '  Trinidad and Tobago'),
('TN', '  Tunisia'),
('TR', '  Turkey'),
('TM', '  Turkmenistan'),
('TC', '  Turks and Caicos Islands'),
('TV', '  Tuvalu'),
('UG', '  Uganda'),
('UA', '  Ukraine'),
('AE', '  United Arab Emirates'),
('GB', '  United Kingdom of Great Britain and Northern Ireland'),
('US', '  United States of America'),
('UM', '  United States Minor Outlying Islands'),
('UY', '  Uruguay'),
('UZ', '  Uzbekistan'),
('VU', '  Vanuatu'),
('VE', '  Venezuela (Bolivarian Republic of)'),
('VN', '  Viet Nam'),
('VG', '  Virgin Islands (British)'),
('VI', '  Virgin Islands (U.S.)'),
('WF', '  Wallis and Futuna'),
('EH', '  Western Sahara'),
('YE', '  Yemen'),
('ZM', '  Zambia'),
('ZW', '  Zimbabwe');

-- --------------------------------------------------------

--
-- Structure de la table `facet`
--

DROP TABLE IF EXISTS `facet`;
CREATE TABLE IF NOT EXISTS `facet` (
  `facetId` int(4) NOT NULL AUTO_INCREMENT,
  `problemId` int(4) NOT NULL,
  `description` varchar(140) NOT NULL,
  `userId` int(4) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`facetId`),
  KEY `facet_idx_facetid_problemid` (`facetId`,`problemId`),
  KEY `facet_idx_problemid_facetid_descriptio` (`problemId`,`facetId`,`description`)
) ENGINE=MyISAM AUTO_INCREMENT=3629 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `language`
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `FR` varchar(400) NOT NULL,
  `EN` varchar(400) NOT NULL DEFAULT 'please update translation',
  `SP` varchar(400) NOT NULL DEFAULT 'Actualizar la traducción',
  PRIMARY KEY (`id`),
  KEY `language_idx_id_fr` (`id`,`FR`)
) ENGINE=InnoDB AUTO_INCREMENT=526 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `language`
--

INSERT INTO `language` (`id`, `FR`, `EN`, `SP`) VALUES
(1, 'Titre', 'Title', ''),
(2, 'Mots-clés', 'Keywords', ''),
(3, 'Bonjour ! Il faut vous connecter pour faire ça.', 'Hello ! You need to login to do that', ''),
(4, 'Voter', 'Vote', ''),
(5, 'Tout est à jour', 'Everything is up to date', ''),
(6, 'Nouveau problème', 'New problem', ''),
(7, 'Votes', 'Votes', ''),
(8, 'Ajouté par', 'Created by', ''),
(9, 'Ajouté le', 'Created on', ''),
(10, 'Contactez-nous', 'Contact Us', ''),
(11, 'Il y a eu un problème', 'There\'s been a problem', ''),
(12, 'Connexion', 'Login', ''),
(13, 'Participer', 'Participate', ''),
(14, 'C\'est noté !', 'Noted !', ''),
(15, 'Ce spam a été traité !', 'This spam has been processed !', ''),
(16, 'Un problème a été ajouté sur votre commune', 'A problem has been logged in your city', ''),
(17, 'Au moins une facette doit être sélectionnée', 'You need to select at least one facet', ''),
(18, 'Une solution a été ajoutée à l\'un de vos problèmes', 'A solution has been added to one of the problems you voted on', ''),
(19, 'Les invitations ont été envoyées aux adresses suivantes :', 'Invitations have been sent to the following addresses :', ''),
(21, 'Toutes les personnes mentionnées ont déjà reçu une invitation', 'All the people mentionned have already received an invitation', ''),
(22, 'Commentaire', 'Comment', ''),
(23, 'ces adresses emails n\'ont pas le bon format : ', 'Please enter the following addresses in the right format : ', ''),
(24, 'Merci de vérifier votre compte pour pouvoir faire ça : Cliquez le lien fourni dans l\'email de validation, ou demandez à en renvoyer un en cliquant sur le bouton ci-dessous.', 'Please verify your account to be able to do that : Click on the link on the validation email or ask for a new one by clicking on the button below', ''),
(25, 'Merci pour votre aide !', 'Thanks for your help !', ''),
(26, 'Merci de définir le besoin qui n\'est pas satisfait', 'Please define the need that isn\'t satisfied', ''),
(27, 'Utilisateur créé !', 'User created !', ''),
(28, 'Description', 'Description', ''),
(29, 'Merci de saisir une adresse ou de définir un point sur la carte', 'Please enter an address or click on the map', ''),
(30, 'Ce numéro est déjà utilisé', 'This number is already used', ''),
(31, 'Votre compte a été activé !', 'Your account is now active !', ''),
(32, 'Ce code est incorrect', 'This code is incorrect', ''),
(33, 'Merci de saisir le code de validation envoyé sur votre téléphone', 'Please enter the validation code sent to your phone', ''),
(34, 'Vous venez de souscrire à une notification', 'You just subscribed to a notification', ''),
(35, 'Participez à Solucracy', 'Participate in Solucracy', ''),
(36, 'Vous n\'êtes plus inscrit à ce type de notification', 'You just unsubscribed from this type of notification', ''),
(37, 'Cet élément a déjà été traité et n\'est pas considéré comme un spam', 'This item has already been processed and is not considered as a spam', ''),
(38, 'Vous ne pouvez pas signaler un élément que vous avez créé vous-même', 'You cannot report an item that you created yourself', ''),
(39, 'Ce spam a déjà été signalé par une autre utilisateur, merci de votre aide !', 'This item has already been reported by another user, thanks for your help !', ''),
(40, 'Votre profil a été désactivé et anonymisé, si vous désirez le réactiver, veuillez nous contacter en fournissant le code suivant :', 'Your profile has been deactivated and anonymized. If you\'d like to reactivate it, please contact us and give us the following code :', ''),
(41, 'Inviter des gens sur Solucracy', 'Invite people on Solucracy', ''),
(42, 'Pseudo', 'Login', ''),
(44, 'Email', 'Email', ''),
(45, 'Mot de Passe', 'Password', ''),
(46, 'Vérification du mot de passe', 'Password check', ''),
(47, 'Pourquoi est-ce important ?', 'Why is this important ?', ''),
(48, 'En cliquant sur Enregistrer, je confirme avoir lu et accepté les conditions générales de ce site', ' By clicking on Save, I confirm having read and accepted the terms and conditions of this website.', ''),
(49, 'Conditions d\'utilisation', 'Terms of use', ''),
(50, 'Langage', 'Language', ''),
(51, 'S\'abonner à la newsletter', 'Subscribe to newsletter', ''),
(52, 'Félicitations ! Vous avez gagné le badge ', 'Congratulations ! You just earned the badge ', ''),
(53, 'Le numéro est trop court', 'The number is too short', ''),
(54, 'On dirait que ce n\'est pas le bon mot de passe...', 'It looks like it\'s not the right password', ''),
(55, 'Le numéro est trop long', 'The number is too long', ''),
(56, 'Félicitations ! Votre problème a été enregistré !', 'Congratulations ! Your problem has been saved !', ''),
(57, 'Enregistrer', 'Save', ''),
(58, 'Ce n\'est pas un numéro de téléphone valide', 'This is not a valid phone number', ''),
(60, 'Merci de valider les conditions d\'utilisation avant de continuer', 'Please check that you have read the terms of use', ''),
(61, 'Lier à un compte', 'Link to an account', ''),
(64, 'Il n\'y a pas assez de votes à afficher sur cette zone', 'There aren\'t enough votes to display in this area', ''),
(66, 'Visible uniquement aux utilisateurs de ce compte', 'Only the account\'s users can see it', ''),
(67, 'Périmètre du problème', 'Problem\'s scope', ''),
(68, 'Pourquoi est-ce un problème pour vous ?', 'How does this problem impact you ?', ''),
(69, 'Lier cette solution au problème sélectionné', 'Link this solution to the selected problem', ''),
(70, 'Et cette solution', 'And this solution', ''),
(71, 'Pourquoi c\'est adapté', 'Why is it relevant', ''),
(72, 'Rechercher', 'Search', ''),
(73, 'Ce problème et cette solution ont déjà été liés.', 'This problem and that solution have already been linked.', ''),
(74, 'Tourisme', 'Tourism', ''),
(75, 'Commerce', 'Business', ''),
(76, 'Education', 'Education', ''),
(77, 'Administration', 'Administration', ''),
(78, 'Loisirs', 'Leisure', ''),
(79, 'Sport', 'Sport', ''),
(80, 'Ajouter un problème', 'Add a problem', ''),
(81, 'Quelles facettes du problème cela résout', 'Which facets of the problem does this solve', ''),
(82, 'Principales raisons pour lesquelles les propositions précédentes n\'étaient pas adaptées', 'Main reasons why the previous proposals were not relevant', ''),
(83, 'Vérifier mon compte', 'Verify my account', ''),
(84, 'Appuyer sur Entrée pour valider chaque mot clé', 'Press Enter to validate each keyword', ''),
(85, 'Catégorie', 'Category', ''),
(86, 'Autre', 'Other', ''),
(87, 'Raisons', 'Reasons', ''),
(88, 'Localisation', 'Location', ''),
(89, 'Toutes les facettes du problème y sont déjà liées', 'All facets of this problem are already linked', ''),
(90, 'Ajouter', 'Add', ''),
(91, 'Revenir à la liste de solutions', 'Back to the solution list', ''),
(92, 'Oups ! On a pas trouvé l\'utilisateur...', 'Oups ! We couldn\'t find the user...', ''),
(93, 'Oups ! On n\'a pas pu accéder à la base de données', 'Oups ! We were not able to access the database', ''),
(94, 'Cette solution pourra-t-elle être adaptée à d\'autres problèmes ?', 'Can this solution solve other problems ?', ''),
(95, 'Avantages', 'Upsides', ''),
(96, 'Inconvénients', 'Downsides', ''),
(97, 'Statut', 'Status', ''),
(98, 'Trier par', 'Sort by', ''),
(99, 'Nb de votes', '# of votes', ''),
(100, 'Il manque quelque chose...', 'There\'s something missing...', ''),
(101, 'Date d\'ajout', 'Creation date', 'Actualizar la traducción'),
(102, 'Vous avez déjà voté pour ce problème', 'You have already voted for this problem', ''),
(103, 'Oups, il y a eu un problème d\'ajout du vote...', 'Oups, sorry there was a problem processing your vote', ''),
(104, 'Félicitations, votre compte est maintenant actif !', 'Congratulations, your account is now active !', ''),
(105, 'Date', 'Date', ''),
(106, 'Merci pour votre vote !', 'Thank you for your vote !', ''),
(107, 'Merci de l\'avoir signalé, un modérateur va le traiter sous peu', 'Thank you for reporting this ! A moderator will process this shortly', ''),
(109, 'Nb de solutions', '# of solutions', ''),
(110, 'Groupes', 'Groups', ''),
(111, 'Tous', 'All', ''),
(112, 'Veuillez cliquer ici pour valider votre compte', 'Please click here to validate your account', ''),
(113, 'Bienvenue sur Solucracy! Merci de confirmer votre email', 'Welcome to Solucracy! Please confirm your email.', ''),
(114, 'Adresses emails', 'Email Addresses', ''),
(115, 'séparées par un point virgule', 'separated by a colon', ''),
(116, 'Envoyer l\'invitation', 'Send invitation', ''),
(117, 'Vous allez recevoir un email de validation. Si vous n\'avez rien reçu d\'ici 10 minutes, merci de vérifier votre dossier \"indésirables\". Si l\'email n\'y est pas, merci de nous contacter. ', 'You will receive a validation email shortly.If you didn\'t get anything, please check your spam folder, and if the email isn\'t there, please contact us.', ''),
(118, 'Vous n\'avez pas les droits nécessaires pour accéder à cette page', 'You don\'t have access to this page', ''),
(119, 'Le problème a été publié', 'Problem has been published', ''),
(120, 'Espace de modération', 'Moderation page', ''),
(121, 'Type', 'Type', ''),
(122, 'Signalé par', 'Reported by', ''),
(123, 'Signalé le', 'Reported on', ''),
(124, 'Spam', 'Spam', ''),
(125, 'Adresse', 'Address', ''),
(127, 'Communauté', 'Community', ''),
(128, 'Numéro de téléphone', 'Phone number', ''),
(130, 'Autoriser les utilisateurs à demander à faire partie du groupe', 'Let users request to be part of the group', ''),
(131, 'Urbanisme', 'Urbanism', ''),
(132, 'Environnement', 'Environment', ''),
(133, 'Cliquez ici', 'Click here', ''),
(134, 'Passer au règlement', 'Proceed to payment', ''),
(135, 'J\'en profite pour m\'abonner à la newsletter', 'I\'d like to subscribe to the newsletter', ''),
(136, 'Créer un compte premium', 'Create a premium account', ''),
(137, 'Nom', 'Name', ''),
(138, 'Merci pour votre message ! Nous vous répondrons d\'ici peu.', 'Thanks for the email! We\'ll be in touch shortly!', ''),
(139, 'Bonjour, que pouvons nous faire pour vous ? ', 'Hello, what can we do for you ?', ''),
(140, 'Pour les collectivités ou associations (cette fonctionnalité sera disponible en 2018, abonnez-vous à la newsletter pour être tenu au courant)', 'For local governments or associations ( this functionality will be available in 2018, subscribe to the newsletter to be kept up to date)', ''),
(141, 'Il y a eu un problème avec les informations que vous avez saisi', 'There was a problem with the information you entered.', ''),
(142, 'Complétez votre profil', 'Complete your profile', ''),
(143, 'www.youtube.com/embed/YfIqkBaT0Ac', 'www.youtube.com/embed/nnH9B3gSN4Y', ''),
(144, 'Je veux être notifié(e) pour les nouveaux problèmes sur ma commune', 'I want to be notified of new problems in my town', ''),
(145, 'Je veux être informé si ma commune crée un compte Solucracy', 'I want to be notified if my town creates a Solucracy account', ''),
(146, 'Cliquez ici pour éditer la description', 'Click here to edit description', ''),
(147, 'Voici les propositions faites pour ce problème', 'Here are the proposals for this problem', ''),
(148, 'Propositions', 'Proposals', ''),
(149, 'Agrandir', 'Expand', ''),
(150, 'Quels sont les besoins sous-jacents à ce problème ?', 'Which need isn\'t fulfilled ?', ''),
(151, 'Raison', 'Reason', ''),
(152, 'Que voulez-vous faire', 'What would you like to do', ''),
(153, 'faqFR', 'faqEN', ''),
(154, 'Trouver une solution existante', 'Find an existing solution', ''),
(155, 'Quel besoin n\'est pas satisfait ?', 'Which need isn\'t fulfilled ?', ''),
(156, 'Je veux être informé par email des solutions proposées pour ce problème', 'I would like to be informed of new solutions for this problem', ''),
(157, 'Aide', 'Help', ''),
(158, 'Validation du compte', 'Account validation', ''),
(159, 'Communautés que vous administrez', 'Communities you manage', ''),
(160, 'Nouvel utilisateur', 'New user', ''),
(161, 'Problèmes', 'Problems', ''),
(162, 'Profil', 'Profile', ''),
(163, 'Vos modifications ont été enregistrées', 'Your modifications have been saved', ''),
(164, 'Pertinence', 'Pertinence', ''),
(165, 'Veuillez cliquer sur ce lien pour définir un nouveau mot de passe dans les prochaines 24 heures', 'Please click on the following link to define a new password within the next 24 hours', ''),
(166, 'Demande de mot de passe', 'Password request', ''),
(167, 'Vous allez recevoir un email pour réinitialiser votre mot de passe.Si vous n\'avez rien reçu d\'ici 10 minutes, merci de vérifier votre dossier \"indésirables\". Si l\'email n\'y est pas, merci de nous contacter. ', 'You will receive an email to reset your password shortly. If you didn\'t get anything, please check your spam folder, and if the email isn\'t there, please contact us.', ''),
(168, 'Nous n\'avons pas trouvé d\'utilisateur avec cet email', 'We couldn\'t find a user with this email', ''),
(169, 'Mot de passe oublié', 'Forgot password', ''),
(170, 'Veuillez saisir votre adresse mail pour que nous puissions vous envoyer un email de réinitialisation', 'Please enter your email address and we\'ll send you a password reset email ', ''),
(171, 'Envoyer', 'Send', ''),
(172, 'Paramètres', 'Settings', ''),
(173, 'Votre requête a expiré', 'Your request has expired', ''),
(174, 'Accomplissements', 'Achievements', ''),
(175, 'Modifier mon profil', 'Update my profile', ''),
(176, 'Problèmes signalés', 'Reported problems', ''),
(177, 'Solutions ajoutées', 'Solutions added', ''),
(178, 'Langue', 'Language', ''),
(179, 'Mettre à jour', 'Update', ''),
(180, 'Merci de remplir le champ mot de passe correctement pour valider les modifications', 'Please fill in the password field properly to validate your modifications', ''),
(181, 'Votés', 'Voted', ''),
(182, 'Changer le mot de passe', 'Change the password', ''),
(183, 'Indésirable', 'Spam', ''),
(185, 'Postez ici votre commentaire', 'Post your comment here', ''),
(186, 'Propositions faites', 'Proposals made', ''),
(187, 'Badges spéciaux', 'Special badges', ''),
(188, 'personnes exprimées sur', 'voted for', ''),
(189, 'impactées', 'impacted', ''),
(190, 'Périmètre', 'Scope', ''),
(191, 'Cette proposition est-elle pertinente pour les différentes personnes impactées', 'Is this proposal pertinent for the users impacted', ''),
(192, 'adapté à', 'adapted to', ''),
(193, 'Une fois connecté, vous pourrez', 'Once logged in, you\'ll be able to', ''),
(194, 'Ajouter de nouveaux problèmes', 'Add new problems', ''),
(195, 'Voter pour tous les problèmes', 'Vote for all the problems', ''),
(196, 'Commenter tous les problèmes', 'Comment on all problems', ''),
(197, 'Participer aux votes de résolution', 'Participate in the resolution process', ''),
(198, 'Gérer votre profil', 'Manage your profile', ''),
(199, 'Etre tenu au courant de l\'évolution du site', 'Know about the latest developments', ''),
(200, 'Rejoignez-nous sur ', 'Join us on', ''),
(201, 'Fermer', 'Close', ''),
(202, 'Félicitations, vous avez créé votre compte sur Solucracy.', 'Congratulations, you have created your account on Solucracy.', ''),
(203, 'Je n\'ai rien reçu', 'I haven\'t received anything', ''),
(204, 'Merci de valider votre email dans les 24h', 'Please validate your email in the next 24 hours', ''),
(205, 'Renvoyer l\'email de validation', 'Resend Validation Email', ''),
(206, 'Voici l\'email que vous avez saisi', 'Here is the email you entered', ''),
(207, 'Merci de la vérifier', 'Please check it', ''),
(208, 'Si elle est correcte', 'If it is correct', ''),
(209, 'pour nous envoyer un email et on s\'occupera de tout', 'to send us an email and we\'ll take care of it', ''),
(210, 'Où voulez vous que vos votes soient enregistrés', 'Where do you want your votes to be recorded', ''),
(211, 'J\'ai signalé ce problème, qu\'en pensez-vous', 'I\'ve logged this problem, what do you think', ''),
(212, 'Pourquoi ne pas le partager maintenant avec vos amis pour que l\'on trouve ensemble une solution', 'Why not share it with your friends right now so that we can find a solution together', ''),
(213, 'Votre problème a été ajouté', 'Your problem has been logged', ''),
(214, 'J\'ai voté pour ce problème, qu\'en pensez-vous', 'I have voted for this problem, what do you think', ''),
(215, 'Je suis d\'accord, qu\'en pensez-vous', 'I agree, what do you think', ''),
(216, 'Trouvons ensemble une solution', 'Let\'s find a solution together', ''),
(217, 'le', 'on', ''),
(218, 'Proposée pour les problèmes suivants', 'Proposed for the following problems', ''),
(219, 'Commentaires', 'Comments', ''),
(220, 'Liste des solutions', 'Solution\'s list', ''),
(221, 'Nb de problèmes liés', '# of problems linked', ''),
(222, 'Merci de saisir votre adresse email et de cliquer sur le bouton qui vous plait le plus', 'Please enter your email address and click on the button you like the most', ''),
(223, 'Pour quelle raison', 'Why', ''),
(224, 'Ajouter une raison', 'Add a new reason', ''),
(225, 'Voici les propositions qui peuvent résoudre votre côté du problème. Pensez-vous qu\'elles sont adaptées ?', 'Here are the proposals that can solve your side of the problem. Do you think they are pertinent ?', ''),
(227, 'Vous avez déjà un compte', 'You already have an account', ''),
(228, 'Vous n\'avez pas de compte ? Enregistrez-vous gratuitement', 'You don\'t have an account ? Register for free', ''),
(229, 'Corriger', 'Correct', ''),
(230, 'français', 'anglais', 'espanol'),
(231, 'Nous avons tous les mêmes problèmes, trouvons ensemble les solutions.', 'We all have the same problems, let\'s find a solution together.', ''),
(232, 'Il n\'y a aucun solution proposée pour le moment', 'No solution has been logged yet', ''),
(233, 'Déjà voté', 'Already voted', ''),
(234, 'Voisinage', 'Neighborhood', ''),
(235, 'Arrondissement', 'District', ''),
(236, 'Ville', 'City', ''),
(237, 'Département', 'Department', ''),
(238, 'Région', 'Region', ''),
(239, 'Pays', 'Country', ''),
(240, 'Monde', 'World', ''),
(241, 'Commune', 'County', ''),
(242, 'Etat', 'State', ''),
(243, 'à évaluer', 'to evaluate', ''),
(244, 'Ajouter une solution', 'Add a solution', ''),
(245, 'Vous avez évalué toutes les propositions actuelles', 'You\'ve evaluated all the current proposals', ''),
(246, 'Nouvelle solution', 'New solution', ''),
(247, 'J\'ai ajouté une solution sur Solucracy!', 'I\'ve added a solution on Solucracy!', ''),
(248, 'Pourquoi ne pas la partager maintenant avec vos amis pour voir si ça résout leurs problèmes ?', 'Why not share it with your friends right now to see if it solves their problems ?', ''),
(249, 'Votre solution a été ajoutée', 'Your solution has been added', ''),
(250, 'J\'ai voté pour cette solution, qu\'en pensez-vous ?', 'I\'ve voted for that solution, do you agree ?', ''),
(251, 'Cette solution est prometteuse', 'That solution has potential', ''),
(252, 'Un pas de plus vers la résolution', 'One more step towards resolution', ''),
(253, 'Evaluer les propositions', 'Evaluate proposals', ''),
(254, 'Bonjour', 'Hello', ''),
(255, 'Solutions', 'Solutions', ''),
(256, 'Quelqu\'un a répondu à mon commentaire', 'Someone replied to my comment', ''),
(257, 'Quelqu\'un a voté pour mon problème', 'Someone voted on my problem', ''),
(258, 'Quelqu\'un a trouvé une solution à mon problème', 'Someone found a solution to my problem', ''),
(259, 'Je voudrais recevoir un email quand :', 'I would like to receive an email when :', ''),
(260, 'Solution', 'Solution', ''),
(261, 'Je pense que cela devrait être désactivé parce que :', 'I think this problem should be deactivated because :', ''),
(262, 'est déjà pris', 'is already taken', ''),
(263, 'doit faire au minimum', 'must be a minimum of', ''),
(264, 'doit faire au maximum', 'must be a maximum of', ''),
(265, 'caractères', 'characters', ''),
(266, 'doit correspondre à ', 'must match', ''),
(267, 'est un champ requis', 'is a required field', ''),
(268, 'Actifs', 'Active', ''),
(269, 'En cours de résolution', 'Voting for resolution', ''),
(270, 'Résolus', 'Solved', ''),
(271, 'Réduire', 'Collapse', ''),
(272, 'Merci de renseigner la nouvelle raison', 'Please define the new reason', ''),
(273, 'Membre depuis le', 'Member since', ''),
(274, 'Cette solution est adaptée', 'This solution is relevant', ''),
(275, 'Afficher dans votre navigateur', 'View it in your browser', ''),
(276, 'Solucracy, au rapport !', 'A few things happened on Solucracy', ''),
(277, 'Voici ce qui pourrait nécessiter votre attention', 'Here\'s what might require your attention', ''),
(278, 'Profil Supprimé', 'Deleted User', ''),
(279, 'Usager', 'User', 'Actualizar la traducción'),
(280, 'Testeur', 'Tester', 'Actualizar la traducción'),
(281, 'Humain', 'Human', 'Actualizar la traducción'),
(282, 'Cyborg', 'Cyborg', 'Actualizar la traducción'),
(283, 'Voix du peuple', 'Voice of the people', 'Actualizar la traducción'),
(284, 'Ambassadeur', 'Ambassador', 'Actualizar la traducción'),
(285, 'Contrôleur Qualité Mondial', 'Worldwide Quality Controller', 'Actualizar la traducción'),
(286, 'Touriste', 'Tourist', 'Actualizar la traducción'),
(287, 'Créateur', 'Creator', 'Actualizar la traducción'),
(288, 'Inventeur', 'Inventor', 'Actualizar la traducción'),
(289, 'Innovateur', 'Innovator', 'Actualizar la traducción'),
(290, 'Ingénieur', 'Engineer', 'Actualizar la traducción'),
(291, 'Génie', 'Genius', 'Actualizar la traducción'),
(292, 'Prodige', 'Prodigy', 'Actualizar la traducción'),
(293, 'Veilleur', 'Watchman', 'Actualizar la traducción'),
(294, 'Sentinelle', 'Sentinel', 'Actualizar la traducción'),
(295, 'Modérateur', 'Moderator', 'Actualizar la traducción'),
(296, 'Admin modérateur', 'Admin moderator', 'Actualizar la traducción'),
(297, 'Archiviste', 'Archivist', 'Actualizar la traducción'),
(298, 'Historien', 'Historian', 'Actualizar la traducción'),
(299, 'Paléontologue', 'Paleontologist', 'Actualizar la traducción'),
(300, 'Maire honoraire', 'Honorary Mayor', 'Actualizar la traducción'),
(301, 'Président honoraire', 'Honorary President', 'Actualizar la traducción'),
(302, 'Maitre du monde', 'Master of the world', 'Actualizar la traducción'),
(303, 'médiateur', 'Mediator', 'Actualizar la traducción'),
(304, 'Fédérateur', 'Unifier', 'Actualizar la traducción'),
(305, 'Emissaire Solucracy', 'Solucracy Emissary', 'Actualizar la traducción'),
(306, 'Meta testeur', 'Meta tester', 'Actualizar la traducción'),
(307, 'Petit impertinent', 'Impertinent', 'Actualizar la traducción'),
(308, 'Citoyen', 'Citizen', 'Actualizar la traducción'),
(309, 'Expert', 'Expert', 'Actualizar la traducción'),
(310, 'Problème', 'Problem', 'Actualizar la traducción'),
(311, 'Proposition', 'Proposal', 'Actualizar la traducción'),
(342, '<b>Loguer un problème </b></br> Les problèmes sont le prix du progrès. Ne m\'apportez que des problèmes. Les bonnes nouvelles m\'affaiblissent.', '<b>Log one problem </b></br> Problems are the price of progress. Don\'t bring me anything but trouble. Good news weakens me.', 'Actualizar la traducción'),
(343, '<b>Loguer 10 problèmes </b></br>Il y a des gens qui, à propos de certains problèmes, font preuve d\'une grande tolérance. C\'est souvent parce qu\'ils s\'en foutent.', '<b>Log 10 problems </b></br> Some people show great tolerance about certain problems. It is often because they don\'t care', 'Actualizar la traducción'),
(344, '<b>Loguer 100 problèmes </b></br> Les plus grandes histoires de succès sont celles de personnes qui, ayant reconnu un problème, l\'ont transformé en une opportunité.', '<b>Log 10 problems </b></br> The greatest success stories were created by people who recognized a problem a turned it into an opportunity', 'Actualizar la traducción'),
(345, '<b>Loguer une solution </b></br> S\'il n\'y a pas de solution, c\'est qu\'il n\'y a pas de problème.', '<b>Log one solution </b></br> If there\'s no solution, It\'s because there\'s no problem', 'Actualizar la traducción'),
(346, '<b>Loguer 10 solutions </b></br> Une des qualités les plus méconnues de l\'homme d\'action est l\'ingéniosité. L\'homme qui s\'impose dans les situations difficiles est celui qui invente une solution, là où les autres étaient dans l\'impasse.', '<b>Log 10 solutions </b></br> One of the most ignored qualities of men of action is ingenuity. The person who will prevail in difficult situations is the one inventing a solution, while others just got stuck.', 'Actualizar la traducción'),
(347, '<b>Loguer 100 solutions </b></br> On devient adulte, responsable, et indépendant le jour où on cesse de chercher des coupables à ses problèmes pour trouver des solutions.', '<b>Log 100 solutions </b></br> We truly become adult and responsible when we stop looking for the culprits of our problems and start looking for solutions', 'Actualizar la traducción'),
(348, '<b>Obtenir 75% de pertinence sur une proposition</b></br> Si tu veux savoir si une action est bonne ou mauvaise,\r\ndemande-toi ce qu’il arriverait si chacun en faisait autant.', '<b>Get 75% pertinence on a proposal</b></br> If you want to know whether an action is good or bad, ask yourself what would happen if everyone else would do the same.', 'Actualizar la traducción'),
(349, '<b>Obtenir 75% de pertinence sur 10 propositions</b></br> Pour pouvoir aboutir à des solutions valables, il faut tenir compte de la réalité', '<b>Get 75% pertinence for 10 proposals</b></br> In order to achieve viable solutions, one should take reality into account', 'Actualizar la traducción'),
(350, '<b>Obtenir 75% de pertinence sur 100 propositions</b></br> Une civilisation qui s\'avère incapable de résoudre les problèmes que suscite son fonctionnement est une civilisation décadente.', '<b>Get 75% de pertinence on 100 proposals</b></br> A civilization that proves incapable of solving the problems it creates is a decadent civilization.', 'Actualizar la traducción'),
(351, '<b>Signaler un spam validé par admin</b></br> You shall not pass !', '<b>Report a spam validated by an admin</b></br> You shall not pass !', 'Actualizar la traducción'),
(352, '<b>Signaler 10 spams validé par admin</b></br> Celui-là qui veille modestement quelques moutons sous les étoiles, s\'il prend conscience de son rôle, se découvre plus qu\'un serviteur. Il est une sentinelle. Et chaque sentinelle est responsable de tout l\'empire.', '<b>Report 10 spams validated by an admin</b></br> He who watches over a few sheep under the stars, becomes more than a servant if he\'s conscious of his role. He is a sentinel. And each sentinel is responsible for the whole empire.', 'Actualizar la traducción'),
(353, '<b>A 10 spams signalés avec -20% d\'erreur</b></br> Qui peut et n\'empêche, pèche.', '<b>Reported 10 spams with less than 20% error</b></br> Who can, and doesn\'t, sins.', 'Actualizar la traducción'),
(354, '<b>A 50 spams traités</b></br> Un grand pouvoir implique de grandes responsabilités', '<b>Processed 50 spams</b></br> With great power comes great responsibility', 'Actualizar la traducción'),
(355, '<b>Faire le lien entre un problème et une solution existants dans le système et obtenir au moins 50% de pertinence</b></br> Un problème sans solution est un problème mal posé.', '<b>Link a problem and an existing solution and get 50% pertinence</b></br> The formulation of the problem is often more essential than its solution', 'Actualizar la traducción'),
(356, '<b>Faire le lien entre 10 problèmes et 10 solutions existants dans le système + au moins 50% de pertinence</b></br> Le problème n\'est pas le problème... Le problème c\'est ton attitude face au problème...', '<b>Link 10 problems and 10 existing solutions and get 50% pertinence</b></br> The problem is not the problem. The problem is your attitude about the problem', 'Actualizar la traducción'),
(357, '<b>Faire le lien entre 100 problèmes et 100 solutions existants dans le système + au moins 50% de pertinence</b></br> Professeur d\'archéologie, expert en sciences occultes, et comment dit-on... Acquéreur d\'antiquités rares.', 'Professor of Archaeology, expert on the occult, and how does one say it… obtainer of rare antiquities.', 'Actualizar la traducción'),
(358, '<b>Valider l\'email</b></br> Vous êtes et resterez à jamais unique. Aussi soyez vous-même.', '<b>Validate your email</b></br> You are and will always be unique. So be your self.', 'Actualizar la traducción'),
(359, '<b>Valider your phone number</b></br> Les êtres qui vont prendre place dans notre vie sont toujours, à la veille de leur rencontre, des inconnus, et l\'écrire est moins une naïveté qu\'un émerveillement.', '<b>Validate your phone number</b></br> People who will take place in our lives always are, before we meet, strangers to us', 'Actualizar la traducción'),
(360, '<b>Loguer le + de problèmes sur sa propre commune</b></br> Quand on soulève un problème, la prudence conseille de ne pas stationner en-dessous.', '<b>Log the most problems in your town</b></br> When we raise a problem, prudence suggests to not remain under.', 'Actualizar la traducción'),
(361, '<b>Loguer le + de problèmes du pays</b></br> Tous les petits obstacles deviennent gros dès qu\'on n\'en tient pas compte.', '<b>Log the most problems in the country</b></br> Any small obstacle will become huge if we ignore it', 'Actualizar la traducción'),
(362, '<b>Loguer le + de problèmes du monde</b></br> Houston, on a un problème.', '<b>Log the most problems in the world</b></br> Houston, we have a problem.', 'Actualizar la traducción'),
(363, '<b>Loguer le plus de propositions pertinentes à + de 75% sur sa commune</b></br> Nous avons tous des moments de profond désespoir, mais lorsque l’on décide d’affronter le problème, on en ressort plus fort.', '<b>Log the most proposals with more than 75% pertinence in your town</b></br> We all have moments of desperation, but if we can face them head on, that\'s when we find out how strong we really are. ', 'Actualizar la traducción'),
(364, '<b>Loguer le plus de propositions pertinentes à + de 75% dans le pays</b></br> Qui parvient à concilier son devoir avec son vouloir mérite le pouvoir.', '<b>Log the most proposals with more than 75% pertinence in your country</b></br> Who can reconcile their will and their duty deserve the power', 'Actualizar la traducción'),
(365, '<b>Loguer le plus de propositions pertinentes à + de 75% dans le monde</b></br> De la part des personnes qui vont être impactées par votre travail et de moi-même, je tiens à vous exprimer ma plus profonde reconnaissance ', '<b>Log the most proposals with more than 75% pertinence in the world</b></br> From every person impacted by your work adn myself, I would like to express my deepest gratitude.', 'Actualizar la traducción'),
(366, '<b>Organisation d\'un atelier citoyen</b></br> La pierre n\'a point d\'espoir d\'être autre chose qu\'une pierre. Mais, de collaborer, elle s\'assemble et devient temple', '<b>Organizing a citizen workshop</b></br> The stone doesn\'t aspire to be anything other than a stone. But, from collaboration, she becomes a temple.', 'Actualizar la traducción'),
(367, '<b>Organisation de 10 ateliers citoyens</b></br> Se réunir est un début, rester ensemble est un progrès, travailler ensemble est la réussite.', '<b>Organizing 10 citizen workshops</b> Coming together is a beginning; keeping together is progress; working together is success.', 'Actualizar la traducción'),
(368, '<b>Invitation de gens sur Solucracy</b></br> Grâce à la liberté des communications, des groupes d\'hommes de même nature pourront se réunir et fonder des communautés. Les nations seront dépassées. ', '<b>Invite people on Solucracy</b></br> Thanks to the freedom of communications, people of a same nature will be able to gather and create communities. Nations will be overcome', 'Actualizar la traducción'),
(369, '<b>Loguer un problème dans un pays étranger</b></br> Je vais en Chine pour mieux voir la France et ses problèmes.', '<b>Log a problem in a foreign country</b></br> I go to China to better see France and its problems.', 'Actualizar la traducción'),
(370, '<b>Voter pour un problème</b></br> Au coeur de la moindre action devrait se trouver un idéal.', '<b>Vote for a problem</b></br> In the heart of each action should be an ideal.', 'Actualizar la traducción'),
(371, '<b>Poster 10 propositions à moins de 25% de pertinence</b></br> Si le seul outil que vous avez est un marteau, vous verrez tout problème comme un clou.', '<b>Log 10 proposals with less than 25% pertinence</b></br> If all you have is a hammer, everything looks like a nail', 'Actualizar la traducción'),
(372, 'Supprimer mon compte', 'Delete my account', 'Actualizar la traducción'),
(374, 'Je vote pour parce que', 'I\'m voting this up because', 'Actualizar la traducción'),
(375, 'Je vote contre parce que', 'I\'m voting this down because', 'Actualizar la traducción'),
(376, 'Veuillez vérifier que tous les champs du formulaire sont bien remplis', 'Please check that the form has been filled properly', 'Actualizar la traducción'),
(377, 'Ajouter au problème', 'Add to problem', 'Actualizar la traducción'),
(378, 'Bienvenue sur Solucracy! Veuillez saisir le code ci-dessous pour valider votre compte. Vous pouvez aussi le saisir sur votre profil :', 'Welcome to Solucracy! Please enter the code below to validate your account. You can also enter it later in your profile settings :', 'Actualizar la traducción'),
(379, 'Ce site utilise des cookies pour faciliter votre navigation', 'This website uses cookies to help with your navigation', 'Actualizar la traducción'),
(380, 'J\'ai compris', 'Got it', 'Actualizar la traducción'),
(381, 'Veuillez saisir votre mot de passe avant de valider les changements', 'Please enter your password before validating the changes', 'Actualizar la traducción'),
(382, 'Merci de rafraichir votre page', 'Please refresh your page', 'Actualizar la traducción'),
(383, 'Vous allez recevoir un email de validation, merci de cliquer sur le lien pour valider votre compte. Si vous n\'avez rien reçu d\'ici 10 minutes, merci de vérifier votre dossier \"indésirables\". Si l\'email n\'y est pas, merci de nous contacter. ', 'You will receive a validation email, please click on the link to validate your account. If you didn\'t get anything, please check your spam folder, and if the email isn\'t there, please contact us.', 'Actualizar la traducción'),
(384, 'Le lien de vérification ne correspond pas à un compte existant, merci de recliquer sur l\'email', 'The validation link doesn\'t point to an existing account, please try clicking on the link again.', 'Actualizar la traducción'),
(385, 'Notifications non lues', 'Unread notifications', 'Actualizar la traducción'),
(386, 'Créer une communauté', 'Create a community', 'Actualizar la traducción'),
(387, 'Type de communauté', 'Community type', 'Actualizar la traducción'),
(388, 'Collectivité', 'Collectivity', 'Actualizar la traducción'),
(389, 'Association', 'Non profit', 'Actualizar la traducción'),
(390, 'Entreprise', 'Company', 'Actualizar la traducción'),
(391, 'Vous êtes administrateur de plusieurs communautés, avec laquelle voulez-vous vous connecter ?', 'You administer multiple communities, which one would you like to log in with ?', 'Actualizar la traducción'),
(392, 'Communautés', 'Communities', 'Actualizar la traducción'),
(393, 'a été créé(e) et sera activé(e) une fois le règlement effectué', 'has been created and will be activated once the payment has come through', 'Actualizar la traducción'),
(394, 'n\'a pu être créé(e), un problème est survenu', 'couldn\'t be created, a problem has occured...', 'Actualizar la traducción'),
(395, 'Rechercher une communauté', 'Look for a community', 'Actualizar la traducción'),
(396, 'S\'abonner', 'Subscribe', 'Actualizar la traducción'),
(397, 'Gestion des admins', 'Manage admins', 'Actualizar la traducción'),
(398, 'Admin depuis :', 'Admin since :', 'Actualizar la traducción'),
(399, 'Cette personne ne possède pas de compte Solucracy, ce qui est nécessaire pour être admin', 'This person doesn\'t own a Solucracyaccount, which is necessary to be an admin', 'Actualizar la traducción'),
(400, 'Vous n\'avez pas les droits nécessaires pour faire ça.', 'You don\'t have the required privileges to do this.', 'Actualizar la traducción'),
(401, 'a été ajouté comme administrateur', 'has been added as an admin', 'Actualizar la traducción'),
(402, 'Vous êtes le dernier administrateur, veuillez annuler votre abonnement pour supprimer vos droits', 'You are the last admin, please cancel your subscription to delete your admin privileges', 'Actualizar la traducción'),
(403, 'Cette personne n\'est plus administratrice', 'That person isn\'t an admin anymore', 'Actualizar la traducción'),
(404, 'Vous avez été désigné comme admin d\'une communauté', 'You\'ve been chosen as a community admin', 'Actualizar la traducción'),
(405, 'Vos droits d\'administrateur pour une communauté ont été révoqués', 'Your admin rights for a community have been revoked', 'Actualizar la traducción'),
(406, 'Contacter vos abonnés', 'Contact followers', 'Actualizar la traducción'),
(407, 'Message', 'Message', 'Actualizar la traducción'),
(408, 'Merci de saisir le message que vous désirez envoyer', 'Please enter the message you would like to send', 'Actualizar la traducción'),
(409, 'A tous vos abonnés', 'To all your followers', 'Actualizar la traducción'),
(410, 'Seulement aux habitants', 'Only to inhabitants', 'Actualizar la traducción'),
(411, 'Une communauté à laquelle vous êtes abonné(e) vous a envoyé un message', 'A community you subscribed to sent you a message', 'Actualizar la traducción'),
(412, 'Votre message a été envoyé !', 'Your message has been sent !', 'Actualizar la traducción'),
(413, 'Contactez les utilisateurs impactés', 'Contact users impacted', 'Actualizar la traducción'),
(414, 'Annuler mon abonnement', 'Cancel my subscription', 'Actualizar la traducción'),
(415, 'Une nouvelle proposition a été faite pour un problème de la communauté', 'A new proposition has been added to problem in my community', 'Actualizar la traducción'),
(416, 'Une proposition pour un problème de ma communauté a atteint au moins 75% de pertinence', 'A proposition on one of my community\'s problems reached at least 75% of pertinence', 'Actualizar la traducción'),
(417, 'Il y a une nouvelle solution pour votre communauté !', 'There\'s a new solution for your community', 'Actualizar la traducción'),
(418, 'Déléguer ce problème', 'Delegate this problème', 'Actualizar la traducción'),
(419, 'Ce n\'est plus votre problème !', 'It\'s not your problem anymore !', 'Actualizar la traducción'),
(420, 'Se désabonner', 'Unsubscribe', 'Actualizar la traducción'),
(421, 'Ok, c\'est tout bon !', 'You\'re all set !', 'Actualizar la traducción'),
(422, 'Cette proposition est soutenue par la communauté en charge du problème :', 'This proposition is supported by the community in charge of the problem :', 'Actualizar la traducción'),
(423, 'Affichez votre soutien pour cette proposition !', 'Show your support for this proposition !', 'Actualizar la traducción'),
(424, 'Votre soutien a été enregistré', 'Your support has been recorded', 'Actualizar la traducción'),
(425, 'Retirez votre soutien à cette proposition :', 'Do not support this proposition anymore :', 'Actualizar la traducción'),
(426, 'Vous ne soutenez plus cette proposition', 'You do not support this proposition anymore', 'Actualizar la traducción'),
(427, 'Autoriser les communautés auxquelles je suis abonné(e) à me contacter', 'Allow communities I follow to contact me', 'Actualizar la traducción'),
(428, 'Demandez de l\'aide aux utilisateurs', 'Ask users for help', 'Actualizar la traducción'),
(429, 'Nous n\'avons plus besoin d\'aide', 'We don\'t need help anymore', 'Actualizar la traducción'),
(430, 'La proposition \"%solutionTitle%\" a été ajoutée sur le problème \"%problemTitle%\", d\'après %userName%, c\'est utile parce que \"%propositionTitle%\" ', 'please update translation', 'Actualizar la traducción'),
(431, 'La solution \"%solutionTitle%\" est estimée à plus de 75% de pertinence par les personnes impactées par le problème \"%problemTitle%\"', 'please update translation', 'Actualizar la traducción'),
(432, 'Le problème suivant \"%problemTitle%\" a été ajouté. Etes-vous impacté ?', 'please update translation', 'Actualizar la traducción'),
(433, 'La communauté \"%communityName%\" a transféré le problème \"%problemTitle%\" à la communauté \"%communityName2%\"', 'please update translation', 'Actualizar la traducción'),
(434, 'La communauté \"%communityName%\" vient d\'activer son compte, voulez-vous vous abonner ?', 'please update translation', 'Actualizar la traducción'),
(435, 'Bienvenue à \"%userName%\", qui vient de valider son compte !', 'please update translation', 'Actualizar la traducción'),
(436, 'Une solution \"%solutionTitle%\" a été ajoutée par \"%userName%\" . Merci !', 'please update translation', 'Actualizar la traducción'),
(437, 'La communauté \"%communityName%\" demande de l\'aide sur le problème \"%problemTitle%\", pouvez-vous apporter une solution ?', 'please update translation', 'Actualizar la traducción'),
(438, 'La communauté \"%communityName%\" apporte son soutien à la proposition \"%solutionTitle%\" faite sur le problème \"%problemTitle%\"', 'please update translation', 'Actualizar la traducción'),
(439, 'La communauté \'%communityName%\" n\'a plus besoin d\'aide sur son problème \"%problemTitle%\"', 'please update translation', 'Actualizar la traducción'),
(440, '%userName% vient de débloquer le badge \"%badgeName%\" . Félicitations !', 'please update translation', 'Actualizar la traducción'),
(441, 'Proposition ajoutée', 'please update translation', 'Actualizar la traducción'),
(442, 'Proposition pertinente', 'please update translation', 'Actualizar la traducción'),
(443, 'Problème ajouté', 'please update translation', 'Actualizar la traducción'),
(444, 'Problème délégué', 'please update translation', 'Actualizar la traducción'),
(445, 'Nouveau compte communauté', 'please update translation', 'Actualizar la traducción'),
(446, 'Nouveau compte utilisateur', 'please update translation', 'Actualizar la traducción'),
(447, 'Nouvelle solution', 'please update translation', 'Actualizar la traducción'),
(448, 'Appel à l\'aide', 'please update translation', 'Actualizar la traducción'),
(449, 'Soutien de la communauté', 'please update translation', 'Actualizar la traducción'),
(450, 'Appel à l\'aide terminé', 'please update translation', 'Actualizar la traducción'),
(451, 'Badge débloqué', 'please update translation', 'Actualizar la traducción'),
(452, 'Plus d\'infos', 'More information', 'Actualizar la traducción'),
(453, 'Actualités', 'Latest news', 'Actualizar la traducción'),
(455, 'Posté le', 'Posted on', 'Actualizar la traducción'),
(456, 'Etes-vous sûr(e) de vouloir annuler votre abonnement ? Pendant les 10 prochains jours, vous pourrez le réactiver avec le lien envoyé par email en saisissant à nouveau les informations de facturation. ', 'Are you sure you want to cancel your subscription ? For the next 10 days, you can reactivate it using the link sent by email and the payment information.', 'Actualizar la traducción'),
(457, 'Je comprends et confirme vouloir annuler mon abonnement', 'I understand and still want to cancel my subscription', 'Actualizar la traducción'),
(458, 'Merci de cocher la case pour confirmer votre action', 'Please check the box to confirm your action', 'Actualizar la traducción'),
(459, 'Une communauté que vous suivez vient de désactiver son compte', 'A community you follow deactivated its account', 'Actualizar la traducción'),
(460, 'a désactivé son compte', 'deactivated its account', 'Actualizar la traducción'),
(461, 'Pouvez-vous nous indiquer pourquoi vous nous abandonnez ?', 'Can you let us know why you\'re leaving us ?', 'Actualizar la traducción'),
(462, 'Nous sommes tristes de vous voir partir et restons à votre disposition si vous changez d\'avis !', 'We\'re sad to see you go but will always be there if you change your mind !', 'Actualizar la traducción'),
(463, 'réactiver mon abonnement', 'Reactivate my subscription', 'Actualizar la traducción'),
(464, 'Le compte de ma communauté a été désactivé par erreur', 'My community\'s account has been deactivated by mistake', 'Actualizar la traducción'),
(465, 'Cet email n\'est utilisé par aucun compte, vous avez peut-être fait une faute de frappe ?', 'This email isn\'t used by any account... A typing mistake maybe ?', 'Actualizar la traducción'),
(466, 'Profil Utilisateur', 'User Profile', 'Actualizar la traducción'),
(467, 'Vous n\'êtes plus connecté en tant que communauté', 'You are not logged in as a community anymore', 'Actualizar la traducción'),
(468, 'Votre compte a été activé et votre email est confirmé !', 'Your account has been activated and your email has been confirmed !', 'Actualizar la traducción'),
(469, 'Revenir à l\'accueil', 'Back to homepage', 'Actualizar la traducción'),
(470, 'Besoin de solution', 'In need of a solution', 'Actualizar la traducción'),
(471, 'Ce problème est actuellement géré par ', 'This problem is currently managed by', 'Actualizar la traducción'),
(472, 'Proportion de publications de cet utilisateur considérées comme des spams par les administrateurs', 'Proportion of this user\'s publications considered as spam by the moderators', 'Actualizar la traducción'),
(473, 'Merci de saisir un email valide', 'Please enter a valid email', 'Actualizar la traducción'),
(474, 'Votre mot de passe doit contenir au moins 1 majuscule, 1 minuscule, 1 chiffre et 1 caractère spécial et être long d\'au moins 8 caractères...désolé... un sabre ne suffit plus pour se protéger des pirates...', 'Your password must contain at least 1 upper case letter, 1 lower case letter, 1 number, 1 special character and be at least 8 characters long....sorry...a sword isn\'t enough anymore to protect yourself from pirates', 'Actualizar la traducción'),
(475, 'Faites votre choix', 'Please select here', 'Actualizar la traducción'),
(476, 'Ce problème est-il localisé ?', 'Is this problem localized ?', 'Actualizar la traducción'),
(477, 'Les gens se plaisent à penser qu\'ils peuvent se débrouiller seuls, alors que rien ne vaut le soutien et les encouragements d\'une équipe.', 'All men like to think that they can do it alone, but a real man knows that there no substitute for support , encouragement or a pit crew.', 'Actualizar la traducción'),
(478, 'Soutien Moral', 'Moral Support', 'Actualizar la traducción'),
(479, 'Communautés auxquelles vous êtes abonné', 'Communities you\'re following', 'Actualizar la traducción'),
(480, 'Gestion des notifications', 'Notification settings', 'Actualizar la traducción'),
(481, 'Bonjour, on dirait que ce code ne correspond pas à une communauté existante ou est expiré. Merci de repasser par la création de compte', 'Hello, it looks like this code is wrong or expired. Please use the account creation page to reactivate your community.', 'Actualizar la traducción'),
(484, 'Le champ de vérification et le champ mot de passe ne correspondent pas.', 'The password field and the password check don\'t match', 'Actualizar la traducción'),
(485, 'Page officielle de ', 'Official page for', 'Actualizar la traducción'),
(486, 'Pourquoi ma collectivité n\'apparait pas ?', 'Why can\'t I find my community ?', 'Actualizar la traducción'),
(487, 'Il est possible que ce compte soit déjà activé. S\'il n\'apparait pas dans les choix possibles, merci de contacter les admins en cliquant sur Contact en bas de la page', 'The account might already be taken. If it doesn\'t appear in the results, you can contact the admins by clicking on Contact at the bottom of the page.', 'Actualizar la traducción'),
(488, 'Gérer vos communautés', 'Manage your communities', 'Actualizar la traducción'),
(489, 'Allons-y !', 'Let\'s go !', 'Actualizar la traducción'),
(490, 'Vous m\'avez convaincu ! Moi aussi je veux jouer !', 'You convinced me ! Now let me play !', 'Actualizar la traducción'),
(491, 'Etes-vous un algorithme super intelligent ?', 'Are you a really smart algorithm ?', 'Actualizar la traducción'),
(492, 'Merci de recopier ici le texte de l\'image', 'Please copy here the text in the image', 'Actualizar la traducción'),
(493, 'Merci de saisir le texte affiché dans l\'image sinon tout le monde va croire que vous êtes un algorithme', 'Please fill in the text displayed in the image otherwise everyone will think you\'re an algorithm', 'Actualizar la traducción'),
(494, 'Rechercher une communauté parmi les comptes activés', 'Look for a community amongst the activated accounts', 'Actualizar la traducción'),
(495, 'Aucun administrateur n\'a été ajouté', 'No new admin has been added', 'Actualizar la traducción'),
(496, 'Cette solution a déjà été mise en place ici', 'This solution has already been implemented here', 'Actualizar la traducción'),
(497, 'Rafraichir l\'image', 'Refresh the picture', 'Actualizar la traducción'),
(498, 'Votre mot de passe a été mis à jour', 'Your password has been updated', 'Actualizar la traducción'),
(499, 'Faire un don', 'Make a donation', 'Actualizar la traducción'),
(500, 'Vous êtes maintenant abonné à cette communauté.', 'You just subscribed to this community', 'Actualizar la traducción'),
(501, 'Vous n\'êtes plus abonné à cette communauté.', 'You just unsubscribed from this community', 'Actualizar la traducción'),
(502, 'Vous ne pouvez pas déléguer des problèmes à vous-même (sinon vous courez au burnout...)', 'You can\'t delegate problems to yourself (you\'re risking a burnout...) ', 'Actualizar la traducción'),
(503, 'a été signalé par', 'has been reported by', 'Actualizar la traducción'),
(504, 'Règlement par carte', 'Pay with card', 'Actualizar la traducción'),
(505, 'Lier ce problème', 'Link this problem', 'Actualizar la traducción'),
(506, 'Nous sommes pour l\'instant à la recherche d\'un site pilote, merci de nous envoyer un email si vous êtes intéressé par ce projet.', 'We\'re currently looking for a beta site to test the community account, please send us an email if you\'re interested in this project', 'Actualizar la traducción'),
(507, 'Merci d\'indiquer le montant de votre donation (en euros)', 'How much would you like to donate ? (in euros)', 'Actualizar la traducción'),
(508, 'Avant de nous faire don de toutes vos économies, merci de lire cet article qui donne un peu plus de détails sur la démarche :-)', 'Before giving us all your savings, please read this article which will give you more details on the approach :-)', 'Actualizar la traducción'),
(509, 'Communauté de communes', 'Intermunicipality', 'Actualizar la traducción'),
(510, 'Solucracy : nouvel article de blog', 'Solucracy : New blog article', 'Actualizar la traducción'),
(511, 'Un outil collaboratif de priorisation et de résolution de problèmes', 'A collaborative tool to prioritize and solve problems', 'Actualizar la traducción'),
(512, 'Vue d\'ensemble des problèmes', 'Overview of the problems', 'Actualizar la traducción');
INSERT INTO `language` (`id`, `FR`, `EN`, `SP`) VALUES
(513, 'Propositions de solutions', 'Potential solutions', 'Actualizar la traducción'),
(514, 'Pour votre collectivité', 'For your community', 'Actualizar la traducción'),
(515, 'Vous avez identifié un manque ? Un besoin ? Un problème ?', 'Did you identify a need ? A problem', 'Actualizar la traducción'),
(516, 'Imaginons que vous considériez un carrefour comme dangereux...Renseignez le problème sur Solucracy et décrivez le besoin qui n\'est pas satisfait.', 'Let\'s say that a crossroad is dangerous...Log the problem in Solucracy and explain which need isn\'t met', 'Actualizar la traducción'),
(517, 'Partagez-le ensuite sur les réseaux sociaux pour que d\'autres puissent voter s\'ils sont impactés également', 'Share it on social networks so that other people can vote as well', 'Actualizar la traducción'),
(518, 'Les autres personnes impactées pourront venir voter et ajouter des informations. Le nombre de votes permettra de définir son importance pour la communauté.', 'Other people will be able to vote and add information. The amount of votes will allow to define how important this is for the community ', 'Actualizar la traducción'),
(519, 'Grâce aux informations fournies par les habitants, les commerçants, etc.. une liste des manques et des points d\'amélioration va peu à peu se constituer', 'Thanks to the information provided by the inhabitants, businesses,etc... a list of needs and improvment opportunities will start to emerge', 'Actualizar la traducción'),
(520, 'Vous ou vos concitoyens pourront ensuite proposer des solutions et évaluer leur pertinence. Il est possible qu\'il faille implémenter plusieurs solutions pour adresser le besoin en fonction de la manière dont les gens sont impactés.', 'Everyone will be able to offer solutions and evaluate their pertinence. We might require multiple solutions to fully address the needs behind a single problem', 'Actualizar la traducción'),
(521, 'Activez votre compte collectivité pour afficher les propositions que vous soutenez, mettre en évidence les solutions que vous avez implémentées, affichez vos actualités, lancer des appels à solution, et communiquer facilement avec vos usagers', 'Activate your community account to display the solutions you support, highlight the ones already implemented, call for solutions, and communicate easily with your users', 'Actualizar la traducción'),
(522, 'Gérez facilement vos administrateurs pour effectuer le suivi de votre activité et mutualiser la gestion des besoins avec d\'autres collectivités.', 'Easily manage your administrators to keep track of your activity and mutualize need management with other communities', 'Actualizar la traducción'),
(523, 'Mes enfants passent par là tous les jours et n\\\'ont aucune visibilité pour traverser !', 'My kids use that road every day and have no visibility over what is coming.', 'Actualizar la traducción'),
(524, 'J\'ai reçu une carte', 'I received a card', 'Actualizar la traducción'),
(525, 'Cliquez ici si vous avez reçu une carte dans votre boite aux lettres', 'Click here if you received a card in your mailbox', 'Actualizar la traducción');

-- --------------------------------------------------------

--
-- Structure de la table `newsitem`
--

DROP TABLE IF EXISTS `newsitem`;
CREATE TABLE IF NOT EXISTS `newsitem` (
  `newsItemId` int(4) NOT NULL AUTO_INCREMENT,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `communityId` int(4) DEFAULT NULL,
  `problemId` int(4) DEFAULT NULL,
  `userId` int(4) DEFAULT NULL,
  `propositionId` int(4) DEFAULT NULL,
  `newsItemTypeId` int(4) NOT NULL,
  `solutionId` int(4) DEFAULT NULL,
  `badgeId` int(4) DEFAULT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`newsItemId`),
  UNIQUE KEY `preventDupes` (`newsItemId`,`createdOn`,`communityId`,`problemId`,`userId`,`propositionId`,`newsItemTypeId`),
  KEY `newsitem_idx_commu_probl_newsi_creat_newsi_useri` (`communityId`,`problemId`,`newsItemTypeId`,`createdOn`,`newsItemId`,`userId`),
  KEY `newsitem_idx_probl_newsi_creat_newsi_commu_useri` (`problemId`,`newsItemTypeId`,`createdOn`,`newsItemId`,`communityId`,`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=904 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `newsitemtype`
--

DROP TABLE IF EXISTS `newsitemtype`;
CREATE TABLE IF NOT EXISTS `newsitemtype` (
  `translationId` int(4) DEFAULT NULL,
  `name` varchar(140) NOT NULL,
  `newsItemTypeId` int(4) NOT NULL AUTO_INCREMENT,
  `titleTranslationId` int(4) NOT NULL,
  PRIMARY KEY (`newsItemTypeId`),
  KEY `newsitemtype_idx_newsitemtypeid_translationid` (`newsItemTypeId`,`translationId`),
  KEY `newsitemtype_idx_newsitemtypeid_titletranslatio` (`newsItemTypeId`,`titleTranslationId`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `newsitemtype`
--

INSERT INTO `newsitemtype` (`translationId`, `name`, `newsItemTypeId`, `titleTranslationId`) VALUES
(430, 'Proposition added', 1, 441),
(431, 'Proposition is pertinent', 2, 442),
(432, 'Problem added', 3, 443),
(433, 'Problem delegated', 4, 444),
(434, 'New community account', 5, 445),
(435, 'New user account', 6, 446),
(436, 'New solution', 7, 447),
(437, 'Help needed', 8, 448),
(438, 'Community support', 9, 449),
(439, 'Help needed stop', 10, 450),
(440, 'Badge unlocked', 11, 451);

-- --------------------------------------------------------

--
-- Structure de la table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE IF NOT EXISTS `newsletter` (
  `email` varchar(140) NOT NULL,
  `statusId` tinyint(1) NOT NULL DEFAULT '1',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`email`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `notificationId` int(4) NOT NULL AUTO_INCREMENT,
  `userId` int(4) NOT NULL,
  `link` varchar(140) NOT NULL,
  `title` varchar(140) NOT NULL,
  `description` text NOT NULL,
  `notificationTypeId` int(4) NOT NULL,
  `statusId` int(11) NOT NULL DEFAULT '0',
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`notificationId`)
) ENGINE=InnoDB AUTO_INCREMENT=316 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `notificationtype`
--

DROP TABLE IF EXISTS `notificationtype`;
CREATE TABLE IF NOT EXISTS `notificationtype` (
  `notificationTypeId` int(4) NOT NULL AUTO_INCREMENT,
  `translationId` int(4) DEFAULT NULL,
  `comment` varchar(200) NOT NULL,
  PRIMARY KEY (`notificationTypeId`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `notificationtype`
--

INSERT INTO `notificationtype` (`notificationTypeId`, `translationId`, `comment`) VALUES
(1, NULL, 'Notify when a new solution is added to the problem'),
(2, NULL, 'Notify when someone replies to my comment'),
(3, NULL, 'Someone commented on my problem'),
(4, NULL, 'Someone voted on my problem'),
(5, NULL, 'Notify me of new problems in my city'),
(6, NULL, 'Notify me if my city creates a community account'),
(7, NULL, 'Community sent me a message'),
(8, NULL, 'Notify me if a community makes me an admin'),
(9, NULL, 'Allow a community to contact me for a problem that impacts me'),
(10, NULL, 'Allow communities I follow to contact me'),
(11, NULL, 'A community I follow cancelled their account'),
(12, NULL, 'Notify me when a new proposition is made for a collectivity problem'),
(13, NULL, 'Notify me when a proposition reaches more than 75% of pertinence on a community\'s problem');

-- --------------------------------------------------------

--
-- Structure de la table `notif_subscription`
--

DROP TABLE IF EXISTS `notif_subscription`;
CREATE TABLE IF NOT EXISTS `notif_subscription` (
  `userId` int(4) NOT NULL,
  `notificationTypeId` int(4) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `entityId` int(4) NOT NULL DEFAULT '0',
  UNIQUE KEY `subscriptionId` (`userId`,`notificationTypeId`,`entityId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `pertinence`
--

DROP TABLE IF EXISTS `pertinence`;
CREATE TABLE IF NOT EXISTS `pertinence` (
  `pertinenceId` int(4) NOT NULL AUTO_INCREMENT,
  `positive` tinyint(1) NOT NULL DEFAULT '0',
  `reason` varchar(140) NOT NULL,
  `facetId` int(4) NOT NULL,
  `propositionId` int(4) NOT NULL,
  `userId` int(4) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pertinenceId`),
  KEY `pertinence_idx_facetid_propositionid` (`facetId`,`propositionId`),
  KEY `pertinence_idx_propositionid_facetid` (`propositionId`,`facetId`),
  KEY `pertinence_idx_positiv_pertine_proposi_facetid` (`positive`,`pertinenceId`,`propositionId`,`facetId`),
  KEY `pertinence_idx_pertinenceid_propositionid` (`pertinenceId`,`propositionId`),
  KEY `pertinence_idx_propositionid` (`propositionId`)
) ENGINE=InnoDB AUTO_INCREMENT=6255 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `pertinencevote`
--

DROP TABLE IF EXISTS `pertinencevote`;
CREATE TABLE IF NOT EXISTS `pertinencevote` (
  `pertinenceVoteId` int(4) NOT NULL AUTO_INCREMENT,
  `userId` int(4) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pertinenceId` int(4) NOT NULL,
  PRIMARY KEY (`pertinenceVoteId`),
  KEY `pertinencevote_idx_pertinenceid_pertinencevotei` (`pertinenceId`,`pertinenceVoteId`),
  KEY `pertinencevote_idx_userid_pertinence_pertinence` (`userId`,`pertinenceId`,`pertinenceVoteId`),
  KEY `pertinencevote_idx_userid_pertinenceid` (`userId`,`pertinenceId`)
) ENGINE=MyISAM AUTO_INCREMENT=294 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `problem`
--

DROP TABLE IF EXISTS `problem`;
CREATE TABLE IF NOT EXISTS `problem` (
  `problemId` int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `categoryId` int(4) NOT NULL,
  `userId` int(4) UNSIGNED DEFAULT NULL,
  `title` varchar(140) DEFAULT NULL,
  `description` text,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `latitude` decimal(11,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `statusId` tinyint(1) NOT NULL,
  `statusUpdate` timestamp NULL DEFAULT NULL COMMENT '0: deactivated /1: activated/2:pending approval/3:pending resolution/4: solved/5:spam/6:not a spam',
  `scopeId` int(2) NOT NULL DEFAULT '243',
  `private` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`problemId`),
  KEY `User_Problem_CON` (`userId`),
  KEY `latitude` (`latitude`,`longitude`),
  KEY `problem_idx_userid_categor_problem_latitud` (`userId`,`categoryId`,`problemId`,`latitude`),
  KEY `problem_idx_problem_latitud_longitu_statusi` (`problemId`,`latitude`,`longitude`,`statusId`),
  KEY `problem_idx_problemid_statusid` (`problemId`,`statusId`),
  KEY `problem_idx_userid_statusid_problemid` (`userId`,`statusId`,`problemId`),
  KEY `problem_idx_statusid_problemid` (`statusId`,`problemId`),
  KEY `problem_idx_latitud_longitu_statusi_problem` (`latitude`,`longitude`,`statusId`,`problemId`),
  KEY `problem_idx_userid_latitu_longit_status_proble` (`userId`,`latitude`,`longitude`,`statusId`,`problemId`),
  KEY `problem_idx_problemid_userid_categoryid` (`problemId`,`userId`,`categoryId`),
  KEY `problem_idx_problemid_title` (`problemId`,`title`),
  KEY `problem_idx_problemid_userid_title` (`problemId`,`userId`,`title`),
  KEY `problem_idx_userid_problemid` (`userId`,`problemId`)
) ENGINE=MyISAM AUTO_INCREMENT=2917 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `problem_tag`
--

DROP TABLE IF EXISTS `problem_tag`;
CREATE TABLE IF NOT EXISTS `problem_tag` (
  `problemId` int(4) UNSIGNED NOT NULL,
  `tagId` int(4) UNSIGNED NOT NULL,
  PRIMARY KEY (`problemId`,`tagId`),
  KEY `tagid` (`tagId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `proposition`
--

DROP TABLE IF EXISTS `proposition`;
CREATE TABLE IF NOT EXISTS `proposition` (
  `propositionId` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(140) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` int(4) NOT NULL,
  `solutionId` int(4) NOT NULL,
  `statusId` int(1) NOT NULL DEFAULT '1',
  `statusUpdate` timestamp NULL DEFAULT NULL,
  `implemented` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`propositionId`),
  KEY `proposition_idx_userid_propositio_statusid` (`userId`,`propositionId`,`statusId`),
  KEY `proposition_idx_propositionid_userid` (`propositionId`,`userId`),
  KEY `proposition_idx_userid_proposi_solutio_statusi` (`userId`,`propositionId`,`solutionId`,`statusId`),
  KEY `proposition_idx_proposi_solutio_userid_title` (`propositionId`,`solutionId`,`userId`,`title`),
  KEY `proposition_idx_propositionid_statusid` (`propositionId`,`statusId`),
  KEY `proposition_idx_solutionid_propositionid` (`solutionId`,`propositionId`),
  KEY `proposition_idx_proposi_solutio_statusi_title` (`propositionId`,`solutionId`,`statusId`,`title`),
  KEY `proposition_idx_propositio_userid_title` (`propositionId`,`userId`,`title`),
  KEY `proposition_idx_userid` (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=3095 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `description`) VALUES
('administrator', 'admin privilege group'),
('editor', 'Can modify entity titles and descriptions'),
('moderator', 'allows to process spams'),
('utilisateur', 'utilisateur de base'),
('verified', 'Phone has been checked');

-- --------------------------------------------------------

--
-- Structure de la table `scope`
--

DROP TABLE IF EXISTS `scope`;
CREATE TABLE IF NOT EXISTS `scope` (
  `scopeId` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `scope`
--

INSERT INTO `scope` (`scopeId`) VALUES
(234),
(235),
(236),
(237),
(238),
(509),
(240),
(241),
(242),
(243);

-- --------------------------------------------------------

--
-- Structure de la table `solution`
--

DROP TABLE IF EXISTS `solution`;
CREATE TABLE IF NOT EXISTS `solution` (
  `solutionId` int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `avantages` text NOT NULL,
  `inconvenients` text NOT NULL,
  `userId` int(4) UNSIGNED DEFAULT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `statusId` int(4) NOT NULL DEFAULT '1' COMMENT '0 : inactive , 1: active, 2 : flagged as spam',
  `categoryId` int(4) NOT NULL,
  `oneOff` tinyint(1) NOT NULL DEFAULT '0',
  `statusUpdate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`solutionId`),
  KEY `User_Solution_CON` (`userId`),
  KEY `solution_idx_solutionid_userid` (`solutionId`,`userId`),
  KEY `solution_idx_solutionid_title` (`solutionId`,`title`),
  KEY `solution_idx_solutio_userid_categor_created` (`solutionId`,`userId`,`categoryId`,`createdOn`),
  KEY `solution_idx_solutionid_userid_title` (`solutionId`,`userId`,`title`)
) ENGINE=InnoDB AUTO_INCREMENT=2788 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `solutionvote`
--

DROP TABLE IF EXISTS `solutionvote`;
CREATE TABLE IF NOT EXISTS `solutionvote` (
  `solutionVoteId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `solutionId` int(4) NOT NULL,
  `comment` varchar(2000) DEFAULT 'No comment',
  PRIMARY KEY (`solutionVoteId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `solution_tag`
--

DROP TABLE IF EXISTS `solution_tag`;
CREATE TABLE IF NOT EXISTS `solution_tag` (
  `solutionId` int(4) UNSIGNED NOT NULL,
  `tagId` int(4) UNSIGNED NOT NULL,
  KEY `solution_tag_idx_solutionid_tagid` (`solutionId`,`tagId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `spam`
--

DROP TABLE IF EXISTS `spam`;
CREATE TABLE IF NOT EXISTS `spam` (
  `spamId` int(4) NOT NULL AUTO_INCREMENT,
  `entity` varchar(30) NOT NULL,
  `entityId` int(4) NOT NULL,
  `flaggedBy` int(4) NOT NULL,
  `flaggedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `processedBy` int(4) DEFAULT NULL,
  `processedOn` timestamp NULL DEFAULT NULL,
  `comment` text NOT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`spamId`),
  UNIQUE KEY `entity` (`entity`,`entityId`,`flaggedBy`),
  KEY `spam_idx_flaggedby_approved` (`flaggedBy`,`approved`),
  KEY `spam_idx_flaggedby` (`flaggedBy`),
  KEY `spam_idx_approv_entity_entity_flagge_flagge` (`approved`,`entity`,`entityId`,`flaggedBy`,`flaggedOn`),
  KEY `spam_idx_entity_approved_entityid` (`entity`,`approved`,`entityId`),
  KEY `spam_idx_entity_entityid` (`entity`,`entityId`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `statusId` int(4) NOT NULL,
  `comment` varchar(20) NOT NULL,
  UNIQUE KEY `statusId` (`statusId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `status`
--

INSERT INTO `status` (`statusId`, `comment`) VALUES
(0, 'Inactive'),
(1, 'Active'),
(2, 'Flagged as spam'),
(5, 'Spam'),
(3, 'Pending resolution'),
(4, 'Solved'),
(6, 'Not a spam'),
(7, 'Unread'),
(8, 'Read'),
(9, 'awaiting payment');

-- --------------------------------------------------------

--
-- Structure de la table `tag`
--

DROP TABLE IF EXISTS `tag`;
CREATE TABLE IF NOT EXISTS `tag` (
  `tagId` int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `language` varchar(3) NOT NULL DEFAULT 'FR',
  PRIMARY KEY (`tagId`),
  KEY `tag_idx_tagid_name` (`tagId`,`name`)
) ENGINE=MyISAM AUTO_INCREMENT=857 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `transactionId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(140) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` int(4) NOT NULL,
  PRIMARY KEY (`transactionId`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userName` text,
  `password` text,
  `email` text,
  `createdOn` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `latitude` decimal(11,8) NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `statusId` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:not activated, 1:active, 2: deactivated',
  `language` varchar(3) NOT NULL,
  `passwordRequestId` text,
  `requestTime` timestamp NULL DEFAULT NULL,
  `communityId` int(4) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  KEY `user_idx_userid_username` (`userId`,`userName`(255)),
  KEY `user_idx_userid_latitude_longitude` (`userId`,`latitude`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=5409 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `userbadge`
--

DROP TABLE IF EXISTS `userbadge`;
CREATE TABLE IF NOT EXISTS `userbadge` (
  `userId` int(4) NOT NULL,
  `badgeId` varchar(4) NOT NULL,
  `scope` varchar(140) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `displayed` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `badgeId` (`badgeId`,`scope`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `userrole`
--

DROP TABLE IF EXISTS `userrole`;
CREATE TABLE IF NOT EXISTS `userrole` (
  `userId` int(4) UNSIGNED NOT NULL,
  `roleId` varchar(255) NOT NULL,
  PRIMARY KEY (`userId`,`roleId`),
  KEY `roleid` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `vote`
--

DROP TABLE IF EXISTS `vote`;
CREATE TABLE IF NOT EXISTS `vote` (
  `voteId` int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` int(4) UNSIGNED DEFAULT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `facetId` int(4) NOT NULL,
  `latitude` decimal(11,8) NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `statusId` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`voteId`),
  KEY `User_externalID` (`userId`),
  KEY `vote_idx_facetid_voteid` (`facetId`,`voteId`),
  KEY `vote_idx_facetid` (`facetId`),
  KEY `vote_idx_userid_facetid_voteid` (`userId`,`facetId`,`voteId`),
  KEY `vote_idx_userid_facetid` (`userId`,`facetId`),
  KEY `vote_idx_facetid_latitud_voteid_longitu` (`facetId`,`latitude`,`voteId`,`longitude`)
) ENGINE=InnoDB AUTO_INCREMENT=4685 DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `problem`
--
ALTER TABLE `problem` ADD FULLTEXT KEY `Title_2` (`title`,`description`);

--
-- Index pour la table `solution`
--
ALTER TABLE `solution` ADD FULLTEXT KEY `title` (`title`,`description`);

--
-- Index pour la table `tag`
--
ALTER TABLE `tag` ADD FULLTEXT KEY `name` (`name`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `userrole`
--
ALTER TABLE `userrole`
  ADD CONSTRAINT `userrole_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`);

--
-- Contraintes pour la table `vote`
--
ALTER TABLE `vote`
  ADD CONSTRAINT `vote_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
